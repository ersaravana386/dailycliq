<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    
    protected $dates = ['deleted_at'];
    protected $guarded = [];

    public function images()
    {
        return $this->hasMany('App\ProductImage', 'product_id', 'id');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Vendor', 'vendor_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand', 'brand_id', 'id');
    }
    public function banner()
    {
        return $this->hasOne('App\Banner', 'product_id', 'id');
    }

    public function attributes()
    {
        return $this->hasMany('App\ProductAttribute', 'product_id', 'id');
    }

    public function filter_attributes($filters)
    {
         
        return $this->hasMany('App\ProductAttribute', 'product_id', 'id');
    }

    public function additional_info()
    {
        return $this->hasMany('App\ProductAdditionalInfo', 'product_id', 'id');
    }

    public function faq()
    {
        return $this->hasMany('App\ProductFaq', 'product_id', 'id');
    }

    public function variant_ids(){
        $variants = Product::where('variant',$this->variant)->get();
        $variant_ids = [];
        foreach($variants as $variant){
            $variant_ids[] = $variant->id;
        }
        return $variant_ids;
    }

    public function inventory(){
        return $this->hasMany('App\Inventory', 'product_id', 'id');
    }

    public function flashsale(){
        return $this->hasMany('App\FlashsaleProducts', 'product_id', 'id');
    }
    public function productreview(){
        return $this->hasMany('App\ProductReview', 'product_id', 'id');
    }
    public function store99(){
        return $this->hasMany('App\Store99', 'product_id', 'id');
    }

    public function whishlist(){
        return $this->hasOne('App\WishList', 'product_id', 'id');
    }

    public function deal(){
        return $this->hasMany('App\DealOftheDay', 'product_id', 'id');
    }
    public function budgetbazar(){
        return $this->hasMany('App\BudgetBazar','product_id','id');
    }
}
