<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    protected $fillable = [
        'company_name', 'contact_person', 'email', 'password',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function categories(){
        return $this->hasMany('App\VendorCategory');
    }
    public function fsaleproducts()
    {
        return $this->hasMany('App\FlashsaleProducts','id','vendor_id');
    }
    public function product(){
        return $this->hasMany('App\Product','vendor_id','id');
    }
    public function order(){
        return $this->hasMany('App\Order','vendor_id','id');
    }
    public function inventory(){
        return $this->hasMany('App\Inventory','vendor_id','id');
    }
}
