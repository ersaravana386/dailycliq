<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $fillable=['offer','product_id','category_id','vendor_id','offer_price','offer_percentage','valid_from','valid_to'];
}
