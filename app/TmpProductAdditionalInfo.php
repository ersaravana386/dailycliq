<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmpProductAdditionalInfo extends Model
{
    protected  $fillable = ['name','value'];

    public function product()
    {
        return $this->belongsTo('App\TmpProduct', 'temp_product_id', 'id');
    }
}
