<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attributes extends Model
{
    public function attribute_values()
    {
        return $this->hasMany('App\AttributeValue');
    }
}
