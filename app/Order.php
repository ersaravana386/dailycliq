<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Order extends Model
{


    
    protected $dates = ['deleted_at'];
    protected $guarded = [];

   public function product(){
    
    return $this->hasMany('App\OrderProduct', 'order_id', 'id');

   }
   public function payment(){

    return $this->belongsTo('App\OrderPayment', 'id', 'order_id');

   }
   public function user(){

    return $this->belongsTo('App\User', 'customer_id', 'id');

   }
   public function vendor(){

    return $this->belongsTo('App\Vendor', 'vendor_id', 'id');

   }
   public function images(){

    return $this->hasMany('App\ProductImage', 'product_id', 'product_id');

   }
   public function invoice(){

    return $this->belongsTo('App\Invoice', 'id', 'order_id');

   }
   public function deliveryaddress(){
    return $this->belongsTo('App\DeliveryAddress', 'customer_id', 'user_id');
   }

}
