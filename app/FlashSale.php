<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlashSale extends Model
{
    public function products()
    {
        return $this->hasMany('App\FlashsaleProducts', 'flash_sale_id', 'id');
    }
}
