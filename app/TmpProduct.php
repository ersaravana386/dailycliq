<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmpProduct extends Model
{

    public function images()
    {
        return $this->hasMany('App\TmpProductImage', 'temp_product_id', 'id');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Vendor', 'vendor_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand', 'brand_id', 'id');
    }

    public function attributes()
    {
        return $this->hasMany('App\TmpProductAttribute', 'temp_product_id', 'id');
    }
}
