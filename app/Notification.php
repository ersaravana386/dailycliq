<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public function vendor(){
        return $this->belongsTo('App\Vendor','added_by','id');
    }
}
