<?php

namespace App;

use Illuminate\Database\Eloquent\Model; 

class Brand extends Model
{
    public function productlog(){
        return $this->belongsTo('App\Brand', 'brand_id', 'id');
    }
    
}
