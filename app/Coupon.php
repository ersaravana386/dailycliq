<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable=['code','description','product_id','category_id','vendor_id','offer_price','offer_percentage','valid_from','valid_to'];
}
