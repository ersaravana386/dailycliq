<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetBazar extends Model
{
    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id', 'id'); 
    }
    public function images()
    {
        return $this->hasMany('App\ProductImage', 'product_id', 'id');
    }
    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }
    public function brand()
    {
        return $this->belongsTo('App\Brand', 'brand_id', 'id');
    }
    public function flashsale(){
        return $this->hasMany('App\FlashsaleProducts', 'product_id', 'id');
    }
    public function deal(){
        return $this->hasMany('App\DealOftheDay', 'product_id', 'id');
    }
    public function whishlist(){
        return $this->hasOne('App\WishList', 'product_id', 'id');
    }
    public function banner()
    {
    return $this->belongsTo('App\Banner', 'product_id', 'id');
    }
}
