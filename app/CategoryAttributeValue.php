<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryAttributeValue extends Model
{
    protected $fillable = [
        'category_attribute_id', 'value'
    ];

    public function attribute(){
        return $this->belongsToOne('App\CategoryAttribute');
    }
    
}
