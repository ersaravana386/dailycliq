<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard('admin')->check()) {
            return redirect()->route('dashboard');
        }
        if (Auth::guard('vendor')->check()) {
            if(Auth::guard('vendor')->user()->active == 0){
                return redirect()->route('showRegistration');
            }else{
                return redirect()->route('vendorDashboard');
            }

        }

        return $next($request);
    }
}
