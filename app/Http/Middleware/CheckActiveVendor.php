<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Route;

class CheckActiveVendor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('vendor')->check() && Route::currentRouteName() != "showRegistration" && Route::currentRouteName() != 'vendorLogout' && Route::currentRouteName() != 'getDistrict'  && Route::currentRouteName() != 'addRegistration') {
            if (Auth::guard('vendor')->user()->approved == 0 && Auth::guard('vendor')->user()->active == 1) {
                Auth::logout();
                return redirect()->route('showLogin');
            }
            if (Auth::guard('vendor')->user()->active == 0) {
                return redirect()->route('showRegistration');
            }
        }
        return $next($request);
    }
}
