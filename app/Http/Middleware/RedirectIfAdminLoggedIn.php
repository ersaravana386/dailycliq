<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RedirectIfAdminLoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard('admin')->check()){
          return redirect('/admin/dashboard');
        }
        if(Auth::guard('vendor')->check()){
          return redirect('/vendor/my-board');
        }
        return $next($request);
    }
}
