<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\AttributeValue;
use App\Category;
use App\CategoryAttribute;
use App\CategoryAttributeValue;
use DB;
use Illuminate\Http\Request;

class AttributeController extends Controller
{
    public function listAttributes(Category $category, Request $request)
    {
        if (!$category) {
            return redirect()->route('showCategories');
        }
        $data = [
            'search' => "",
            'categories' => [],
            'id' => $category->id,
            'category' => $category,
        ];
        return view('admin.categories.attributes', $data);
    }

    public function showNewAttribute(Category $category, Request $request)
    {
        if (!$category) {
            return redirect()->route('showCategories');
        }

        $attributes = Attribute::where('active', 1)->get();
        $data = [
            'search' => "",
            'categories' => [],
            'id' => $category->id,
            'attributes' => $attributes,
        ];
        return view('admin.categories.newattribute', $data);
    }

    public function getAttributeValues(Attribute $attribute, Request $request)
    {
        if (!$attribute) {
            return response()->json(['status' => 0, 'errors' => ['no_data' => 'No data']]);
        }

        $attribute_values = AttributeValue::where('attribute_id', $attribute->id)->get();
        $data = [
            'status' => 1,
            'attribute_values' => $attribute_values,
        ];
        return response()->json($data);
    }

    public function saveCategoryAttribute(Request $request)
    {
        $attribute = Attribute::where('id', $request->attribute)->first();
        if (!$attribute) {
            return response()->json(['status' => 0, 'errors' => ['could_not_save' => 'Could not save attribute & values']]);
        }

        $attribute_name = $attribute->name;
        $attribute_values = $request->attribute_value;
        $category = $request->category;
        $searchable = 0;
        if ($request->searchable && $request->searchable == "on") {
            $searchable = 1;
        }

        if (CategoryAttribute::where('attribute', $attribute_name)->where('category_id', $category)->count()) {
            return response()->json(['status' => 0, 'errors' => ['could_not_save' => 'Category already has this attribute, try edit this attribute']]);
        }

        DB::beginTransaction();
        try {
            $category_attribute = new CategoryAttribute();
            $category_attribute->attribute = $attribute_name;
            $category_attribute->category_id = $category;
            $category_attribute->searchable = $searchable;
            $category_attribute->save();
            $insert_array = [];
            foreach ($attribute_values as $attribute_value) {
                $insert_array[] = [
                    'category_attribute_id' => $category_attribute->id,
                    'value' => $attribute_value,
                ];
            }
            CategoryAttributeValue::insert($insert_array);
        } catch (\Exception $e) {
            // print_r($e->getMessage());exit;
            DB::rollback();
            return response()->json(['status' => 0, 'errors' => ['could_not_save' => 'Could not save attribute & values']]);
        }
        DB::commit();
        return response()->json(['status' => 1, 'message' => 'Cateogry attributes saved successfully']);
    }

    public function updateSearchable(Request $request)
    {
        $attr = CategoryAttribute::where('id', $request->id)->first();
        // print_r($attr->toArray());exit;
        if ($attr) {
            if ($request->value == "true") {
                $attr->searchable = 1;
            } else {
                $attr->searchable = 0;
            }
            $attr->save();
        }
        return response()->json(['status' => true]);
    }

    public function showEditAttribute(CategoryAttribute $attribute = null, Request $request)
    {
        $main_attribute = Attribute::where('name', $attribute->attribute)->first();
        $data = [
            'category_attribute' => $attribute,
            'attribute' => $main_attribute,
        ];
        return view('admin.categories.editattribute', $data);
    }

    public function updateCategoryAttribute(Request $request)
    {
        $attribute_values = $request->attribute_value;
        $searchable = 0;
        if ($request->searchable && $request->searchable == "on") {
            $searchable = 1;
        }

        $category_attribute = CategoryAttribute::where('id', $request->attribute)->first();

        DB::beginTransaction();
        try {
            $category_attribute->searchable = $searchable;
            $category_attribute->save();
            $insert_array = [];
            foreach ($attribute_values as $attribute_value) {
                $insert_array[] = [
                    'category_attribute_id' => $category_attribute->id,
                    'value' => $attribute_value,
                ];
            }
            CategoryAttributeValue::where('category_attribute_id', $category_attribute->id)->delete();
            CategoryAttributeValue::insert($insert_array);
        } catch (\Exception $e) {
            // print_r($e->getMessage());exit;
            DB::rollback();
            return response()->json(['status' => 0, 'errors' => ['could_not_save' => 'Could not update attribute & values']]);
        }
        DB::commit();
        return response()->json(['status' => 1, 'message' => 'Cateogry attributes updated successfully']);
    }

    public function deleteAttribute(Request $request)
    {
        CategoryAttributeValue::where('category_attribute_id', $request->id)->delete();
        CategoryAttribute::where('id', $request->id)->delete();
        return response()->json(['status' => true]);
    }

    
    public function testSearch(Request $request)
    {
        $categories = [];
        $properties = Category::find($request->category)->properties;
        print_r($properties);       
    }
}
