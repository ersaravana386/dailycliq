<?php

namespace App\Http\Controllers;

use App\ProductGiftideal;
use Illuminate\Http\Request;

class ProductGiftidealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductGiftideal  $productGiftideal
     * @return \Illuminate\Http\Response
     */
    public function show(ProductGiftideal $productGiftideal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductGiftideal  $productGiftideal
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductGiftideal $productGiftideal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductGiftideal  $productGiftideal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductGiftideal $productGiftideal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductGiftideal  $productGiftideal
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductGiftideal $productGiftideal)
    {
        //
    }
}
