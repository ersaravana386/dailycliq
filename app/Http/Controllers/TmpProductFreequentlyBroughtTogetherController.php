<?php

namespace App\Http\Controllers;

use App\TmpProductFreequentlyBroughtTogether;
use Illuminate\Http\Request;

class TmpProductFreequentlyBroughtTogetherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TmpProductFreequentlyBroughtTogether  $tmpProductFreequentlyBroughtTogether
     * @return \Illuminate\Http\Response
     */
    public function show(TmpProductFreequentlyBroughtTogether $tmpProductFreequentlyBroughtTogether)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TmpProductFreequentlyBroughtTogether  $tmpProductFreequentlyBroughtTogether
     * @return \Illuminate\Http\Response
     */
    public function edit(TmpProductFreequentlyBroughtTogether $tmpProductFreequentlyBroughtTogether)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TmpProductFreequentlyBroughtTogether  $tmpProductFreequentlyBroughtTogether
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TmpProductFreequentlyBroughtTogether $tmpProductFreequentlyBroughtTogether)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TmpProductFreequentlyBroughtTogether  $tmpProductFreequentlyBroughtTogether
     * @return \Illuminate\Http\Response
     */
    public function destroy(TmpProductFreequentlyBroughtTogether $tmpProductFreequentlyBroughtTogether)
    {
        //
    }
}
