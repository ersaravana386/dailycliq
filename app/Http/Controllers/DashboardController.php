<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Murugan Start
use DB;

//Murugan End

class DashboardController extends Controller
{
    //Murugan Start
    public function dashboard(Request $request){
        
        $users_count = DB::table('users')
        //->where('username', '=', $username)
        //->where('password', '=', $password)
        ->count();
        $total_paid = DB::table('order_payment')
        ->sum('amount');
        $total_product_live= DB::table('products')
        ->where('approved', '=', 1)
        ->count();
        $total_order = DB::table('order_products')
        ->count();
       


//Order table List  - 1=>Complete,2=>Pending,3=>Refunded,4=>Failed,5=>Canceled

        $pending_order = DB::table('orders')
        ->where('status', '=', 2)
        ->count();
        $confirm_order = DB::table('orders')
        ->where('status', '=', 2)
        ->count();
        $refund_order = DB::table('orders')
        ->where('status', '=', 3)
        ->count();
        $cancel_order = DB::table('orders')
        ->where('status', '=', 5)
        ->count();
        

        //Vendor Graph 

        $data1= DB::table('vendors')
       ->select(
        DB::raw('active as active'),
        DB::raw('count(*) as number'))
       ->groupBy('active')
       ->get();
        $array['array'] = ['Active', 'Number'];
        //print_r($data);exit;
    
        //print_r($data1);exit;

        //Product Sales Graphs
        
        $data2= DB::table('products')
       //->join("order_products","order_products.product_id","=","products.id")
       ->join('order_products','order_products.product_id','=','products.id')
       ->select(
        DB::raw('products.name as product_id'),
        DB::raw('SUM(order_products.unit) as number'))
       ->groupBy('order_products.product_id')
       ->get(); 
      $array2[] = ['product_id', 'Number'];
    // print_r($data);exit;
       foreach($data2 as $key => $value)
       {
        $array2[++$key] = [$value->product_id, $value->number];
       }
       //print_r($array);exit;
     //return view('product')->with('product_id', json_encode($array,JSON_NUMERIC_CHECK));

        //Year Wise Sales Report Details 

        $visitor = DB::table('order_payments')
        ->select(
        DB::raw("year(created_at) as year"),
        DB::raw("SUM(amount) as amount"))
        ->orderBy("created_at")
        ->groupBy(DB::raw("year(created_at)"))
        ->get();
        $result[] = ['Year','Amount'];
        foreach ($visitor as $key => $value) {
        $result[++$key] = [$value->year, (int)$value->amount];

      //return view('year')->with('order_payments',json_encode($result));

}



     

        $data= array(
            'order_payments' => json_encode($result),
            'product_id'=>json_encode($array2,JSON_NUMERIC_CHECK),
            'data'=>$data1,
            'users_count'=>$users_count,
            'total_paid' =>$total_paid,
            'total_product_live'=>$total_product_live,
            'total_order'=>$total_order,
            'pending_order'=>$pending_order,
            'confirm_order'=>$confirm_order,
            'refund_order'=>$refund_order,
            'cancel_order'=>$cancel_order
        );

       // print_r($data);exit;


        return view('admin/dashboard',$data);
    }
  //Murugan End   
}
