<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\ColorClass;
use App\Order;
use App\Http\Controllers\Controller;
use App\LengthClass;
use App\Product;
use App\OrderProduct;
use App\WeightClass;
use App\Inventory;
use App\DealOfTheDay;
use App\Vendor;
use Auth;
use DB;
use Illuminate\Http\Request;
use Validator;
use Crypt;

class ReportController extends Controller
{
    public function showVendorSalesReport(Request $request){
        $search = $request->search; 
        $query = Vendor::query();
        $query->select('id','company_name');
        $query->with(['order'=>function($q){
            $q->where('status',1);
            $q->withCount(['product as products_sold'=>function($q){
                $q->select(
                    DB::raw('sum(unit) as products_sold'),   
                );
            }]);
            $q->withCount(['product as sum'=>function($q){
                $q->select(
                    DB::raw('sum(net_total) as sum'),
                );
            }]);
        }]);
        $query->groupBy('company_name','id');
        if ($search) {
            $query->where(function ($sub) use ($search) {            
                $sub->orWhere('company_name', 'like', "%" . $search . "%");
            });
        }
        $details = $query->paginate(10);
        $data = [
            'details' => $details,
            'search' => $search,
            'active' =>array('reports','sales'),
        ];
            return view('admin.reports.salesreport.vendors',$data);
    }

    public function showVendorOrderReport(Request $request){
        // DB::enableQueryLog();
        $search = $request->search; 
        $query = Vendor::query();
        $query->select('company_name','id');
        $query->withCount(['order as completed_orders'=>function($q){
            $q->select(
                DB::raw('count(CASE WHEN status = "1" THEN 1 END) AS "completed_orders"'),
            );
        }]);
        $query->withCount(['order as pending_orders'=>function($q){
            $q->select(
                DB::raw('count(CASE WHEN status = "2" THEN 1 END) as pending_orders'),
            );
        }]);
        $query->withCount(['order as refunded_orders'=>function($q){
            $q->select(
                DB::raw('count(CASE WHEN status = "3" THEN 1 END) as refunded_orders'),
            );
        }]);
        $query->withCount(['order as failed_orders'=>function($q){
            $q->select(
                DB::raw('count(CASE WHEN status = "4" THEN 1 END) as failed_orders'),
            );
        }]);
        $query->withCount(['order as cancelled_orders'=>function($q){
            $q->select(
                DB::raw('count(CASE WHEN status = "5" THEN 1 END) as cancelled_orders'),
            );
        }]);
        $query->groupBy('company_name','id');
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('company_name', 'like', "%" . $search . "%");
            });
        }
        $details = $query->paginate(10);
        // dd(DB::getQueryLog());exit;
        $data = [
            'details' => $details,
            'search' => $search,
            'active' =>array('reports','order'),
        ];
            return view('admin.reports.orderreport.vendorslist',$data);
    }
    public function   showVendorStockReport(Request $request){
        $search = $request->search; 
        $query = Vendor::query()->select('company_name','id');
        $query->withCount(['inventory as count'=>function($q){
            $q->select(DB::raw('SUM(unit) as count'));
        }]);
        $query->groupBy('company_name','id');
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('company_name', 'like', "%" . $search . "%");
            });
        }
        $details = $query->paginate(10);
        $data = [
            'details' => $details,
            'search' => $search,
            'active' =>array('reports','stock'),
        ];
            return view('admin.reports.stockreport.vendorslist',$data);
    }
    public function salesReportReport($vendorid,Request $request){
        $user     = Auth::user()->id;
        $datalist = $request;
        $search   = $request->search;
        $category = Category::all();
        $brand    = Brand::all();
        $query    = OrderProduct::query();
        $query->with(['productlist','productlist.category','productlist.brand','order','order.user','order.payment']);
        $query->whereHas('productlist',function($query) use ($vendorid){
            $query->where('vendor_id',$vendorid);
        });
        if($datalist->customername){
            $query->whereHas('order.user', function ($q)  use($datalist){
                $q->where('name', 'like', "%" . $datalist->customername . "%");
            });
        }
        $query->whereHas('order',function($q) use($datalist,$vendorid){
            $q->where('vendor_id','=', $vendorid);
            if($datalist->referancenumber){
                $q->where('order_reference_number',$datalist->referancenumber);
            }
            if(($datalist->orderstatus!='Order Status') && ($datalist->orderstatus!='')){
                $q->where('status',$datalist->orderstatus);
            }
            $from  = date('Y-m-d',strtotime($datalist->orderfrom));
            $to    = date('Y-m-d',strtotime($datalist->orderto));
            if(($from!='1970-01-01' ) && ($to!='1970-01-01')){
                if($from > $to ){
                    $tmp  = $to;
                    $to   = $from;
                    $from = $to;
                }
                $from =$from.' 00:00:00';
                $to =$to.' 23:59:59';
                $q->WhereBetween('order_at', [$from, $to]);
            }
         
        });
        if(($datalist->paymenttype!='Payment Type') && ($datalist->paymenttype!='')){
            $query->with(['order.payment'=>function($q  ) use($datalist){
                $q->where('payment_type', 'like', "%" . $datalist->paymenttype . "%");
               }]);
        }
        if(($datalist->categoryselected!='Category' && ($datalist->categoryselected!=''))){
            $query->with(['productlist.category'=>function($q) use($datalist){
                $q->where('id',$datalist->categoryselected );
               }]);
        }
        $products =$query->paginate(10);
        //print_r($products); exit();
        $data = [
            'referancenumber' =>$datalist->referancenumber,
            'customername' =>$datalist->customername,
            'orderstatus' =>$datalist->orderstatus,
            'paymenttype' =>$datalist->paymenttype,
            'categoryselected' =>$datalist->categoryselected,
            'orderfrom' =>$datalist->orderfrom,
            'orderto' =>$datalist->orderto,
            'category' => $category,
            'products' => $products,
            'search' => $search,
            'active' =>array('reports','sales'),
        ];
       return view('admin.reports.salesreport.salesreport', $data);
    }
    public function stockReportReport($vendorid,Request $request){
        $query = Inventory::query();
        $query->with('product');
        $query->with(['vendor'=>function($query) use ($vendorid){
            $query->where('id',$vendorid);
        }]);
        if($request->process!=''){
          $query->where('process_id',$request->process);
        }
        $from  = date('Y-m-d',strtotime($request->stockfrom));
        $to    = date('Y-m-d',strtotime($request->stockto));
        if(($from!='1970-01-01' ) && ($to!='1970-01-01')){
            if($from > $to ){
                $tmp  = $to;
                $to   = $from;
                $from = $to;
            }
            $from =$from.' 00:00:00';
            $to =$to.' 23:59:59';
            $query->WhereBetween('created_at', [$from, $to]);
        }
        $query->where('vendor_id',$vendorid);
        $inventory = $query->paginate(10);
        $vendor=Vendor::select('id','company_name')->where('id',$vendorid)->get();
        //print_r($inventory); exit();
        $data = [
            'process'=>$request->process,
            'stockfrom'=>$request->stockfrom,
            'stockto'=>$request->stockto,
            'stock' => $inventory,
            'vendor'=>$vendor,
            'active' =>array('reports','stock'),
        ];
        return view('admin.reports.stockreport.stockreport', $data);
    }
    public function orderReportReport($vendorid,Request $request){
        $datalist = $request;
        $name =  $request->customername;
        $search =  $request->search;
        $category = Category::all();
        $brand    = Brand::all();
        $query = Order::query();
        $query->with(['payment', 'vendor','user']);
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('name', 'like', "%" . $search . "%");
                $sub->orWhere('order_reference_number', 'like', "%" . $search . "%");
                
            });
        }
        $query->with(['product' => function($q){
            $q->sum('unit');
        }]);
        $query->where('vendor_id',$vendorid);
        if($request->referancenumber!=''){
                $query->where('orders.order_reference_number', $request->referancenumber);
            }
        if($name!=''){
            $query->whereHas('user', function ($query)  use($name){
                $query->where('name', 'like', "%" . $name . "%");
            });
        }
        if(($datalist->orderstatus!='Order Status') && ($datalist->orderstatus!='')){
            $query->where('orders.status', $request->orderstatus);
        }
        $payment = $request->paymenttype;
        if(($datalist->paymenttype!='Payment Type') && ($datalist->paymenttype!='')){
            
            $query->whereHas('payment', function ($que)  use($payment){
                $que->where('payment_type', 'like', "%" . $payment . "%");
            });  
        }
        $from  = date('Y-m-d',strtotime($datalist->orderfrom));
        $to    = date('Y-m-d',strtotime($datalist->orderto));
        if(($from!='1970-01-01' ) && ($to!='1970-01-01')){
            if($from > $to ){
                $tmp  = $to;
                $to   = $from;
                $from = $to;
            }
            $from =$from.' 00:00:00';
            $to =$to.' 23:59:59';
            $q->WhereBetween('order_at', [$from, $to]);
        }
        $query->where('vendor_id',$vendorid);
        $query->orderBy('created_at', 'desc');
        $products = $query->paginate(10);
        $data = [
            'referancenumber' =>$datalist->referancenumber,
            'customername' =>$datalist->customername,
            'orderstatus' =>$datalist->orderstatus,
            'paymenttype' =>$datalist->paymenttype,
            'categoryselected' =>$datalist->categoryselected,
            'orderfrom' =>$datalist->orderfrom,
            'orderto' =>$datalist->orderto,
            'category' => $category,
            'products' => $products,
            'search' => $search,
            'active' =>array('reports','order'),
        ];
        return view('admin.reports.orderreport.orderreport', $data);
    }
}
