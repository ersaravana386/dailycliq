<?php

namespace App\Http\Controllers;

use App\EmailUs;
use Illuminate\Http\Request;

class EmailUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmailUs  $emailUs
     * @return \Illuminate\Http\Response
     */
    public function show(EmailUs $emailUs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmailUs  $emailUs
     * @return \Illuminate\Http\Response
     */
    public function edit(EmailUs $emailUs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmailUs  $emailUs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmailUs $emailUs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmailUs  $emailUs
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmailUs $emailUs)
    {
        //
    }
}
