<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;
use Auth;
use DB;
use Route;

class NotificationController extends Controller
{
   public function getVendorNotification(){
  
        $query=Notification::query();
        $query->where('added_to',Auth::user()->id);
        $query->where('is_view',0);
        $query->orderby('id','DESC');
        $notification = $query->limit(5)->get();
        $result='';
        foreach($notification as $key=>$data){
            
            $result = $result."<a href='".route('showNotification',['id'=>$data->id])."' class='media-list-link read'><div class='media'><div class='media-body'>
            <p class='noti-text'><strong>".$data->process."</strong></p><br>
            <span>".date('M d Y,h i A')."</span>
            </div></div><!-- media --></a><!-- loop ends here -->";
        }
        if($result!=''){
            $result.="<div class='dropdown-footer'>
            <a href='".route('showAllNotifications')."'><i class='fas fa-angle-down'></i> Show All Notifications</a>
        </div>";
        }
        return $result;
   }
   public function showNotification(Notification $id){
    
        $id->is_view='1';
        $id->save();
        return redirect()->route($id->link);

   }
    public function markAllNotification(){
        \DB::table('notifications')->where('added_to',Auth::user()->id)->update(['is_view' => '1']);
    }
    public function showAllNotifications(Request $request){
        $query=Notification::query();
        $query->where('added_to',Auth::user()->id);
        $query->where('is_view',0);
        $notification = $query->paginate(10);
        $search =$request->search;
        $data=[
            'search' =>$search,     
            'notification'=>$notification
        ];
        return view('vendors/notifications', $data);
    }
    public function markAllNotificationList(Request $request){
        \DB::table('notifications')->where('added_to',Auth::user()->id)->update(['is_view' => '1']);
        $query=Notification::query();
        $query->where('added_to',Auth::user()->id);
        $query->where('is_view',0);
        $notification = $query->paginate(10);
        $search =$request->search;
        $data=[
            'search' =>$search,     
            'notification'=>$notification
        ];
        return view('vendors/notifications', $data);
    }
    public function getAdminNotification(){
        $query=Notification::query();
        $query->where('is_view',0);
        $query->where('added_to',2);
        $notification = $query->limit(3)->get();
        $data="";
        foreach ($notification as $key=>$item){
            $data=$data."<a href='".route('viewAdminNotification',['id'=>$item->id])."'class='media-list-link read'>
                  <div class='media'>
                    <img src='https://via.placeholder.com/500' alt=''>
                    <div class='media-body'>
                    <p class='noti-text'><strong>".$item->process."</strong>.</p>
                    <p>".$item->created_at->diffForHumans(null, true, true, 2)."&nbspago</p>
                      <span></span>
                    </div>
                  </div>
                </a>";
        }
        if($data!=""){
            $data=$data.'<div class="dropdown-footer">
            <a href="'.route('viewAllNotifications').'"><i class="fas fa-angle-down"></i> Show All Notifications</a>
          </div>';
        }
        return response()->json(['status'=>1,'data'=>$data]);
   }
   public function viewAdminNotification($id){
       $data =Notification::find($id);
       $data->is_view=1;
       $data->save();
       if($data->admin_link != ""){
            $route=unserialize($data->admin_link);
       }
       if($route['route'] == 'flashsale'){
                return Redirect()->route('productApproveFlashSale',['flashsaleid'=>$route['id'],'id'=>$route['vendor_id']]);
       }elseif($route['route'] == 'dealoftheday'){
            return Redirect()->route('vendorProductsDealOfTheDay',['vendorid'=>$route['vendor_id']]);
       }elseif($route['route'] == 'order'){
            return Redirect()->route('showOrdersOrder',['vendor'=>$route['vendor_id']]);
       }
   }
   public function viewAllNotifications(){
        $query=Notification::query();
        $query->where('is_view',0);
        $query->where('added_to',2);
        $notifications = $query->get();
        $data=[
            'notifications'=>$notifications,
        ];
        return view('admin.allnotifications',$data);
   }
   public function adminMarkAllAsRead(){
       DB::table('notifications')->where('added_to',2)->update(['is_view'=>1]);
       return response()->json(['status'=>1]);
   }
   public function notificationCount(){
       $query=Notification::where([
           ['is_view',0],
           ['added_to',2],
        ])->count();
       return response()->json(['status'=>true,'data'=>$query]);
   }
}
