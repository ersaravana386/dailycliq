<?php

namespace App\Http\Controllers;

use App\Giftideal;
use Illuminate\Http\Request;

class GiftidealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Giftideal  $giftideal
     * @return \Illuminate\Http\Response
     */
    public function show(Giftideal $giftideal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Giftideal  $giftideal
     * @return \Illuminate\Http\Response
     */
    public function edit(Giftideal $giftideal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Giftideal  $giftideal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Giftideal $giftideal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Giftideal  $giftideal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Giftideal $giftideal)
    {
        //
    }
}
