<?php
namespace App\Http\Controllers;
use App\Brand;
use App\Category;
use App\Manufacturer;
use App\TaxClass;
use App\WeightClass;
use App\LengthClass;

use App\TmpProduct;
use App\TmpProductImage;
use App\TmpProductAdditionalInfo;
use App\TmpProductFaq;
use App\TmpProductAttribute;

use App\Product;
use App\ProductImage;
use App\ProductAdditionalInfo;
use App\ProductFaq;
use App\ProductAttribute;
use App\FlashSale;
use App\Vendor;
use App\FlashsaleProducts;
use App\Notification;


use Illuminate\Http\Request;
use Validator;
use DB;

class FlashSaleController extends Controller
{
    public function showFlashSale(Request $request){

        $search = $request->search;
        // DB::enableQueryLog();    
        $query = FlashSale::where('starting_date','>=',date('Y-m-d H:i:s'));
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('starting_date', 'like', "%" . $search . "%");
                $sub->orWhere('ending_date', 'like', "%" . $search . "%");
            });
        }
        $query->orderBy('starting_date','desc');
        $details = $query->paginate(10);
        // dd(DB::getQueryLog());
        $data = [
            'details' => $details,
            'search' => $search,
        ];
            return view('admin.flashsale.list',$data);
    }
    public function newFlashSale(){
        return view('admin.flashsale.new');
    }
    public function saveFlashSale(Request $request){
        $rules = [
            'startdate' => 'required',
            'enddate' => 'required',
        ];
        $messages = [
            'startdate.required' => "Statrt rate required",
            'enddate.required' => "End Date Required"
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
        if($request->startdate >= $request->enddate){
            return response()->json(['status' => 0, 'datemessage' => "Selected dates are invalid"]);
        }
        $data= new FlashSale();
        DB::beginTransaction();      
        try{    
            $data->starting_date=$request->startdate;
            $data->ending_date=$request->enddate;
            $data->status=1;
            $data->save();
            DB::commit();
            return response()->json(['status' => 1]);
        }catch(\Exception $e){
            DB::rollback();
            print_r($e->getMessage());  
            // throw $e;
            return response()->json(['status' => 0]);
        }
    }
    public function deleteFlashSale(FlashSale $flashsale,Request $request){
        try{
            DB::beginTransaction();
            FlashSale::where('id',$flashsale->id)->delete();
            DB::commit();
            return redirect()->route('showFlashSale');
        }catch(\Exception $e){
         print_r($e->getMessage());
            DB::rollback();
        }
        
    }
    public function statusFlashSale(FlashSale $id,Request $request){
        $flashsale=FlashSale::where('id',$id->id)->first();
        if($flashsale->status == 0){
            $flashsale->status="1";
        }else{
            $flashsale->status="0";
        }
        try{
            DB::beginTransaction();
            $flashsale->save();
            DB::commit();
            return redirect()->route('showFlashSale');
        }catch(\Exception $e){
            print_r($e->getMessage());
            DB::rollback(); 
            // throw $e;
        }
    }
    public function vendorsFlashSale($flashsale,Request $request){
        // $search = $request->search;
        // $query = FlashsaleProducts::with('vendor')
        // ->where('flash_sale_id',$flashsale);
        // if ($search) {
        //     $query->where(function ($sub) use ($search) {
        //         $sub->orWhere('company_name', 'like', "%" . $search . "%");
        //         $sub->orWhere('code', 'like', "%" . $search . "%");
        //     });
        // }
        // $query->get();
        // $details = $query->paginate(10);
        // $data = [
        //     'details' => $details,
        //     'search' => $search,
        // ];
        //     return view('admin.flashsale.vendors',$data);
        $search = $request->search;
        $query=FlashsaleProducts::query();
        $query->join('vendors','vendors.id','=','flashsale_products.vendor_id')
        ->where('flashsale_products.flash_sale_id',$flashsale)
        ->selectRaw('vendors.company_name,vendors.id,MAX(flashsale_Products.flash_sale_id) AS flash_sale_id')
        ->selectRaw('count(vendor_id) as product_count')
        ->groupBy('company_name','vendors.id');
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('company_name', 'like', "%" . $search . "%");
            });
        }
        $products = $query->paginate(10);
        $data = [
            'products' => $products,
            'search' => $search,
        ];
        return view('admin.flashsale.vendors', $data);
    }
    public function productApproveFlashSale($flashsaleid,$id,Request $request){
        $query=Flashsaleproducts::query()->where('vendor_id',$id);
        $query->where('flash_sale_id',$flashsaleid);
        $query->with('product');
        $search = $request->search;
        if ($search) {
            $query->whereHas('product',function ($sub) use ($search) {
                $sub->Where('name', 'like', "%" . $search . "%");
            });
        }
        $query->orderBy('created_at','desc');
        $query->get();
        $details = $query->paginate(10);
        $data = [
            'details' => $details,
            'search' => $search,
        ];
            return view('admin.flashsale.approveproducts',$data);

    }
    public function approvalProductFlashSale(Request $request){
        $flashsaleproduct=FlashsaleProducts::where([
            ['product_id',$request->id],
            ['vendor_id',$request->vendor_id],
            ['flash_sale_id',$request->fs_id],
        ])->first();
        $notification = new Notification;
        $data=array('route'=>'flashsale','id'=>$request->fs_id,'vendor_id'=>$request->vendor_id);
        DB::beginTransaction();
        try{
            $flashsaleproduct->approve='1';
            $flashsaleproduct->save();
            $notification->added_by = Auth()->user()->id;
            $notification->added_to=$request->vendor_id;
            $notification->process="Product listed for Flash sales approved by admin ".Auth()->User()->name.".";
            $notification->link= 'swqcqw';
            $notification->admin_link= serialize($data);
            $notification->is_view=0;
            $notification->save();
            DB::commit();
            return response()->json(['status'=>true,]);
        }catch(\Exception $e){
            DB::rollback();
            throw $e;
            return response()->json(['status'=>false]);
        }
    }
    public function rejectProductFlashSale(Request $request){
        $flashsaleproduct=FlashsaleProducts::where([
            ['product_id',$request->id],
            ['vendor_id',$request->vendor_id],
            ['flash_sale_id',$request->fs_id],
        ])->first();
        $notification = new Notification;
        $data=array('route'=>'flashsale','id'=>$request->fs_id,'vendor_id'=>$request->vendor_id);
        DB::beginTransaction();
        try{
            $flashsaleproduct->approve='2';
            $flashsaleproduct->save();
            $notification->added_by = Auth()->user()->id;
            $notification->added_to=$request->vendor_id;
            $notification->process="Product listed for flash sale rejected by admin ".Auth()->User()->name.".";
            $notification->link="adqadqwdqdwqdw";
            $notification->admin_link= serialize($data);
            $notification->is_view=0;
            $notification->save();
            DB::commit();
            return response()->json(['status'=>true]);
        }catch(\Exception $e){
            DB::rollback();
            // throw $e;
            return response()->json(['status'=>false]);
        }
    }
    public function multipleVendorApproveFlashSale(Request $request){
        $datas=$request->checked;
        $val=$request->val;
        if($datas == "" || $val == ""){
            return response()->json(['status'=>false]);
        }
        try{
            foreach($datas as $id){
                $fsp=FlashSaleProducts::where('vendor_id',$id);
                DB::beginTransaction();
                    if($val == 0){
                        $fsp->update(['approve'=>2]);  
                    }
                    else{
                        $fsp->update(['approve'=>1]); 
                    }
                    DB::commit();
            }
            return response()->json(['status'=>true]);
        }catch(\Exception $e){
                    DB::rollback();
                    // throw $e;
                    return response()->json(['status'=>false]);
                    
        } 
    }
    public function multipleProductsApproveFlashSale(Request $request){
        $datas=$request->checked;
        $val=$request->val;
        try{
            foreach($datas as $id){
                $fsp=FlashSaleProducts::where('product_id',$id)->first();
                DB::beginTransaction();
                    if($val == 0){
                        $fsp->approve='2';
                        $fsp->save();   
                    }
                    else{
                        $fsp->approve='1';
                        $fsp->save();
                    }
                    DB::commit();
            }
            return response()->json(['status'=>true]);
        }catch(\Exception $e){
                    DB::rollback();
                    // throw $e;
                    print_r($e->getMessage());  
                    return response()->json(['status'=>false,"message"=>"error, try again"]);
                    
        } 
    }
}
