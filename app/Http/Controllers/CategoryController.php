<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Error;
use Validator;

class CategoryController extends Controller
{

    public function showCategories(Request $request)
    {
        $search = $request->search;
        $query = Category::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('name', 'like', "%" . $search . "%");
                $sub->orWhere('description', 'like', "%" . $search . "%");
            });
        }
        $query->where('active', '1');
        $query->where('parent_id', 0);
        $query->orderBy('created_at', 'desc');
        $categories = $query->paginate(10);
        $data = [
            'categories' => $categories,
            'search' => $search,
        ];
        return view('admin.categories.list', $data);
    }

    public function showNewCategory(Request $request)
    {
        return view('admin.categories.new');
    }

    public function editCategory($id = null, Request $request)
    {
        $category = Category::where('id', '=', $id)->first();
        $data = [
            'category' => $category
        ];
        return view('admin.categories.edit', $data);
    }

    public function updateCategory(Request $request)
    {
        $rules = [
            'id' => 'required',
            'name' => 'required',
            'image' => 'mimes:jpeg,jpg,png|max:20000'
        ];
        $messages = [
            'id.required' => "Category not found",
            'name.required' => "Company name is required",
            'image.mimes' => "Invalid file format only .jpeg,jpg,png files are allowed",
            'image.max' => "Maxinum allowed size is 2mb",
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);

        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }


        $category = Category::where('id', $request->id)->first();

        if (!$category) {
            return response()->json(['status' => 0, 'errors' => ['id' => "Category not found"]]);
        }

        $category->name = $request->name;

        if ($request->description) {
            $category->description = $request->description;
        }else{
            $category->description = "";
        }

        if (request()->image) {
            try {
                $imageName = time() . '.' . request()->image->getClientOriginalExtension();
                request()->image->move(public_path('images/categories/'), $imageName);
                $category->image = 'images/categories/' . $imageName;
            } catch (\Exception $e) {
            }
        }

        $category->save();

        return response()->json(['status' => true, 'message' => 'Category updated successfully']);
    }

    public function newCategory(Request $request)
    {
        $rules = [
            'name' => 'required',
            'image' => 'mimes:jpeg,jpg,png|max:20000'
        ];
        $messages = [
            'name.required' => "Company name is required",
            'image.mimes' => "Invalid file format only .jpeg,jpg,png files are allowed",
            'image.max' => "Maxinum allowed size is 2mb",
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);

        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }


        $category = new Category();
        $category->name = $request->name;

        if ($request->parent_id) {
            $category->parent_id = $request->parent_id;
        } else {
            $category->parent_id = 0;
        }

        if ($request->description) {
            $category->description = $request->description;
        }

        if (request()->image) {
            try {
                $imageName = time() . '.' . request()->image->getClientOriginalExtension();
                request()->image->move(public_path('images/categories/'), $imageName);
                $category->image = 'images/categories/' . $imageName;
            } catch (\Exception $e) {
            }
        }

        $category->save();

        return response()->json(['status' => true, 'message' => 'Category updated successfully']);
    }

    public function blockCategory(Request $request)
    {
        try {
            $category = Category::where('id', $request->id)->first();
            if ($category) {
                if ($category->active == 0) {
                    throw new Error('Category inactive');
                } else {
                    $category->active = 0;
                    $category->save();
                    return response()->json(['status' => true, 'message' => 'Category deactivated successfully']);
                }
            } else {
                throw new Error('Category not found');
            }
        } catch (\Exception $err) {
            return response()->json(['status' => false, 'errors' => ['failed' => "Could not find category"]]);
        }
    }

    //subcategories listing
    public function showSubCategories($id = null, Request $request)
    {
        $search = $request->search;
        $query = Category::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('name', 'like', "%" . $search . "%");
                $sub->orWhere('description', 'like', "%" . $search . "%");
            });
        }
        $query->where('active', '1');
        $query->where('parent_id', $id);
        $query->orderBy('created_at', 'desc');
        $categories = $query->paginate(10);
        $category = Category::where('id', $id)->first();
        if (!$category) {
            return redirect()->route('showCategories');
        }

        $data = [
            'categories' => $categories,
            'main_category' => $category,
            'search' => $search,
        ];
        return view('admin.categories.subcategories', $data);
    }

    //new subcategory
    public function showNewSubCategory($id = null, Request $request)
    {
        $category = Category::where('id', $id)->first();
        if (!$category) {
            return redirect()->route('showCategories');
        }
        $data = [
            'main_category' => $category
        ];
        return view('admin.categories.newsubcategory', $data);
    }

    //save subcategory
    public function newSubCategory(Request $request)
    {
        $rules = [
            'name' => 'required',
            'image' => 'mimes:jpeg,jpg,png|max:20000'
        ];

        $messages = [
            'name.required' => "Company name is required",
            'image.mimes' => "Invalid file format only .jpeg,jpg,png files are allowed",
            'image.max' => "Maxinum allowed size is 2mb",
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);

        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }


        $category = new Category();
        $category->name = $request->name;

        if ($request->parent_id) {
            $category->parent_id = $request->parent_id;
        } else {
            $category->parent_id = 0;
        }

        if ($request->description) {
            $category->description = $request->description;
        }else{
            $category->description = "";
        }

        if (request()->image) {
            try {
                $imageName = time() . '.' . request()->image->getClientOriginalExtension();
                request()->image->move(public_path('images/categories/'), $imageName);
                $category->image = 'images/categories/' . $imageName;
            } catch (\Exception $e) {
            }
        }

        try {
            $category->save();
            return response()->json(['status' => true, 'message' => 'Category added successfully']);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'errors' => ['couldnot_save' => 'Category could not add successfully']]);
        }
    }

    //edit subcategory
    public function editSubCategory($main = null, $id = null, Request $request)
    {
        $main_category = Category::where('id', '=', $main)->first();
        $category = Category::where('id', $id)->first();
        if (!$category) {
            return redirect()->route('showCategories');
        }
        
        $data = [
            'category' => $category,
            'main_category' => $main_category
        ];
        return view('admin.categories.editsubcategory', $data);
    }
}
