<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Coupon;
use App\Hotdeal;
use Auth;

class CouponController extends Controller
{
    public function newBuyerZone(Request $request)
    {
        $coupons = Coupon::where(function ($query) {
            $query->where(function ($sub) {
                $sub->where('user_id', '!=', null);
                $sub->where('user_id', Auth::user()->id);
            });
            $query->orWhere(function ($sub) {
                $sub->where('user_id', null);
            });
        })
            ->whereDate('valid_from', '<=', date('Y-m-d H:i:s'))
            ->whereDate('valid_to', '>=', date('Y-m-d H:i:s'))
            ->where('new_user', 1)
            ->get();

        $hotdeals =  Hotdeal::select('id', 'product_id', 'vendor_id')->with(['product' => function ($q) {
            $q->select('id', 'name', 'description', 'price', 'mrp', 'wholesale', 'brand_id', 'other_brand')->with([
                'brand',
                'images'
            ]);

            if (auth('api')->user()) {
                $q->with(['whishlist' => function ($sub) {
                    $sub->where('user_id', auth('api')->user()->id);
                }]);
            }
        }])->orderBy('created_at', 'desc')->limit(10)->get();

        $data = [
            'coupons' => $coupons,
            'hotdeals' => $hotdeals,
            'status' => true
        ];

        return response()->json($data);
    }
}
