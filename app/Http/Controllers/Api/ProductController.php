<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\ProductAttribute;
use App\Category;
use App\CategoryAttribute;
use App\FlashsaleProducts;
use App\WishList;
use App\ProductLog;
use App\DealOftheDay;
use App\FlashSale;
use App\User;
use App\UserLog;
use App\Vendor;
use App\BudgetBazar;
use Auth;
use DB;

use Validator;

class ProductController extends Controller
{
    public function getProducts(Request $request)
    {

        $rules = [
            'sort' => 'in:popular,most_reviews,newest,low_price,high_price',
            'search'=> 'trim'
        ];
        $messages = [
            'sort.in' => "Invalid sort argument",
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);

        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => false, 'errors' => $errors]);
        }

        $query = Product::query();

        if ($request->category) {
            $category_ids = Category::SubCategories($request->category);
            $category_ids[] = $request->category;
            $query->whereIn('category_id', $category_ids);
        }

        $filters = [];
        if ($request->filters && sizeof($request->filters) > 0) {
            $filters = $request->filters;
        }

        //filter products
        $product_filter = [
            'category' => function ($query) {
                $query->select('id', 'name', 'description', 'image');
            },
            'brand' => function ($query) {
                $query->select('id', 'name', 'logo');
            },
            'images',
            'flashsale' => function ($query) {
                $query
                    ->where('active_status', 1)
                    ->where('approve', 1)
                    ->whereHas('flashsale', function ($q) {
                        $q->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
                        $q->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
                    });
            },
            'deal' => function ($sub) {
                $sub
                    ->where('approve', 1)
                    ->whereDate('start_date', date('Y-m-d'));
            }
        ];

        if (auth('api')->user()) {
            $product_filter['whishlist'] = function ($query) {
                $query->where('user_id', auth('api')->user()->id);
            };
        }

        //end filter products

        $query->select('id', 'name', 'category_id', 'vendor_id', 'brand_id', 'other_brand', 'price', 'mrp', 'created_at', 'updated_at', 'stock_unit','assured')
            ->with($product_filter);

        //start filter 
        if (sizeof($filters) > 0) {
            $query->whereHas('attributes', function ($sub) use ($filters) {
                $sub->where(function ($sub_sub) use ($filters) {
                    foreach ($filters as $key => $value) {
                        $sub_sub->orWhere(function ($q) use ($key, $value) {
                            $q->where('attribute', $key)->Where('value', $value);
                        });
                    }
                });
            });
        }
        //end filter

        //start sorting
        if ($request->sort) {
            if ($request->sort == "popular" || $request->sort == "most_reviews") {
                $query->orderBy('star', 'desc');
            } elseif ($request->sort == "newest") {
                $query->orderBy('created_at', 'desc');
            } elseif ($request->sort == "high_price") {
                $query->orderBy('mrp', 'desc');
            } elseif ($request->sort == "high_price") {
                $query->orderBy('mrp', 'asc');
            }
        }
        //end sorting

        if($request->search){
            $query->where('name','like','%'.$request->search."%");
        }

        $products = $query->get();


        $filters = $this->category_filters($request);

        $data = [
            'products' => $products,
            'status' => true,
            'filters' => $filters
        ];
        return response()->json($data);
    }

    public function category_filters($request)
    {
        $filters = [];
        if ($request->category) {
            $filters = CategoryAttribute::where('category_id', $request->category)->where('searchable', 1)->with('attribute_values')->get();
        }
        return $filters;
    }

    public function getProductDetails(Request $request)
    {
        $rules = [
            'product' => 'required'
        ];
        $messages = [
            'product.required' => "Product is required",
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => false, 'errors' => $errors]);
        }

        try {
            $is_exists = Product::find($request->product)->count();
            if ($is_exists) {
                $query = Product::query();
                $query->where('id', $request->product);

                $product_filter = [
                    'category' => function ($query) {
                        $query->select('id', 'name', 'description', 'image');
                    },
                    'brand' => function ($query) {
                        $query->select('id', 'name', 'logo');
                    },
                    'vendor' => function ($query) {
                        $query->select('id', 'code', 'company_name', 'store_name', 'image', 'star');
                    },
                    'additional_info',
                    'attributes',
                    'images',
                    'faq',
                    'flashsale' => function ($query) {
                        $query
                            ->where('active_status', 1)
                            ->where('approve', 1)
                            ->whereHas('flashsale', function ($q) {
                                $q->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
                                $q->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
                            });
                    },
                    'productreview' =>function ($query) {
                        $user = User::where('id',auth('api')->user()->id)->first();
                        $query
                        ->where('customer_id',$user->id)
                         ->where('approve',1);
                    },
                    'deal' => function ($sub) {
                        $sub
                            ->where('approve', 1)
                            ->whereDate('start_date', date('Y-m-d'));
                    }
                    
                ];
                if (auth('api')->user()) {
                    $product_filter['whishlist'] = function ($query) {
                        $query->where('user_id', auth('api')->user()->id);
                    };
                }
                $product = $query->select('id', 'name', 'stock_unit', 'category_id', 'vendor_id', 'brand_id', 'other_brand', 'price', 'mrp', 'created_at', 'updated_at','assured')
                    ->with($product_filter)->first();

                if (auth('api')->user()) {
                    $this->saveProductLog($product);
                }
                return response()->json(['status' => true, 'data' => ['product' => $product]]);
            } else {
                throw new \Exception('Product not found');
            }
        } catch (\Exception $e) {
            $errors['couldnot_save'] = $e->getMessage();
            return response()->json(['status' => false, 'errors' => $errors]);
        }
    }

    public function saveProductLog($product)
    {
        $productlog = new ProductLog();
        $productlog->product_id = isset($product->id) ? $product->id : null;
        $productlog->brand_id = isset($product->brand_id) ? $product->brand_id : null;
        $productlog->user_id = auth('api')->user()->id;
        $productlog->category_id = isset($product->category_id) ? $product->category_id : null;
        $productlog->save();
    }

    public function getProductVariantAttributes(Request $request)
    {
        $rules = [
            'product' => 'required'
        ];
        $messages = [
            'product.required' => "Product is required",
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => false, 'errors' => $errors]);
        }

        try {
            $selected_attributes = $request->attributers;
            // print_r($selected_attributes);exit;

            $product = Product::find($request->product);
            if (!$product) {
                throw new \Exception('Product not found');
            }

            $variant_ids = $product->variant_ids();

            if ($selected_attributes && sizeof($selected_attributes) > 0) {
                $query = ProductAttribute::query();
                $query->whereIn('product_id', $variant_ids)->get();
                $query->where(function ($sub) use ($selected_attributes) {
                    for ($i = 0; $i < count($selected_attributes); $i++) {
                        $sub->orWhere(function ($sub_sub) use ($selected_attributes, $i) {
                            $sub_sub->where('attribute', $selected_attributes[$i]['attribute']);
                            $sub_sub->where('value', $selected_attributes[$i]['value']);
                        });
                    }
                });
                $attributes  = $query->get();
                if (sizeof($attributes) > 0) {
                    $variant_ids = [];
                    foreach ($attributes as $attribute) {
                        $variant_ids[] = $attribute->product_id;
                    }
                }
            }

            $query = ProductAttribute::query();
            $attributes = $query->whereIn('product_id', $variant_ids)->get();
            $attributes = $attributes->groupBy('attribute');
            $items = [];
            $product_id = 0;
            foreach ($attributes as $key => $attribute) {
                $i = 0;
                foreach ($attribute as $item) {
                    $product_id =  $item->product->id;
                    $items[$key][$i]['product'] = $product_id;
                    $items[$key][$i]['value'] = $item->value;
                    $items[$key][$i]['image'] = $item->product->images->first()->image;
                    ++$i;
                }
            }
            return response()->json([
                'status' => true,
                'data' => [
                    'attributes' => $items,
                    'selected_attributes' => $selected_attributes,
                    'product_id' => $product_id
                ]
            ]);
        } catch (\Exception $e) {
            $errors['could_not_get'] = $e->getMessage();
            return response()->json(['status' => false, 'errors' => $errors]);
        }
    }

    public function addRemoveWishList(Request $request)
    {
        $wishlist = WishList::where('user_id', Auth::user()->id)->where('product_id', $request->product)->first();
       
        try {
            if (!$wishlist) {
                $wishlist = new WishList();
                $wishlist->product_id = $request->product;
                $wishlist->user_id = Auth::user()->id;
                $wishlist->save();

                $user = User::where('id',Auth::user()->id)
                        ->select('id','first_name')->first();
                $name = $user->first_name;
                $data_array[] = [
                    'type' => 'ADD_TO_WISHLIST',
                    'user_id' => Auth::user()->id,
                   'activity' => $name . ' add the product to wish list  on ' . date('Y/m/d H:i:s'),
                    'created_at' => now(),
                    'updated_at' => now()
                   ];
                if(!empty($data_array)){
                    try{
                        DB::beginTransaction();
                        UserLog::insert($data_array);
                        DB::commit();
                    }catch (\Exception $e) {
                        DB::rollback();
                       $errors['couldnot_save'] = $e->getMessage();
                        return response()->json(['status' => false, 'errors' => $errors]);
                    }     
                }
                $message = "Added to wish list";
            } else {
                $wishlist->delete();
                $user = User::where('id',Auth::user()->id)
                        ->select('id','first_name')->first();
                $name = $user->first_name;
                $data_array[] = [
                    'type' => 'REMOVE_FROM_WISHLIST',
                    'user_id' => Auth::user()->id,
                   'activity' => $name . ' removed the product from wish list  on ' . date('Y/m/d H:i:s'),
                    'created_at' => now(),
                    'updated_at' => now()
                   ];
                if(!empty($data_array)){
                    try{
                        DB::beginTransaction();
                        UserLog::insert($data_array);
                        DB::commit();
                    }catch (\Exception $e) {
                        DB::rollback();
                       $errors['couldnot_save'] = $e->getMessage();
                        return response()->json(['status' => false, 'errors' => $errors]);
                    }     
                }
                $message = "Removed wish list";
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => "Could not perform the action"]);
        }
        return response()->json(['status' => true, 'message' => $message]);
    }

    public function getWishList(Request $request)
    {
        $rules = [
            'sort' => 'in:popular,most_reviews,newest,low_price,high_price',
        ];
        $messages = [
            'sort.in' => "Invalid sort argument",
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);

        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => false, 'errors' => $errors]);
        }

        $wishlists = WishList::select('id', 'product_id')->where('user_id', Auth::user()->id)->get();
        $wishlistids = [];
        foreach ($wishlists as $wishlist) {
            $wishlistids[] = $wishlist->product_id;
        }

        $query = Product::query();

        if (sizeof($wishlistids) > 0) {
            $query->whereIn('id', $wishlistids);
        } else {
            $data = [
                'products' => [],
                'status' => true
            ];
            return response()->json($data);
        }



        //filter products
        $product_filter = [
            'category' => function ($query) {
                $query->select('id', 'name', 'description', 'image');
            },
            'brand' => function ($query) {
                $query->select('id', 'name', 'logo');
            },
            'images',
            'flashsale' => function ($query) {
                $query
                    ->where('active_status', 1)
                    ->where('approve', 1)
                    ->whereHas('flashsale', function ($q) {
                        $q->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
                        $q->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
                    });
            },
            'deal' => function ($sub) {
                $sub
                    ->where('approve', 1)
                    ->whereDate('start_date', date('Y-m-d'));
            }
        ];

        if (auth('api')->user()) {
            $product_filter['whishlist'] = function ($query) {
                $query->where('user_id', auth('api')->user()->id);
            };
        }

        //end filter products

        $query->select('id', 'name', 'category_id', 'vendor_id', 'brand_id', 'other_brand', 'price', 'mrp', 'created_at', 'updated_at', 'stock_unit','assured')
            ->with($product_filter);

        //start sorting
        if ($request->sort) {
            if ($request->sort == "popular" || $request->sort == "most_reviews") {
                $query->orderBy('star', 'desc');
            } elseif ($request->sort == "newest") {
                $query->orderBy('created_at', 'desc');
            } elseif ($request->sort == "high_price") {
                $query->orderBy('mrp', 'desc');
            } elseif ($request->sort == "high_price") {
                $query->orderBy('mrp', 'asc');
            }
        }
        //end sorting

        $products = $query->get();

        $data = [
            'products' => $products,
            'status' => true
        ];
        return response()->json($data);
    }

    public function otherOffers(Request $request)
    {
        $rules = [
            'sort' => 'in:popular,most_reviews,newest,low_price,high_price',
        ];
        $messages = [
            'sort.in' => "Invalid sort argument",
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);

        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => false, 'errors' => $errors]);
        }


        $product = Product::find($request->product);

        $query = Product::query();

        if ($product->upc && $product->ean) {
            $query->where(function ($sub) use ($product) {
                $sub->where('upc', $product->upc);
                $sub->orWhere('ean', $product->ean);
            });
            $query->where('variant', '!=', $product->id);
        } else {
            $data = [
                'products' => [],
                'status' => true
            ];
            return response()->json($data);
        }

        //filter products
        $product_filter = [
            'category' => function ($query) {
                $query->select('id', 'name', 'description', 'image');
            },
            'brand' => function ($query) {
                $query->select('id', 'name', 'logo');
            },
            'images',
            'flashsale' => function ($query) {
                $query
                    ->where('active_status', 1)
                    ->where('approve', 1)
                    ->whereHas('flashsale', function ($q) {
                        $q->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
                        $q->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
                    });
            },
            'deal' => function ($query) {
                $query
                    ->where('approve', 1)
                    ->whereDate('start_date', date('Y-m-d'));
            },
            'vendor' => function ($query) {
                $query->select('id', 'code', 'company_name', 'store_name', 'image');
            }
        ];

        if (auth('api')->user()) {
            $product_filter['whishlist'] = function ($query) {
                $query->where('user_id', auth('api')->user()->id);
            };
        }

        //end filter products

        $query->select('id', 'name', 'category_id', 'vendor_id', 'brand_id', 'other_brand', 'price', 'mrp', 'created_at', 'updated_at', 'stock_unit','assured')
            ->with($product_filter);

        //start sorting
        if ($request->sort) {
            if ($request->sort == "popular" || $request->sort == "most_reviews") {
                $query->orderBy('star', 'desc');
            } elseif ($request->sort == "newest") {
                $query->orderBy('created_at', 'desc');
            } elseif ($request->sort == "high_price") {
                $query->orderBy('mrp', 'desc');
            } elseif ($request->sort == "high_price") {
                $query->orderBy('mrp', 'asc');
            }
        }
        //end sorting

        $products =  $query->get();


        $data = [
            'products' => $products,
            'status' => true
        ];
        return response()->json($data);
    }

    public function getFlashSale(Request $request)
    {
        $rules = [
            'sort' => 'in:popular,most_reviews,newest,low_price,high_price',
        ];
        $messages = [
            'sort.in' => "Invalid sort argument",
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);

        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => false, 'errors' => $errors]);
        }

        $flashsales = FlashSale::orderBy('starting_date')->whereDate('ending_date', '>=', date('Y-m-d'))->with(['products' => function ($query) use($request) {
            $query->where('approve', 1)->with(['product' => function ($q) use ($request){
                $q->select('id', 'name', 'description', 'price', 'mrp', 'wholesale', 'brand_id', 'other_brand')->with([
                    'brand', 
                    'images',
                    'flashsale' => function ($sub) {
                        $sub
                            ->where('active_status', 1)
                            ->where('approve', 1)
                            ->whereHas('flashsale', function ($sub_sub) {
                                $sub_sub->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
                                $sub_sub->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
                            });
                    },
                    'deal' => function ($sub) {
                        $sub
                            ->where('approve', 1)
                            ->whereDate('start_date', date('Y-m-d'));
                    },
                ]);
                 //start sorting
                 if ($request->sort) {
                    if ($request->sort == "popular" || $request->sort == "most_reviews") {
                        $q->orderBy('star', 'desc');
                    } elseif ($request->sort == "newest") {
                        $q->orderBy('created_at', 'desc');
                    } elseif ($request->sort == "high_price") {
                        $q->orderBy('mrp', 'desc');
                    } elseif ($request->sort == "high_price") {
                        $q->orderBy('mrp', 'asc');
                    }
                }
                //end sorting
                if (auth('api')->user()) {
                    $q->with(['whishlist' => function ($sub) {
                        $sub->where('user_id', auth('api')->user()->id);
                    }]);
                }
                $q->limit(4);
            }]);
        }])->get();

        if (!$flashsales) {
            $data = [
                'status' => true,
                'flashsales' => []
            ];
        } else {
            $data = [
                'status' => true,
                'flashsales' => $flashsales
            ];
        }
        return response()->json($data);
    }

    public function getDealOfTheDay(Request $request)
    {
        $rules = [
            'sort' => 'in:popular,most_reviews,newest,low_price,high_price',
        ];
        $messages = [
            'sort.in' => "Invalid sort argument",
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);

        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => false, 'errors' => $errors]);
        }

        $dealsoftheday =  DealOftheDay::where('approve', 1)->whereDate('start_date', date('Y-m-d'))->with(['product' => function ($query) use($request) {
            $query->select('id', 'name', 'description', 'price', 'mrp', 'wholesale', 'brand_id', 'other_brand')->with('brand', 'images');
            if ($request->sort) {
                if ($request->sort == "popular" || $request->sort == "most_reviews") {
                    $query->orderBy('star', 'desc');
                } elseif ($request->sort == "newest") {
                    $query->orderBy('created_at', 'desc');
                } elseif ($request->sort == "high_price") {
                    $query->orderBy('mrp', 'desc');
                } elseif ($request->sort == "high_price") {
                    $query->orderBy('mrp', 'asc');
                }
            }
            if (auth('api')->user()) {
                $query->with(['whishlist' => function ($sub) {
                    $sub->where('user_id', auth('api')->user()->id);
                }]);
            }
        }])->get();

        $data = [
            'status' => true,
            'deals' => $dealsoftheday
        ];
        return response()->json($data);
    }

    public function pathayapuraproductListing(Request $request)
    {
        $query = Product::query();
        if ($request->category) {
            $category_ids = Category::SubCategories($request->category);
            // return response()->json($category_ids);
            // exit();
            $category_ids[] = $request->category;
            $result = $query->select('id', 'name', 'category_id', 'pathayapura_listing', 'price', 'mrp', 'star')
            ->with([
                'images' => function ($query) {
                    $query->select('product_id', 'image');
                }
            ]);
            $query->whereIn('category_id', $category_ids);
            $query->paginate(10);
            $product = $query->get();
            return response()->json(['status' => true, 'products' => $product]);
        }
        else{
            $category_ids=1;
            $result = $query->select('id', 'name', 'category_id', 'pathayapura_listing', 'price', 'mrp', 'star')
            ->with([
                'images' => function ($query) {
                    $query->select('product_id', 'image');
                }
            ]);
            $query->where('category_id', 1);
            $query->orWhere('pathayapura_listing',1);
            $query->paginate(10);
            $product = $query->get();
            return response()->json(['status' => true, 'products' => $product]);
        }
    }
    public function shopBytown(Request $request){
        $search = $request->search;
        $query = Product::query();
        if($search){
            $query->where(function ($sub) use ($search){
                $sub->orWhere('town','like',"%".$search."%");
            });
        }
         //filter products
         $product_filter = [
            'category' => function ($query) {
                $query->select('id', 'name', 'description', 'image');
            },
            'brand' => function ($query) {
                $query->select('id', 'name', 'logo');
            },
            'images',
            'flashsale' => function ($query) {
                $query
                    ->where('active_status', 1)
                    ->where('approve', 1)
                    ->whereHas('flashsale', function ($q) {
                        $q->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
                        $q->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
                    });
            },
            'deal' => function ($sub) {
                $sub
                    ->where('approve', 1)
                    ->whereDate('start_date', date('Y-m-d'));
            }
        ];

        if (auth('api')->user()) {
            $product_filter['whishlist'] = function ($query) {
                $query->where('user_id', auth('api')->user()->id);
            };
        }

        //end filter products

        $query->select('id','name','variant','description','dcin','ean','upc','brand_id','other_brand','category_id','pathayapura_listing','warranty_details','gst','sess','price','mrp','wholesale','order_quantity','stock_unit','cod','commission','weight','weight_class_id','length','width','height','length_class_id','shipping_processing_time','return_policy','free_delivery','vendor_id','completed_status','approved','admin_comments','step','star','assured','featured_product','town','created_at','updated_at','deleted_at')
                ->with($product_filter);
        $query->where('completed_status',1);
        $query->where('approved',1);
        $query->orderBy('created_at','desc');
        $query->paginate(10);
        $result = $query->get();
        return response()->json(['status'=>true,'product'=>$result]); 
    }
    
    // public function getBudgetbazar(Request $request){
    //      $query = BudgetBazar::query();
       
    //     //filter products
    //     $product_filter = [
    //         'category' => function ($query) {
    //             $query->select('id', 'name', 'description', 'image');
    //         },
    //         'brand' => function ($query) {
    //             $query->select('id', 'name', 'logo');
    //         },
    //         'product' => function ($query) {
    //             $query->select('id','name','brand_id','price','mrp','star','completed_status','approved')
    //          ->where('completed_status',1)
    //         ->where('approved',1);
    //         },
    //         'images',
    //         'flashsale' => function ($query) {
    //             $query
    //                 ->where('active_status', 1)
    //                 ->where('approve', 1)
    //                 ->whereHas('flashsale', function ($q) {
    //                     $q->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
    //                     $q->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
    //                 });
    //         },
    //         'deal' => function ($sub) {
    //             $sub
    //                 ->where('approve', 1)
    //                 ->whereDate('start_date', date('Y-m-d'));
    //         }
    //     ];

    //     if (auth('api')->user()) {
    //         $product_filter['whishlist'] = function ($query) {
    //             $query->where('user_id', auth('api')->user()->id);
    //         };
    //     }

    //     //end filter products

        
    //    $query->select('id','product_id','vendor_id','active')
    //             ->with($product_filter);    
            
    //         $query->paginate(10);
    //         $query->where('active',1);
    //         $product = $query->get();
    //     $query = Vendor::query();
    //     $search = $request->search;
    //     if ($search) {
    //         $query->where(function ($sub) use ($search) {
    //             $sub->orWhere('name', 'like', "%" . $search . "%");
    //         });
    //     }
    //     $query->where('status',1);
    //     $query->where('active',1);
    //     $query->where('approved',1);
    //     $query->orderBy('created_at','desc');
    //     $query->paginate(10);
    //     $result = $query->get();
    //     return response()->json(['status'=>true,'result'=>$result]); 
    // }

   public function getCategories(Request $request){

    $banner = Banner::where('banner_type',5)
                    ->where('status',1)->get();
       $query = Category::query();
      
        if ($request->category) {
            
            $query->where('parent_id',$request->category);
            $subcategory = $query->get();
        
            return response()->json(['status' => true,'banner' => $banner, 'result' => $subcategory ]);
        }
        else{
            
            $query->where('active',1);
            $result = $query->get();
            return response()->json(['status' => true,'banner' => $banner, 'result' => $result]);
        }      
   }

    public function getBudgetbazar(Request $request){
        $query = BudgetBazar::query();

        $result = $query->select('id','product_id','vendor_id','active')
                ->with([
                    'product' => function($query) {
                        $query->select('id','name','brand_id','price','mrp','star','completed_status','approved')
                            ->where('completed_status',1)
                            ->where('approved',1);
                    },

                    'images' => function($query) {
                        $query->select('product_id','image');
                    }
                ]);    
            
            $query->paginate(10);
            $query->where('active',1);
            $product = $query->get();

        return response()->json(['status' => true, 'Budgetbazarproducts' => $product]);
        
    }

}
