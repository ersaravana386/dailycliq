<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Brand;
use App\ProductLog;
use App\Product;
use Auth;
use App\Http\Controllers\ProductController;


class BrandController extends Controller
{
    public function getFeaturedbrand(Request $request){
        $log = ProductLog::orderBy('created_at','desc')
                    ->where('brand_id','!=',null)
                    ->select('id','product_id','user_id','brand_id')
                    ->limit(5)->get();
        if(!$log){
            return [];
        }
        $search = $request->search;
        $query = Brand::query();
        if($search){
            $query->where(function ($sub) use ($search){
                $sub->orWhere('name','like',"%".$search."%");
            });
        }
        $query->where('featured',1);
        $query->orderBy('created_at','desc');
        $query->paginate(10);
        $brands = $query->get();
        if(auth('api')->user()){
            $log->where('user_id',auth('api')->user()->id);
            return response()->json(['status'=>true,'featured_brands'=>$brands, 'recent_search'=>$log]);
        }
        else{
            return response()->json(['status'=>false,'featured_brands'=>$brands,'recent_search'=>'null']);
        }    
    }

    public function getOtherbrand(Request $request){
        $log = ProductLog::orderBy('created_at','desc')
                        ->where('brand_id','!=',null)
                        ->select('id','product_id','brand_id','user_id')
                        ->limit(5)->get();
        if(!$log){
            return [];
        }
        $search = $request->search;
        $query = Brand::query();
        if($search){
            $query->where(function ($sub) use ($search){
                $sub->orWhere('name','like',"%".$search."%");
            });
        }
        $query->where('featured',0);
        $query->orderBy('created_at','desc');
        $query->paginate(10);
        $brands=$query->get();
        if(auth('api')->user()){
            $log->where('user_id',auth('api')->user()->id);
            return response()->json(['status'=>true,'other brand products'=>$brands,'recent search'=>$log]);
        }
        else{
            return response()->json(['status' => false,'other brand products' => $brands, 'recent search' => 'null']);
        }  
    }

    public function getbrand(Request $request){
        $query = Product::query();
        $brands = $query->select('id', 'name','price','mrp','star','brand_id')
            ->with([
                'images' => function ($query) {
                    $query->select('product_id', 'image');
                }
            ])
            ->where('brand_id', $request->id)
            ->where('completed_status',1)
            ->where('approved',1)
            ->get();
        return response()->json(['status' => true, 'brands' => $brands]);
    }
    
    public function getAllbrands(Request $request){
        $query =  Brand::query();
        $brands = $query->paginate(10);
        $result= $query->get();
        return response()->json(['status' => true, 'brands' => $result]);
    }
}
