<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Vendor;
use App\Product;
use App\Banner;
    
class VendorController extends Controller
{
    public function getvendorbyid(Request $request){
        $vendor = Vendor::where('id',$request->id)
                        ->select('id','company_name','address','email','mobile','image')
                        ->where('status',1)
                        ->where('approved',1)
                        ->where('active',1)
                        ->get();
    
        
        $query = Product::query();

        //filter products
        $product_filter = [
            'category' => function ($query) {
                $query->select('id', 'name', 'description', 'image');
            },
            'brand' => function ($query) {
                $query->select('id', 'name', 'logo');
            },
         
            'images',
            'flashsale' => function ($query) {
                $query
                    ->where('active_status', 1)
                    ->where('approve', 1)
                    ->whereHas('flashsale', function ($q) {
                        $q->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
                        $q->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
                    });
            },
            'deal' => function ($sub) {
                $sub
                    ->where('approve', 1)
                    ->whereDate('start_date', date('Y-m-d'));
            }
        ];

        if (auth('api')->user()) {
            $product_filter['whishlist'] = function ($query) {
                $query->where('user_id', auth('api')->user()->id);
            };
        }

        //end filter products

         $query->select('id', 'name','price','mrp','star','vendor_id')
                     ->with($product_filter);
            $query->where('vendor_id', $request->id);
            $query->where('completed_status',1);
            $query->where('approved',1);
            $query->paginate(10);
            $vendorproducts=$query->get();

        $banner_image = Banner::where('product_id',$request->id)
                        ->select('id','product_id','image')
                        ->get();

        $data = [
            'vendor' => $vendor,
            'banner image' => $banner_image,
            'vendor products' => $vendorproducts
        ];
        return response()->json(['status' => true, 'vendor ' => $data]);               
    }
   public function getProductsbyeanupc(Request $request){
       $products = Product::select('id','name','description','dcin','ean','upc','warranty_details','price','mrp','wholesale','stock_unit','return_policy','vendor_id','completed_status','approved','star','town')
                            ->whereColumn('ean','upc')
                            ->where('vendor_id','!=',$request->id)
                            ->where('completed_status',1)
                            ->where('approved',1)
                            ->get();
        return response()->json(['status' => true, 'products ' => $products]);
   }
}
