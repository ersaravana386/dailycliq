<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Product;
use App\Cart;
use App\Coupon;
use App\Category;
use App\DeliveryAddress;
use App\Order;
use App\OrderProduct;

use Auth;

class OrderController extends Controller
{
    public function checkout(Request $request)
    {
        if ($request->address) {
            $address = DeliveryAddress::find($request->address);
        } else {
            $address = Auth::user()->deliveryaddress->first();
        }

        $products = Cart::where('user_id', Auth::user()->id)->with(['product' => function ($q) use ($request) {
            $q->select('id', 'name', 'description', 'price', 'mrp', 'wholesale', 'brand_id', 'other_brand', 'stock_unit', 'assured', 'category_id', 'vendor_id')
                ->with(
                    [
                        'brand',
                        'images',
                        'flashsale' => function ($sub) {
                            $sub
                                ->where('active_status', 1)
                                ->where('approve', 1)
                                ->whereHas('flashsale', function ($sub_sub) {
                                    $sub_sub->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
                                    $sub_sub->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
                                });
                        },
                        'deal' => function ($sub) {
                            $sub
                                ->where('approve', 1)
                                ->whereDate('start_date', date('Y-m-d'));
                        },
                        'store99' => function ($sub) {
                            $sub
                                ->where('active', 1);
                        },
                    ]
                );

            if (auth('api')->user()) {
                $q->with(['whishlist' => function ($sub) {
                    $sub->where('user_id', auth('api')->user()->id);
                }]);
            }
            // $q->limit(4);
        }])->get();

        if(!sizeof($products)){
            return response()->json(['status' => false, 'message' => "No products in cart"]);
        }

        $coupon = null;
        $total_discount = 0;
        $grand_total = 0;

        foreach ($products as $product) {
            $grand_total += ($product->product->price * $product->units);
        }

        $product_ids = [];
        $category_ids = [];
        $vendor_ids = [];
        foreach ($products as $product) {
            $product_ids[] = $product->product->id;
            $main_categories[] = $product->product->category_id;
            $vendor_ids[] = $product->product->vendor_id;
        }
        foreach ($main_categories as $category) {
            $category_ids[] = $category;
            $temp_categories = Category::SubCategories($category);
            foreach ($temp_categories as $tcat) {
                $category_ids[] = $tcat;
            }
        }


        $coupons = Coupon::where(function ($query) use ($product_ids, $category_ids, $vendor_ids, $grand_total) {
            $query->where(function ($sub) {
                $sub->where('user_id', Auth::user()->id);
                $sub->where('category_id', null);
                $sub->where('vendor_id', null);
                $sub->where('product_id', null);
            });
            $query->orWhere(function ($sub) use ($category_ids) {
                $sub->where('user_id', Auth::user()->id);
                $sub->whereIn('category_id', $category_ids);
                $sub->where('vendor_id', null);
                $sub->where('product_id', null);
            });
            $query->orWhere(function ($sub) use ($category_ids, $vendor_ids) {
                $sub->where('user_id', Auth::user()->id);
                $sub->whereIn('category_id', $category_ids);
                $sub->whereIn('vendor_id', $vendor_ids);
                $sub->where('product_id', null);
            });
            $query->orWhere(function ($sub) use ($product_ids, $category_ids, $vendor_ids) {
                $sub->where('user_id', Auth::user()->id);
                $sub->whereIn('category_id', $category_ids);
                $sub->whereIn('vendor_id', $vendor_ids);
                $sub->whereIn('product_id', $product_ids);
            });

            $query->orWhere(function ($sub) use ($product_ids) {
                $sub->whereIn('product_id', $product_ids);
                $sub->where('user_id', null);
                $sub->where('category_id', null);
                $sub->where('vendor_id', null);
            });
            $query->orWhere(function ($sub) use ($product_ids) {
                $sub->whereIn('product_id', $product_ids);
                $sub->where('user_id', Auth::user()->id);
                $sub->where('category_id', null);
                $sub->where('vendor_id', null);
            });
            $query->orWhere(function ($sub) use ($product_ids, $category_ids) {
                $sub->whereIn('product_id', $product_ids);
                $sub->where('user_id', Auth::user()->id);
                $sub->whereIn('category_id', $category_ids);
                $sub->where('vendor_id', null);
            });
            $query->orWhere(function ($sub) use ($product_ids, $category_ids, $vendor_ids) {
                $sub->whereIn('product_id', $product_ids);
                $sub->where('user_id', Auth::user()->id);
                $sub->whereIn('category_id', $category_ids);
                $sub->whereIn('vendor_id', $vendor_ids);
            });

            $query->orWhere(function ($sub) use ($category_ids) {
                $sub->whereIn('category_id', $category_ids);
                $sub->where('product_id', null);
                $sub->where('user_id', null);
                $sub->where('vendor_id', null);
            });
            $query->orWhere(function ($sub) use ($category_ids, $product_ids) {
                $sub->whereIn('category_id', $category_ids);
                $sub->whereIn('product_id', $product_ids);
                $sub->where('user_id', null);
                $sub->where('vendor_id', null);
            });
            $query->orWhere(function ($sub) use ($category_ids, $product_ids) {
                $sub->whereIn('category_id', $category_ids);
                $sub->whereIn('product_id', $product_ids);
                $sub->where('user_id', Auth::user()->id);
                $sub->where('vendor_id', null);
            });
            $query->orWhere(function ($sub) use ($category_ids, $product_ids, $vendor_ids) {
                $sub->whereIn('category_id', $category_ids);
                $sub->whereIn('product_id', $product_ids);
                $sub->where('user_id', Auth::user()->id);
                $sub->whereIn('vendor_id', $vendor_ids);
            });

            $query->orWhere(function ($sub) use ($vendor_ids) {
                $sub->whereIn('vendor_id', $vendor_ids);
                $sub->where('category_id', null);
                $sub->where('product_id', null);
                $sub->where('user_id', null);
            });
            $query->orWhere(function ($sub) use ($vendor_ids, $category_ids, $product_ids) {
                $sub->whereIn('vendor_id', $vendor_ids);
                $sub->whereIn('category_id', $category_ids);
                $sub->where('product_id', null);
                $sub->where('user_id', null);
            });
            $query->orWhere(function ($sub) use ($vendor_ids, $category_ids, $product_ids) {
                $sub->whereIn('vendor_id', $vendor_ids);
                $sub->whereIn('category_id', $category_ids);
                $sub->whereIn('product_id', $product_ids);
                $sub->where('user_id', null);
            });
            $query->orWhere(function ($sub) use ($vendor_ids, $category_ids, $product_ids) {
                $sub->whereIn('vendor_id', $vendor_ids);
                $sub->whereIn('category_id', $category_ids);
                $sub->whereIn('product_id', $product_ids);
                $sub->where('user_id', Auth::user()->id);
            });

            $query->orWhere(function ($sub) {
                $sub->where('vendor_id', null);
                $sub->where('category_id', null);
                $sub->where('product_id', null);
                $sub->where('user_id', null);
            });

            $query->orWhere(function ($sub) use ($grand_total) {
                $sub->where('vendor_id', null);
                $sub->where('category_id', null);
                $sub->where('product_id', null);
                $sub->where('user_id', null);
                $sub->where('above_price', '!=', null);
                $sub->where('above_price', '>', $grand_total);
            });
        })
            ->whereDate('valid_from', '>=', date('Y-m-d H:i:s'))
            ->whereDate('valid_to', '>=', date('Y-m-d H:i:s'))
            ->get();

        if ($request->coupon) {
            $coupon = Coupon::where('code', 'like', $request->coupon)->first();
            if (!$coupon) {
                return response()->json(['status' => false, 'message' => "Coupon not applicable"]);
            } else {
                if (($coupon->user_id == null || $coupon->user_id == Auth::user()->id) && $coupon->vendor_id == null && $coupon->product_id == null && $coupon->category_id == null) {
                    if ($grand_total * ($coupon->offer_percentage / 100) >=  $coupon->offer_amount) {
                        $total_discount = $coupon->offer_amount;
                    } else {
                        $total_discount = $grand_total * ($coupon->offer_percentage / 100);
                    }
                } else {
                    $temp_price = 0;
                    $applied = 0;
                    foreach ($products as $product) {
                        if ($temp_price < $product->product->price) {
                            $temp_price = $product->product->price;
                        }
                        if ($temp_price > $product->product->price && $applied = 1) {
                            continue;
                        }

                        if ($coupon->vendor_id == $product->product->vendor_id && in_array($coupon->category_id, $category_ids) && $coupon->product_id == $product->prodict->id) {
                            $total_discount = $product->product->price * ($coupon->offer_percentage / 100);
                            $applied = 1;
                        } else if (in_array($coupon->category_id, $category_ids) && $coupon->product_id == $product->prodict->id) {
                            $total_discount = $product->product->price * ($coupon->offer_percentage / 100);
                            $applied = 1;
                        } else if ($coupon->product_id == $product->product->id) {
                            $total_discount = $product->product->price * ($coupon->offer_percentage / 100);
                            $applied = 1;
                        }
                    }
                }
            }
        }


        $points = 0;
        $data = [
            'address' => $address,
            'products' => $products,
            'points' => $points,
            'coupons' => $coupons,
            'applied_coupon' => $coupon,
            'grand_total' => round($grand_total, 2),
            'total_discount' => round($total_discount, 2)
        ];

        return response()->json(['status' => true, 'data' => $data, 'neworder' => 1]);
    }
    public function getOrder(Request $request)
    {
        $order = $order = OrderProduct::whereHas('order', function ($query) {
            $query->where('customer_id', Auth::user()->id);
        })->whereHas('product')->with([
            'order' => function ($query) {
                $query->select('id', 'order_reference_number', 'status', 'order_at', 'paid_at', 'total');
            },
            'product' => function ($query) {
                $query->select('id', 'name', 'description', 'other_brand', 'brand_id')->with([
                    'brand'
                ]);
            }
        ])->get();

        $completed = [];
        $pending = [];
        $refunded = [];
        $failed = [];
        $cancelled = [];
<<<<<<< HEAD
        $i=0;
        
            foreach ($order as $key => $value) {
                $status = $value->customer_id;
                if($status == 1){
                 $completed[] = $order[$i];
                 $i++;
                 }
                if($status == 2){
                    $pending[] = $order[$i];
                    $i++;
                 }
                 if($status==3){
                    $refunded[] = $order[$i];
                    $i++;
                 }
                 if($status == 4){
                    $failed[] = $order[$i];
                    $i++;
                 }
                 if($status == 5){
                    $cancelled[] = $order[$i];
                    $i++;
                 }
             }
=======
>>>>>>> d3711b33889d27268be133f62f18a1c71b8e25fa

        $i = 0;

        foreach ($order as $key => $value) {
            $status = $value->order->status;
            if ($status == 1) {
                $completed[] = $order[$i];
                $i++;
            }
            if ($status == 2) {
                $pending[] = $order[$i];
                $i++;
            }
            if ($status == 3) {
                $refunded[] = $order[$i];
                $i++;
            }
            if ($status == 4) {
                $failed[] = $order[$i];
                $i++;
            }
            if ($status == 5) {
                $cancelled[] = $order[$i];
                $i++;
            }
        }

        return response()->json(['status' => true, 'completed' => $completed, 'pending' => $pending, 'refunded' => $refunded, 'failed' => $failed, 'cancelled' => $cancelled]);
    }
}
