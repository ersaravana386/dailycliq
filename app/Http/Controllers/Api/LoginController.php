<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Validator;
use Hash;
use Auth;
use DB;
use App\UserLog;
use App\Mail\TestEmail;
use App\User; 
use App\PointMatrix;
use App\Point;

class LoginController extends Controller
{
    public function sendOtp(Request $request)
    {
        $rules = [
            'mobile' => 'required',
        ];
        $messages = [
            'mobile.required' => "Mobile number is required",
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);

        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => false, 'errors' => $errors]);
        }

        try {
            $user = User::where('mobile', $request->mobile)->first();
            $new_user_flag = 0;
            if (!$user) {
                $user = new User();
                $user->mobile = $request->mobile;
                $user->password = Hash::make(Str::random(12));
                $user->api_token = Str::random(60);
                $new_user_flag = 1;
            }
            $otp = Str::random(4);
            $user->mobile_otp = $otp;
            $user->save();

            if ($new_user_flag)
                $this->addDqPoints('NEW_MEMBER', $user);

            return response()->json(['status' => true, 'otp' => $otp, 'message' => "Otp send successfully"]);
        } catch (\Exception $e) {
            $errors['couldnot_save'] = $e->getMessage();
            return response()->json(['status' => false, 'errors' => $errors]);
        }
    }

    public function verifyOtp(Request $request)
    {
        $rules = [
            'otp' => 'required',
            'mobile' => 'required',
        ];
        $messages = [
            'otp.required' => "Otp is required",
            'mobile.required' => "Mobile number is required",
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);

        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => false, 'errors' => $errors]);
        }

        try {

            $user = User::where('mobile', $request->mobile)->first();
            if (!$user) {
                throw new \Exception('User not found');
            }
            if ($user->mobile_otp != $request->otp) {
                throw new \Exception('incorrect Otp');
            }
            $user->api_token = Str::random(60);
            $user->mobile_otp = null;
            $user->save();
            Auth::loginUsingId($user->id);
            return response()->json([
                'status' => true,
                'data' => [
                    'user' => $user,
                    'token' => $user->api_token
                ],
                'message' => "Otp verified successfully"
            ]);
        } catch (\Exception $e) {
            $errors['couldnot_save'] = $e->getMessage();
            return response()->json(['status' => false, 'errors' => $errors]);
        }
    }
    public function loginwithsocialmedia(Request $request)
    {

        $rules = [
            'email' => 'required'

        ];
        $messages = [
            'email.required' => "Email is required"

        ];

        $validator = Validator::make(request()->all(), $rules, $messages);

        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => false, 'errors' => $errors]);
        }
        try {
            $user = User::where('email', $request->email)->first();

            if (!$user) {
                $user = new User();
                if ($request->image) {
                    $imageName = time() . '.' . $request->image->extension();
                    $path = 'images/users/8/';
                    $request->image->move(public_path($path), $imageName);
                }
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->email = $request->email;
                $user->image = $path . $imageName;
                $user->password = Hash::make(Str::random(12));
                $user->api_token = Str::random(60);
                $user->save();
                $this->addDqPoints('NEW_MEMBER', $user);

                $data = ['message' => 'You are now on the list - and will be the first to know about our latest styles, exclusive offers, and much more!!'];
                Mail::to($user->email)->send(new TestEmail($data));

                $name = $user->first_name;

                $data_array[] = [
                    'type' => 'USER_LOGIN',
                    'user_id' => $user->id,
                    'activity' => $name . ' logged in on ' . date('Y/m/d H:i:s'),
                    'created_at' => now(),
                    'updated_at' => now()
                ];
                
                if (!empty($data_array)) {
                    try {
                        DB::beginTransaction();
                        UserLog::insert($data_array);
                        DB::commit();
                    } catch (\Exception $e) {
                        DB::rollback();
                        $errors['couldnot_save'] = $e->getMessage();
                        return response()->json(['status' => false, 'errors' => $errors]);
                    }
                }

                return response()->json([
                    'status' => true,
                    'data' => [
                        'user' => $user,
                        'token' => $user->api_token
                    ],
                    'message' => "User added successfully"
                ]);
            } else {
                $user->email = $request->email;
                $user->api_token = Str::random(60);
                $user->save();

                $name = $user->first_name;

                $data_array[] = [
                    'type' => 'USER_LOGIN',
                    'user_id' => $user->id,
                    'activity' => $name . ' logged in on ' . date('Y/m/d H:i:s'),
                    'created_at' => now(),
                    'updated_at' => now()
                ];
                if (!empty($data_array)) {
                    try {
                        DB::beginTransaction();
                        UserLog::insert($data_array);
                        DB::commit();
                    } catch (\Exception $e) {
                        DB::rollback();
                        $errors['couldnot_save'] = $e->getMessage();
                        return response()->json(['status' => false, 'errors' => $errors]);
                    }
                }
                Auth::loginUsingId($user->id);
                return response()->json([
                    'status' => true,
                    'data' => [
                        'user' => $user,
                        'token' => $user->api_token
                    ],
                    'message' => "User details updated successfully"
                ]);
            }
        } catch (\Exception $e) {
            $errors['couldnot save'] = $e->getMessage();
            return response()->json(['status' => false, 'errors' => $errors]);
        }
    }

    public function addDqPoints($activity = null, $user)
    {
        
        $point_matrix_elem = PointMatrix::where('activity', $activity)->first();
        if ($point_matrix_elem) {
            try {
                DB::beginTransaction();
                $point = new Point();
                $point->user_id = $user->id;
                $point->activity = $point_matrix_elem->activity;
                $point->points = $point_matrix_elem->point;
                $point->save();
                $user->points += $point_matrix_elem->point;
                $user->save();
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
            }
        }
    }
}
