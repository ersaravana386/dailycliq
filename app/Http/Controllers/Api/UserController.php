<?php

namespace App\Http\Controllers\Api;

use App\BudgetBazar;
use App\Coupon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DeliveryAddress;
use App\ProductLog;
use Validator;
use Auth;
use DB;
use App\OrderProduct;
use App\User;
use App\WishList;
use App\EmailUs;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmailUs;

class UserController extends Controller
{
    public function setGender(Request $request)
    {
        $rules = [
            'gender' => 'required'
        ];
        $messages = [
            'gender.required' => "Gender is required",
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => false, 'errors' => $errors]);
        }

        try {
            Auth::user()->gender = $request->gender;
            Auth::user()->save();
            return response()->json(['status' => true, 'message' => 'Gender saved successfully']);
        } catch (\Exception $e) {
            $errors['couldnot_save'] = $e->getMessage();
            return response()->json(['status' => false, 'errors' => $errors]);
        }
    }
    public function saveProfile(Request $request)
    {
        $rules = [
            'image' => 'image|max:2048|mimes:jpeg,png,jpg,gif,svg',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'mobile' => 'required',
            'dob' => 'required',
        ];
        $messages = [
            'image.image' => "Image is not valid",
            'image.max' => "Image maximum size allowed is 2 MB",
            'image.mimes' => "Allowed image types are jpeg,png,jpg,gif,svg",
            'first_name.required' => "First name is required",
            'last_name.required' => "Last name is required",
            'email.required' => "Email is required",
            'mobile.required' => "Mobile is required",
            'dob.required' => "Date Of Birth is required",
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => false, 'errors' => $errors]);
        }

        try {
            if ($request->image) {
                $imageName = time() . '.' . $request->image->extension();
                $path = 'images/users/' . Auth::user()->id;
                $request->image->move(public_path($path), $imageName);
                Auth::user()->image = $path . "/" . $imageName;
            }

            Auth::user()->first_name = $request->first_name;
            Auth::user()->last_name = $request->last_name;
            Auth::user()->mobile = $request->mobile;
            Auth::user()->email = $request->email;
            Auth::user()->dob = $request->dob;
            Auth::user()->gender = $request->gender;
            Auth::user()->save();
            return response()->json(['status' => true, 'message' => 'Profile updated successfully']);
        } catch (\Exception $e) {
            $errors['couldnot_save'] = $e->getMessage();
            return response()->json(['status' => false, 'errors' => $errors]);
        }
    }

    public function getProfile(Request $request)
    {
        $data = [
            'user' => Auth::user()->toArray()
        ];
        return response()->json(['status' => true, 'data' => $data]);
    }

    public function deliveryAddressinsertion(Request $request)
    {
        $rules = [
            'name' => 'required',
            'mobile_number' => 'required',
            'pin_code' => 'required',
            'city' => 'required',
            'state' => 'required'
        ];
        $messages = [
            'name.required' => 'Name is required',
            'mobile_number.required' => 'Mobile number is required',
            'pin_code.required' => 'Pin code is required',
            'city.required' => 'City is required',
            'state.required' => 'State is required'
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => false, 'errors' => $errors]);
        }
        try {
            DB::beginTransaction();
            $collection = collect([
                $bname = $request->building_name,
                $sname = $request->street_name,
                $landmark = $request->landmark
            ]);
            $address = $collection->implode(',', 'collection');

            $data_array[] = [
                'user_id' => Auth::user()->id,
                'name' => $request->name,
                'mobile_number' => $request->mobile_number,
                'pin_code' => $request->pin_code,
                'address' => $address,
                'city' => $request->city,
                'state' => $request->state,
                'created_at' => now(),
                'updated_at' => now()
            ];

            if (!empty($data_array)) {
                DeliveryAddress::insert($data_array);
            }
            DB::commit();
            return response()->json(['status' => true, 'message' => 'Data added']);
        } catch (\Exception $e) {
            DB::rollback();
            $errors['couldnot_save'] = $e->getMessage();
            return response()->json(['status' => false, 'errors' => $errors]);
        }
    }

    public function getAddress(Request $request)
    {
        $addresses = DeliveryAddress::where('user_id', Auth::user()->id)->get();
        return response()->json(['status' => true, 'addresses' => $addresses]);
    }

    public function editAddress(Request $request)
    {
        $address = DeliveryAddress::find($request->id);
        return response()->json(['status' => true, 'address' => $address]);
    }

    public function updateAddress($id = null, Request $request)
    {
        $address = DeliveryAddress::where('id', $id)->first();
        if (!$address) {
            return response()->json(['status' => false, 'errors' => ['coulld_not_save' => 'Address details could not update successfully']]);
        }
        $rules = [
            'name' => 'required',
            'mobile_number' => 'required',
            'pin_code' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required'
        ];
        $messages = [
            'name.required' => 'Name is required',
            'mobile_number.required' => 'Mobile number is required',
            'pin_code.required' => 'Pin code is required',
            'address.required' => 'Address is required',
            'city.required' => 'City is required',
            'state.required' => 'State is required'
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => false, 'errors' => $errors]);
        }

        $collection = collect([
            $bname = $request->building_name,
            $sname = $request->street_name,
            $landmark = $request->landmark
        ]);

        $stringadd = $collection->implode(',', 'collection');
        $address->user_id = Auth::user()->id;
        $address->name = $request->name;
        $address->mobile_number = $request->mobile_number;
        $address->pin_code = $request->pin_code;
        $address->address = $stringadd;
        $address->city = $request->city;
        $address->state = $request->state;

        try {
            DB::beginTransaction();
            $address->save();
            DB::commit();
            return response()->json(['status' => true, 'message' => 'Data updated']);
        } catch (\Exception $e) {
            DB::rollback();
            $errors['couldnot_save'] = $e->getMessage();
            return response()->json(['status' => false, 'errors' => $errors]);
        }
    }
    public function deleteAddress($id = null, Request $request)
    {
        $address = DeliveryAddress::find($request->id);
        if ($address) {
            $address->delete();
            return response()->json(['status' => true, 'message' => 'Data deleted']);
        } else {
            return response()->json(['status' => false, 'message' => 'error']);
        }
    }

    public function reviewInsert(Request $request)
    {
        $rules = [
            'image' => 'image|max:2048|mimes:jpeg,png,jpg,gif,svg',
            'heading' => 'required',
            'comment' => 'required', 
            'star' => 'required'
        ];
        $messages = [
            'image.image' => "Image is not valid",
            'image.max' => "Image maximum size allowed is 2 MB",
            'image.mimes' => "Allowed image types are jpeg,png,jpg,gif,svg",
            'heading.required' => 'Review heading is required',
            'comment.required' => 'Review comment is required',
            'star.required' => 'Star value is required'
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => false, 'errors' => $errors]);
        }
        try {
            if ($request->image) {
                $imageName = time() . '.' . $request->image->extension();
                $path = 'images/productreview/';
                $request->image->move(public_path($path), $imageName);
            }
            $data_array[] = [
                'customer_id' => $request->customer_id,
                'product_id' => $request->product_id,
                'heading' => $request->heading,
                'comment' => $request->comment,
                'star' => $request->star,
                'image' => $path . $imageName,
                'approve' => 1,
                'created_at' => now(),
                'updated_at' => now()
            ];
            if (!empty($data_array)) {
                try {
                    DB::beginTransaction();
                    ProductReview::insert($data_array);
                    $result = DB::table('product_reviews')
                        ->groupBy('product_id')
                        ->where('product_id', $request->product_id)
                        ->selectRaw('product_id,avg(star) as star')
                        ->where('approve', 1)
                        ->get();
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollback();
                    $errors['couldnot_save'] = $e->getMessage();
                    return response()->json(['status' => false, 'errors' => $errors]);
                }
                $id = $request->product_id;
                $product = Product::find($id);
                $product->star = $result[0]->star;
                try {
                    DB::beginTransaction();
                    $product->save();
                    DB::commit();
                    return response()->json(['status' => true, 'message' => 'Data inserted and updated successfully']);
                } catch (\Exception $e) {

                    DB::rollback();
                    $errors['couldnot_save'] = $e->getMessage();
                    return response()->json(['status' => false, 'errors' => $errors]);
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            $errors['couldnot_save'] = $e->getMessage();
            return response()->json(['status' => false, 'errors' => $errors]);
        }
    }

    public function history(Request $request)
    {
        $log = ProductLog::where('user_id', Auth::user()->id)->with(['product' => function ($q) use ($request) {
            $q->select('id', 'name', 'description', 'price', 'mrp', 'wholesale', 'brand_id', 'other_brand', 'created_at')->with('brand', 'images');
            //start sorting
            $q->orderBy('created_at', 'desc');
            //end sorting
            if (auth('api')->user()) {
                $q->with(['whishlist' => function ($sub) {
                    $sub->where('user_id', Auth::user()->id);
                }]);
            }
            $q->limit(4);
        }])->orderBy('created_at')->limit(20)->get();
        return response()->json(['status' => true, 'log' => $log]);
    }

    public function getMainProfile(Request $request)
    {
        $order = $order = OrderProduct::whereHas('order', function ($query) {
            $query->where('customer_id', Auth::user()->id);
        })->whereHas('product')->with([
            'order' => function ($query) {
                $query->select('id', 'order_reference_number', 'status', 'order_at', 'paid_at', 'total');
            },
            'product' => function ($query) {
                $query->select('id', 'name', 'description', 'other_brand', 'brand_id')->with([
                    'brand'
                ]);
            }
            
        ])->get();

        $completed = [];
        $pending = [];
        $refunded = [];
        $failed = [];
        $cancelled = [];

        $i = 0;

        foreach ($order as $key => $value) {
            $status = $value->order->status;
            if ($status == 1) {
                $completed[] = $order[$i];
                $i++;
            }
            if ($status == 2) {
                $pending[] = $order[$i];
                $i++;
            }
            if ($status == 3) {
                $refunded[] = $order[$i];
                $i++;
            }
            if ($status == 4) {
                $failed[] = $order[$i];
                $i++;
            }
            if ($status == 5) {
                $cancelled[] = $order[$i];
                $i++;
            }
        }


        //budget bazaar
        $query = BudgetBazar::query();

        $query->select('id', 'product_id', 'vendor_id', 'active')
            ->whereHas('product', function ($query) {
                $query->where('completed_status', 1);
                $query->where('approved', 1);
            })
            ->with([
                'product' => function ($query) {
                    $query->select('id', 'name', 'brand_id', 'price', 'mrp', 'star', 'completed_status', 'approved')
                        ->where('completed_status', 1)
                        ->where('approved', 1);

                    $product_filter = [
                        'category' => function ($sub) {
                            $sub->select('id', 'name', 'description', 'image');
                        },
                        'brand' => function ($sub) {
                            $sub->select('id', 'name', 'logo');
                        },
                        'images',
                        'flashsale' => function ($sub) {
                            $sub
                                ->where('active_status', 1)
                                ->where('approve', 1)
                                ->whereHas('flashsale', function ($q) {
                                    $q->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
                                    $q->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
                                });
                        },
                        'deal' => function ($sub) {
                            $sub
                                ->where('approve', 1)
                                ->whereDate('start_date', date('Y-m-d'));
                        },
                        'whishlist' => function ($query) {
                            $query->where('user_id', auth('api')->user()->id);
                        }
                    ];
                },
                'banner' => function ($query) {
                    $query
                        ->select('id','banner_type','product_id','image','status')
                        ->where('banner_type',3)
                        ->where('status','1');
                }
            ]);

        $query->paginate(10);
        $query->where('active', 1);
        $budgetbazaar = $query->get();
        //end budget bazaar

        $orders = [
            'completed' => sizeof($completed),
            'pending' => sizeof($pending),
            'refunded' => sizeof($refunded),
            'failed' => sizeof($failed),
            'cancelled' => sizeof($cancelled)
        ];

        $data = [
            'orders' => $orders,
            'points' => Auth::user()->points,
            'coupons' => Coupon::where('user_id', Auth::user()->id)->count(),
            'wishlist' => WishList::where('user_id', Auth::user()->id)->count(),
            'budgetbazaar' => $budgetbazaar,
            'status' => true
        ];
        print json_encode($data);
    }
    public function emailus(Request $request){
   
        $rules = [
            'subject' => 'required',
            'message' => 'required',
            'phone_number' => 'required',
            'email' => 'required'
        ];
        $messages = [
            'subject.required' => 'Subject is required',
            'message.required' => 'Message is required',
            'phone_number.required' => 'Phone number is required',
            'email.required' => 'Email is required'
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => false, 'errors' => $errors]);
        }
        try{
           $user= User::where('id',auth('api')->user()->id)->first();
            if ($request->image) {
                $imageName = time() . '.' . $request->image->extension();
                $path = 'images/productreview/';
                $request->image->move(public_path($path), $imageName);
            }
            $data_array[] = [
                'user_id' =>$user->id,
                'subject' => $request->subject,
                'message' => $request->message,
                'phone_number' => $request->phone_number,
                'email' => $request->email,
                'image' => $path. $imageName,
                'created_at' => now(),
                'updated_at' => now()
            ];
            
            if(!empty($data_array)){
                try{
                    DB::beginTransaction();
                    EmailUs::insert($data_array);

                    $attachment = $path. $imageName;
                    $subject = ' New Email Us from '. $user->first_name;    
                    $message = $request->message;
                    $phone = $request->phone_number;
                    $email = $request->email;
                    
                    $data =[
                        'message' => 'Message :'. $message ,
                         'phone' => 'Phone Number :'. $phone, 
                         'email' => 'Email :'. $email
                        ];
                    
                   
                   
                    Mail::to('admin@gmail.com')->send(new SendEmailUs($attachment,$subject,$data));
                    
                   
                    DB::commit();
                    return response()->json(['status' => true, 'message' => 'Data inserted  successfully']);
                }catch(\Exception $e){
                    DB::rollback();
                    $errors['couldnot_save'] = $e->getMessage();
                    return response()->json(['status' => false, 'errors' => $errors]);
                }
            }    
        }catch(\Exception $e){
            DB::rollback();
            $errors['couldnot_save'] = $e->getMessage();
            return response()->json(['status' => false, 'errors' => $errors]);
        }
        
    }
    public function logout(Request $request){
            $user = User::where('id',auth('api')->user()->id)->first();
            $user->api_token = null;
            $user->save();
            Auth::logout();
            return response()->json([
                'message' => 'Successfully logged out'
            ]);
        
    }
}
