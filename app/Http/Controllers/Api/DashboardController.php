<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Brand;
use App\FlashSale;
use App\DealOftheDay;
use App\Product;
use App\ProductLog;
use App\Store99;
use App\Banner;
use App\GetItFree;
use App\User;
use App\Point;
use Auth;
use App\Http\Controllers\ProductController;

class DashboardController extends Controller
{
    public function getDashboardDetails(Request $request)
    {
        $categories = Category::where('active', 1)->where('parent_id', 0)->select('id', 'name', 'description', 'image')->get();
        $brands = Brand::where('featured', 1)->get();
        $upcoming_flashsale = $this->upcoming_flashsale($request);
        $dealsoftheday = $this->dealoftheday($request);
        $newarrivals = $this->newArrivals($request);
        $you_may_like = $this->youMayLike($request);
        $new_trends = $this->newTrends($request);
        $store99 = $this->store99($request);

        $data = [
            'categories' => $categories,
            'brands' => $brands,
            'flashsale' => $upcoming_flashsale,
            'dealsoftheday' => $dealsoftheday,
            'newarrivals' => $newarrivals,
            'you_may_like' => $you_may_like,
            'new_trends' => $new_trends,
            'store99' => $store99
        ];
        return response()->json($data);
    }

    public function store99(Request $request)
    {
        $store99 =  Store99::select('id', 'product_id', 'vendor_id')->where('active', 1)->with(['product' => function ($q) {
            $q->select('id', 'name', 'description', 'price', 'mrp', 'wholesale', 'brand_id', 'other_brand')->with([
                'brand',
                'images',
                'flashsale' => function ($query) {
                    $query
                        ->where('active_status', 1)
                        ->where('approve', 1)
                        ->whereHas('flashsale', function ($q) {
                            $q->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
                            $q->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
                        });
                },
                'deal' => function ($query) {
                    $query
                        ->where('approve', 1)
                        ->whereDate('start_date', date('Y-m-d'));
                },
            ]);

            if (auth('api')->user()) {
                $q->with(['whishlist' => function ($sub) {
                    $sub->where('user_id', auth('api')->user()->id);
                }]);
            }
            $q->limit(4);
        }])->get();
        return $store99;
    }

    public function youMayLike(Request $request)
    {
        $log = ProductLog::orderBy('created_at', 'desc')->where('category_id', '!=', null)->first();
        if (!$log) {
            return [];
        }
        $category = $log->category_id;

        $query = Product::query();

        if ($category) {
            $category_ids = Category::SubCategories($category);
            $category_ids[] = $category;
            $query->whereIn('category_id', $category_ids);
        }

        //filter products
        $product_filter = [
            'category' => function ($query) {
                $query->select('id', 'name', 'description', 'image');
            },
            'brand' => function ($query) {
                $query->select('id', 'name', 'logo');
            },
            'images',
            'flashsale' => function ($query) {
                $query
                    ->where('active_status', 1)
                    ->where('approve', 1)
                    ->whereHas('flashsale', function ($q) {
                        $q->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
                        $q->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
                    });
            },
            'deal' => function ($query) {
                $query
                    ->where('approve', 1)
                    ->whereDate('start_date', date('Y-m-d'));
            },
        ];

        if (auth('api')->user()) {
            $product_filter['whishlist'] = function ($query) {
                $query->where('user_id', auth('api')->user()->id);
            };
        }

        //end filter products

        $products = $query->select('id', 'name', 'category_id', 'vendor_id', 'brand_id', 'other_brand', 'price', 'mrp', 'created_at', 'updated_at', 'stock_unit', 'assured')
            ->with($product_filter)->get();

        return $products;
    }

    public function upcoming_flashsale(Request $request)
    {
        $flashsale = FlashSale::orderBy('starting_date')->whereDate('ending_date', '>=', date('Y-m-d'))->with(['products' => function ($query) {
            $query->where('approve', 1)->with(['product' => function ($q) {
                $q->select('id', 'name', 'description', 'price', 'mrp', 'wholesale', 'brand_id', 'other_brand')->with([
                    'brand',
                    'images'
                ]);
                if (auth('api')->user()) {
                    $q->with(['whishlist' => function ($sub) {
                        $sub->where('user_id', auth('api')->user()->id);
                    }]);
                }
                $q->limit(4);
            }]);
        }])->first();
        if (!$flashsale) {
            return null;
        } else {
            return $flashsale;
        }
    }

    public function dealoftheday(Request $request)
    {
        $dealsoftheday =  DealOftheDay::where('approve', 1)->whereDate('start_date', date('Y-m-d'))->with(['product' => function ($query) {
            $query->select('id', 'name', 'description', 'price', 'mrp', 'wholesale', 'brand_id', 'other_brand')->with('brand', 'images');
            if (auth('api')->user()) {
                $query->with(['whishlist' => function ($sub) {
                    $sub->where('user_id', auth('api')->user()->id);
                }]);
            }
            $query->limit(4);
        }])->get();
        return $dealsoftheday;
    }

    public function newArrivals(Request $request)
    {
        $query = Product::query();

        //filter products
        $product_filter = [
            'category' => function ($query) {
                $query->select('id', 'name', 'description', 'image');
            },
            'brand' => function ($query) {
                $query->select('id', 'name', 'logo');
            },
            'images',
            'flashsale' => function ($query) {
                $query
                    ->where('active_status', 1)
                    ->where('approve', 1)
                    ->whereHas('flashsale', function ($q) {
                        $q->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
                        $q->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
                    });
            },
            'deal' => function ($query) {
                $query
                    ->where('approve', 1)
                    ->whereDate('start_date', date('Y-m-d'));
            }
        ];

        if (auth('api')->user()) {
            $product_filter['whishlist'] = function ($query) {
                $query->where('user_id', auth('api')->user()->id);
            };
        }

        //end filter products

        $products = $query->select('id', 'name', 'category_id', 'vendor_id', 'brand_id', 'other_brand', 'price', 'mrp', 'created_at', 'updated_at', 'stock_unit', 'assured')
            ->with($product_filter)->orderBy('created_at', 'desc')->limit(20)->get();

        return response()->json([
            'status' => true, 'products' => $products
        ]);
    }

    public function newTrends(Request $request)
    {
        $query = Product::query();
        $trends_categories = [5, 6, 7, 9];
        $category_ids = [5, 6, 7, 9];
        foreach ($trends_categories as $cat) {
            $temp_cats = Category::SubCategories($cat);
            foreach ($temp_cats as $tcat) {
                $category_ids[] = $tcat;
            }
        }


        $query->whereIn('category_id', $category_ids);

        //filter products
        $product_filter = [
            'category' => function ($query) {
                $query->select('id', 'name', 'description', 'image');
            },
            'brand' => function ($query) {
                $query->select('id', 'name', 'logo');
            },
            'images',
            'flashsale' => function ($query) {
                $query
                    ->where('active_status', 1)
                    ->where('approve', 1)
                    ->whereHas('flashsale', function ($q) {
                        $q->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
                        $q->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
                    });
            },
            'deal' => function ($query) {
                $query
                    ->where('approve', 1)
                    ->whereDate('start_date', date('Y-m-d'));
            }
        ];

        if (auth('api')->user()) {
            $product_filter['whishlist'] = function ($query) {
                $query->where('user_id', auth('api')->user()->id);
            };
        }

        //end filter products

        $products = $query->select('id', 'name', 'category_id', 'vendor_id', 'brand_id', 'other_brand', 'price', 'mrp', 'created_at', 'updated_at', 'stock_unit', 'assured')
            ->with($product_filter)->orderBy('created_at', 'desc')->limit(20)->get();

        return $products;
    }

    public function getHomebanner(Request $request)
    {
        $result = Banner::where([['banner_type', 1], ['status', 1]])
            ->select('id', 'banner_type', 'product_id', 'image', 'status', 'created_at', 'updated_at')
            ->get();
        return response()->json($result);
    }

    public function getOfferbanner(Request $request)
    {
        $date = now();
        $result = Banner::where([['banner_type', 2], ['status', 1]])
            ->where('offer_date_from', '<=', $date)
            ->where('offer_date_to', '>=', $date)
            ->select('id', 'banner_type', 'product_id', 'image', 'offer_date_from', 'offer_date_to', 'status', 'created_at', 'updated_at')
            ->get();
        return response()->json($result);
    }


    public function dashboardpathayapura(Request $request)
    {
        $banners = Banner::where([['banner_type', 1], ['pathayapura_banner', 1], ['status', 1]])
            ->select('id', 'banner_type', 'product_id', 'image', 'status', 'pathayapura_banner', 'created_at', 'updated_at')
            ->get();

        $categories = Category::where('parent_id', 1)
            ->select('id', 'name', 'parent_id', 'image')->get();

        $featuredproducts = $this->featuredproducts($request);

        $bestsellers = $this->bestsellers($request);

        $mix_bestsellers = [];

        $i = 0;
        $j = 0;
        $banner = $this->getBanner($j);

        if ($banner) {
            $banner->type = "banner";
            $mix_bestsellers[] = $banner;
            $j++;
        }

        foreach ($bestsellers as $bestseller) {
            if ($i % 4 == 0 && $i != 0) {
                $banner = $this->getBanner($j);
                if ($banner) {
                    $banner->type = "banner";
                    $mix_bestsellers[] = $banner;
                    $j++;
                }
            }
            $bestseller->type = "product";
            $mix_bestsellers[] = $bestseller;
            $i++;
        }

        $pathayapuranewarrivals = $this->pathayapuranewarrivals($request);

        $moreitems = $this->moreitems($request);

        $data = [
            'banner' => $banners,
            'category' => $categories,
            'featuredproducts' => $featuredproducts,
            'bestsellers' => $mix_bestsellers,
            'pathayapuranewarrivals' => $pathayapuranewarrivals,
            'moreitems' => $moreitems
        ];
        return response()->json(['status' => true, 'result' => $data]);
    }

    public function getBanner($nth)
    {
        return Banner::where([['banner_type', 2], ['pathayapura_banner', 1], ['status', 1]])
            ->select('id', 'banner_type', 'product_id', 'image', 'status', 'pathayapura_banner', 'created_at', 'updated_at')
            ->get($nth)->first();
    }

    public function featuredproducts(Request $request)
    {

        $category = $request->category_id;
        if (!$category) {
            $category = 1;
        }

        $query = Product::query();
        
        //filter products
        $product_filter = [
            'category' => function ($query) {
                $query->select('id', 'name', 'description', 'image');
            },
            'brand' => function ($query) {
                $query->select('id', 'name', 'logo');
            },
         
            'images',
            'flashsale' => function ($query) {
                $query
                    ->where('active_status', 1)
                    ->where('approve', 1)
                    ->whereHas('flashsale', function ($q) {
                        $q->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
                        $q->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
                    });
            },
            'deal' => function ($sub) {
                $sub
                    ->where('approve', 1)
                    ->whereDate('start_date', date('Y-m-d'));
            }
        ];

        if (auth('api')->user()) {
            $product_filter['whishlist'] = function ($query) {
                $query->where('user_id', auth('api')->user()->id);
            };
        }

        //end filter products

         $query->select('id', 'name', 'featured_product', 'price', 'mrp')
            ->with($product_filter); 
        $query->where('featured_product', 1);

        //filter categories
        $category_ids = [];
        if ($category) {
            $category_ids = Category::SubCategories($category);
            $category_ids[] = $category;
            $query->whereIn('category_id', $category_ids);
        }
        //end filter categories

        $query->paginate(10);
        $featuredproducts = $query->get();
        return response()->json($featuredproducts);
        // return $featuredproducts;
    }

    public function bestsellers(Request $request)
    {

        $category = $request->category_id;
        if (!$category) {
            $category = 1;
        }

        $query = Product::query();

        //filter products
        $product_filter = [
            'category' => function ($query) {
                $query->select('id', 'name', 'description', 'image');
            },
            'images' => function ($query) {
                $query->select('id', 'product_id', 'image');
            },
            'flashsale' => function ($query) {
                $query
                    ->where('active_status', 1)
                    ->where('approve', 1)
                    ->whereHas('flashsale', function ($q) {
                        $q->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
                        $q->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
                    });
            },
            'deal' => function ($sub) {
                $sub
                    ->where('approve', 1)
                    ->whereDate('start_date', date('Y-m-d'));
            }
        ];

        if (auth('api')->user()) {
            $product_filter['whishlist'] = function ($query) {
                $query->where('user_id', auth('api')->user()->id);
            };
        }

        //end filter products


         $query->select('id', 'name', 'star', 'price', 'mrp')
            ->with($product_filter);

        //filter categories
        $category_ids = [];
        if ($category) {
            $category_ids = Category::SubCategories($category);
            $category_ids[] = $category;
            $query->whereIn('category_id', $category_ids);
        }
        //end filter categories

        $query->orderBy('star', 'desc');
        $query->paginate(12);
        $bestsellers = $query->get();
        return $bestsellers;
    }

    public function pathayapuranewarrivals(Request $request)
    {
        $query = Product::query();

        //filter products
        $product_filter = [
            'category' => function ($query) {
                $query->select('id', 'name', 'description', 'image');
            },
            'images' => function ($query) {
                $query->select('id', 'product_id', 'image');
            },
            'flashsale' => function ($query) {
                $query
                    ->where('active_status', 1)
                    ->where('approve', 1)
                    ->whereHas('flashsale', function ($q) {
                        $q->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
                        $q->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
                    });
            },
        ];

        if (auth('api')->user()) {
            $product_filter['whishlist'] = function ($query) {
                $query->where('user_id', auth('api')->user()->id);
            };
        }

        //end filter products

        $result = $query->select('id', 'name', 'price', 'created_at', 'updated_at')
            ->with($product_filter)->orderBy('created_at', 'desc');
        $query->paginate(10);
        $products = $query->get();

        return response()->json(['status' => true, 'products' => $products]);
    }
    public function moreitems(Request $request)
    {
        $query = Product::query();

        //filter products
        $product_filter = [
            'category' => function ($query) {
                $query->select('id', 'name', 'description', 'image');
            },
            'images' => function ($query) {
                $query->select('id', 'product_id', 'image');
            },
            'flashsale' => function ($query) {
                $query
                    ->where('active_status', 1)
                    ->where('approve', 1)
                    ->whereHas('flashsale', function ($q) {
                        $q->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
                        $q->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
                    });
            },
            'deal' => function ($sub) {
                $sub
                    ->where('approve', 1)
                    ->whereDate('start_date', date('Y-m-d'));
            }
        ];

        if (auth('api')->user()) {
            $product_filter['whishlist'] = function ($query) {
                $query->where('user_id', auth('api')->user()->id);
            };
        }

        //end filter products
        $items = $query->select('id', 'name', 'price', 'mrp', 'created_at', 'updated_at')
            ->with($product_filter);
        $query->where('pathayapura_listing', 1);
        $query->orderBy('created_at', 'desc');
        $query->paginate(10);
        $moreitems = $query->get();
        return $moreitems;
    }
    public function getitfree(Request $request){
        $getitfree =  GetItFree::select('id', 'product_id', 'active')->where('active', 1)->with(['product' => function ($q) {
            $q->select('id', 'name', 'description', 'price', 'mrp', 'wholesale', 'brand_id', 'other_brand')->with([
                'brand',
                'images'     
            ]);
            $q->limit(4);
        }])->get();
        return response()->json(['status' => true, 'get it free' => $getitfree]);
    }
    public function getPointdetails(Request $request){
        $banner = Banner::where('banner_type',4)
                        ->where('status',1)
                        ->get();
        $totalpoints = User::select('id','first_name','last_name','points')
                            ->where('id',auth('api')->user()->id)
                            ->get();
        $allpoints = Point::select('id','user_id','activity','points','used','created_at','updated_at')
                        ->where('user_id',auth('api')->user()->id)
                        ->get();
        $gainedpoints = Point::select('id','user_id','activity','points','used','created_at','updated_at')
                            ->where('user_id',auth('api')->user()->id)
                            ->where('used','!=',1)
                            ->get();
        $usedpoints = Point::select('id','user_id','activity','points','used','created_at','updated_at')
                            ->where('user_id',auth('api')->user()->id)
                            ->where('used',1)
                            ->get();
        $data = [
            'status' => true,
            'banner' => $banner,
            'total points' => $totalpoints,
            'all points' => $allpoints,
            'gained points' => $gainedpoints,
            'used points' => $usedpoints
        ];
        return response()->json($data);
    }
    public function pointMall(Request $request){
        $query = Product::query();
        $products = $query->select('id', 'name','price','mrp','star')
            ->with([
                'images' => function ($query) {
                    $query->select('product_id', 'image');
                }
            ])
            ->where('id', $request->id)
            ->where('completed_status',1)
            ->where('approved',1)
            ->get();
        return response()->json(['status' => true, 'products' => $products]);
    }
}
