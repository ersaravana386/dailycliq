<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Guide;
use Validator;

class GuideController extends Controller
{
    public function getGuide(Request $request){
        $guide = Guide::where('status',1)
                        ->get();
         return response()->json($guide);
    }
    
}
