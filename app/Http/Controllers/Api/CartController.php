<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Cart;
use Auth;
use DB;
use App\User;
use App\UserLog;

use Validator;

class CartController extends Controller
{
    public function cart(Request $request)
    {

        $rules = [
            'product' => 'required',
            'units' => 'required', 
        ];
        $messages = [
            'product.required' => "Product is required",
            'units.required' => "Unit is required",
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);

        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => false, 'errors' => $errors]);
        }

        try {
            $cart = Cart::where('user_id', Auth::user()->id)->where('product_id', $request->product)->first();
            if (!$cart) {
                $cart = new Cart();
                $cart->product_id = $request->product;
                $cart->user_id = Auth::user()->id;
                $cart->units = $cart->units + $request->units;
            }
            $cart->units = $request->units;
             $cart->save();
           
            $user = User::where('id',Auth::user()->id)
                        ->select('id','first_name')->first();
        
            $name = $user->first_name;
           
            $data_array[] = [
                'type' => 'ADD_TO_CART',
                'user_id' => Auth::user()->id,
                'activity' => $name . ' add the product to cart  on ' . date('Y/m/d H:i:s'),
                'created_at' => now(),
                'updated_at' => now()
            ];
            if(!empty($data_array)){
                try{
                    DB::beginTransaction();
                    UserLog::insert($data_array);
                    DB::commit();
                }catch (\Exception $e) {
                   DB::rollback();
                   $errors['couldnot_save'] = $e->getMessage();
                   return response()->json(['status' => false, 'errors' => $errors]);
               }     
            }
            return response()->json(['status' => true, 'message' => 'Added to cart succcessfully']);
        } catch (\Exception $e) {
            $errors['couldnot_save'] = $e->getMessage();
            return response()->json(['status' => false, 'errors' => $errors]);
        }
    } 

    public function getCart(Request $request)
    {
        $carts = Cart::select('id', 'product_id', 'units')->where('user_id', Auth::user()->id)->with(['product' => function ($q) {
            $q->select('id', 'name', 'description', 'price', 'mrp', 'wholesale', 'brand_id', 'other_brand','assured')
                ->with([
                    'brand', 
                    'images',
                    'flashsale' => function ($query) {
                        $query
                            ->where('active_status', 1)
                            ->where('approve', 1)
                            ->whereHas('flashsale', function ($sub) {
                                $sub->whereDate('starting_date', '<=', date('Y-m-d H:i:s'));
                                $sub->whereDate('ending_date', '>=', date('Y-m-d H:i:s'));
                            });
                    },
                    'deal' => function ($query) {
                        $query
                            ->where('approve', 1)
                            ->whereDate('start_date', date('Y-m-d'));
                    },
                    'store99' => function ($query) {
                        $query
                            ->where('active', 1);
                    },
                    'whishlist' => function ($query) {
                        $query->where('user_id', auth('api')->user()->id);
                    }
                ]);
        }])->get();
        return response()->json(['status' => true, 'data' => ['carts' => $carts],'neworder'=>1]);
    }

    public function deleteCart(Request $request)
    {
        $remove_all = $request->remove_all;
        if ($remove_all) {
            Cart::where('user_id', Auth::user()->id)->delete();

            $user = User::where('id',Auth::user()->id)
                        ->select('id','first_name')->first();

            $name = $user->first_name;

            $data_array[] = [
                'type' => 'REMOVE_FROM_CART',
                'user_id' => Auth::user()->id,
                'activity' => $name . ' removed the product from cart  on ' . date('Y/m/d H:i:s'),
                'created_at' => now(),
                'updated_at' => now()
                ];
            if(!empty($data_array)){
                try{
                    DB::beginTransaction();
                    UserLog::insert($data_array);
                    DB::commit();
                }catch (\Exception $e) {
                    DB::rollback();
                    $errors['couldnot_save'] = $e->getMessage();
                    return response()->json(['status' => false, 'errors' => $errors]);
                 }     
            }   
            $message = "Cart items removed successfully";
            return response()->json(['status' => true, 'message' => $message]);
        } elseif ($request->id) {
            Cart::where('user_id', Auth::user()->id)->where('id',$request->id)->delete();

            $user = User::where('id',Auth::user()->id)
                        ->select('id','first_name')->first();

            $name = $user->first_name;

            $data_array[] = [
                 'type' => 'REMOVE_FROM_CART',
                 'user_id' => Auth::user()->id,
                'activity' => $name . ' removed the product from cart  on ' . date('Y/m/d H:i:s'),
                 'created_at' => now(),
                 'updated_at' => now()
                ];
            if(!empty($data_array)){
              try{
                DB::beginTransaction();
                UserLog::insert($data_array);
                DB::commit();
            }catch (\Exception $e) {
                 DB::rollback();
                $errors['couldnot_save'] = $e->getMessage();
                 return response()->json(['status' => false, 'errors' => $errors]);
          }     
        }
        $message = "Cart item removed successfully";
        return response()->json(['status' => true, 'message' => $message]);
        }
      
    }
    public function updateCartquantity(Request $request){
           $quantity = $request->quantity;
           $id = $request->id;
           $query =  Cart::find($id)
                        ->where('user_id',auth('api')->user()->id)
                        ->first();
                            
            $aa=$query->units;
            if($query->units < $quantity)
            {
                return response()->json(['status' => 'Out of stock']);
            }
            else
            {
                $query->units = $query->units - $quantity;
                try{
                    DB::beginTransaction();
                    $query->save();
                    DB::commit();
                }catch (\Exception $e) {
                    DB::rollback();
                    $errors['couldnot_save'] = $e->getMessage();
                    return response()->json(['status' => false, 'errors' => $errors]);
                }
               
            }
           return response()->json(['status' => true, 'quantity' => $query]);
    }
}
