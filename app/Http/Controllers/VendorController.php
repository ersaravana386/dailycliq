<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Vendor;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendInvitationVendor;
use DB;

class VendorController extends Controller
{
    public function showVendors(Request $request){
        $search = $request->search;
        $query = Vendor::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('company_name', 'like', "%" . $search . "%");
                $sub->orWhere('email', 'like', "%" . $search . "%");
                $sub->orWhere('mobile', 'like', "%" . $search . "%");
            });
        }
        $query->orWhere('active', '1');
        $query->orderBy('created_at', 'desc');
        $vendors = $query->paginate(5);
        $data = [
            'vendors' => $vendors,
            'search' => $search,
        ];
        return view('admin.vendors.list',$data);
    }
    public function showNewVendors(){
        return view('admin.vendors.new');
    }
    public function addVendor(Request $request){
        $rules = [
            'company_name' => "required",
            'contact_person' =>"required",
            'address' =>"required",
            'mobile' => 'required|unique:vendors,mobile,NULL,id,deleted_at,NULL',
            'email' => 'required|unique:vendors,email,NULL,id,deleted_at,NULL',
            'trade_license_number' => 'required|unique:vendors,trade_license_number,NULL,id,deleted_at,NULL',
            'trade_license_document' => 'required',
            'gst_number' => 'required|unique:vendors,gst_number,NULL,id,deleted_at,NULL',
            'gst_document' => 'required'
        ];
        $messages = [
            'company_name.required' => "Company name is required",
            'contact_person.required' => "Contact person name is required",
            'address.required' =>"Address is required",
            'mobile.required' => "Mobile number is required",
            'mobile.unique' => "Mobile number already exists",
            'email.required' => "Email is required",
            'email.unique' => "Email already exists",
            'trade_license_number.required' => 'Trade license number is required',
            'trade_license_number.unique' => 'Trade license number already exists',
            'trade_license_document.required' => 'Trade license document is required',
            'gst_number.required' => 'GST number is required',
            'gst_number.unique' => 'GST number already exists',
            'gst_document.required' => 'GST document is required'
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
        //password generate
        $string = "";
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size = strlen($chars);
        for ($i = 0; $i < 6; $i++) {
            $string .= $chars[rand(0, $size - 1)];
        }
        $password = $string;

        //insert into table
        $vendor = new Vendor();
        $vendor->code           = time() . rand(111, 999);
        $vendor->company_name   = $request->company_name;
        $vendor->password       = bcrypt($password);
        $vendor->contact_person = $request->contact_person;
        $vendor->address        = $request->address;
        $vendor->mobile         = $request->mobile;
        $vendor->email          = $request->email;
        $vendor->phone          = $request->phone;
        $vendor->trade_license_number = $request->trade_license_number;
        if (request()->trade_license_document) {
            try{
                $imageName = time() . '.' . request()->trade_license_document->getClientOriginalExtension();
                request()->trade_license_document->move(public_path('images/vendors/trade_license_document/'), $imageName);
                $vendor->trade_license_document = 'images/vendors/trade_license_document/' . $imageName;
            }catch (\Exception $e) {
            }
        }
        $vendor->gst_number     = $request->gst_number;
        if (request()->gst_document) {
            try{
                $imageName = time() . '.' . request()->gst_document->getClientOriginalExtension();
                request()->gst_document->move(public_path('images/vendors/gst_document/'), $imageName);
                $vendor->gst_document = 'images/vendors/gst_document/' . $imageName;
            }catch (\Exception $e) {
            }
        }
        if (request()->image) {
            try{
                $imageName = time() . '.' . request()->image->getClientOriginalExtension();
                request()->image->move(public_path('images/vendors/profile/'), $imageName);
                $vendor->image = 'images/vendors/profile/' . $imageName;
            }catch (\Exception $e) {
            }
        }else{
            $vendor->image = 'images/vendors/profile/default.png';
        }
        try {
            $vendor->save();
            return response()->json(['status' => 1, 'message' => "New vendor added successfully"]);
        } catch (\Exception $e) {
            print_R($e->getMessage());
            $errors['couldnot_save'] = "Could not save profile";
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
    }
    public function deleteVendor(Request $request){
        $rules = [
            'vendorCodeDelete' => "required"
        ];
        $messages = [
            'vendorCodeDelete.required' => "Vendor code is required",
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            } 
            return response()->json(['status' => 0, 'errors' => $errors]);
        }

        $vendor = Vendor::where('code',$request->vendorCodeDelete)->first();
        if ($vendor) {
            try {
                $vendor->delete();
                return response()->json(['status' => 1, 'message' => "Vendor has been deleted!"]);
            } catch (\Exception $e) {
                $errors['couldnot_save'] = "Could not delete vendor";
                return response()->json(['status' => 0, 'errors' => $errors]);
            }
        }else{
            $errors['couldnot_save'] = "Could not delete vendor";
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
    }
    public function showEditVendor(Request $request){

        $vendor = Vendor::where('code','=',$request->code)->firstorfail();    
        $data = [ 'vendor' => $vendor];
        return view('admin.vendors.edit',$data);
    }
    public function updateVendor(Request $request){
        $vendor = Vendor::where('code','=',$request->code)->firstOrFail();
        $rules = [
            'company_name' => "required",
            'contact_person' =>"required",
            'address' =>"required",
            'mobile' => 'required|unique:vendors,mobile,{$request->code},code,deleted_at,NULL'.$vendor->id,
            'email' => 'required|unique:vendors,email,{$request->code},code,deleted_at,NULL'.$vendor->id,
            'trade_license_number' => 'required|unique:vendors,trade_license_number,{$request->code},code,deleted_at,NULL'.$vendor->id,
            'gst_number' => 'required|unique:vendors,gst_number,{$request->code},code,deleted_at,NULL'.$vendor->id,
        ];
        $messages = [
            'company_name.required' => "Company name is required",
            'contact_person.required' => "Contact person name is required",
            'address.required' =>"Address is required",
            'mobile.required' => "Mobile number is required",
            'mobile.unique' => "Mobile number already exists",
            'email.required' => "Email is required",
            'email.unique' => "Email already exists",
            'trade_license_number.required' => 'Trade license number is required',
            'gst_number.required' => 'GST number is required',
            'trade_license_number.unique' => 'Trade license number already exists',
            'gst_number.unique' => 'GST number already exists'
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }

        //insert into table
        $vendor->company_name   = $request->company_name;
        $vendor->contact_person = $request->contact_person;
        $vendor->address        = $request->address;
        $vendor->mobile         = $request->mobile;
        $vendor->email          = $request->email;
        $vendor->phone          = $request->phone;
        $vendor->trade_license_number = $request->trade_license_number;
        if (request()->trade_license_document) {
            try{
                $imageName = time() . '.' . request()->trade_license_document->getClientOriginalExtension();
                request()->trade_license_document->move(public_path('images/vendors/trade_license_document/'), $imageName);
                $vendor->trade_license_document = 'images/vendors/trade_license_document/' . $imageName;
            }catch (\Exception $e) {
            }
        }
        $vendor->gst_number     = $request->gst_number;
        if (request()->gst_document) {
            try{
                $imageName = time() . '.' . request()->gst_document->getClientOriginalExtension();
                request()->gst_document->move(public_path('images/vendors/gst_document/'), $imageName);
                $vendor->gst_document = 'images/vendors/gst_document/' . $imageName;
            }catch (\Exception $e) {
            }
        }
        if (request()->image) {
            try{
                $imageName = time() . '.' . request()->image->getClientOriginalExtension();
                request()->image->move(public_path('images/vendors/profile/'), $imageName);
                $vendor->image = 'images/vendors/profile/' . $imageName;
            }catch (\Exception $e) {
            }
        }
        try {
            $vendor->save();
            return response()->json(['status' => 1, 'message' => "Vendor updated successfully"]);
        } catch (\Exception $e) {
           // print_R($e->getMessage());
            $errors['couldnot_save'] = "Could not update profile";
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
    }
    public function blockVendor(Request $request){
        $rules = [
            'vendorCodeBlock' => "required"
        ];
        $messages = [
            'vendorCodeBlock.required' => "Vendor code is required",
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }

        $vendor = Vendor::where('code',$request->vendorCodeBlock)->first();
        ($vendor->active == 1) ?  $term = 'blocked' : $term = 'unblocked';

        if ($vendor) {
            try {
                $vendorUpdate = Vendor::where('code',$request->vendorCodeBlock)->update(['active'=> !$vendor->active]);
                return response()->json(['status' => 1, 'code' =>$vendor->code, 'active' => !$vendor->active, "name" => $vendor->name, 'message' => "Vendor ".$term." successfully!"]);
            } catch (\Exception $e) {
                print_R($e->getMessage());
                $errors['couldnot_save'] = "Couldn't ".$term." vendor";
                return response()->json(['status' => 0, 'errors' => $errors]);
            }
        }else{
            $errors['couldnot_save'] = "Could not perform"; 
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
    }
    public function sendInvite(Request $request){
        $vendor = Vendor::where('code',$request->code)->first();
        if($vendor){
            $token = rand(1001, 9999) . time() . rand(1001, 9999);
            $vendor->reset_token =  $token;
            try {
                $vendor->save();
                $resetLink = route("resetLink",['email'=>$vendor->email,'token'=>$token]);
                $data = array('resetLink' => $resetLink, 'name' => $vendor->contact_person, 'email' => $vendor->email);
                Mail::to($vendor->email)->send(new SendInvitationVendor($data));
                return response()->json(['status' => 1, 'message' => "Invitation sent successfully"]);
            } catch (\Exception $e) {
                print_R($e->getMessage());exit;
                return response()->json(['status' => 0, 'errors' => "Could not update user token" ]);
            }
        }else{
            return response()->json(['status' => 0, 'errors' => "Couldn't invited"]);
        }
    }
    public function sendOrdes(Request $request){
        $vendor = Vendor::where('code',$request->code)->first();
        if($vendor){
            $content ='';
            switch($request->status){
                case 1:
                    $content = "There is a new order from ".$requst->customer."<br>Order Reference Id :".$request->$ref."<br>";
                break;
                case 2:
                    $content = "Your Product ".$requst->product."<br>was Approved by DailyCliq<br>";
                break;
                case 3:
                    $content = "Your Product ".$requst->product."<br>was  Rejected by DailyCliq<br>";
                break;
                case 4:
                    $content = "Your Product ".$requst->product."<br>was  moved to Re-view please update missing thinks<br>";
                break;

            }
            try {
                $data = array('content' => $content, 'name' => $vendor->contact_person, 'email' => $vendor->email);
                Mail::to($vendor->email)->send(new SendOrders($data));
               // return response()->json(['status' => 1, 'message' => "Invitation sent successfully"]);
            } catch (\Exception $e) {
                print_r($e->getMessage());exit;
              //  return response()->json(['status' => 0, 'errors' => "Could not update user token" ]);
            }
        }else{
            return response()->json(['status' => 0, 'errors' => "Couldn't invited"]);
        }

    }
    public function approveVendor(Vendor $vendor, Request $request){
        if ($vendor) {
            $vendor->approved = 1;
            $vendor->save();
            return redirect()->route('showVendors');
        } else {
            return redirect()->route('dashboard');
        }
    }
}
