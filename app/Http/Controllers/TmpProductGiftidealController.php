<?php

namespace App\Http\Controllers;

use App\TmpProductGiftideal;
use Illuminate\Http\Request;

class TmpProductGiftidealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TmpProductGiftideal  $tmpProductGiftideal
     * @return \Illuminate\Http\Response
     */
    public function show(TmpProductGiftideal $tmpProductGiftideal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TmpProductGiftideal  $tmpProductGiftideal
     * @return \Illuminate\Http\Response
     */
    public function edit(TmpProductGiftideal $tmpProductGiftideal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TmpProductGiftideal  $tmpProductGiftideal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TmpProductGiftideal $tmpProductGiftideal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TmpProductGiftideal  $tmpProductGiftideal
     * @return \Illuminate\Http\Response
     */
    public function destroy(TmpProductGiftideal $tmpProductGiftideal)
    {
        //
    }
}
