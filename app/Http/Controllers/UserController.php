<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use DB;

class UserController extends Controller
{
    public function allUsersUserManagement(Request $request){
        $search=$request->search;
        $query=User::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('first_name', 'like', "%" . $search . "%");
                $sub->orWhere('email', 'like', "%" . $search . "%");
                $sub->orWhere('gender', 'like', "%" . $search . "%");
                $sub->orWhere('dob', 'like', "%" . $search . "%");
                $sub->orWhere('mobile', 'like', "%" . $search . "%");
            });
        }
        $query->orderBy('created_at', 'desc');
        $users = $query->paginate(10);
        $data = [
            'users' => $users,
            'search' => $search,
        ];
        return view('admin.usermanagement.userlist', $data);
    }
    public function changeStatusUserManagement(Request $request){
        $id=$request->id;
        $val=$request->val;
        if($id == "" || $val == ""){
            return response()->json(['status'=>false]);
        }else{
            $user=User::find($id);
            if($val == 1){
                $message= 'User Blocked Sucessfully';
            }else{
                $message= 'User Unblocked Sucessfully';
            }
            DB::beginTransaction();
            try{    
                $user->status=$val;
                $user->save();
                DB::Commit();
                return response()->json(['status'=>true,'message'=>$message]);
            }catch(\Exception $e){
                DB::rollback();
                throw $e;
                $error="Couldn't Save Changes"; 
                return response()->json(['status'=>false,'message'=>$error]);
            }

        }
    }

}
