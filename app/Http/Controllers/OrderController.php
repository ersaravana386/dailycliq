<?php

namespace App\Http\Controllers;
use App\Order;
use Illuminate\Http\Request;
use DB;
use App\Product;
use App\OrderProduct;
Use App\Vendor;
use App\Notification;

class OrderController extends Controller
{
    public function showVendorsOrder(Request $request){
        $search = $request->search; 
        $query = Vendor::query();
        $query->select('company_name','id');
        $query->withCount(['order as completed_orders'=>function($q){
            $q->select(
                DB::raw('count(CASE WHEN status = "1" THEN 1 END) AS "completed_orders"'),
            );
        }]);
        $query->withCount(['order as pending_orders'=>function($q){
            $q->select(
                DB::raw('count(CASE WHEN status = "2" THEN 1 END) as pending_orders'),
            );
        }]);
        $query->withCount(['order as refunded_orders'=>function($q){
            $q->select(
                DB::raw('count(CASE WHEN status = "3" THEN 1 END) as refunded_orders'),
            );
        }]);
        $query->withCount(['order as failed_orders'=>function($q){
            $q->select(
                DB::raw('count(CASE WHEN status = "4" THEN 1 END) as failed_orders'),
            );
        }]);
        $query->withCount(['order as cancelled_orders'=>function($q){
            $q->select(
                DB::raw('count(CASE WHEN status = "5" THEN 1 END) as cancelled_orders'),
            );
        }]);
        $query->groupBy('company_name','id');
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('company_name', 'like', "%" . $search . "%");
            });
        }
        $details = $query->paginate(10);
        // dd(DB::getQueryLog());exit;
        $data = [
            'details' => $details,
            'search' => $search,
        ];
        return view('admin.orders.vendorslist', $data);
    }
    public function showOrdersOrder($vendor,Request $request){
        $search=$request->search;
        $query=Order::query();
        $query->with('payment');
        $query->with('product');
        $query->with('user');
        $query->where('vendor_id',$vendor);
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('order_reference_number', 'like', "%" . $search . "%");      
                $sub->orWhere('order_at', 'like', "%" . $search . "%");   
                $sub->orWhere('paid_at', 'like', "%" . $search . "%");   
            });
        }
        $orders = $query->paginate(10);
        //print_r($orders); exit();
        $data = [
            'orders' => $orders,
            'search' => $search,
        ];
        return view('admin.orders.vendororderlist', $data);
    }
    public function orderChangeStatus(Request $request){
        $id=$request->id;
        $status= $request->status;
        $notification = new Notification;
        if(!empty($id) && !empty($status)){
        $order = Order::find($id); 
        $data=array('route'=>'order','vendor_id'=>$order->vendor_id);
         try {
             if($status !=5){
                 $order->status = $status;
                 $order->save();
                 $notification->added_by = Auth()->user()->id;
                 $notification->added_to=$order->vendor_id;
                 $notification->process="Order status changed by admin ".Auth()->User()->name.".";
                 $notification->link="abcdefghijklmop";
                 $notification->admin_link= serialize($data);
                 $notification->is_view=0;
                 $notification->save();
                 echo "<p class='alert alert-success mg-b-4'>Order details updated successfully</p>";
             }else{
                 $query = Order::query();
                 $query->with('product');
                 $query->where('id',$id);
                 $products = $query->get();
                 $arr[]='';
                 if(!empty($products[0]->product)){
                     foreach($products as $key=>$list){
                         try {
                             DB::beginTransaction();
                             $arr = ['stock_unit'=> $list->product[$key]['unit']+$list->product[$key]->productlist['stock_unit']];
                             Product::where(array('id'=>$list->product[$key]->productlist['id']))->update($arr);
                             DB::commit();
                         } catch (\Exception $e) {
                             // print_r($e->getMessage());exit;
                             DB::rollback();
                             echo "<p class='alert alert-danger mg-b-4'>Unable to chanage order details</p>";
                         }    
                     }
                     $order->status = $status;
                     $order->save();
                     $notification->added_by = Auth()->user()->id;
                     $notification->added_to=$order->vendor_id;
                     $notification->process="Order status changed by admin ".Auth()->User()->name.".";
                     $notification->link="abcdefghijklmop";
                     $notification->admin_link= serialize($data);
                     $notification->is_view=0;
                     $notification->save();
                     echo "<p class='alert alert-success mg-b-4'>Order details updated successfully</p>"; 
                 }
             }
         } catch (\Exception $e) {
             //print_r($e->getMessage());exit;
             DB::rollback();
            echo "<p class='alert alert-danger mg-b-4'>Unable to chanage order details</p>";
         }
        }else{
            echo "<p class='alert alert-danger mg-b-4'>Unable to process your request</p>";
        }

    }
    public function viewProductsOrder($orderid,Request $request){
        $search=$request->search;
        if($orderid){
            $query = OrderProduct::query();
            $query->with('productlist');
            $query->where('order_id', $orderid);
            $query->orderBy('created_at', 'desc');
            if ($search) {
                $query->where(function ($sub) use ($search) {
                    $sub->orWhere('brand', 'like', "%" . $search . "%");
                    $sub->orWhere('name', 'like', "%" . $search . "%");
                });
            }
            $products = $query->get();
            // print_r($products); exit();
            $data = [
                'products' => $products,
                'search'=>$search,
            ];
            return view('admin.orders.orderedproductslist', $data);
        }else{
            return redirect()->back();
        }
    }
    public function SortOrder($vendorid,Request $request){

        $name =  $request->customername;
        $search =  $request->search;
        
        $query = Order::query();
        $query->with(['payment', 'vendor','user']);
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('name', 'like', "%" . $search . "%");
                $sub->orWhere('order_reference_number', 'like', "%" . $search . "%");
                
            });
        }
        $query->with(['product' => function($q){
            $q->sum('unit');
        }]);
        if($request->referancenumber!=''){
                $query->where('orders.order_reference_number', $request->referancenumber);
            }
        if($name!=''){
            $query->whereHas('user', function ($query)  use($name){
                $query->where('name', 'like', "%" . $name . "%");
            });
        }
        if($request->orderstatus!='Order Status'){
            $query->where('orders.status', $request->orderstatus);
        }
        $payment = $request->paymenttype;
        if($payment!='Payment Type'){
            
            $query->whereHas('payment', function ($que)  use($payment){
                $que->where('payment_type', 'like', "%" . $payment . "%");
            });  
        }
        $from  = date('Y-m-d',strtotime($request->orderfrom));
        $to    = date('Y-m-d',strtotime($request->orderto));
        //print_r($from.' '.$to); exit();
        if(($from!='1970-01-01') && ($to!='1970-01-01')){
            if($from > $to ){
                $tmp  = $to;
                $to   = $from;
                $from = $to;
            }
            $from =$from.' 00:00:00';
            $to =$to.' 23:59:59';
            $query->WhereBetween('order_at', [$from, $to]);
        }
        $query->where('vendor_id',$vendorid);
        $query->orderBy('created_at', 'desc');
        $orders = $query->paginate(10);
        $data = [
            'orders' => $orders,
            'search' => $search,
        ];
        return view('admin.orders.vendororderlist', $data);
    }
}