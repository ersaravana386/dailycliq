<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;


use App\Admin;

class AdminController extends Controller
{
    public function allAdminsAdminManagement(Request $request){
        $search=$request->search;
        $query=Admin::query();
        $query->where('isAdmin','!=',1);
        if($search){
            $query->where(function($q) use ($search){
                $q->orWhere('name', 'like', "%" . $search . "%");
            });
        }
        $query->orderBy('created_at');
        $admins=$query->paginate(10);
        $data=[
            'admins'=>$admins,
            'search' => $search,
        ];
        return view('admin.admins.list',$data);

    }
    public function addNewAdminAdminManagement(request $request){
        return view('admin.admins.new');
    }

    public function deleteAdminAdminManagement(Request $request){
        if($request->id == ""){
            return response()->json(['status'=>0]);
        }else{
         DB::beginTransaction();
         try{
            $admin=Admin::find($request->id);
            $admin->delete();
            DB::commit();
            return response()->json(['status'=>1]);
         }catch(\Exception $e){
             DB::rollback();
             print_r($e->getMessage());  
             throw $e;
             return response()->json(['status'=>0,'message'=>"error occured"]);
         }    
        }
    }

    public function blockAdminAdminManagement(Request $request){
        if($request->id !="" && $request->val !=""){
            DB::beginTransaction();
         try{
            $admin=Admin::find($request->id);
            $admin->status=$request->val;
            $admin->save();
            DB::commit();
            return response()->json(['status'=>1]);
         }catch(\Exception $e){
             DB::rollback();
             print_r($e->getMessage());  
             throw $e;
             return response()->json(['status'=>0,'message'=>"error occured"]);
         }    
        }else{
            return response()->json(['status'=>0,'message'=>'Unable To Make Any Change']);
        }
    }

    public function newAdminAdminManagement(Request $request){
        $rules = [
            'name'      => 'required',
            'email'     => 'required|email|unique:admins',
            'mobile'    => 'required|unique:admins',
            'status'    => 'required',
            'pass'      => 'required',
            'image'           => 'mimes:jpeg,jpg,png|max:10000|nullable',
        ];
        $messages = [
            'name.required'    => 'Admin name is required',
            'email.required'   => 'Admin email is required',
            'email.unique'   => 'Email already in use',
            'mobile.required'  => 'Admin mobile is required',
            'mobile.unique'  => 'Mobile number already in use',
            'status.required'  => 'Admin status is required',
            'pass'             => 'Password is required',
            'image.mimes' => "Invalid file format only .jpeg,jpg,png files are allowed",
            'image.max' => "Maxinum allowed size is 2mb",
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'message' => $errors]);
            // return response()->json(['status' => 0]);
        }
        if($request->image){
            $imageName = time() . '.' . $request->image->getClientOriginalExtension();
            request()->image->move(public_path('images/admins/' . $request->id . '/'), $imageName);
            DB::beginTransaction();      
            try{ 
                $password=bcrypt($request->pass);
                $data=[
                        'name'=>$request->name,
                        'email'=>$request->email,
                        'mobile'=>$request->mobile,
                        'password'=>$password,
                        'image'=>'images/admins/' . $request->id . '/' . $imageName,
                        'status'=>$request->status,
                        'isAdmin'=>0,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'), 
                ]; 
                Admin::insert($data);
                DB::commit();
                return response()->json(['status' => 1]);
            }catch(\Exception $e){
                DB::rollback();
                print_r($e->getMessage());  
                $data=$e->getMessage();
                throw $e;
                return response()->json(['status' => 0,'message'=>$data]);
            }
        }
        else{
            DB::beginTransaction();      
            try{ 
                $password=bcrypt($request->pass);
                $data=[
                        'name'=>$request->name,
                        'email'=>$request->email,
                        'mobile'=>$request->mobile,
                        'password'=>$password,
                        'status'=>$request->status,
                        'isAdmin'=>0,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'), 
                ]; 
                Admin::insert($data);
                DB::commit();
                return response()->json(['status' => 1]);
            }catch(\Exception $e){
                DB::rollback();
                print_r($e->getMessage());  
                $data=$e->getMessage();    
                throw $e;
                return response()->json(['status' => 0,'message'=>$data]);
            }
        }     
    }

    public function editAdminAdminManagement($id,Request $request){
        $query=Admin::query();
        $query->where('id',$id);
        $admins=$query->get();
        $data=[
            'admins'=>$admins,
        ];
        return view('admin.admins.edit',$data);

    }
    public function updateAdminAdminManagement(Request $request){
        $rules = [
            'name'      => 'required',
            'email'     => 'required|email|unique:admins,email,'.$request->admin_id,
            'mobile'    => 'required|unique:admins,mobile,'.$request->admin_id,
            'status'    => 'required',
            'pass'      => 'nullable',
            'image'     => 'mimes:jpeg,jpg,png|max:10000|nullable',
        ];
        $messages = [
            'name.required'    => 'Admin name is required',
            'email.required'   => 'Admin email is required',
            'email.unique'   => 'Email already in use',
            'mobile.required'  => 'Admin mobile is required',
            'mobile.unique'   => 'Mobile number already in use',
            'status.required'  => 'Admin status is required',   
            'image.mimes' => "Invalid file format only .jpeg,jpg,png files are allowed",
            'image.max' => "Maxinum allowed size is 2mb",
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'message' => $errors]);
            // return response()->json(['status' => 0]);
        }
        if($request->image){
            $imageName = time() . '.' . $request->image->getClientOriginalExtension();
            request()->image->move(public_path('images/admins/' . $request->id . '/'), $imageName);
            DB::beginTransaction();      
            try{ 
                if($request->pass != ""){
                    $password=bcrypt($request->pass);
                    $data=[
                        'name'=>$request->name,
                        'email'=>$request->email,
                        'mobile'=>$request->mobile,
                        'password'=>$password,
                        'image'=>'images/admins/' . $request->id . '/' . $imageName,
                        'status'=>$request->status,
                        'isAdmin'=>0,
                        'updated_at'=>date('Y-m-d H:i:s'), 
                    ]; 
                }else{
                    $data=[
                        'name'=>$request->name,
                        'email'=>$request->email,
                        'mobile'=>$request->mobile,
                        'image'=>'images/admins/' . $request->id . '/' . $imageName,
                        'status'=>$request->status,
                        'isAdmin'=>0,
                        'updated_at'=>date('Y-m-d H:i:s'), 
                    ]; 
                }  
                $data= Admin::where('id',$request->admin_id)->update($data);
                DB::commit();
                return response()->json(['status' => 1]);
            }catch(\Exception $e){
                DB::rollback();
                print_r($e->getMessage());    
                throw $e;
                return response()->json(['status' => 0]);
            }
        }
        else{
            DB::beginTransaction();      
            try{ 
                if($request->pass != ""){
                    $password=bcrypt($request->pass);
                    $data=[
                        'name'=>$request->name,
                        'email'=>$request->email,
                        'mobile'=>$request->mobile,
                        'password'=>$password,
                        'status'=>$request->status,
                        'isAdmin'=>0,
                        'updated_at'=>date('Y-m-d H:i:s'), 
                    ]; 
                }else{
                    $data=[
                        'name'=>$request->name,
                        'email'=>$request->email,
                        'mobile'=>$request->mobile,
                        'status'=>$request->status,
                        'isAdmin'=>0,
                        'updated_at'=>date('Y-m-d H:i:s'), 
                    ]; 
                }  
                $data= Admin::where('id',$request->admin_id)->update($data);
                DB::commit();
                return response()->json(['status' => 1]);
            }catch(\Exception $e){
                DB::rollback();
                $data=$e->getMessage();    
                throw $e;
                return response()->json(['status' => 0,'message'=>$data]);
            }
        }     
    }    
}
