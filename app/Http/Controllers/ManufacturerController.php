<?php

namespace App\Http\Controllers;

use App\Manufacturer;
use Illuminate\Http\Request;
use Validator;

class ManufacturerController extends Controller
{
    public function showManufacturers(Request $request)
    {
        $search = $request->search;
        $query = Manufacturer::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('name', 'like', "%" . $search . "%");
            });
        }
        $query->orderBy('created_at', 'desc');
        $manufacurers = $query->paginate(10);
        $data = [
            'manufacturers' => $manufacurers,
            'search' => $search,
        ];
        return view('admin.manufacturers.list', $data);
    }

    public function showNewManufacturer(Request $request)
    {
        $data = [];
        return view('admin.manufacturers.new', $data);
    }

    public function newManufacturer(Request $request)
    {
        $rules = [
            'name' => 'required',
            'image' => 'mimes:jpeg,jpg,png|max:20000',
        ];
        $messages = [
            'name.required' => "Manufacturer name is required",
            'image.mimes' => "Invalid file format only .jpeg,jpg,png files are allowed",
            'image.max' => "Maxinum allowed size is 2mb",
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);

        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }

        $manufacturer = new Manufacturer();
        $manufacturer->name = $request->name;
        try {
            if (request()->image) {
                $imageName = time() . '.' . request()->image->getClientOriginalExtension();
                request()->image->move(public_path('images/manufacturers/'), $imageName);
                $manufacturer->image = 'images/manufacturers/' . $imageName;
            }
            $manufacturer->save();
            return response()->json(['status' => true, 'message' => 'Manufacturer added successfully']);
        } catch (\Exception $e) {
            // print_r($e->getMessage());
            return response()->json(['status' => false, 'errors' => ['coulld_not_save' => 'Manufacturer details could not save successfully']]);
        }

    }

    public function updateManufacturer($id = null, Request $request)
    {
        $manufacturer = Manufacturer::where('id', $id)->first();
        if (!$manufacturer) {
            return response()->json(['status' => false, 'errors' => ['coulld_not_save' => 'Manufacturer details could not save successfully']]);
        }
        $rules = [
            'name' => 'required',
            'image' => 'mimes:jpeg,jpg,png|max:20000',
        ];
        $messages = [
            'name.required' => "Manufacturer name is required",
            'image.mimes' => "Invalid file format only .jpeg,jpg,png files are allowed",
            'image.max' => "Maxinum allowed size is 2mb",
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);

        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }

        $manufacturer->name = $request->name;
        try {
            if (request()->image) {
                $imageName = time() . '.' . request()->image->getClientOriginalExtension();
                request()->image->move(public_path('images/manufacturers/'), $imageName);
                $manufacturer->image = 'images/manufacturers/' . $imageName;
            }
            $manufacturer->save();
            return response()->json(['status' => true, 'message' => 'Manufacturer updated successfully']);
        } catch (\Exception $e) {
            // print_r($e->getMessage());
            return response()->json(['status' => false, 'errors' => ['coulld_not_save' => 'Manufacturer details could not update successfully']]);
        }
    }

    public function editManufacturer($id = null, Request $request)
    {
        $manufacturer = Manufacturer::where('id', $id)->first();
        if (!$manufacturer) {
            return redirect()->route('showManufacturers');
        }
        $data = [
            'manufacturer' => $manufacturer,
        ];
        return view('admin.manufacturers.edit', $data);
    }

}
