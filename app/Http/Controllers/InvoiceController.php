<?php

namespace App\Http\Controllers;

use App\Invoice;
use Illuminate\Http\Request;
use Auth;
use DB;
use App\Order;
use App\OrderProduct;
use App\Product;


class InvoiceController extends Controller
{
    public function showInvoice(Request $request){
      
        if($request->order){
            
            $query = OrderProduct::query();
            $query->with('productlist');
            $query->with('order','order.invoice');
            $query->where('order_id',$request->order);
            $product = $query->get();
            if(!empty($product[0]->productlist)){
                $data =[
                    'product' =>$product,
                    'active'  =>array('orders'),
                ];
            }else{
                return redirect()->route('showOrder');
            }
            
            return view('vendors/order/invoice',$data);
        }else{
            return redirect()->route('showOrder');
        }

    }
}
