<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;
use Validator;

class BrandController extends Controller
{
    public function showBrands(Request $request)
    {
        $search = $request->search;
        $query = Brand::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('name', 'like', "%" . $search . "%");
            });
        }
        $query->orderBy('created_at', 'desc');
        $brands = $query->paginate(10);
        $data = [
            'brands' => $brands,
            'search' => $search,
        ];
        return view('admin.brands.list', $data);
    }

    public function showNewBrand(Request $request)
    {
        $data = [];
        return view('admin.brands.new', $data);
    }

    public function newBrand(Request $request)
    {
        $rules = [
            'name' => 'required',
            'featured'=>'required',
            'image' => 'mimes:jpeg,jpg,png|max:20000',
        ];
        $messages = [
            'name.required' => "Brand name is required",
            'featured.required'=>'This field is required',
            'image.mimes' => "Invalid file format only .jpeg,jpg,png files are allowed",
            'image.max' => "Maxinum allowed size is 2mb",
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);

        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }

        $brand = new Brand();
        $brand->name = $request->name;
        $brand->featured = $request->featured;
        try {
            if (request()->image) {
                $imageName = time() . '.' . request()->image->getClientOriginalExtension();
                request()->image->move(public_path('images/brands/'), $imageName);
                $brand->logo = 'images/brands/' . $imageName;
            }
            $brand->save();
            return response()->json(['status' => true, 'message' => 'Brand added successfully']);
        } catch (\Exception $e) {
            // print_r($e->getMessage());
            throw $e;
            return response()->json(['status' => false, 'errors' => ['coulld_not_save' => 'Brand details could not save successfully']]);
        }

    }

    public function updateBrand($id = null, Request $request)
    {
        $brand = Brand::where('id', $id)->first();
        if (!$brand) {
            return response()->json(['status' => false, 'errors' => ['coulld_not_save' => 'Brand details could not update successfully']]);
        }
        $rules = [
            'name' => 'required',
            'featured'=>'required',
            'image' => 'mimes:jpeg,jpg,png|max:20000',
        ];
        $messages = [
            'name.required' => "Brand name is required",
            'featured.required'=>'This field is required',
            'image.mimes' => "Invalid file format only .jpeg,jpg,png files are allowed",
            'image.max' => "Maxinum allowed size is 2mb",
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);

        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }

        $brand->name = $request->name;
        $brand->featured = $request->featured;
        try {
            if (request()->image) {
                $imageName = time() . '.' . request()->image->getClientOriginalExtension();
                request()->image->move(public_path('images/brands/'), $imageName);
                $brand->logo = 'images/brands/' . $imageName;
            }
            $brand->save();
            return response()->json(['status' => true, 'message' => 'Brand updated successfully']);
        } catch (\Exception $e) {
            // print_r($e->getMessage());
            throw $e;
            return response()->json(['status' => false, 'errors' => ['coulld_not_save' => 'Brand details could not update successfully']]);
        }
    }

    public function editBrand($id = null, Request $request)
    {
        $brand = Brand::where('id', $id)->first();
        if (!$brand) {
            return redirect()->route('showBrands');
        }
        $data = [
            'brand' => $brand,
        ];
        return view('admin.brands.edit', $data);
    }


    public function searchBrandAdmin(Request $request){
        $items = Brand::select('id','name')->where('name','like',"%".$request->q."%")->limit(20)->get();
        if(count($items) <= 0){
            $items[] = [
                'id'=> "other",
                'text'=> "Other"
            ];
        }else{
            foreach($items as $item){
                $items[] = [
                    'id'=> $item->id,
                    'text'=> $item->name
                ];
            }
        }
        return response()->json(['items'=>$items]);
    }

    public function searchBrandByIdAdmin(Request $request){
        $item = Brand::select('name as id','name as text')->where('id',$request->id)->first();
        if($item){
            return response()->json(['item'=>$item, 'status'=>true]);
        }else{
            return response()->json(['status'=>false]);
        }        
    }
    
    public function brandAddFeatured(Request $request){
        $brand = Brand::where('id', $request->id)->first();
        if (!$brand) {
            return response()->json(['status' => false, 'errors' => ['coulld_not_save' => 'Brand details could not update successfully']]);
        }else{
            $brand->featured = $request->featured;
            $brand->save();
            return response()->json(['status' => true]);
        }
    }
    
}
