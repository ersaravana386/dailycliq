<?php

namespace App\Http\Controllers\Vendor;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Vendor;

class ProfileController extends Controller
{
    public function showVendorProfile(Request $request)
    {
        $vendor = Vendor::where('id', '=', Auth::user()->id)->firstOrFail();
        $data = ['vendor' => $vendor];
        return view('vendors.profile', $data);
    }
    public function updateVendorProfile(Request $request)
    {
        $rules = [
            'company_name' => "required",
            'contact_person' => "required",
            'address' => "required",
            'pin' => "required",
            'mobile' => 'required|unique:vendors,mobile,{$request->code},code,deleted_at,NULL',
            'email' => 'required|unique:vendors,email,{$request->code},code,deleted_at,NULL',
            'trade_license_number' => 'required|unique:vendors,trade_license_number,{$request->code},code,deleted_at,NULL',
            'gst_number' => 'required|unique:vendors,gst_number,{$request->code},code,deleted_at,NULL',
        ];
        $messages = [
            'company_name.required' => "Company name is required",
            'contact_person.required' => "Contact person name is required",
            'address.required' => "Address is required",
            'pin.required' => "Pin code is required",
            'mobile.required' => "Mobile number is required",
            'mobile.unique' => "Mobile number already exists",
            'email.required' => "Email is required",
            'email.unique' => "Email already exists",
            'trade_license_number.required' => 'Trade license number isrequired',
            'gst_number.required' => 'GST number is required',
            'trade_license_number.unique' => 'Trade license number already exists',
            'gst_number.unique' => 'GST number already exists'
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }

        //insert into table
        $vendor = Vendor::where('code', '=', $request->code)->firstOrFail();
        $vendor->company_name   = $request->company_name;
        $vendor->contact_person = $request->contact_person;
        $vendor->address        = $request->address;
        $vendor->pin            = $request->pin;
        $vendor->mobile         = $request->mobile;
        $vendor->email          = $request->email;
        $vendor->phone          = $request->phone;
        $vendor->trade_license_number = $request->trade_license_number;
        if (request()->trade_license_document) {
            try {
                $imageName = time() . '.' . request()->trade_license_document->getClientOriginalExtension();
                request()->trade_license_document->move(public_path('images/vendors/trade_license_document/'), $imageName);
                $vendor->trade_license_document = 'images/vendors/trade_license_document/' . $imageName;
            } catch (\Exception $e) {
            }
        }
        $vendor->gst_number     = $request->gst_number;
        if (request()->gst_document) {
            try {
                $imageName = time() . '.' . request()->gst_document->getClientOriginalExtension();
                request()->gst_document->move(public_path('images/vendors/gst_document/'), $imageName);
                $vendor->gst_document = 'images/vendors/gst_document/' . $imageName;
            } catch (\Exception $e) {
            }
        }
        if (request()->image) {
            try {
                $imageName = time() . '.' . request()->image->getClientOriginalExtension();
                request()->image->move(public_path('images/vendors/profile/'), $imageName);
                $vendor->image = 'images/vendors/profile/' . $imageName;
            } catch (\Exception $e) {
            }
        }
        try {
            $vendor->save();
            return response()->json(['status' => 1, 'message' => "Profile updated successfully"]);
        } catch (\Exception $e) {
            // print_R($e->getMessage());
            $errors['couldnot_save'] = "Could not update profile";
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
    }
}
