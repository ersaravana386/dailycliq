<?php

namespace App\Http\Controllers\Vendor;

use App\Brand;
use App\Category;
use App\ColorClass;
use App\Order;
use App\Http\Controllers\Controller;
use App\LengthClass;
use App\Product;
use App\OrderProduct;
use App\WeightClass;
use App\Inventory;
use Auth;
use DB;
use Illuminate\Http\Request;
use Validator;
use Crypt;

class ReportController extends Controller
{
    public function salesReport(Request $request){
        $user     = Auth::user()->id;
        $datalist = $request;
        $search   = $request->search;
        $category = Category::all();
        $brand    = Brand::all();
        DB::enableQueryLog();
        $query    = OrderProduct::query();
        $query->with(['productlist','productlist.brand','order','order.user','order.payment']);
        if($datalist->customername){
            $query->whereHas('order.user', function ($q)  use($datalist){
                $q->where('name', 'like', "%" . $datalist->customername . "%");
            });
        }
        $query->whereHas('order',function($q) use($datalist){
            $q->where('vendor_id','=', Auth::user()->id);
            if($datalist->referancenumber){
                $q->where('order_reference_number',$datalist->referancenumber);
            }
            if(($datalist->orderstatus!='Order Status') && ($datalist->orderstatus!='')){
                $q->where('status',$datalist->orderstatus);
            }
            $from  = date('Y-m-d',strtotime($datalist->orderfrom));
            $to    = date('Y-m-d',strtotime($datalist->orderto));
            if(($from!='1970-01-01' ) && ($to!='1970-01-01')){
                if($from > $to ){
                    $tmp  = $to;
                    $to   = $from;
                    $from = $to;
                }
                $from =$from.' 00:00:00';
                $to =$to.' 23:59:59';
                $q->WhereBetween('order_at', [$from, $to]);
            }
         
        });
        if(($datalist->paymenttype!='Payment Type') && ($datalist->paymenttype!='')){
            $query->whereHas('order.payment',function($q) use($datalist){
                $q->where('payment_type', 'like', "%" . $datalist->paymenttype . "%");
               });
        }
        if(($datalist->categoryselected!='Category' && ($datalist->categoryselected!=''))){
            $query->whereHas('productlist.category',function($q) use($datalist){
                $q->where('id',$datalist->categoryselected );
               });
        }
        $products =$query->paginate(10);
        $query = DB::getQueryLog();
       // print_r($query); exit();
       // print_r($products); exit();
        $data = [
            'referancenumber' =>$datalist->referancenumber,
            'customername' =>$datalist->customername,
            'orderstatus' =>$datalist->orderstatus,
            'paymenttype' =>$datalist->paymenttype,
            'categoryselected' =>$datalist->categoryselected,
            'orderfrom' =>$datalist->orderfrom,
            'orderto' =>$datalist->orderto,
            'category' => $category,
            'products' => $products,
            'search' => $search,
            'active' =>array('reports','sales'),
        ];
        return view('vendors.report.list', $data);
    }
    public function stockReport(Request $request){

       
        $query = Inventory::query();
        $query->with('product');
        if($request->process!=''){
          $query->where('process_id',$request->process);
        }
        $from  = date('Y-m-d',strtotime($request->stockfrom));
        $to    = date('Y-m-d',strtotime($request->stockto));
        if(($from!='1970-01-01' ) && ($to!='1970-01-01')){
            if($from > $to ){
                $tmp  = $to;
                $to   = $from;
                $from = $to;
            }
            $from =$from.' 00:00:00';
            $to =$to.' 23:59:59';
            $query->WhereBetween('created_at', [$from, $to]);
        }
        $query->where('vendor_id',  Auth::user()->id);
        $inventory = $query->paginate(10);
        //print_r($inventory); exit();
        $data = [
            'process'=>$request->process,
            'stockfrom'=>$request->stockfrom,
            'stockto'=>$request->stockto,
            'stock' => $inventory,
            'active' =>array('reports','stock'),
        ];
        return view('vendors.report.stock', $data);

    }
    public function orderReport(Request $request){
        

        $datalist = $request;
        $name =  $request->customername;
        $search =  $request->search;
        $category = Category::all();
        $brand    = Brand::all();
        $query = Order::query();
        $query->with(['payment', 'vendor','user']);
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('name', 'like', "%" . $search . "%");
                $sub->orWhere('order_reference_number', 'like', "%" . $search . "%");
                
            });
        }
        $query->with(['product' => function($q){
            
            return $q->sum('unit');
        }]);
        if($request->referancenumber!=''){
                $query->where('orders.order_reference_number', $request->referancenumber);
            }
        if($name!=''){
            $query->whereHas('user', function ($query)  use($name){
                $query->where('name', 'like', "%" . $name . "%");
            });
        }
        if(($datalist->orderstatus!='Order Status') && ($datalist->orderstatus!='')){
            $query->where('orders.status', $request->orderstatus);
        }
        $payment = $request->paymenttype;
        if(($datalist->paymenttype!='Payment Type') && ($datalist->paymenttype!='')){
            
            $query->whereHas('payment', function ($que)  use($payment){
                $que->where('payment_type', 'like', "%" . $payment . "%");
            });  
        }
        $from  = date('Y-m-d',strtotime($datalist->orderfrom));
        $to    = date('Y-m-d',strtotime($datalist->orderto));
        if(($from!='1970-01-01' ) && ($to!='1970-01-01')){
            if($from > $to ){
                $tmp  = $to;
                $to   = $from;
                $from = $to;
            }
            $from =$from.' 00:00:00';
            $to =$to.' 23:59:59';
            $q->WhereBetween('order_at', [$from, $to]);
        }
        $query->where('vendor_id',  Auth::user()->id);
        $query->orderBy('created_at', 'desc');
        $products = $query->paginate(10);
        //print_r($products); exit();
        $data = [
            'referancenumber' =>$datalist->referancenumber,
            'customername' =>$datalist->customername,
            'orderstatus' =>$datalist->orderstatus,
            'paymenttype' =>$datalist->paymenttype,
            'categoryselected' =>$datalist->categoryselected,
            'orderfrom' =>$datalist->orderfrom,
            'orderto' =>$datalist->orderto,
            'category' => $category,
            'products' => $products,
            'search' => $search,
            'active' =>array('reports','order'),
        ];
        return view('vendors.report.order', $data);
    }
}
