<?php

namespace App\Http\Controllers\Vendor;
use App\Brand;
use App\Category;
use App\ColorClass;
use App\Http\Controllers\Controller;
use App\Product;
use App\OrderProduct;
use App\Order;
use App\Notification;
use App\OrderLog;
use Auth;
use DB;
use Illuminate\Http\Request;
use Validator;
use Crypt;

class OrderController extends Controller
{
   public function showOrder(Request $request){
         
        $search =$request->search;
        $query = Order::query();
        $query->with('payment');
        $query->with('vendor');
        $query->with(['product' => function($query){
            $query->sum('unit');
         }]);
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('name', 'like', "%" . $search . "%");
                $sub->orWhere('order_reference_number', 'like', "%" . $search . "%");
               
            });
        }
        //$query->where('completed_status', '1');
        $query->where('vendor_id',  Auth::user()->id);
        $query->orderBy('created_at', 'desc');
        $orders = $query->paginate(10);
        //print_r($orders); exit();
        $data = [
            'orders' => $orders,
            'search' => $search,
            'active' =>array('orders'),
        ];
        return view('vendors.order.list', $data);

   }
   public function orderChangeStatus(Request $request){

       $id=$request->id;
       $status= $request->status;
       if(!empty($id) && !empty($status)){
        $order = Order::find($id);
        try {
            if($status !=5){
                $order->status = $status;
                $order->save();
                switch($status){
                    case 1:$process ='Complete';
                    break;
                    case 2:$process ='Pending';
                    break;
                    case 3:$process ='Refunded';
                    break;
                    case 4:$process = 'Failed';
                    break;
                }
                  // Notification for vendor//
                  $date =date('Y-m-d h:i:s');
                  $notification =['added_by'=> Auth::user()->id,'added_to'=> Auth::user()->id,'is_view'=>0,'process'=>'You have change order status of Order No '.$order->order_reference_number .' to '.$process,'link'=>'showOrder','created_at'=>$date];
                  Notification::insert($notification);
                  //Notification for vendor end here //
                  // Order log//
                  DB::enableQueryLog();
                  $date =date('Y-m-d h:i:s');
                  $order =['added_type'=>'Vendor','order_id'=>$order->id,'added_by'=> Auth::user()->id,'type'=>'ORDER_STATUS_CHANGE','activity'=>'Change order status of Order No '.$order->order_reference_number .' to '.$process,'created_at'=>$date];
                  OrderLog::insert($order);
                  $query = DB::getQueryLog();
                  
                  //Order log//
                echo "<p class='alert alert-success mg-b-4'>Order details updated successfully</p>";
            }else{
                $query = Order::query();
                $query->with('product');
                $query->where('id',$id);
                $products = $query->get();
                $arr[]='';
                if(!empty($products[0]->product)){
                    foreach($products as $key=>$list){
                        try {
                            DB::beginTransaction();
                            $arr = ['stock_unit'=> $list->product[$key]['unit']+$list->product[$key]->productlist['stock_unit']];
                            Product::where(array('id'=>$list->product[$key]->productlist['id']))->update($arr);
                            // Notification for vendor//
                            $date =date('Y-m-d h:i:s');
                            $notification =['added_by'=> Auth::user()->id,'added_to'=> Auth::user()->id,'is_view'=>0,'process'=>'You have change order status of Order No '.$order->order_reference_number.' to Canceled','link'=>'showOrder','created_at'=>$date];
                            Notification::insert($notification);
                            //Notification for vendor end here //
                            // Order log//
                            $date =date('Y-m-d h:i:s');
                            $order =['added_type'=>'Vendor','order_id'=>$order->id,'added_by'=> Auth::user()->id,'type'=>'ORDER_STATUS_CHANGE','activity'=>'Change order status of Order No '.$order->order_reference_number .' to  Canceled','created_at'=>$date];
                            OrderLog::insert($order);
                            //Order log//
                            DB::commit();
                        } catch (\Exception $e) {
                            // print_r($e->getMessage());exit;
                            DB::rollback();
                            echo "<p class='alert alert-danger mg-b-4'>Unable to chanage order details</p>";
                        }    
                    }
                    $order->status = $status;
                    $order->save();
                    echo "<p class='alert alert-success mg-b-4'>Order details updated successfully</p>"; 
                }
            }
        } catch (\Exception $e) {
            //print_r($e->getMessage());exit;
            DB::rollback();
           echo "<p class='alert alert-danger mg-b-4'>Unable to chanage order details</p>";
        }
       }else{
           echo "<p class='alert alert-danger mg-b-4'>Unable to process your request</p>";
       }

   }
   public function showOrderedProduct(Request $request){
    
        if($request->order){

            $query = OrderProduct::query();
            $query->with('productlist');
            $query->where('order_id', $request->order);
            $query->orderBy('created_at', 'desc');
            $products = $query->get();
            // print_r($products); exit();
            $data = [
                'products' => $products,
                'active' =>array('orders'),
            ];
            return view('vendors.order.productlist', $data);
        }else{
            return redirect()->route('vendorDashboard');
        }
    }
    public function SortOrder(Request $request){

        $name =  $request->customername;
        $search =  $request->search;
        $query = Order::query();
        $query->with(['payment', 'vendor','user']);
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('name', 'like', "%" . $search . "%");
                $sub->orWhere('order_reference_number', 'like', "%" . $search . "%");
                
            });
        }
        $query->with(['product' => function($q){
            $q->sum('unit');
        }]);
        if($request->referancenumber!=''){
                $query->where('orders.order_reference_number', $request->referancenumber);
            }
        if($name!=''){
            $query->whereHas('user', function ($query)  use($name){
                $query->where('first_name', 'like', "%" . $name . "%");
                $query->orWhere('last_name', 'like', "%" . $name . "%");
            });
        }
        if($request->orderstatus!='Order Status'){
            $query->where('orders.status', $request->orderstatus);
        }
        $payment = $request->paymenttype;
        if($payment!='Payment Type'){
            
            $query->whereHas('payment', function ($que)  use($payment){
                $que->where('payment_type', 'like', "%" . $payment . "%");
            });  
        }
        $from  = date('Y-m-d',strtotime($request->orderfrom));
        $to    = date('Y-m-d',strtotime($request->orderto));
        //print_r($from.' '.$to); exit();
        if(($from!='1970-01-01') && ($to!='1970-01-01')){
            if($from > $to ){
                $tmp  = $to;
                $to   = $from;
                $from = $to;
            }
            $from =$from.' 00:00:00';
            $to =$to.' 23:59:59';
            $query->WhereBetween('order_at', [$from, $to]);
        }
        $query->where('vendor_id',  Auth::user()->id);
        $query->orderBy('created_at', 'desc');
        $orders = $query->paginate(10);
        $data = [
            'referancenumber' =>$request->referancenumber,
            'customername' =>$request->customername,
            'orderstatus' =>$request->orderstatus,
            'paymenttype' =>$request->paymenttype,
            'orderstatus' =>$request->orderstatus,
            'orderfrom' =>$request->orderfrom,
            'orderto' =>$request->orderto,
            'orders' => $orders,
            'search' => $search,
            'active' =>array('orders'),
        ];
        return view('vendors.order.list', $data);
    }
    public function showCustomerDetails(Request $request){
        if($request->order){
            $query = Order::query();
            $query->with('user');
            $query->where('id',$request->order);
            $query->orderBy('created_at', 'desc');
            $users = $query->get();
            // print_r($products); exit();
            $data = [
                'users' => $users,
                'active' =>array('orders'),
            ];
            return view('vendors.order.userview', $data);
        }else{
            return redirect()->route('vendorDashboard');
        }
    }
}
