<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Brand;

class BrandController extends Controller
{
    public function searchBrand(Request $request){
        $items = Brand::select('id','name')->where('name','like',"%".$request->q."%")->limit(20)->get();
        if(count($items) <= 0){
            $items[] = [
                'id'=> "other",
                'text'=> "Other"
            ];
        }else{
            foreach($items as $item){
                $items[] = [
                    'id'=> $item->id,
                    'text'=> $item->name
                ];
            }
        }
        return response()->json(['items'=>$items]);
    }

    public function searchBrandById(Request $request){
        $item = Brand::select('id','name as text')->where('id',$request->id)->first();
        if($item){
            return response()->json(['item'=>$item, 'status'=>true]);
        }else{
            return response()->json(['status'=>false]);
        }        
    }
}
