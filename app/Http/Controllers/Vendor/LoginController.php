<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Vendor;
use App\Otp;
use Illuminate\Support\Facades\Mail;
use App\Mail\VendorSendResetPassword;
use App\Mail\SendInvitationVendor;
use Error;
use Exception;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $guard = 'vendor';
    protected $redirectTo = '/loginvendor';
    public function __construct()
    {
        $this->middleware('guest')->except('vendorLogout');
    }

    public function showLogin(Request $request)
    {
        $locale = App::getLocale();
        return view('vendors/login');
    }
    public function loginVendor(Request $request)
    {
        $rules = [
            'email'     => 'required',
            'password'    => 'required'
        ];
        $messages = [
            'email.required'    => 'Email is required',
            'password.required'   => 'Password is required',
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'errors' => $validator->errors()->first()]);
        }

        if (auth()->guard('vendor')->attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = auth()->guard('vendor')->user();
            if ($user->active == 1 && $user->approved == 0) {
                Auth::logout();
                return response()->json(['status' => 0, 'errors' => ['Waiting for admin approval']]);
            }
            return response()->json(['status' => 1, 'user' => $user]);
        } else {
            return response()->json(['status' => 0, 'errors' => ['Invalid credential']]);
        }
    }
    public function vendorLogout()
    {
        Auth::logout();
        return redirect('/');
    }
    public function vendorForgotPassword(Request $request)
    {
        $email = $request->email3;
        if ($email) {
            $user = Vendor::where('email', '=', $request->email3)->where('active', '=', 1)->first();
            if ($user) {
                if ($user->active == 1) {
                    $token = rand(10001, 99999) . time() . rand(10001, 99999);
                    $user->reset_token =  $token;
                    try {
                        $user->save();
                        $resetLink = route("resetLink", ['email' => $email, 'token' => $token]);
                        $data = array('link' => $resetLink, 'name' => $user->contact_person, 'email' => $user->email, 'home_link' => route('loginVendor'));
                        Mail::to($email)->send(new VendorSendResetPassword($data));
                        return response()->json(['status' => true, 'message' => 'Your link for resetting password has been sent to your email']);
                    } catch (\Exception $e) {
                        print_r($e->getMessage());
                        $errors['email3'] = "Could not reset password";
                        return response()->json(['status' => 0, 'errors' => $errors]);
                    }
                } else {
                    return response()->json(['status' => false, 'errors' => ['email3' => 'User has been blocked by admin']]);
                }
            } else {
                return response()->json(['status' => false, 'errors' => ['email3' => 'Email entered is not registered with us']]);
            }
        } else {
            return response()->json(['status' => false, 'errors' => ['email3' => 'Email is not provided']]);
        }
    }
    public function resetLink($email, $token, Request $request)
    {
        $token_time =  substr($token, 5, 11);
        $cur_time = time();
        $expiry = $cur_time - $token_time;
        if ($expiry > 1800) {
            return view('vendors.expiredPasswordReset');
        } else {
            $user = Vendor::where([
                ['email', '=', $email],
                ['reset_token', '=', $token]
            ])->first();
            if ($user) {
                $data['email'] = $email;
                return view('vendors.resetPasswordLogin', $data);
            } else {
                return view('vendors.expiredPasswordReset');
            }
        }
    }
    public function vendorchangepassword(Request $request)
    {
        $rules = [
            'email'     => 'required',
            'new_password'    => 'required'
        ];
        $messages = [
            'email.required'    => 'Email is required',
            'new_password.required'   => 'Password is required',
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'errors' => $validator->errors()->first()]);
        }
        $user = Vendor::where('email', '=', $request->email)->where('active', '=', 1)->first();
        if ($user) {
            if ($user->active == 1) {
                $user->reset_token =  null;
                $user->password = bcrypt($request->new_password);
                $user->reset_date =  date('Y-m-d H:i:s');
                try {
                    $user->save();
                    return response()->json(['status' => true, 'message' => 'Password is successfully changed. Please login to continue']);
                } catch (\Exception $e) {
                    // print_r($e->getMessage());
                    $errors['email3'] = "Could not reset password";
                    return response()->json(['status' => 0, 'errors' => $errors]);
                }
            } else {
                return response()->json(['status' => false, 'errors' => ['User has been blocked by admin']]);
            }
        } else {
            return response()->json(['status' => false, 'errors' => ['Malicious Attempt']]);
        }
    }
    public function showSignUp(Request $request)
    {

        return view('vendors/signup');
    }
    public function signUpVendor(Request $request)
    {
        $rules = [
            'mobile' => 'unique:vendors,mobile,required',
            'email' => 'unique:vendors,email,required"',
            'address' => "required",
            'pin' => "required",
            'contact_person' => "required",
            'password' => "required"
        ];
        $messages = [
            'email.required' => "Email is required",
            'email.unique' => "Email already exists",
            'mobile.required' => "Mobile number is required",
            'address.required' => "Address is required",
            'pin.required' => "PIN Code is required",
            'mobile.unique' => "Mobile already exists",
            'contact_person.required' => "Full name is required",
            'contact_person.required' => "Full name is required",
            'password.required' => "Password is required",

        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }

        //insert into table
        $vendor = new Vendor();
        $vendor->email          = $request->email;
        $vendor->mobile         = $request->mobile;
        $vendor->contact_person = $request->contact_person;
        $vendor->address        = $request->address;
        $vendor->pin            = $request->pin;
        $vendor->active         = 0;
        $vendor->password       = bcrypt($request->password);
        $vendor->code           = time() . rand(111, 999);
        try {
            $vendor->save();
            Auth::guard('vendor')->loginUsingId($vendor->id);
            return response()->json(['status' => 1, 'message' => "Successfully Registered. Check your email for login "]);
        } catch (\Exception $e) {
          // print_r($e->getMessage()); exit();
            $errors['couldnot_save'] = "Could not save profile";
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
    }
    public function termsConditions()
    {
    }

    public function sendOtp(Request $request)
    {
        $digits = 4;
        $otp = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);

        $verify =  Otp::where('mobile', '=', $request->post('mobile'))->first();
        if (!$verify) {
            $verify = new Otp();
            $verify->mobile = $request->post('mobile');
        }

        $verify->otp = $otp;
        $verify->save();

        //send otp
        $curl = curl_init();
        $sendurl = "mobile=" . $request->post('mobile') . "&invisible=1&otp=" . $otp . "&authkey=" . env('SMS_KEY', '309064ATU0pQyy2ZbO5dfb5bfdP1') . "&template_id=5dfcc470d6fc05241866743b";
        $sendurl = "https://api.msg91.com/api/v5/otp?" . $sendurl;
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => $sendurl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            // echo "cURL Error #:" . $err;
            return response()->json(['status' => false, "message" => "Could not send OTP"]);
        } else {
            // echo $response;
            return response()->json(['status' => true, "message" => "OTP send successfully"]);
        }
    }

    public function verifyOtp(Request $request)
    {
        $otp = $request->post('otp');
        $mobile = $request->post('mobile');
        try {
            if ($otp && $mobile) {
                $verify = Otp::where('mobile', $mobile)->where('otp', $otp)->first();
                if ($verify) {
                    return response()->json(['status' => true, 'message' => "OTP verfified successfully"]);
                } else {
                    throw new Error("Invalid");
                }
            } else {
                throw new Error("Invalid");
            }
        } catch (\Error $e) {
            return response()->json(['status' => false, 'message' => "Invalid OTP"]);
        }
    }
}
