<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Auth;
use DB;
use App\Notification;
use Validator;
use App\Vendor;
use App\VendorCategory;
use App\Category;
use App\District;
use App\State;
use App\Order;
use App\Product;
use App\OrderProduct;
use App\BusinessType;
use Illuminate\Support\Facades\Mail;

class DashboardController extends Controller
{
    public function showRegistration(Request  $request)
    {
        if ($user = Auth::user()->active == 1) {
            return redirect()->route('vendorDashboard');
        }
        $query = Category::query();
        $query->where('active', '1');
        $query->where('parent_id', 0);
        $categories = $query->get();
        $states = State::all();
        $business_types = BusinessType::all();
        $data = array(
            'categories' => $categories,
            'states' => $states,
            'business_types' => $business_types
        );
        return view('vendors/registration', $data);
    }
    public function vendorDashboard(Request $request)
    {
        //todays sale
        DB::enableQueryLog();
        $from =date('Y-m-d 00:00:00');
        $to   =date('Y-m-d 23:59:59');
        $query = Order::query();
        $query->WhereBetween('order_at', [$from, $to]);
        $query->where('vendor_id',  Auth::user()->id);
        $query->where('status','1');
        $query->select(array(DB::raw('SUM(total) as total')));
        $today = $query->get();
        $query = DB::getQueryLog();
        //print_r($query); exit();
        //this week sale
        $from =date('Y-m-d', strtotime("this week")); 
        $to   =date('Y-m-d 23:59:59');
        $query = Order::query();
        $query->WhereBetween('order_at', [$from, $to]);
        $query->where('vendor_id',  Auth::user()->id); 
        $query->where('status','1');
        $query->select(array(DB::raw('SUM(total) as total')));
        $week = $query->get();
        //this month  sale
        $from =date('Y-m').'-01 00:00:00'; 
        $to   =date('Y-m').'-31 23:59:59';
        $query = Order::query();
        $query->WhereBetween('order_at', [$from, $to]);
        $query->where('vendor_id',  Auth::user()->id);
        $query->where('status','1');
        $query->select(array(DB::raw('SUM(total) as total')));
        $month = $query->get();
        //total sale  sale
        $query = Order::query();
        //$query->WhereBetween('order_at', [$from, $to]);
        $query->where('vendor_id',  Auth::user()->id);
        $query->where('status','1');
        $query->select(array(DB::raw('SUM(total) as total')));
        $sale = $query->get();
        //completed order
        $query = Order::query();
        $query->where('vendor_id',  Auth::user()->id);
        $query->where('status','1');
        $query->select(array(DB::raw('COUNT(id) as count')));
        $completed = $query->get();
        //pending order
        $query = Order::query();
        $query->where('vendor_id',  Auth::user()->id);
        $query->where('status','2');
        $query->select(array(DB::raw('COUNT(id) as count')));
        $pending = $query->get();
        //cencelled
        $query = Order::query();
        $query->where('vendor_id',  Auth::user()->id);
        $query->where(function($query) {
            $query->where('status', '4')
                ->orWhere('status', '5');
        });
        $query->select(array(DB::raw('COUNT(id) as count')));
        $cancelled = $query->get();
        //cencelled 
        // Active Listing
        $query = Product::query();
        $query->where('completed_status', '1');
        $query->where('approved', '1');
        $query->where('stock_unit', '>','0');
        $query->where('vendor_id',  Auth::user()->id);
        $query->select(array(DB::raw('COUNT(id) as count')));
        $activelisting = $query->get();
        $query = \DB::getQueryLog();
        //print_r(end($query)); exit();
        // Pending Listing
        $query = Product::query();
        $query->where('completed_status', '0');
        $query->where('step','!=','7');
        $query->where('approved', '1');
        $query->where('vendor_id',  Auth::user()->id);
        $query->select(array(DB::raw('COUNT(id) as count')));
        $pendinglisting = $query->get();
        $query = \DB::getQueryLog();
        //print_r(end($query)); exit();
        //Out Of Stock Listing
        $query = Product::query();
        $query->where('stock_unit','<=','0');
        $query->where('stock_unit','<=','');
        $query->where('vendor_id',  Auth::user()->id);
        $query->where('vendor_id',  Auth::user()->id);
        $query->select( DB::raw('COUNT(id) as count'));
        $outofstock = $query->get();
        //Limited Stock Listing
        $query = Product::query();
        $query->where('stock_unit','<=','10');
        $query->where('vendor_id',  Auth::user()->id);
        $query->where('vendor_id',  Auth::user()->id);
        $query->select( DB::raw('COUNT(id) as count'));
        $limitedstock = $query->get();
        $query = \DB::getQueryLog();
        //most selling products
        $query = Order::query();
        $query->where('vendor_id',  Auth::user()->id);
        $query->with('product','product.productlist');
        $query->limit(5);
        $most = $query->get();
        $data = [
            'today' => $today,
            'week' => $week,
            'month' => $month,
            'sale' => $sale,
            'completed'=>$completed,
            'pending'=>$pending,
            'cancelled'=>$cancelled,
            'activelisting' =>$activelisting,
            'pendinglisting' =>$pendinglisting,
            'outofstock' =>$outofstock,
            'limitedstock' =>$limitedstock,
            'most'        =>$most,
            'active'      =>array('dashboard'),
        ];
        return view('vendors/dashboard',$data);
    }
    public function showLandingPage(Request $request)
    {
        return view('vendors/landing');
    }
    public function addRegistration(Request $request)
    {
        $rules = [
            'company_name' => "required",
            'store_name' => "required",
            'location'   => 'required',
            'email' => 'unique:vendors,email,required"',
            'name_as_in_bank' => 'required',
            'account_type' => 'required',
            'account_number' => 'required',
            'ifsc_code' => 'required',
            'category' => 'required',
            'id_proof' => 'required',
            'pan_number' => 'required',
            'pancard' => 'required',
            'cancel_cheque' => 'required',
            'business_type' => 'required'
        ];
        $messages = [
            'company_name.required' => "Company name is required",
            'store_name.required' => "Store name is required",
            'location.required' => "Location is required",
            'mobile.unique' => "Mobile already exists",
            'email.required' => "Email is required",
            'email.unique' => "Email already exists",
            'trade_license_document.required' => 'Trade license document is required',
            'name_as_in_bank' => 'Name as in bank is required',
            'account_type' => 'Account type is required',
            'account_number' => 'Account number is required',
            'ifsc_code' => 'IFSC code is required',
            'category' => 'Categories are required',
            'id_proof' => 'Id proof is required',
            'pan_number.required' => 'PAN number is required',
            'pancard' => 'Pancard is required',
            'cancel_cheque' => 'Cancelled cheque is required',
            'business_type' => 'Business type is required'
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
        //password generate
        $string = "";
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size = strlen($chars);
        for ($i = 0; $i < 6; $i++) {
            $string .= $chars[rand(0, $size - 1)];
        }

        $token = rand(1001, 9999) . time() . rand(1001, 9999);
        //insert into table
        $vendor = Vendor::where('code', '=', Auth::user()->code)->firstOrFail();
        $vendor->code = time() . rand(111, 999);
        $vendor->company_name = $request->company_name;
        $vendor->business_type = $request->business_type;
        $vendor->store_name = $request->store_name;
        $vendor->phone = $request->phone;
        $vendor->reset_token = $token;
        $vendor->name_as_in_bank = $request->name_as_in_bank;
        $vendor->account_type = $request->account_type;
        $vendor->active         = 1;
        $vendor->fssai_number = $request->fssai;
        $pan_number = null;
        if ($request->pan_number) {
            $pan_number = strtoupper($request->pan_number);
        }
        $vendor->pan_number = $pan_number;

        $account_number = null;
        if ($request->account_number) {
            $account_number = strtoupper($request->account_number);
        }
        $vendor->account_number = $account_number;
        

        $ifsc_code = null;
        if ($request->ifsc_code) {
            $ifsc_code = strtoupper($request->ifsc_code);
        }
        $vendor->ifsc_code = $ifsc_code;

        $gst_number = null;
        if ($request->gst_number) {
            $gst_number = strtoupper($request->gst_number);
        }
        $vendor->gst_number = $gst_number;

        if (request()->trade_license_document) {
            try {
                $imageName = time() . '.' . request()->trade_license_document->getClientOriginalExtension();
                request()->trade_license_document->move(public_path('images/vendors/trade_license_document/'), $imageName);
                $vendor->trade_license_document = 'images/vendors/trade_license_document/' . $imageName;
            } catch (\Exception $e) {
            }
        }

        if (request()->gst_document) {
            try {
                $imageName = time() . '.' . request()->gst_document->getClientOriginalExtension();
                request()->gst_document->move(public_path('images/vendors/gst_document/'), $imageName);
                $vendor->gst_document = 'images/vendors/gst_document/' . $imageName;
            } catch (\Exception $e) {
            }
        }
        if (request()->profile_picture) {
            try {
                $imageName = time() . '.' . request()->profile_picture->getClientOriginalExtension();
                request()->profile_picture->move(public_path('images/vendors/profile/'), $imageName);
                $vendor->image = 'images/vendors/profile/' . $imageName;
            } catch (\Exception $e) {
            }
        } else {
            $vendor->image = 'images/vendors/profile/default.png';
        }
        if (request()->id_proof) {
            try {
                $imageName = time() . '.' . request()->id_proof->getClientOriginalExtension();
                request()->id_proof->move(public_path('images/vendors/id_proof/'), $imageName);
                $vendor->id_proof = 'images/vendors/id_proof/' . $imageName;
            } catch (\Exception $e) {
            }
        }
        if (request()->pancard) {
            try {
                $imageName = time() . '.' . request()->pancard->getClientOriginalExtension();
                request()->pancard->move(public_path('images/vendors/pancard/'), $imageName);
                $vendor->pancard = 'images/vendors/pancard/' . $imageName;
            } catch (\Exception $e) {
            }
        }
        if (request()->cancel_cheque) {
            try {
                $imageName = time() . '.' . request()->cancel_cheque->getClientOriginalExtension();
                request()->cancel_cheque->move(public_path('images/vendors/cancel_cheque/'), $imageName);
                $vendor->cancel_cheque = 'images/vendors/cancel_cheque/' . $imageName;
            } catch (\Exception $e) {
            }
        }

        try {
            $vendor->save();
            foreach ($request->category as $cats) {
                $categories = new VendorCategory;
                $categories->vendor_id     = Auth::user()->id;
                $categories->category_id =  $cats;
                $categories->save();
            }
            // $resetLink = route("resetLink",['email'=>$request->email,'token'=>$vendor->reset_token]);
            // $data = array('resetLink' => $resetLink, 'name' => $vendor->contact_person, 'email' => $vendor->email);
            // Mail::to($vendor->email)->send(new SendInvitationVendor($data));
            return response()->json(['status' => 1, 'message' => "Request successfully submitted, waiting for admin approval"]);
        } catch (\Exception $e) {
            // print_R($e->getMessage());
            $errors['couldnot_save'] = "Could not save profile";
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
    }
    public function getDistrict(Request $request)
    {
        $state = $request->state;
        $district   = District::where('state_id', $state)->get();
        if ($district) {
            return response()->json(['status' => 1, 'district' => $district]);
        } else {
            return response()->json(['status' => 0, 'errors' => "No districts found"]);
        }
    }
}
