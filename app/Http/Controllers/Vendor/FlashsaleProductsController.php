<?php

namespace App\Http\Controllers\Vendor;
use App\Brand;
use App\Category;
use App\ColorClass;
use App\Http\Controllers\Controller;
use App\LengthClass;
use App\Product;
use App\ProductAttribute;
use App\ProductImage;
use App\ProductAdditionalInfo;
use App\ProductFaq;
use App\WeightClass;
use App\FlashSale;
use App\Notification;
use Auth;
use DB;
use App\FlashsaleProducts;
use Illuminate\Http\Request;
use Validator;
use Crypt;
class FlashsaleProductsController extends Controller
{
   public function flashSale(Request $request){

    $search = $request->search;
    $query = FlashSale::query();
    if ($search) {
        $query->where(function ($sub) use ($search) {
            $sub->orWhere('name', 'like', "%" . $search . "%");
        });
    }
    $query->where('status', '1');
    $query->orderBy('created_at', 'desc');
    $flashsale = $query->paginate(10);

    $data = [
        'flashsale' => $flashsale,
        'search' => $search,
        'active' =>array('advertising','flashsale'),
    ];
    return view('vendors.flashsale.list', $data);

   }
   public function showFlashsale(FlashSale $flashsale){
    
    $id    = $flashsale->id;
    $query = Product::query();
    $query->with('images');
    $query->where('vendor_id', '=', Auth::user()->id);
    $query->whereNotIn('id',function($query)  use ($id){
         $query->select('product_id')->from('flashsale_products')->where('flash_sale_id','=',$id);
    });
    $query->orderBy('created_at', 'desc')->limit(20);
    $products =  $query->paginate(10);
    $flashsale->with('product');
    $query = FlashsaleProducts::query();
    $query->with('product');
    $query->where('vendor_id', '=', Auth::user()->id);
    $query->where('flash_sale_id', '=', $flashsale->id);
    $query->orderBy('created_at', 'desc')->limit(20);
    $flashsaleproducts =  $query->get();
    $data = [
        'products' => $products,
        'flashsale' => $flashsale,
        'flashsaleproducts' => $flashsaleproducts,
        'active' =>array('advertising','flashsale'),
    ];
    return view('vendors.flashsale.flashsaleproduct', $data);
     
   }
   public function saveFlashsaleProduct(FlashSale $flashsale,Request $request){
        $rules = [
          'offer' => "required|numeric",
          'product' => "required|numeric",
        ];
        $messages = [
            'offer.required' => "Offer Percentage is required",
            'offer.number' => "Offer Percentage should be a number",
            'product.required' => "Product Details is required",
            'product.number' => "roduct Details should be required",
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
        $product_id    = $request->product;
        $offer         = $request->offer;
        $flash_sale_id = $flashsale->id;
        $vendor_id     = Auth::user()->id;
        $query = Product::query();
        $query->where('id', '=',$product_id);
        $products         =  $query->get();
        if($offer!=''){
            $offer_amount     = ($products[0]->mrp)-($products[0]->mrp*$offer)/100;
            $offer_percentage = $offer;
        }else{
            $errors['couldnot_save'] = "Could not save flashsale  details";
            return response()->json(['status' => 0, 'errors' => $errors]);
        }

        try {
            DB::beginTransaction();
            $attr_array[] = [
                'product_id' =>  $product_id,
                'vendor_id' => $vendor_id,
                'flash_sale_id' => $flash_sale_id,
                'offer_amount' => $offer_amount,
                'offer_percentage' => $offer_percentage,
                'active_status' => 1,
                'approve' => 0,
                'created_at' => now(),
                'updated_at' => now(),
            ];
            if (!empty($attr_array)) {
                FlashsaleProducts::insert($attr_array);
            }
            DB::commit();
            return response()->json(['status' => 1, 'message'=>'Product Added Successfully','redirect' => route('showFlashsale', ['flashsale' =>$flash_sale_id])]);
        } catch (\Exception $e) {
            DB::rollback();
            $errors['couldnot_save'] = "Could not save flashsale details";
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
   }
   public function deleteFlashsaleProduct(Flashsale $flashsale,Request $request){

        try {
            DB::beginTransaction();
            FlashsaleProducts::where(array('product_id'=>$request->product,'flash_sale_id'=>$flashsale->id))->delete();
            DB::commit();
            return redirect()->route('showFlashsale',['flashsale'=>$flashsale->id]);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('showFlashsale',['flashsale'=>$flashsale->id]);
        }
   }
}
