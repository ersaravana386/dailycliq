<?php

namespace App\Http\Controllers\Vendor;
use App\Brand;
use App\Category;
use App\ColorClass;
use App\Http\Controllers\Controller;
use App\LengthClass;
use App\Product;
use App\ProductImage;
use App\ProductFaq;
use App\WeightClass;
use App\FlashSale;
use App\DealOftheDay;
use Auth;
use DB;
use App\FlashsaleProducts;
use Illuminate\Http\Request;
use Validator;
use Crypt;
use Carbon\Carbon;

class DealOftheDayController extends Controller
{
  public function showDealOftheDay(Request $request){

    $search = $request->search;
    $query = Product::query();
    if ($search) {
        $query->where(function ($sub) use ($search) {
            $sub->orWhere('name', 'like', "%" . $search . "%");
        });
    }
    $query->where('approved', '1');
    $query->where('vendor_id', '=', Auth::user()->id);
    $query->whereNotIn('id',function($query){
        $query->select('product_id')->from('deal_ofthe_days')->where('created_at','!=',date('Y-m-d').' 00:00:00');
   });
    $query->orderBy('created_at', 'desc');
    $product = $query->paginate( );
    $query = DealOftheDay::query();
    $query->where('vendor_id', '=', Auth::user()->id);
    $query->whereDate('created_at', '=',Carbon::today());
    $query->orderBy('created_at', 'desc');
    $dealoftheday = $query->get();
    $data = [
        'products' => $product,
        'dealoftheday' => $dealoftheday,
        'search' => $search,
        'active' =>array('advertising','dealoftheday'),
    ];
    return view('vendors.dealoftheday.list', $data);
  }


 public function saveDealOftheDayProduct(Request $request){
    $rules = [
      'offer' => "required|numeric",
      'product' => "required|numeric",
      'offer_date' => "required",
    ];
    $messages = [
        'offer.required' => "Offer Percentage is required",
        'offer.number' => "Offer Percentage should be a number",
        'product.required' => "Product Details is required",
        'product.number' => "Product Details should be required",
        'offer_date.number' => "Offer Date should be required",
    ];
    $validator = Validator::make(request()->all(), $rules, $messages);
    if (!$validator->passes()) {
        $messages = $validator->messages();
        $errors = [];
        foreach ($rules as $key => $value) {
            $err = $messages->first($key);
            if ($err) {
                $errors[$key] = $err;
            }
        }
        return response()->json(['status' => 0, 'errors' => $errors]);
    }
    $product_id    = $request->product;
    $offer         = $request->offer;
    $offer_date         = $request->offer_date;
    $vendor_id     = Auth::user()->id;
    if($offer!=''){
        $query = Product::query();
        $query->where('id', '=',$product_id);
        $products         =  $query->get();
        $offer_amount     = ($products[0]->mrp)-($products[0]->mrp*$offer)/100;
        $offer_percentage = $offer;
    }else{
        $errors['couldnot_save'] = "Could not save deal of the day  details";
        return response()->json(['status' => 0, 'errors' => $errors]);
    }

    try {
        DB::beginTransaction();
        $attr_array[] = [
            'product_id' =>  $product_id,
            'vendor_id' => $vendor_id,
            'category_id' => $products[0]->category_id,
            'offer_amount' => $offer_amount,
            'offer_percentage' => $offer_percentage,
            'start_date' => date('Y-m-d',strtotime($offer_date)),
            'approve' => 0,
            'created_at' => now(),
            'updated_at' => now(),
        ];
        if (!empty($attr_array)) {
            DealOftheDay::insert($attr_array);
        }
        DB::commit();
        return response()->json(['status' => 1, 'message'=>'Product Added Successfully','redirect' => route('showDealOftheDay')]);
    } catch (\Exception $e) {
        DB::rollback();
         //print_r($e->getMessage());exit;
        $errors['couldnot_save'] = "Could not save deal of the day details";
        return response()->json(['status' => 0, 'errors' => $errors]);
    }
 }
 public function deleteDealOftheDayProduct(DealOftheDay $dealoftheday,Request $request){

    try {
        DB::beginTransaction();
        DealOftheDay::where(array('product_id'=>$request->product,'id'=>$dealoftheday->id))->delete();
        DB::commit();
        return redirect()->route('showDealOftheDay');
    } catch (\Exception $e) {
        DB::rollback();
        return redirect()->route('showDealOftheDay');
    }
 }
}
