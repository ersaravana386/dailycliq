<?php

namespace App\Http\Controllers\Vendor;

use App\Brand;
use App\Category;
use App\ColorClass;
use App\Http\Controllers\Controller;
use App\LengthClass;
use App\Product;
use App\TmpProduct;
use App\TmpProductAttribute;
use App\TmpProductImage;
use App\TmpProductAdditionalInfo;
use App\TmpProductFaq;
use App\WeightClass;
use App\Notification;
use App\Giftideal;
use App\TmpProductGiftideal;
use App\TmpProductFreequentlyBroughtTogether;
use App\Town;
use Auth;
use DB;
use Illuminate\Http\Request;
use Validator;
use Crypt;

class ProductController extends Controller
{
    public function showCatelog(Request $request)
    {
        $search = $request->search;
        $query = Product::query();
        $query->with('images');
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('name', 'like', "%" . $search . "%");
            });
        }
        //$query->where('completed_status', '1');
        $query->where('vendor_id',  Auth::user()->id);
        $query->orderBy('created_at', 'desc');
        $products = $query->paginate(10);
        // Active Listing
        \DB::enableQueryLog();
        $query = Product::query();
        $query->where('completed_status', '1');
        $query->where('approved', '1');
        $query->where('stock_unit', '>','0');
        $query->where('vendor_id',  Auth::user()->id);
        $query->select(array(DB::raw('COUNT(id) as count')));
        $activelisting = $query->get();
        $query = \DB::getQueryLog();
        //print_r(end($query)); exit();
        // Pending Listing
        $query = Product::query();
        $query->where('completed_status', '0');
        $query->where('step','!=','7');
        $query->where('approved', '1');
        $query->where('vendor_id',  Auth::user()->id);
        $query->select(array(DB::raw('COUNT(id) as count')));
        $pending = $query->get();
        $query = \DB::getQueryLog();
        //print_r(end($query)); exit();
        //Draft Listing
        $query = Product::query();
        $query->where('stock_unit','<=','0');
        $query->where('stock_unit','<=','');
        $query->where('vendor_id',  Auth::user()->id);
        $query->where('vendor_id',  Auth::user()->id);
        $query->select( DB::raw('COUNT(id) as count'));
        $stock = $query->get();
        $query = \DB::getQueryLog();
        //print_r(end($query)); exit();
        $data = [
            'products' => $products,
            'activelisting' => $activelisting,
            'pending' => $pending,
            'stock' =>$stock,
            'search' => $search,
        ];
        return view('vendors.products.list', $data);
    }
    public function deleteVendorApprovedProduct(Product $product){
        
        $product->delete();
        return redirect()->route('showCatelog');
    }

    //new product registration step -1
    public function showNewCatelog(Request $request)
    {
        $category_id = $request->category;
        $product_id = isset($request->product) ? $request->product : null;
        $product = new TmpProduct();
        $attributes = [];
        if ($category_id) {
            $category = Category::find($category_id);
            if (!$category) {
                return redirect()->route('showCatelog');
            }
        } else if ($product_id != 0) {
            $product = TmpProduct::find($product_id);
            if ($product->attributes->count() > 0) {
                foreach ($product->attributes as $rec) {
                    $attributes[$rec->attribute] = $rec->value;
                }
            }
            $category = Category::find($product->category_id);
            if (!$category) {
                return redirect()->route('showCatelog');
            }
        } else {
            return redirect()->route('showCatelog');
        }

        $query = TmpProductGiftideal::query();
        $query->where('tmp_product_id', '=', $product_id);
        $query->orderBy('created_at', 'desc')->limit(20);
        $query->select('tmp_gift_id');
        $productgift =  $query->get();
    
        $query = TmpProductFreequentlyBroughtTogether::query();
        $query->where('tmp_product_1', '=', $product_id);
        $query->orderBy('created_at', 'desc')->limit(20);
        $freequently_bought_together =  $query->get();

        $gift = Giftideal::all();
        $town = Town::all();
        
        $query = Product::query();
        $query->where('vendor_id', '=',Auth::user()->id);
        $query->select('id','name');
        $productlist =  $query->get();
    
        // $brands = Brand::get();
        $data = [
            // 'brands' => $brands,
            'category' => $category,
            'properties' => $category->properties,
            'product' => $product,
            'gift'    => $gift,
            'town'    =>$town,
            'productgift'    =>$productgift,
            'freequently_bought_together'    =>$freequently_bought_together,
            'productlist'    =>$productlist,
            'attributes' => $attributes,
            'step' => 1
        ];
        return view('vendors.products.new', $data);
    }

    //new product variant registration step -1
    public function showNewCatelognewvarient(TmpProduct $product, Request $request)
    {
        $variantid = $request->variant;
        $category_id = $request->category;
        $variant_id = isset($variantid) ? $variantid : null;

        $product_details = new TmpProduct();
        $variant = new TmpProduct();

        $category = [];

        if ($category_id) {
            $category = Category::find($category_id);
            if (!$category) {
                return redirect()->route('showCatelog');
            }
        } else if ($variant_id != 0) {
            $product_details = $variant = TmpProduct::with('attributes')->where('id', $variant_id)->first();
            $category = Category::find($product->category_id);
            if (!$category || !$product_details) {
                return redirect()->route('showCatelog');
            }
        } else if ($product->id) {
            $product_details = TmpProduct::with('attributes')->where('id', $product->id)->first();
            $category = Category::find($product->category_id);
            if (!$category) {
                return redirect()->route('showCatelog');
            }
        } else {
            return redirect()->route('showCatelog');
        }

        $query = TmpProductGiftideal::query();
        $query->where('tmp_product_id', '=', $variant_id);
        $query->orderBy('created_at', 'desc')->limit(20);
        $query->select('tmp_gift_id');
        $productgift =  $query->get();
        if(count($productgift)<=0){
            $query = TmpProductGiftideal::query();
            $query->where('tmp_product_id', '=', $product_details->id);
            $query->orderBy('created_at', 'desc')->limit(20);
            $query->select('tmp_gift_id');
            $productgift =  $query->get();
        }
        
        $query = TmpProductFreequentlyBroughtTogether::query();
        $query->where('tmp_product_1', '=', $variant_id);
        $query->orderBy('created_at', 'desc')->limit(20);
        $freequently_bought_together =  $query->get();

        if(count($freequently_bought_together)<=0){
            $query = TmpProductFreequentlyBroughtTogether::query();
            $query->where('tmp_product_1', '=', $product_details->id);
            $query->orderBy('created_at', 'desc')->limit(20);
            $freequently_bought_together =  $query->get();
        }

        $gift = Giftideal::all();
        $town = Town::all();
        
        $query = Product::query();
        $query->where('vendor_id', '=',Auth::user()->id);
        $query->select('id','name');
        $productlist =  $query->get();
          
        // $brands = Brand::get();
        $data = [
            // 'brands'        => $brands,
            'category'      => $category,
            'properties'    => $category->properties,
            'product'       => $product_details,
            'gift'          =>$gift,
            'town'          =>$town,
            'productgift'    =>$productgift,
            'freequently_bought_together'    =>$freequently_bought_together,
            'productlist'    =>$productlist,
            'variant'       => $variant,
            'step'          => 1
        ];

        return view('vendors.products.newproductvariant', $data);
    }

    // Save new product variant 
    public function saveNewvariantCatelog(Request $request)
    {

        if (isset($request->variant_id) && $request->variant_id) {
            $temp_product = TmpProduct::find($request->variant_id);
        } else {
            $temp_product = new TmpProduct();
            $temp_product->variant = isset($request->product_id) ? $request->product_id : null;
        }

        $temp_product->name                 = isset($request->name) ? $request->name : null;
        $temp_product->dcin                 = isset($request->dcin) ? $request->dcin : null;
        $temp_product->hsn                 = isset($request->hsn) ? $request->hsn : null;
        $temp_product->ean                  = isset($request->ean) ? $request->ean : null;
        $temp_product->upc                  = isset($request->upc) ? $request->upc : null;
        $temp_product->brand_id    = isset($request->brand) ? $request->brand : null;
        $temp_product->other_brand          = isset($request->brand_other) ? $request->brand_other : null;
        $temp_product->category_id          = isset($request->category) ? $request->category : null;
        $temp_product->pathayapura_listing  = isset($request->pathayapura_listing) ? 1 : 0;
        $temp_product->assured  = isset($request->assured) ? 1 : 0;
        $temp_product->town  = isset($request->town) ? $request->town : null;
        $temp_product->featured_product  = isset($request->featured_product) ? 1 : 0;
        $temp_product->description          = isset($request->description) ? $request->description : null;
        $temp_product->vendor_id            = Auth::user()->id;
        if (isset($temp_product->step) && $temp_product->step < 1) {
            $temp_product->step = 1;
        } else if (!$temp_product->step) {
            $temp_product->step = 1;
        }
        try {
            DB::beginTransaction();
            $temp_product->save();
            if ($temp_product->id) {
                $attr_array = [];
                if (isset($request->attr) && count($request->attr) > 0) {
                    foreach ($request->attr as $key => $value) {
                        if ($value) {
                            $attr_array[] = [
                                'attribute' => $key,
                                'value' => $value,
                                'temp_product_id' => $temp_product->id,
                                'created_at' => now(),
                                'updated_at' => now(),
                            ];
                        }
                    }
                }
                if (count($attr_array) > 0) {
                    TmpProductAttribute::insert($attr_array);
                }
                  //save gift ideals
                  $attr_array = [];
                  if (isset($request->gifts) && count($request->gifts) > 0) {
                      foreach ($request->gifts as $key => $value) {
                          if ($value) {
                              $attr_array[] = [
                                  'tmp_product_id' =>  $temp_product->id,
                                  'tmp_vendor_id' => Auth::user()->id,
                                  'tmp_gift_id' => $value,
                                  'created_at' => now(),
                                  'updated_at' => now(),
                              ];
                          }
                      }
                  }
                  if (count($attr_array) > 0) {
                      TmpProductGiftideal::insert($attr_array);
                  }
                  ///save freequently bought together
                  $attr_array = [];
                  //print_r($request->freequently_bought_together); exit();
                  if (isset($request->freequently_bought_together) && count($request->freequently_bought_together) > 0) {
                              $attr_array[] = [
                                  'tmp_product_1' =>  $temp_product->id,
                                  'tmp_product_2' => @$request->freequently_bought_together['0'],
                                  'tmp_product_3' => @$request->freequently_bought_together['1'],
                                  'created_at' => now(),
                                  'updated_at' => now(),
                              ];      
              
                  }
                  if (count($attr_array) > 0) {
                      TmpProductFreequentlyBroughtTogether::insert($attr_array);
                  }
  
  
                DB::commit();
                if ($request->save != 'Save') {
                    return response()->json(['status' => 1, 'redirect' => route('showNewvarinatPricingCatelog', ['product' => $temp_product->variant, 'variant' => $temp_product->id])]);
                } else {
                    return response()->json(['status' => 1, 'message' => "Product saved temporarly"]);
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            // print_r($e->getMessage());exit;
            $errors['couldnot_save'] = "Could not update product";
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
    }

    public function saveNewCatelog(Request $request)
    {
        if (isset($request->product_id) && $request->product_id) {
            $temp_product = TmpProduct::find($request->product_id);
        } else {
            $temp_product = new TmpProduct();
        }
          
        $temp_product->name = isset($request->name) ? $request->name : null;
        $temp_product->dcin = isset($request->dcin) ? $request->dcin : null;
        $temp_product->hsn = isset($request->hsn) ? $request->hsn : null;
        $temp_product->ean = isset($request->ean) ? $request->ean : null;
        $temp_product->upc = isset($request->upc) ? $request->upc : null; 
        $temp_product->brand_id    = isset($request->brand) ? $request->brand : null;
        $temp_product->other_brand = isset($request->brand_other) ? $request->brand_other : null;
        $temp_product->category_id = isset($request->category) ? $request->category : null;
        $temp_product->description = isset($request->description) ? $request->description : null;
        $temp_product->pathayapura_listing  = isset($request->pathayapura_listing) ? 1 : 0;
        $temp_product->assured  = isset($request->assured) ? 1 : 0;
        $temp_product->town  = isset($request->town) ? $request->town : null;
        $temp_product->featured_product  = isset($request->featured_product) ? 1 : 0;
        $temp_product->vendor_id = Auth::user()->id;
        if (isset($temp_product->step) && $temp_product->step < 1) {
            $temp_product->step = 1;
        } else if (!$temp_product->step) {
            $temp_product->step = 1;
        }

        try {
            DB::beginTransaction();
            $temp_product->save();
            if ($temp_product->id) {          

                $temp_product->variant = $temp_product->id;
                $temp_product->save();
                
                $attr_array = [];
                if (isset($request->attr) && count($request->attr) > 0) {
                    foreach ($request->attr as $key => $value) {
                        if ($value) {
                            $attr_array[] = [
                                'attribute' => $key,
                                'value' => $value,
                                'temp_product_id' => $temp_product->id,
                                'created_at' => now(),
                                'updated_at' => now(),
                            ];
                        }
                    }
                }
                if (count($attr_array) > 0) {
                    TmpProductAttribute::insert($attr_array);
                }
                //save gift ideals
                $attr_array = [];
                if (isset($request->gifts) && count($request->gifts) > 0) {
                    foreach ($request->gifts as $key => $value) {
                        if ($value) {
                            $attr_array[] = [
                                'tmp_product_id' =>  $temp_product->id,
                                'tmp_vendor_id' => Auth::user()->id,
                                'tmp_gift_id' => $value,
                                'created_at' => now(),
                                'updated_at' => now(),
                            ];
                        }
                    }
                }
                if (count($attr_array) > 0) {
                    TmpProductGiftideal::where('tmp_product_id', $temp_product->id)->delete();
                    TmpProductGiftideal::insert($attr_array);
                }
                
                ///save freequently bought together
                $attr_array = [];
                //print_r($request->freequently_bought_together); exit();
                if (isset($request->freequently_bought_together) && count($request->freequently_bought_together) > 0) {
                            $attr_array[] = [
                                'tmp_product_1' =>  $temp_product->id,
                                'tmp_product_2' => @$request->freequently_bought_together['0'],
                                'tmp_product_3' => @$request->freequently_bought_together['1'],
                                'created_at' => now(),
                                'updated_at' => now(),
                            ];      
            
                }
                if (count($attr_array) > 0) {
                    TmpProductFreequentlyBroughtTogether::where('tmp_product_1', $temp_product->id)->delete();
                    TmpProductFreequentlyBroughtTogether::insert($attr_array);
                }
                DB::commit();
                if ($request->save != 'Save') {
                    return response()->json(['status' => 1, 'redirect' => route('showNewPricingCatelog', ['product' => $temp_product->id])]);
                } else {
                    return response()->json(['status' => 1, 'self' => route('showNewCatelog', ['product' => $temp_product->id]) ,'message' => "Product saved temporarly"]);
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            print_r($e->getMessage());
            exit;
            $errors['couldnot_save'] = "Could not update product";
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
    }

    //new product registration step-2
    public function showNewPricingCatelog(TmpProduct $product, Request $request)
    {

        $weight_classes = WeightClass::get();
        $data = [
            'product' => $product,
            'weight_classes' => $weight_classes,
            'step' => 2
        ];
        return view('vendors.products.pricing', $data);
    }
    //new varinat product registration step-2
    public function showNewvarinatPricingCatelog(TmpProduct $product, TmpProduct $variant, Request $request)
    {
        if ($variant->step > 1) {
            $product = $variant;
        }

        // print_r($product->toArray());exit;

        $weight_classes = WeightClass::get();
        $data = [
            'product' => $product,
            'variant' => $variant,
            'weight_classes' => $weight_classes,
            'step' => 2
        ];
        return view('vendors.products.variantpricing', $data);
    }

    public function saveNewCatelogPricing(TmpProduct $product, Request $request)
    {
        $rules = [
            //'gst' => "numeric",
            'price' => 'required|numeric',
            'mrp' => 'required|numeric',
            'wholesale' => 'required|numeric',
            'order_quantity' => 'required|numeric',
            'commission' => 'required|numeric',
        ];
        $messages = [
           // 'gst.numeric' => "GST must be a number",
            'price.required' => 'Price is required',
            'price.numeric' => "Price must be a number",
            'mrp.required' => 'MRP is required"',
            'mrp.numeric' => "MRP must be a number",
            'wholesale.required' => 'Wholesale price is required',
            'wholesale.numeric' => "Wholesale must be a number",
            'order_qunatity.required' => 'Min. Order Qunatity is required',
            'order_qunatity.numeric' => "Order Quantity must be a number",
            'commission.required' => 'Commision is required',
            'commission.numeric' => "Commision must be a number",
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }

        try {
            DB::beginTransaction();
            $product->gst = $request->gst;
            $product->price = $request->price;
            $product->mrp = $request->mrp;
            $product->wholesale = $request->wholesale;
            $product->order_quantity = $request->order_quantity;
            $product->commission = $request->commission;
            if (isset($request->sess) && $request->sess) {
                $product->sess = 1;
            } else {
                $product->sess = 0;
            }
            if (isset($request->cod) && $request->cod) {
                $product->cod = 1;
            } else {
                $product->cod = 0;
            }

            if (isset($product->step) && $product->step < 2) {
                $product->step = 2;
            }

            $product->save();
            DB::commit();
            if ($request->save != 'Save') {
                return response()->json(['status' => 1, 'redirect' => route('showNewCatelogPackageDetails', ['product' => $product->id])]);
            } else {
                return response()->json(['status' => 1, 'message' => "Product pricing details saved temporarly"]);
            }
        } catch (\Exception $e) {
            print_r($e->getMessage());
            exit;
            DB::rollback();
            $errors['couldnot_save'] = "Could not update product pricing details";
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
    }

    public function saveNewvariantCatelogPricing(TmpProduct $product, TmpProduct $variant, Request $request)
    {
        $rules = [
            'price'             => 'required|numeric',
            'mrp'               => 'required|numeric',
            'wholesale'         => 'required|numeric',
            'order_quantity'    => 'required|numeric',
            'commission'        => 'required|numeric',
        ];
        $messages = [

            'price.required'            => 'Price is required',
            'price.numeric'             => "Price must be a number",
            'mrp.required'              => 'MRP is required"',
            'mrp.numeric'               => "MRP must be a number",
            'wholesale.required'        => 'Wholesale price is required',
            'wholesale.numeric'         => "Wholesale must be a number",
            'order_qunatity.required'   => 'Min. Order quantity is required',
            'order_qunatity.numeric'    => "Order quantity must be a number",
            'commission.required'       => 'Commission is required',
            'commission.numeric'        => "Commission must be a number",
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }

        try {
            DB::beginTransaction();
            $variant->gst               = $request->gst;
            $variant->price             = $request->price;
            $variant->mrp               = $request->mrp;
            $variant->wholesale         = $request->wholesale;
            $variant->order_quantity    = $request->order_quantity;
            $variant->commission    = $request->commission;
            if (isset($request->sess) && $request->sess) {
                $variant->sess = 1;
            } else {
                $variant->sess = 0;
            }
            if (isset($request->cod) && $request->cod) {
                $variant->cod = 1;
            } else {
                $variant->cod = 0;
            }

            if (isset($variant->step) && $variant->step < 2) {
                $variant->step = 2;
            }

            $variant->save();
            DB::commit();
            if ($request->save != 'Save') {
                return response()->json(['status' => 1, 'redirect' => route('showNewvariantCatelogPackageDetails', ['product' => $variant->variant, 'variant' => $variant->id])]);
            } else {
                return response()->json(['status' => 1, 'message' => "Product pricing details saved temporarly"]);
            }
        } catch (\Exception $e) {
             print_r($e->getMessage()); exit();
            DB::rollback();
            $errors['couldnot_save'] = "Could not update product pricing details";
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
    }

    //new product registration step-3
    public function showNewCatelogPackageDetails(TmpProduct $product, Request $request)
    {
        // print_r($product->toArray());
        $length_classes = LengthClass::get();
        $weight_classes = WeightClass::get();
        $data = [
            'product' => $product,
            'length_classes' => $length_classes,
            'weight_classes' => $weight_classes,
            'step' => 3
        ];
        return view('vendors.products.package', $data);
    }

    //new product variant registration step-3
    public function showNewvariantCatelogPackageDetails(TmpProduct $product, TmpProduct $variant, Request $request)
    {
        if ($variant->step > 2) {
            $product = $variant;
        }
        // print_r($product->toArray());exit;
        $length_classes = LengthClass::get();
        $weight_classes = WeightClass::get();
        $data = [
            'product'           => $product,
            'variant'           => $variant,
            'length_classes'    => $length_classes,
            'weight_classes'    => $weight_classes,
            'step' => 3
        ];
        return view('vendors.products.variantpackage', $data);
    }

    public function saveNewCatelogPackageDetails(TmpProduct $product, Request $request)
    {
        // print_r($request->all());
        $rules = [
            'weight' => "required|numeric",
            'weight_unit' => 'required',
            'return_policy' =>'required', 
            // 'length' => 'required|numeric',
            // 'width' => 'required|numeric',
            // 'height' => 'required|numeric',
            // 'dimensions_unit' => 'required',
            'shipping_processing_time' => 'required|numeric',
        ];
        $messages = [
            'weight.required' => "Weight is required",
            'weight.numeric' => "Weight must be a number",
            'weight_unit.required' => 'Weight unit is required',
            'return_policy.required' =>'Return & Refund Policy is required',
            // 'length.required' => 'Length is required',
            // 'length.numeric' => 'Length must be a number',
            // 'width.required' => 'Width is required',
            // 'width.numeric' => 'Width must be a number',
            // 'height.required' => 'Height is required',
            // 'height.numeric' => 'Height must be a number',
            // 'dimensions_unit.required' => 'Dimensions unit is required',
            'shipping_processing_time.required' => 'Shipping processing time is required',
            'shipping_processing_time.numeric' => 'Shipping processing time must be a number',
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }

        try {
            DB::beginTransaction();
            $product->weight = $request->weight;
            $product->weight_class_id = $request->weight_unit;
            $product->length = $request->length;
            $product->width = $request->width;
            $product->height = $request->height;
            $product->return_policy = $request->return_policy;
            $product->warranty_details = $request->warranty_details;
            $product->length_class_id = $request->dimensions_unit;
            $product->shipping_processing_time = $request->shipping_processing_time;

            if (isset($request->free_delivery) && $request->free_delivery) {
                $product->free_delivery = 1;
            } else {
                $product->free_delivery = 0;
            }

            if (isset($product->step) && $product->step < 3) {
                $product->step = 3;
            }

            $product->save();
            DB::commit();
            if ($request->save != 'Save') {
                return response()->json(['status' => 1, 'redirect' => route('showNewCatelogImages', ['product' => $product->id])]);
            } else {
                return response()->json(['status' => 1, 'message' => "Product package details saved temporarly"]);
            }
        } catch (\Exception $e) {
            DB::rollback();
            $errors['couldnot_save'] = "Could not update product package details";
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
    }

    // save new variant product Catelog Package Details
    public function saveNewvariantCatelogPackageDetails(TmpProduct $product, TmpProduct $variant, Request $request)
    {
        $rules = [
            'weight'                    => "required|numeric",
            'weight_unit'               => 'required',
            'dimensions_unit'           => 'required',
            'shipping_processing_time'  => 'required|numeric',
            'return_policy'             => 'required',
        ];
        $messages = [
            'weight.required'                   => "Weight is required",
            'weight.numeric'                    => "Weight must be a number",
            'weight_unit.required'              => 'Weight unit is required',
            'dimensions_unit.required'          => 'Dimensions unit is required',
            'shipping_processing_time.required' => 'Shipping processing time is required',
            'shipping_processing_time.numeric'  => 'Shipping processing time must be a number',
            'retirn_policy.required'            => 'Return & refund policy is required',
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
        try {
            DB::beginTransaction();
            $variant->weight                    = $request->weight;
            $variant->weight_class_id           = $request->weight_unit;
            $variant->length                    = $request->length;
            $variant->width                     = $request->width;
            $variant->height                    = $request->height;
            $variant->length_class_id           = $request->dimensions_unit;
            $variant->shipping_processing_time  = $request->shipping_processing_time;
            $variant->return_policy             = $request->return_policy;
            $variant->warranty_details          = $request->warranty_details;
           

            if (isset($request->free_delivery) && $request->free_delivery) {
                $variant->free_delivery = 1;
            } else {
                $variant->free_delivery = 0;
            }

            if (isset($variant->step) && $variant->step < 3) {
                $variant->step = 3;
            }

            $variant->save();
            DB::commit();
            if ($request->save != 'Save') {
                return response()->json(['status' => 1, 'redirect' => route('showNewvariantCatelogImages', ['product' => $product->id, 'variant' => $variant->id])]);
            } else {
                return response()->json(['status' => 1, 'message' => "Product package details saved temporarly"]);
            }
        } catch (\Exception $e) {
            DB::rollback();
            $errors['couldnot_save'] = "Could not update product package details";
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
    }

    public function searchProduct(Request $request)
    {
        $search = $request->q;
        $query = Product::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('name', 'like', "%" . $search . "%");
            });
        }
        $query->orderBy('created_at', 'desc')->limit(20);
        $products = $query->get();
        $data = [];
        foreach ($products as $product) {
            $data['label'] = $product->name;
            $data['value'] = $product->id;
        }
        return response()->json($data);
    }

    public function showVendorCategories(Request $request)
    {
        $data = [
            'vendor_categories' => Category::where('active',1)->where('parent_id',0)->get(),
            'search' => "",
        ];
        return view('vendors.products.categories', $data);
    }

    public function showNewCatelogImages(TmpProduct $product, Request $request)
    {
        $query = TmpProductImage::query();
        $query->where('temp_product_id', '=', $request->product);
        $query->orderBy('created_at', 'desc')->limit(20);
        $product_images =  $query->get();
        $data = [
            'product_images' => $product_images,
            'product' => $product,
            'step' => 4
        ];
        return view('vendors.products.images', $data);
    }
    // showing variant product images
    public function showNewvariantCatelogImages(TmpProduct $product, TmpProduct $variant, Request $request)
    {
        if ($variant->step > 3) {
            $product = $variant;
        }

        $query = TmpProductImage::query();
        $query->where('temp_product_id', '=', $request->product);
        $query->orderBy('created_at', 'desc')->limit(20);
        $product_images =  $query->get();
        $data = [
            'product_images' => $product_images,
            'product' => $product,
            'variant' => $variant,
            'step' => 4
        ];
        return view('vendors.products.variantimages', $data);
    }

    public function saveNewCatelogImage(TmpProduct $product, Request $request)
    {
        $resp = [];
        if (request()->image) {
            try {
                $imageName = time() . '.' . request()->image->getClientOriginalExtension();
                request()->image->move(public_path('images/products/' . $product->id . '/'), $imageName);

                $product_image = new TmpProductImage([
                    'image' =>  'images/products/' . $product->id . '/' . $imageName
                ]);

                $product->images()->save($product_image);
                 
                if (isset($product->step) && $product->step < 4) {
                    $product->step = 6;
                    $product->completed_status = 1;
                    $product->save();

                    // Notification for vendor//
                    $date =date('Y-m-d h:i:s');
                    $notification =['added_by'=> Auth::user()->id,'added_to'=> Auth::user()->id,'is_view'=>0,'process'=>'You have added a new product','link'=>'showVendorProductList','created_at'=>$date];
                    Notification::insert($notification);
                    //Notification for vendor end here //
                }
                $redirect = 0;

                if ($request->save == 'Save') {
                    $redirect = 1;
                }



                $resp = [
                    'status' => true,
                    'message' => 'Product image Uploaded successfully',
                    'redirect' => $redirect,
                    'product' => $product->id
                ];
            } catch (\Exception $e) {
                // print_r($e->getMessage());exit;
                $errors['couldnot_save'] = "Could not upload product image";
                $resp = [
                    'status' => false,
                    'errors' => $errors
                ];
            }
        } else {
            $errors['couldnot_save'] = "Could not upload product image";
            $resp = [
                'status' => false,
                'errors' => $errors
            ];
        }
        return response()->json($resp);
    }

    // saving variant product images
    public function saveNewvariantCatelogImage(TmpProduct $product, TmpProduct $variant, Request $request)
    {
        $resp = [];
        if (request()->image) {
            try {
                $imageName = time() . '.' . request()->image->getClientOriginalExtension();
                request()->image->move(public_path('images/products/' . $variant->id . '/'), $imageName);

                $product_image = new TmpProductImage([
                    'image' =>  'images/products/' . $variant->id . '/' . $imageName
                ]);

                $variant->images()->save($product_image);

                if (isset($variant->step) && $variant->step < 4) {
                    $variant->step = 6;
                    $variant->completed_status= 1;
                    $variant->save();
                     // Notification for vendor//
                     $date =date('Y-m-d h:i:s');
                     $notification =['added_by'=> Auth::user()->id,'added_to'=> Auth::user()->id,'is_view'=>0,'process'=>'You have added a new product waiting for approval','link'=>'showVendorProductList','created_at'=>$date];
                     Notification::insert($notification);
                     //Notification for vendor end here //
                }

                $redirect = 0;

                if ($request->save == 'Save') {
                    $redirect = 1;
                }

                $resp = [
                    'status' => true,
                    'message' => 'Product image Uploaded successfully',
                    'redirect' => $redirect,
                    'product' => $product->id,
                    'variant' => $variant->id
                ];
            } catch (\Exception $e) {
                // print_r($e->getMessage());exit;
                $errors['couldnot_save'] = "Could not upload product image";
                $resp = [
                    'status' => false,
                    'errors' => $errors
                ];
            }
        } else {
            $errors['couldnot_save'] = "Could not upload product image";
            $resp = [
                'status' => false,
                'errors' => $errors
            ];
        }
        return response()->json($resp);
    }


    public function saveNewvariantCatelogImageOld(TmpProduct $product, TmpProduct $variant, Request $request)
    {
        // print_r($request->all());
        $images = $request->images;
        if (isset($images) &&  count($images) > 0) {
            $images = TmpProductImage::whereIn('id', $images)->get()->toArray();
            if (count($images) > 0) {
                foreach ($images as $key => $image) {
                    $images[$key]['temp_product_id'] = $variant->id;
                    unset($images[$key]['id']);
                }
                TmpProductImage::insert($images);
                if (isset($variant->step) && $variant->step < 4) {
                    $variant->step = 6;
                    $variant->completed_status = 1;
                    $variant->save();
                     // Notification for vendor//
                     $date =date('Y-m-d h:i:s');
                     $notification =['added_by'=> Auth::user()->id,'added_to'=> Auth::user()->id,'is_view'=>0,'process'=>'You have added a new product waiting for approval','link'=>'showVendorProductList','created_at'=>$date];
                     Notification::insert($notification);
                     //Notification for vendor end here //
                }
            }
        }
        return response()->json(['status' => true, 'redirect' => 1]);
    }

    public function deleteCatelogueImage(TmpProductImage $image, Request $request)
    {
        if ($image) {
            $id = $image->temp_product_id;
            $path = public_path() . "/" . $image->image;
            if(file_exists($path)){
                unlink($path);
            }
            $image->delete();
            return redirect()->route('showNewCatelogImages', ['product' => $id]);
        } else {
            return redirect()->route('vendorDashboard');
        }
    }

    ///Additional Information Section Start Here //// 

    public function showNewCatelogAdditionalInfo(TmpProduct $product, Request $request)
    {
        // print_r($product->toArray());
        $query = TmpProductAdditionalInfo::query();
        $query->where('temp_product_id', '=', $request->product->id);
        $query->orderBy('created_at', 'desc')->limit(20);
        $additional_fields =  $query->get();
        $data = [
            'product' => $product,
            'additional_fields' => $additional_fields,
            'step' => 5
        ];
        return view('vendors.products.additional_info', $data);
    }

    //adding additional information to the variant product
    public function showNewvariantCatelogAdditionalInfo(TmpProduct $product, TmpProduct $variant, Request $request)
    {
        if ($variant->step > 4) {
            $product = $variant;
        }

        $query = TmpProductAdditionalInfo::query();
        $query->where('temp_product_id', '=', $product->id);
        $query->orderBy('created_at', 'desc')->limit(20);
        $additional_fields =  $query->get();
        $data = [
            'product' => $product,
            'variant' => $variant,
            'additional_fields' => $additional_fields,
            'step' => 5
        ];
        return view('vendors.products.variantadditional_info', $data);
    }

    public function saveNewCatelogAdditionalInfo(TmpProduct $product, Request $request)
    {
        // $rules = [
        //     'name' => "required",
        //     'field_details' => 'required',
        // ];
        // $messages = [
        //     'name.required' => "Filed Name is required",
        //     'filed_details.required' => "Field Details is required",
        // ];

        // $validator = Validator::make(request()->all(), $rules, $messages);
        // if (!$validator->passes()) {
        //     $messages = $validator->messages();
        //     $errors = [];
        //     foreach ($rules as $key => $value) {
        //         $err = $messages->first($key);
        //         if ($err) {
        //             $errors[$key] = $err;
        //         }
        //     }
        //     return response()->json(['status' => 0, 'errors' => $errors]);
        // }
        $attr_array= array();
        if(!empty($request->name) || !empty($request->field_details)){
            $attr_array[] = [
                'name' =>  $request->name,
                'value' => $request->field_details,
                'temp_product_id' => $product->id,
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }
      
        if (!empty($attr_array)) {
            TmpProductAdditionalInfo::insert($attr_array);
        }

        if (isset($product->step) && $product->step < 5) {
            $product->step = 5;
            $product->save();
        }


        if ($request->save != "Save") {
            return response()->json(['status' => 1, 'redirect' => route('showNewCatelogueFaq', ['product' => $product->id])]);
        } else {
            return response()->json(['status' => 0, 'message' => "Product addditional information details saved temporarly"]);
        }
    }

    //saving additional information to the variant product
    public function saveNewvariantCatelogAdditionalInfo(TmpProduct $product, TmpProduct $variant, Request $request)
    {
        // $rules = [
        //     'name'          => "required",
        //     'field_details' => 'required',
        // ];
        // $messages = [
        //     'name.required'             => "Filed Name is required",
        //     'filed_details.required'    => "Field Details is required",
        // ];

        // $validator = Validator::make(request()->all(), $rules, $messages);
        // if (!$validator->passes()) {
        //     $messages = $validator->messages();
        //     $errors = [];
        //     foreach ($rules as $key => $value) {
        //         $err = $messages->first($key);
        //         if ($err) {
        //             $errors[$key] = $err;
        //         }
        //     }
        //     return response()->json(['status' => 0, 'errors' => $errors]);
        // }
        if(!empty($request->name) && !empty($request->field_details)){
            $attr_array[] = [
                'name' =>  $request->name,
                'value' => $request->field_details,
                'temp_product_id' => $variant->id,
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }else{
            $attr_array = array();
        }

        if (count($attr_array) > 0) {
            TmpProductAdditionalInfo::insert($attr_array);
        }

        if (isset($variant->step) && $variant->step < 5) {
            $variant->step = 5;
            $variant->save();
        }

        if ($request->save != "Save") {
            return response()->json(['status' => 1, 'redirect' => route('showNewvariantCatelogueFaq', ['product' => $variant->variant, 'variant' => $variant->id])]);
        } else {
            return response()->json(['status' => 0, 'message' => "Product addditional information details saved temporarly"]);
        }
    }

    public function saveNewvariantCatelogAdditionalInfoOld(TmpProduct $product, TmpProduct $variant, Request $request)
    {
        // print_r($request->all());
        $infos = $request->infos;
        if (isset($infos) &&  count($infos) > 0) {
            $infos = TmpProductAdditionalInfo::whereIn('id', $infos)->get()->toArray();
            if (count($infos) > 0) {
                foreach ($infos as $key => $info) {
                    $infos[$key]['temp_product_id'] = $variant->id;
                    unset($infos[$key]['id']);
                }
                TmpProductAdditionalInfo::insert($infos);
                if (isset($variant->step) && $variant->step < 5) {
                    $variant->step = 5;
                    $variant->save();
                }
            }
        }
        return response()->json(['status' => true, 'redirect' => 1]);
    }

    public function deleteCatelogAdditionalInfo(TmpProductAdditionalInfo $additionalinfo, Request $request)
    {
        if ($additionalinfo) {
            $id = $additionalinfo->temp_product_id;
            $additionalinfo->delete();
            return redirect()->route('showNewCatelogAdditionalInfo', ['product' => $id]);
        } else {
            return redirect()->route('vendorDashboard');
        }
    }


    public function deleteCatelogAdditionalInfoVariant(TmpProductAdditionalInfo $additionalinfo, Request $request)
    {
        if ($additionalinfo) {
            $id = $additionalinfo->temp_product_id;
            $additionalinfo->delete();
            return redirect()->route('showNewvariantCatelogAdditionalInfo', ['product' => $additionalinfo->product->variant, 'variant' => $additionalinfo->product->id]);
        } else {
            return redirect()->route('vendorDashboard');
        }
    }

    ///Additional Information Section End Here //// 

    // faq
    public function showNewCatelogueFaq(TmpProduct $product, Request $request)
    {
        // print_r($product->toArray());

        $query = TmpProductFaq::query();
        $query->where('temp_product_id', '=', $request->product->id);
        $query->orderBy('created_at', 'desc')->limit(20);
        $faq =  $query->get();
        $data = [
            'product' => $product,
            'faq' => $faq,
            'step' => 6
        ];

        return view('vendors.products.faq', $data);
    }

    // variant faq
    public function showNewvariantCatelogueFaq(TmpProduct $product, TmpProduct $variant, Request $request)
    {
        if ($variant->step > 5) {
            $product = $variant;
        }

        $query = TmpProductFaq::query();
        $query->where('temp_product_id', '=', $product->id);
        $query->orderBy('created_at', 'desc')->limit(20);
        $faq   =  $query->get();
        $data  = [
            'product' => $product,
            'variant' => $variant,
            'faq' => $faq,
            'step' => 6
        ];

        return view('vendors.products.variantfaq', $data);
    }

    public function saveNewCatelogueFaq(TmpProduct $product, Request $request)
    {
        // $rules = [
        //     'question' => "required",
        //     'field_answer' => 'required',
        // ];
        // $messages = [
        //     'question.required' => "Question is required",
        //     'filed_answer.required' => "Answer is required",
        // ];

        // $validator = Validator::make(request()->all(), $rules, $messages);
        // if (!$validator->passes()) {
        //     $messages = $validator->messages();
        //     $errors = [];
        //     foreach ($rules as $key => $value) {
        //         $err = $messages->first($key);
        //         if ($err) {
        //             $errors[$key] = $err;
        //         }
        //     }
        //     return response()->json(['status' => 0, 'errors' => $errors]);
        // }
        if(!empty($request->question) && !empty($request->field_answer)){
            $attr_array[] = [
                'question' =>  $request->question,
                'answer' => $request->field_answer,
                'temp_product_id' => $product->id,
                'created_at' => now(),
                'updated_at' => now(),
            ];
       }else{
        $attr_array=array();
       }
        if (count($attr_array) > 0) {
            TmpProductFaq::insert($attr_array);
        }

        if (isset($product->step) && $product->step < 6) {
            $product->completed_status = 1;
            $product->step = 6;
            $product->save();
        }

        if ($request->save != "Save") {
            return response()->json(['status' => 1, 'redirect' => route('showVendorProductList')]);
        } else {
            return response()->json(['status' => 1, 'message' => "Product addditional information details saved temporarly"]);
        }
    }
    // save FAQ of variant product
    public function saveNewvariantCatelogueFaq(TmpProduct $product, TmpProduct $variant, Request $request)
    {
        // $rules = [
        //     'question'      => "required",
        //     'field_answer'  => 'required',
        // ];
        // $messages = [
        //     'question.required'     => "Question is required",
        //     'filed_answer.required' => "Answer is required",
        // ];

        // $validator = Validator::make(request()->all(), $rules, $messages);
        // if (!$validator->passes()) {
        //     $messages = $validator->messages();
        //     $errors = [];
        //     foreach ($rules as $key => $value) {
        //         $err = $messages->first($key);
        //         if ($err) {
        //             $errors[$key] = $err;
        //         }
        //     }
        //     return response()->json(['status' => 0, 'errors' => $errors]);
        // }
        if(!empty($request->question) && !empty($request->field_answer)){
            $attr_array[] = [
                'question' =>  $request->question,
                'answer' => $request->field_answer,
                'temp_product_id' => $variant->id,
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }else{
            $attr_array = array();
        }
       
        if (count($attr_array) > 0) {
            TmpProductFaq::insert($attr_array);
        }

        if (isset($variant->step) && $variant->step < 6) {
            $variant->completed_status = 1;
            $variant->step = 6;
            $variant->save();
        }
        
        if ($request->save != "Save") {
            return response()->json(['status' => 1, 'redirect' => route('showVendorProductList')]);
        } else {
            return response()->json(['status' => 0, 'message' => "Product addditional information details saved temporarly"]);
        }
    }

    public function saveNewvariantCatelogueFaqOld(TmpProduct $product, TmpProduct $variant, Request $request)
    {
        // print_r($request->all());
        $infos = $request->infos;
        if (isset($infos) &&  count($infos) > 0) {
            $infos = TmpProductFaq::whereIn('id', $infos)->get()->toArray();
            if (count($infos) > 0) {
                foreach ($infos as $key => $info) {
                    $infos[$key]['temp_product_id'] = $variant->id;
                    unset($infos[$key]['id']);
                }
                TmpProductFaq::insert($infos);
                if (isset($variant->step) && $variant->step < 6) {
                    $variant->completed_status = 1;
                    $variant->step = 6;
                    $variant->save();
                }
            }
        }
        return response()->json(['status' => true, 'redirect' => 1]);
    }

    public function deleteCatelogueFaq(TmpProductFaq $faq, Request $request)
    {
        if ($faq) {
            $id = $faq->temp_product_id;
            $faq->delete();
            return redirect()->route('showNewCatelogueFaq', ['product' => $id]);
        } else {
            return redirect()->route('vendorDashboard');
        }
    }

    public function deleteCatelogueFaqVariant(TmpProductFaq $faq, Request $request)
    {
        if ($faq) {
            $id = $faq->temp_product_id;
            $faq->delete();
            return redirect()->route('showNewvariantCatelogueFaq', ['product' => $faq->product->variant, 'variant' => $faq->product->id]);
        } else {
            return redirect()->route('vendorDashboard');
        }
    }

    // show vendor
    public function showVendorProductList(Request $request)
    {

        $search = $request->search;
        $query = TmpProduct::query();
        $query->with('images');
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('name', 'like', "%" . $search . "%");
            });
        }
        // $query->where('completed_status', '1');
        $query->where('vendor_id',  Auth::user()->id);
        $query->orderBy('created_at', 'desc');
        $products = $query->paginate(10);

        // $products = TmpProduct::with('images')->where('vendor_id', Auth::user()->id)->get();
        // print_r($products->toArray());exit;
        $data = [
            'products' => $products,
            'search' => $search
        ];
        return view('vendors.products.productlist', $data);
    }

    //delete product
    public function deleteCatelog(TmpProduct $product, Request $request)
    {
        try {
            DB::beginTransaction();
            TmpProductAdditionalInfo::where('temp_product_id', $product->id)->delete();
            TmpProductAttribute::where('temp_product_id', $product->id)->delete();
            TmpProductFaq::where('temp_product_id', $product->id)->delete();;
            TmpProductImage::where('temp_product_id', $product->id)->delete();
            $product->delete();
            DB::commit();
            return redirect()->route('showVendorProductList');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('showVendorProductList');
        }
    }
}
