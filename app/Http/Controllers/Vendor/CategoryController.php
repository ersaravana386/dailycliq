<?php

namespace App\Http\Controllers\Vendor;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function getSubCategories(Request $request)
    {
        $category = Category::find($request->id);
        $categories = [];
        if ($category) {
            foreach ($category->childrens as $cat) {
                $categories[] = [
                    'id' => $cat->id,
                    'name' => $cat->name,
                    'subcategory_count' => $cat->childrens()->count(),
                ];
            }
        }
        return [
            'status' => 1,
            'data' => $categories,
            'category' => isset($category->name) ? $category->name : "",
        ];
    }
}
