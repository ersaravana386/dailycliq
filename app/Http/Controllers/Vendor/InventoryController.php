<?php

namespace App\Http\Controllers\Vendor;
use App\Brand;
use App\Category;
use App\ColorClass;
use App\Http\Controllers\Controller;
use App\LengthClass;
use App\Inventory;
use App\Product;
use App\ProductAttribute;
use App\ProductImage;
use App\ProductAdditionalInfo;
use App\ProductFaq;
use App\WeightClass;
use Auth;
use DB;
use Illuminate\Http\Request;
use Validator;
use Crypt;
class InventoryController extends Controller
{
    public function showInventory(Request $request)
    {
        $search = $request->search;
        DB::enableQueryLog();
        $query = Product::query();
         
        if ($search) {
           
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('name', 'like', "%" . $search . "%");
                $sub->orWhere('dcin', 'like', "%" . $search . "%");
                $sub->orWhere('price', 'like', "%" . $search . "%");
                $sub->orWhere('mrp', 'like', "%" . $search . "%");
            });
            $query->with('brand',function ($sub)  use($search){
                $sub->where('id','!=','');
                //return $sub->select('*')->get();
            });
            // $query->with(['category'=>function($sub) use($search){
            //    return  $sub->select('*')->get();
            // }]);
        }
        //$query->where('completed_status', '1');
        $query->where('vendor_id',  Auth::user()->id);
        $query->orderBy('created_at', 'desc');
        $products = $query->paginate(10);    
        $query = DB::getQueryLog();
        //print_r($products); exit();
        $data = [
            'products' => $products,
            'search' => $search,
            'active' =>array('inventory'),
        ];
        return view('vendors.inventory.list', $data);
    }
    public function showProduct(Request $request){
        
        $search = $request->search;
        $query = Product::query();
        $query->where('id',$request->product);
        $products = $query->get();
        $query = Inventory::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->orWhere('process', 'like', "%" . $search . "%");
                $sub->orWhere('unit', 'like', "%" . $search . "%");
                if(($search == 'In Stock') || ($search == 'in stock'))
                {
                    $search =1;
                }
                if(($search == 'Out Stock') || ($search == 'out stock'))
                {
                    $search =2;
                }
                if(($search == 'Purchase') || ($search == 'purchase'))
                {
                    $search =3;
                }
                $sub->orWhere('process_id', 'like', "%" . $search . "%");
            });
        }
        $query->where('product_id',  $request->product);
        $query->where('vendor_id',  Auth::user()->id);
        $query->orderBy('created_at', 'desc');
        $inventory = $query->paginate(10);
        $data = [
            'products' => $products,
            'inventory' => $inventory,
            'search' => $search,
            'active' =>array('inventory'),
        ];
        return view('vendors.inventory.inventory', $data);
    }
    public function saveInventory(Product $product,Request $request){

        $rules = [
        'unit' => "required|numeric",
        'process' => 'required',
        ];
        $messages = [
            'unit.required' => "Unit is required",
            'unit.number' => "Unit should be a number",
            'process.required' => "Process is required",
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
        $product_id = $product->id;
        $vendor_id  = $product->vendor_id;
        $process_id = $request->process;
        $unit       = $request->unit;
        $stock_unit = $product->stock_unit;
        if($stock_unit ==NULL){
            $stock_unit = 0;
        }
        switch($process_id){
            case 1:
                $process  = 'Stock Added By Vendor'; 
                $update   = $stock_unit+$unit;
            break;
            case 2:
                $process  = 'Stock Reduced By Vendor'; 
                $update   = $stock_unit-$unit;
                if($update < 0){
                    $update = 0;
                    $unit   = $stock_unit;
                }
            break;  
            case 3:
                $process  = 'Purchased By Vendor'; 
                $update   = $stock_unit+$unit;
            break;
        } 
        try {
            DB::beginTransaction();
            $product->stock_unit = $update;
            $product->save();
            $attr_array[] = [
                'product_id' =>  $product->id,
                'vendor_id' => $product->vendor_id,
                'process' => $process,
                'process_id' => $process_id,
                'unit' => $unit,
                'created_at' => now(),
                'updated_at' => now(),
            ];
            if (!empty($attr_array)) {
                Inventory::insert($attr_array);
            }
            DB::commit();
            return response()->json(['status' => 1, 'redirect' => route('showProduct', ['product' => $product->id])]);
        } catch (\Exception $e) {
             //print_r($e->getMessage());exit;
            DB::rollback();
            $errors['couldnot_save'] = "Could not save product stock details";
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
    }
    public function deleteInventory(Inventory $inventory,Request $request){
      
        $query = Product::query();
        $query->where('id',$inventory->product_id);
        $products = $query->get();
        if(!empty($inventory->product_id)){
            switch($inventory->product_id){
                case 1:
                    $update   = $product->stock_unit-$inventory->$unit;
                break;
                case 2:
                    $update   = $product->stock_unit+$inventory->$unit;
                    if($update < 0){
                        $update = 0;
                    }
                break;  
                case 3:
                    $product->stock_unit-$inventory->$unit;
                break;
            }
            try {
                DB::beginTransaction();
                $product->stock_unit = $update;
                $product->save();
                DB::commit();
                return response()->json(['status' => 1, 'redirect' => route('showProduct', ['product' => $product->id])]);
            } catch (\Exception $e) {
                 //print_r($e->getMessage());exit;
                DB::rollback();
                $errors['couldnot_save'] = "Could not save product stock details";
                return response()->json(['status' => 0, 'errors' => $errors]);
            }
        }else{
            return redirect()->route('vendorDashboard');
        }
    }


}
