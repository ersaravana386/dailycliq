<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;
use App\Product;
use Validator;
use DB;
use DateTime;

class BannerController extends Controller
{
    public function showProductsBanner(Request $request){
        $search=$request->search;
        $query=Product::query();
        $query->with('vendor');
        $query->with('category');
        $query->with('banner');
        if ($search) {
            $query->orwhere(function ($sub) use ($search) {
                $sub->orWhere('name', 'like', "%" . $search . "%");
            });
            $query->whereHas('vendor',function ($sub) use ($search) {
                $sub->Where('company_name', 'like', "%" . $search . "%");
            });
            $query->orWhereHas('category',function ($sub) use ($search) {
                $sub->Where('name', 'like', "%" . $search . "%");
            });     
        }
        $query->orderBy('created_at','desc');
        $products=$query->paginate(10);
        $data=[
            "products"=>$products,
            'search'=>$search,
        ];
        return view('admin.banners.list',$data);
    }
    public function addNewBanner($id,Request $request){

        return view('admin.banners.newbanner',['product'=>$id]);
    }
    public function newProductBanner(Request $request){
        $rules = [
            'startdate'      => 'nullable',
            'enddate'        => 'nullable',
            'banner_type'    => 'required',
            'image'          => 'nullable',
            'pathayapura_banner'=> 'required',
            'acc_status'         => 'required',
        ];
        $messages = [
            // 'startdate.required'    => 'Start Date is required',
            // 'enddate.required'      => 'End Date is required',
            'banner_type.required'  => 'Banner Type is required',
            'pathayapura_banner.required'      => 'Pathayapura Banner is required',
            'acc_status.required'       =>  'Account status is required'
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            // return response()->json(['status' => 0, 'message' => $errors]);
            return response()->json(['status' => 0]);
        }
        if($request->image){
            $imageName = time() . '.' . $request->image->getClientOriginalExtension();
            request()->image->move(public_path('images/banners/' . $request->pro_id . '/'), $imageName);
            DB::beginTransaction();      
            try{ 
                if($request->banner_type == 2){
                    $fromdate=$request->startdate;
                    $todate=$request->enddate;
                    $data=[
                        'product_id'=>$request->pro_id,
                        'banner_type'=>$request->banner_type,
                        'offer_date_from'=>$fromdate,
                        'offer_date_to'=>$todate,
                        'image'=>'images/banners/' . $request->pro_id . '/' . $imageName,
                        'status'=>$request->acc_status,
                        'pathayapura_banner'=>$request->pathayapura_banner,
                        'created_at'=>date('Y-m-d H:i:s'), 
                        'updated_at'=>date('Y-m-d H:i:s'), 
                    ]; 
                }else{
                    $data=[
                        'product_id'=>$request->pro_id,
                        'banner_type'=>$request->banner_type,
                        'offer_date_from'=>NULL,
                        'offer_date_to'=>NULL,
                        'image'=>'images/banners/' . $request->pro_id . '/' . $imageName,
                        'status'=>$request->acc_status,
                        'pathayapura_banner'=>$request->pathayapura_banner,
                        'created_at'=>date('Y-m-d H:i:s'), 
                        'updated_at'=>date('Y-m-d H:i:s'), 
                    ]; 
                }  
                $data= Banner::insert($data);
                DB::commit();
                return response()->json(['status' => 1]);
            }catch(\Exception $e){
                DB::rollback();
                print_r($e->getMessage());    
                throw $e;
                return response()->json(['status' => 0]);
            }
        }
        else{
            $fromdate=$request->startdate;
            $todate=$request->enddate;
            DB::beginTransaction();      
            try{ 
                if($request->banner_type == 2){
                    $fromdate=$request->startdate;
                    $todate=$request->enddate;
                    $data=[
                        'product_id'=>$request->pro_id,
                        'banner_type'=>$request->banner_type,
                        'offer_date_from'=>$fromdate,
                        'offer_date_to'=>$todate,
                        'status'=>$request->acc_status,
                        'pathayapura_banner'=>$request->pathayapura_banner,
                        'created_at'=>date('Y-m-d H:i:s'), 
                        'updated_at'=>date('Y-m-d H:i:s'), 
                    ]; 
                }else{
                    $data=[
                        'product_id'=>$request->pro_id,
                        'banner_type'=>$request->banner_type,
                        'offer_date_from'=>NULL,
                        'offer_date_to'=>NULL,
                        'status'=>$request->acc_status,
                        'pathayapura_banner'=>$request->pathayapura_banner,
                        'created_at'=>date('Y-m-d H:i:s'), 
                        'updated_at'=>date('Y-m-d H:i:s'), 
                    ]; 
                }  
                $data= Banner::where('id',$request->ban_val)->update($data);
                DB::commit();
                return response()->json(['status' => 1]);
            }catch(\Exception $e){
                DB::rollback();
                $data=$e->getMessage();    
                throw $e;
                return response()->json(['status' => 0,'message'=>$data]);
            }
        }     
    }   
    public function editProductBanner($id,Request $request){
        $query=Banner::query();
        $query->where('product_id',$id)->get();
        $banners=$query->get();
        $data=[
            'banners'=>$banners,
        ];
        return view('admin.banners.editbanner',$data);
    }
    public function updateProductBanner(Request $request){
        $rules = [
            'startdate'      => 'nullable',
            'enddate'        => 'nullable',
            'banner_type'    => 'required',
            'image'          => 'nullable',
            'pathayapura_banner'=> 'required',
            'acc_status'         => 'required',
        ];
        $messages = [
            // 'startdate.required'    => 'Start Date is required',
            // 'enddate.required'      => 'End Date is required',
            'banner_type.required'  => 'Banner Type is required',
            'pathayapura_banner.required'      => 'Pathayapura Banner is required',
            'acc_status.required'       =>  'Account status is required'
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            // return response()->json(['status' => 0, 'message' => $errors]);
            return response()->json(['status' => 0]);
        }
        if($request->image){
            $imageName = time() . '.' . $request->image->getClientOriginalExtension();
            request()->image->move(public_path('images/banners/' . $request->pro_val . '/'), $imageName);
            DB::beginTransaction();      
            try{ 
                if($request->banner_type == 2){
                    $fromdate=$request->startdate;
                    $todate=$request->enddate;
                    $data=[
                        'product_id'=>$request->pro_val,
                        'banner_type'=>$request->banner_type,
                        'offer_date_from'=>$fromdate,
                        'offer_date_to'=>$todate,
                        'image'=>'images/banners/' . $request->pro_val . '/' . $imageName,
                        'status'=>$request->acc_status,
                        'pathayapura_banner'=>$request->pathayapura_banner,
                        'updated_at'=>date('Y-m-d H:i:s'), 
                    ]; 
                }else{
                    $data=[
                        'product_id'=>$request->pro_val,
                        'banner_type'=>$request->banner_type,
                        'offer_date_from'=>NULL,
                        'offer_date_to'=>NULL,
                        'image'=>'images/banners/' . $request->pro_val . '/' . $imageName,
                        'status'=>$request->acc_status,
                        'pathayapura_banner'=>$request->pathayapura_banner,
                        'updated_at'=>date('Y-m-d H:i:s'), 
                    ]; 
                }  
                $data= Banner::where('id',$request->ban_val)->update($data);
                DB::commit();
                return response()->json(['status' => 1]);
            }catch(\Exception $e){
                DB::rollback();
                print_r($e->getMessage());    
                throw $e;
                return response()->json(['status' => 0]);
            }
        }
        else{
            $fromdate=$request->startdate;
            $todate=$request->enddate;
            DB::beginTransaction();      
            try{ 
                if($request->banner_type == 2){
                    $fromdate=$request->startdate;
                    $todate=$request->enddate;
                    $data=[
                        'product_id'=>$request->pro_val,
                        'banner_type'=>$request->banner_type,
                        'offer_date_from'=>$fromdate,
                        'offer_date_to'=>$todate,
                        'status'=>$request->acc_status,
                        'pathayapura_banner'=>$request->pathayapura_banner,
                        'updated_at'=>date('Y-m-d H:i:s'), 
                    ]; 
                }else{
                    $data=[
                        'product_id'=>$request->pro_val,
                        'banner_type'=>$request->banner_type,
                        'offer_date_from'=>NULL,
                        'offer_date_to'=>NULL,
                        'status'=>$request->acc_status,
                        'pathayapura_banner'=>$request->pathayapura_banner,
                        'updated_at'=>date('Y-m-d H:i:s'), 
                    ]; 
                }  
                $data= Banner::where('id',$request->ban_val)->update($data);
                DB::commit();
                return response()->json(['status' => 1]);
            }catch(\Exception $e){
                DB::rollback();
                $data=$e->getMessage();    
                throw $e;
                return response()->json(['status' => 0,'message'=>$data]);
            }
        }     
    }
}
