<?php

namespace App\Http\Controllers;
use App\Brand;
use App\Category;
use App\Manufacturer;
use App\TaxClass;
use App\WeightClass;
use App\LengthClass;

use App\TmpProduct;
use App\TmpProductImage;
use App\TmpProductAdditionalInfo;
use App\TmpProductFaq;
use App\TmpProductAttribute;

use App\Product;
use App\ProductImage;
use App\ProductAdditionalInfo;
use App\ProductFaq;
use App\ProductAttribute;
use App\FlashSale;
use App\DealOftheDay;
use App\Vendor;
use App\FlashsaleProducts;
use App\Notification;


use Illuminate\Http\Request;
use Validator;
use DB;

class DealOfTheDayController extends Controller
{
    public function showVendorDealOfTheDay(Request $request){
        $search = $request->search;
        $date=date('Y-m-d');    
        $query = DealOftheDay::query()->select('vendor_id');
        $query->selectRaw('COUNT(product_id) as product_count,MAX(product_id) as product_id');
        $query->with(['vendor'=>function($q){
            $q->select('company_name','id');
        }]);
        $query->groupBy('vendor_id');
        if ($search) {
            $query->whereHas('vendor',function ($sub) use ($search) {           
                $sub->Where('company_name', 'like', "%" . $search . "%");
            });
        }
        $details = $query->paginate(10);
        $data = [
            'details' => $details,
            'search' => $search,
        ];
            return view('admin.dealoftheday.list',$data);
    }
    public function vendorProductsDealOfTheDay($vendorid,Request $request){
        $search = $request->search;
        $date=date('Y-m-d');
        $query = DealOftheDay::query();
        $query->with('vendor');
        $query->with('product');
        $query->with('category');
        $query->where('vendor_id',$vendorid);
        if ($search) {
            $query->whereHas('product',function ($sub) use ($search) {
                $sub->Where('name', 'like', "%" . $search . "%");
            });
        }
        $query->orderBy('created_at','desc');
        $query->get();
        $details = $query->paginate(10);
        $data = [
            'details' => $details,
            'search' => $search,
        ];
            return view('admin.dealoftheday.vendorproducts',$data);
    }
    public function rejectProductsDealOfTheDay(Request $request){
        $query=DealOftheDay::query()->where([
            ['product_id',$request->id],
            ['vendor_id',$request->vendor_id],
        ])->first();
        $notification = new Notification;
        $data=array('route'=>'dealoftheday','vendor_id'=>$request->vendor_id);
        try{
            DB::beginTransaction();
            $query->approve=2;
            $query->save();
            $notification->added_by = Auth()->user()->id;
            $notification->added_to=$request->vendor_id;
            $notification->process="Product listed for deal of the day rejected by admin ".Auth()->User()->name.".";
            $notification->link="adqadqwdqdwqdw";
            $notification->admin_link= serialize($data);
            $notification->is_view=0;
            $notification->save();
            DB::commit();
            return response()->json(['status'=>true]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['status'=>false]);
            // throw $e;
            print_r($e->getMessage());  
        }
        return redirect()->back();
    }
    public function approveProductsDealOfTheDay(Request $request){
        $query=DealOftheDay::query()->where([
            ['product_id',$request->id],
            ['vendor_id',$request->vendor_id],
        ])->first();
        $notification = new Notification;
        $data=array('route'=>'dealoftheday','vendor_id'=>$request->vendor_id);
        try{
            DB::beginTransaction();
            $query->approve=1;
            $query->save();
            $notification->added_by = Auth()->user()->id;
            $notification->added_to=$request->vendor_id;
            $notification->process="Product listed for deal of the day approved by admin ".Auth()->User()->name.".";
            $notification->link="adqadqwdqdwqdw";
            $notification->admin_link= serialize($data);
            $notification->is_view=0;
            $notification->save();
            DB::commit();
            return response()->json(['status'=>true]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['status'=>false]);
            // throw $e;
            print_r($e->getMessage());  
        }
    }
    public function multipleVendorDealOfTheDay(Request $request){
        $datas=$request->checked;
            $val=$request->val;
            if($datas == "" || $val == ""){
                return response()->json(['status'=>false]);
            }
            try{
                foreach($datas as $id){
                    $fsp=DealOftheDay::where('vendor_id',$id);
                    DB::beginTransaction();
                        if($val == 0){
                            $fsp->update(['approve'=>2]);  
                        }
                        else{
                            $fsp->update(['approve'=>1]); 
                        }
                        DB::commit();
                }
                return response()->json(['status'=>true]);
            }catch(\Exception $e){
                        DB::rollback();
                        // throw $e;
                        return response()->json(['status'=>false]);
                        
            } 
    }
    public function multipleProductsDealOfTheDay(Request $request){
        $datas=$request->checked;
        $val=$request->val;
        try{
            foreach($datas as $id){
                $fsp=DealOftheDay::where('product_id',$id)->first();
                DB::beginTransaction();
                    if($val == 0){
                        $fsp->approve='2';
                        $fsp->save();   
                    }
                    else{
                        $fsp->approve='1';
                        $fsp->save();
                    }
                    DB::commit();
            }
            return response()->json(['status'=>true]);
        }catch(\Exception $e){
                    DB::rollback();
                    // throw $e;
                    print_r($e->getMessage());  
                    // return response()->json(["message"=>"error, try again"]);
                    
        } 
    }
}
