<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Admin;
use Illuminate\Support\Facades\Mail;
use App\Mail\AdminSendResetPassword;
use Session;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $guard = 'admin';
    protected $redirectTo = '/login';
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLogin(Request $request)
    {
        $locale = App::getLocale();
        return view('admin/login');
    }

    public function login(Request $request)
    {
        $rules = [
            'email'     => 'required',
            'password'    => 'required'
        ];
        $messages = [
            'email.required'    => 'Email is required',
            'password.required'   => 'Password is required',
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'errors' => $validator->errors()->first()]);
        }

        if (auth()->guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = auth()->guard('admin')->user();
            Session::put('isAdmin', $user->isAdmin);
            Session::put('name', $user->name);
            return response()->json(['status' => 1, 'user' => $user,'session'=>Session::get('isAdmin')]);
        } else {
            return response()->json(['status' => 0, 'errors' => ['Invalid Email / Password']]);
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->forget('isAdmin');
        $request->session()->forget('name');
        return redirect()->route('showLogin');
    }
    public function forgotPassword(Request $request)
    {
        $email = $request->email3;
        if ($email) {
            $user = Admin::where('email', '=', $request->email3)->first();
            if ($user) {
                $string = "";
                $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                $size = strlen($chars);
                for ($i = 0; $i < 6; $i++) {
                    $string .= $chars[rand(0, $size - 1)];
                }
                $password = $string;
                $user->password = bcrypt($password);
                try {
                    $user->save();
                    $data = array('password' => $password, 'name' => $user->name, 'email' => $user->email);
                    Mail::to($email)->send(new AdminSendResetPassword($data));
                    return response()->json(['status' => true, 'message' => 'New password has been sent to your email']);
                } catch (\Exception $e) {
                    print_r($e->getMessage());
                    $errors['email3'] = "Could not reset password";
                    return response()->json(['status' => 0, 'errors' => $errors]);
                }
            } else {
                return response()->json(['status' => false, 'errors' => ['email3' => 'Email entered is not registered with us']]);
            }
        } else {
            return response()->json(['status' => false, 'errors' => ['email3' => 'Email is not provided']]);
        }
    }
}
