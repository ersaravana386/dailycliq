<?php

namespace App\Http\Controllers;
use Auth;
use Validator;
use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function showProfile(Request $request){
        return view('admin.profile');
    }
    public function updateProfile(Request $request)
    {
        $rules = [
            'name' => "required",
            'mobile' => 'unique:admins,mobile,' . Auth::user()->id . "|required",
            'email' => 'unique:admins,email,' . Auth::user()->id . "|required|email",
        ];
        $messages = [
            'name.required' => "Name is required",
            'mobile.required' => "Mobile number is required",
            'mobile.unique' => "Mobile number is required",
            'email.required' => "Email is required",
            'email.unique' => "Email must be unique"
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $err = $messages->first($key);
                if ($err) {
                    $errors[$key] = $err;
                }
            }
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
        
        $admin = Admin::find($request->id);
        if ($admin) {
            $admin->name = $request->name;
            $admin->email = $request->email;
            $admin->mobile = $request->mobile;
            $imageName = null;
            if (request()->image) {
                try{
                    $imageName = time() . '.' . request()->image->getClientOriginalExtension();
                    request()->image->move(public_path('profiles/admin'), $imageName);
                    $admin->image = 'profiles/admin/' . $imageName;
                }catch (\Exception $e) {
                }
            }
            try {
                $admin->save();
                return response()->json(['status' => 1, 'message' => "Profile updated successfully!"]);
            } catch (\Exception $e) {
                $errors['couldnot_save'] = "Could not save profile";
                return response()->json(['status' => 0, 'errors' => $errors]);
            }
        }
        $errors['couldnot_save'] = "Something went wrong";
        return response()->json(['status' => 0, 'errors' => $errors]);
    }
    public function updateProfilePassword(Request $request){
        $rules = [
            'current_password' => "required",
            'new_password' => "required"
        ];
        $messages = [
            'current_password.required' => "Current password is required",
            'new_password.required' => "New password is required",
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if (!$validator->passes()) {
            $messages = $validator->messages();
            $errors = [];
            foreach ($rules as $key => $value) {
                $errors[$key] = $messages->first($key);
            }
            return response()->json(['status'=>0,'errors'=>$errors]);
        }

        $user = Admin::find($request->id);
        if(!$user){
            $errors['couldnot_find'] = "Could not find user";
            return response()->json(['status'=>0,'errors'=>$errors]);
        }
        if (Hash::check($request->current_password, $user->password)){
            $user->password = bcrypt($request->new_password);
        }else{
            $errors['couldnot_find'] = "Current Password invalid";
            return response()->json(['status'=>0,'errors'=>$errors]);
        }


        try {
            $user->save();
            return response()->json(['status'=>1,'message'=>"Password updated successfully!"]);
        }catch(\Exception $e) {
            $errors['couldnot_save'] = "Could not update Password";
            return response()->json(['status'=>0,'errors'=>$errors]);
        }
    }
}
