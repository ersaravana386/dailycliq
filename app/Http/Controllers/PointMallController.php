<?php

namespace App\Http\Controllers;

use App\PointMall;
use Illuminate\Http\Request;

class PointMallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PointMall  $pointMall
     * @return \Illuminate\Http\Response
     */
    public function show(PointMall $pointMall)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PointMall  $pointMall
     * @return \Illuminate\Http\Response
     */
    public function edit(PointMall $pointMall)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PointMall  $pointMall
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PointMall $pointMall)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PointMall  $pointMall
     * @return \Illuminate\Http\Response
     */
    public function destroy(PointMall $pointMall)
    {
        //
    }
}
