<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAdditionalInfo extends Model
{
    protected $guarded = [];
}
