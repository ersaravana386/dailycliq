<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryAddress extends Model
{
    protected $fillable = [
        'user_id','name','mobile_number','pin_code','address','city','state'
    ];
}
