<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Banner extends Model
{
    protected $guarded = [];
    protected $table="banners";
    public function product(){
        return $this->belongsTo('App\Product','poduct_id','id');
    }
}
