<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmpProductFaq extends Model
{
    public function product()
    {
        return $this->belongsTo('App\TmpProduct', 'temp_product_id', 'id');
    }
}
