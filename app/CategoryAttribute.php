<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryAttribute extends Model
{
    public function attribute_values()
    {
        return $this->hasMany('App\CategoryAttributeValue');
    }
}
