<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlashsaleProducts extends Model
{
    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }
    public function images()
    {
        return $this->hasMany('App\ProductImage', 'product_id', 'product_id');
    }

    public function flashsale(){
        return $this->belongsTo('App\FlashSale', 'flash_sale_id', 'id');
    }
    public function vendor()
    {
        return $this->belongsTo('App\Vendor', 'vendor_id', 'id');
    }
 
}
