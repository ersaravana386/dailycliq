<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function attributes()
    {
        return $this->hasMany('App\CategoryAttribute');
    }

    public function parent()
    {
        return $this->belongsTo('App\Category', 'parent_id','id');
    }
    public function childrens()
    {
        return $this->hasMany('App\Category', 'parent_id','id');
    }
    
    public function getPropertiesAttribute(){
        $categories = [];
        $category = $this;
        $categories[] = $this;
        $i = 0;
        // do {
        //     $i++;
        //     $category = $category->parent;
        //     if ($category) {
        //         $categories[] = $category;
        //     }
        // } while (isset($category->id));
        $categories = array_reverse($categories);
        $attributes = [];        
        for ($i = 0; $i < count($categories); $i++) {
            $temp_attributes = $categories[$i]->attributes()->get();
            foreach($temp_attributes as $attribute)
            {
                // $attributes[$attribute->attribute]['category'] = $attribute->category_id;
                $attributes[$attribute->attribute]['searchable'] = $attribute->searchable;
                $temp_values = $attribute->attribute_values->toArray();                
                foreach($temp_values as $value){
                    $attributes[$attribute->attribute]['values'][] =  $value['value'];
                } 
            }
        }
        return $attributes;
    } 

    public static function SubCategories($category){
        $category = Category::find($category);
        if($category){
            $category_ids = [];
            $subcategories = $category->childrens->toArray();
            foreach($subcategories as $sub){
                $category_ids[] = $sub['id'];
                $childs = Category::SubCategories($sub['id']);
                foreach($childs as $child){
                    $category_ids[] = $child;
                }
            }
            return $category_ids;exit;
        }else{
            return [];
        }
    }
}
