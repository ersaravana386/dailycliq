<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class SendEmailUs extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $attachment;

    public function __construct($attachment,$subject,$data)
    {
         $this->attachment = $attachment; 
         $this->subject = $subject;
         $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
   
        $name = 'DailyCliq';
        return $this->view('admin.emails.sendEmailus')
                    ->subject($this->subject)
                    ->attach($this->attachment)
                    ->with(['enquiry_message1' => $this->data['message']])
                    ->with(['enquiry_message2' => $this->data['phone']])
                    ->with(['enquiry_message3' => $this->data['email']]);
                    
    }
}
