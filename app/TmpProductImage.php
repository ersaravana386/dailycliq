<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmpProductImage extends Model
{
    protected $guarded = [];
}
