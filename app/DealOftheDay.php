<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DealOftheDay extends Model
{
    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }
    public function images()
    {
        return $this->hasMany('App\ProductImage', 'product_id', 'product_id');
    }
    public function vendor(){
        return $this->belongsTo('App\Vendor','vendor_id','id');
    }
    public function category(){
        return $this->belongsTo('App\Category','category_id','id');
    }
 
}
