<?php

use Illuminate\Database\Seeder;

class DistrictTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            ['name'=> 'Andhra Pradesh', 'state_id' => ''],
            ['name'=> 'Andhra Pradesh', 'state_id' => ''],
            ['name'=> 'Andhra Pradesh', 'state_id' => ''],
            ['name'=> 'Andhra Pradesh', 'state_id' => ''],
            ['name'=> 'Andhra Pradesh', 'state_id' => ''],
            ['name'=> 'Andhra Pradesh', 'state_id' => ''],
            ['name'=> 'Andhra Pradesh', 'state_id' => ''],
            ['name'=> 'Andhra Pradesh', 'state_id' => ''],
            ['name'=> 'Andhra Pradesh', 'state_id' => ''],
            ['name'=> 'Andhra Pradesh', 'state_id' => ''],
            ['name'=> 'Andhra Pradesh', 'state_id' => ''],
            ['name'=> 'Andhra Pradesh', 'state_id' => ''],
            ['name'=> 'Andhra Pradesh', 'state_id' => ''],
        ];
        DB::table('districts')->insert($rows);
    }
}
