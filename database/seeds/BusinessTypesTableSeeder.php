<?php

use Illuminate\Database\Seeder;

class BusinessTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            ['name' => 'None I am an individual' ],
            ['name' => 'State-owned business' ],
            ['name' => 'Publicity listed bussiness' ],
            ['name' => 'Privately-owned business' ],
            ['name' => 'Charity' ]
        ];
        DB::table('business_types')->insert($rows);
    }
}
