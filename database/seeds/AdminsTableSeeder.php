<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name' => 'Saleesh',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456'),
            'mobile' => '9747282318',
            'status' => '1'
       ]);
    }
}
