<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            ['name'=> 'Andhra Pradesh'],
            ['name'=> 'Arunachal Pradesh'],
            ['name'=> 'Assam'],
            ['name'=> 'Bihar'],
            ['name'=> 'Chhattisgarh'],
            ['name'=> 'Delhi'],
            ['name'=> 'Goa'],
            ['name'=> 'Gujarat'],
            ['name'=> 'Haryana'],
            ['name'=> 'Himachal Pradesh'],
            ['name'=> 'Jammu & Kashmir'],
            ['name'=> 'Jharkhand'],
            ['name'=> 'Karnataka'],
            ['name'=> 'Kerala'],
            ['name'=> 'Madhya Pradesh'],
            ['name'=> 'Maharashtra'],
            ['name'=> 'Manipur'],
            ['name'=> 'Meghalaya'],
            ['name'=> 'Mizoram'],
            ['name'=> 'Nagaland'],
            ['name'=> 'Odisha'],
            ['name'=> 'Punjab'],
            ['name'=> 'Rajasthan'],
            ['name'=> 'Sikkim'],
            ['name'=> 'Tamil Nadu'],
            ['name'=> 'Telangana'],
            ['name'=> 'Tripura'],
            ['name'=> 'Uttar Pradesh'],
            ['name'=> 'Uttarakhand'],
            ['name'=> 'West Bengal']
        ];
        DB::table('states')->insert($rows);
    }
}
