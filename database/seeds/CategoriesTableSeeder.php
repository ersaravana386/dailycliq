<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            ['name'=> 'Pathayappura','parent_id' => '0',],
            ['name'=> 'Mobiles & Accessories','parent_id' => '0',],
            ['name'=> 'Computers & Accessories','parent_id' => '0',],
            ['name'=> 'Electronics','parent_id' => '0',],
            ['name'=> "Men's Fashion",'parent_id' => '0',],
            ['name'=> "Women's Fashion",'parent_id' => '0',],
            ['name'=> 'Baby & Kids','parent_id' => '0',],
            ['name'=> 'Home & Kitchen','parent_id' => '0',],
            ['name'=> 'Beauty, Health & Diet','parent_id' => '0',],
            ['name'=> 'Sports & Fitness','parent_id' => '0',],
            ['name'=> 'Automobile','parent_id' => '0',],
            ['name'=> 'Industrial & Scientific Supplies','parent_id' => '0',],
            ['name'=> 'Food & Beverages','parent_id' => '0',],
            ['name'=> 'Books ','parent_id' => '0',],
            ['name'=> 'Movies, Music & Video Games','parent_id' => '0',],
            ['name'=> 'School Supplies & Stationery','parent_id' => '0',]
        ];
        DB::table('categories')->insert($rows);
    }
}
