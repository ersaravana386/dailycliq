<?php

use Illuminate\Database\Seeder;

class VendorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vendors')->insert([
            'code' =>'1558159035490',
            'company_name' => 'Shoprix',
            'contact_person' => 'Lithin Km',
            'email' => 'lithin.km@iroidtechnologies.com',
            'password' => bcrypt('123456'),
            'mobile' => '9747282318',
            'phone' => '04602238207',
            'address' => 'Nh-47, Thaliparamba',
            'status' => '1'
       ]);
    }
}
