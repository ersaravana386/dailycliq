<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 150);
            $table->longText('description')->nullable();
            $table->mediumText('image')->nullable();
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->integer('top')->default(1);
            $table->integer('sort_order')->default(1);
            $table->boolean('active')->default(1);
            $table->timestamps();

            //indexes
            $table->index('parent_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
