<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateToTempProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tmp_products', function (Blueprint $table) {
            $table->float('weight', 15, 2)->nullable()->after('cod');
            $table->unsignedBigInteger('weight_class_id')->nullable()->after('weight');
            $table->float('length', 15, 2)->nullable()->after('weight_class_id');
            $table->float('width', 15, 2)->nullable()->after('length');
            $table->float('height', 15, 2)->nullable()->after('width');
            $table->unsignedBigInteger('length_class_id')->nullable()->after('height');
            $table->integer('shipping_processing_time')->nullable()->after('length_class_id');
            $table->boolean('free_delivery')->default(0)->after('shipping_processing_time');
            $table->unsignedBigInteger('vendor_id')->nullable()->after('free_delivery');

            $table->index('weight_class_id');
            $table->index('length_class_id');
            $table->index('vendor_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tmp_products', function (Blueprint $table) {
            $table->dropIndex('tmp_products_weight_class_id_index');
            $table->dropIndex('tmp_products_length_class_id_index');
            $table->dropIndex('tmp_products_vendor_id_index');
            $table->dropColumn('weight');
            $table->dropColumn('weight_classes_id');
            $table->dropColumn('length');
            $table->dropColumn('width');
            $table->dropColumn('height');
            $table->dropColumn('length_classes_id');
            $table->dropColumn('shipping_processing_time');
            $table->dropColumn('free_delivery');
            $table->dropColumn('vendor_id');
        });
    }
}
