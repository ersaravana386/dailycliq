<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDescriptionTaxClasses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tax_classes', function (Blueprint $table) {
            $table->mediumText('description')->nullable()->change();
            $table->boolean('active')->after('description')->default(1);
            $table->unsignedInteger('sort_order')->after('active')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tax_classes', function (Blueprint $table) {
            $table->dropColumn('active');
            $table->dropColumn('sort_order');
        });
    }
}
