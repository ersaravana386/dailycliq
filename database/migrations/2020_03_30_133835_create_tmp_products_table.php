<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmpProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('variant')->nullable();
            $table->unsignedBigInteger('vendor_id')->nullable();
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('weight_class_id')->nullable();
            $table->unsignedBigInteger('length_class_id')->nullable();

            $table->string('name',150)->nullable();
            $table->mediumText('description')->nullable();
            $table->string('dcin',50)->nullable();
            $table->string('hsn',50)->nullable();
            $table->string('ean',50)->nullable();
            $table->string('upc',50)->nullable();
            
            $table->string('other_brand')->nullable();
            $table->boolean('pathayapura_listing')->default(0)->nullable();
            $table->mediumText('warranty_details')->nullable();
            $table->float('gst')->nullable();
            $table->boolean('sess')->default(0)->nullable();
            $table->float('price')->nullable();
            $table->float('mrp')->nullable();
            $table->float('wholesale')->nullable();
            $table->integer('order_quantity')->nullable();
            $table->boolean('cod')->default(0);
            $table->float('commission')->nullable();
            $table->float('weight')->nullable();
            $table->float('length')->nullable();
            $table->float('width')->nullable();
            $table->float('height')->nullable();
            $table->integer('shipping_processing_time')->nullable();
            $table->mediumText('return_policy')->nullable();
            $table->boolean('free_delivery')->default(0);

            $table->mediumText('admin_comments')->nullable();
            $table->integer('step')->default(0);
            $table->boolean('completed_status')->default(0);
            $table->boolean('approved')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_products');
    }
}
