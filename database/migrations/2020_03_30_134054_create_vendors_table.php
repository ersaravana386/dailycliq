<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code',30);
            $table->string('company_name',80);
            $table->string('store_name',80);
            $table->unsignedBigInteger('business_type');
            $table->string('contact_person',800);
            $table->string('email',100);
            $table->string('mobile',20);
            $table->string('phone',20);
            $table->text('address');
            $table->string('pin',20);
            $table->string('location',20);
            $table->float('latitude');
            $table->float('longitude');
            $table->string('image',80);
            $table->string('pancard',80);
            $table->string('id_proof',80);
            $table->string('trade_license_number',50);
            $table->string('trade_license_document');
            $table->string('gst_number',50);
            $table->string('fssai_number',20);
            $table->string('pan_number');
            $table->string('gst_document');
            $table->string('name_as_in_bank');
            $table->string('account_type',20);
            $table->string('account_number',25);
            $table->string('ifsc_code',25);
            $table->string('cancel_cheque');
            $table->string('otp',10);
            $table->timestamp('email_verified_at');
            $table->string('reset_token',20);
            $table->dateTime('reset_date');
            $table->string('remember_token',100);
            $table->boolean('status')->default(1);
            $table->boolean('active')->default(1);
            $table->boolean('approved')->default(0);
            $table->float('star')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
