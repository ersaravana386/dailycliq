<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_reference_number',500)->unique();
            $table->bigInteger('customer_id');
            $table->bigInteger('payment_method_id')->nullable();
            $table->bigInteger('status');
            $table->timestamp('order_at')->nullable();
            $table->timestamp('paid_at')->nullable();
            $table->bigInteger('shipping_address_id')->nullable();
            $table->bigInteger('billing_address_id')->nullable();
            $table->decimal('total', 15, 8)->nullable();
            $table->text('admin_comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
