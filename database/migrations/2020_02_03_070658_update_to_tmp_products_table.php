<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateToTmpProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tmp_products', function (Blueprint $table) {
            $table->decimal('gst', 15, 2)->after('pathayapura_listing')->nullable()->unsigned();
            $table->boolean('sess')->default(0)->after('gst')->nullable()->unsigned();
            $table->decimal('price', 15, 2)->after('sess')->nullable()->unsigned();
            $table->decimal('mrp', 15, 2)->after('price')->nullable()->unsigned();
            $table->decimal('wholesale', 15, 2)->after('mrp')->nullable()->unsigned();
            $table->bigInteger('wholesale_unit')->after('wholesale')->unsigned()->nullable();
            $table->boolean('cod')->default(0)->after('wholesale_unit')->nullable();
            $table->boolean('completed_status')->default(0)->after('cod');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tmp_products', function (Blueprint $table) {
            $table->dropColumn('gst');
            $table->dropColumn('sess');
            $table->dropColumn('price');
            $table->dropColumn('mrp');
            $table->dropColumn('wholesale');
            $table->dropColumn('wholesale_unit');
            $table->dropColumn('cod');
        });
    }
}
