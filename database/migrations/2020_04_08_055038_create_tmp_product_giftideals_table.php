<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmpProductGiftidealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_product_giftideals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tmp_product_id');
            $table->bigInteger('tmp_vendor_id');
            $table->bigInteger('tmp_gift_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_product_giftideals');
    }
}
