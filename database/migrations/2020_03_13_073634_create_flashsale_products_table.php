<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlashsaleProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flashsale_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('flash_sale_id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('vendor_id');
            $table->float('offer_amount', 15, 2)->nullable();
            $table->unsignedBigInteger('offer_percentage');
            $table->unsignedBigInteger('unit');
            $table->unsignedInteger('active_status');
            $table->unsignedInteger('approve');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flashsale_products');
    }
}
