<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LengthClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('length_classes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',30);
            $table->double('value')->nullable();
            $table->integer('unit')->nullable();
            $table->boolean('active')->default(1);
            $table->integer('sort_order')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('length_classes');
    }
}
