<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FlashsaleProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flashsale_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('flash_sale_id')->nullable();
            $table->unsignedBigInteger('product_id')->nullable();
            $table->unsignedBigInteger('vendor_id')->nullable();
            $table->double('offer_amount')->nullable();
            $table->double('offer_percentage')->nullable();
            $table->boolean('active_status')->default(1);
            $table->boolean('approve')->default(0);
            $table->timestamps();

            //indexes
            $table->index('flash_sale_id');
            $table->index('product_id');
            $table->index('vendor_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flashsale_products');
    }
}
