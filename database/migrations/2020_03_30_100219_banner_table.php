<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BannerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('banner_type');
            $table->unsignedBigInteger('product_id')->nullable();
            $table->text('image')->nullable();
            $table->dateTime('offer_date_from')->nullable();
            $table->dateTime('offer_date_to')->nullable();
            $table->boolean('status')->default(1);
            $table->boolean('pathayapura_banner')->default(0);
            $table->timestamps();

            //indexes
            $table->index('banner_type');
            $table->index('product_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
