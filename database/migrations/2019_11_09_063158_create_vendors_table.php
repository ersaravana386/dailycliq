<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code',30);
            $table->string('company_name',80)->nullable();
            $table->string('store_name',80)->nullable();;
            $table->string('contact_person',80)->nullable();
            $table->string('email',100);
            $table->string('mobile',20);
            $table->string('phone',20)->nullable();
            $table->text('address')->nullable();
            $table->string('location',20)->nullable();
            $table->string('latitude',20)->nullable();
            $table->string('longitude',20)->nullable();
            $table->string('image',80)->default('/profiles/default.png');
            $table->string('pancard',80)->nullable();
            $table->string('id_proof',80)->nullable();
            $table->string('trade_license_number',50)->nullable();
            $table->string('trade_license_document',255)->nullable();
            $table->string('gst_number',50)->nullable();
            $table->string('gst_document',255)->nullable();
            $table->string('name_as_in_bank',255)->nullable();
            $table->string('account_type',20)->nullable();
            $table->string('account_number',25)->nullable();
            $table->string('ifsc_code',25)->nullable();
            $table->string('cancel_cheque',255)->nullable();
            $table->string('otp',10)->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password',100);
            $table->string('reset_token',20)->nullable();
            $table->datetime('reset_date')->nullable();
            $table->rememberToken();
            $table->boolean('status')->default(true);
            $table->boolean('active')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
