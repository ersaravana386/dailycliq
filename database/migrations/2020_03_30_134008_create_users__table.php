<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name', 50)->nullable();
            $table->string('last_name', 50)->nullable();
            $table->string('email')->nullable();
            $table->string('mobile', 30)->nullable();
            $table->enum('gender', ['m', 'f'])->nullable();
            $table->date('dob')->nullable();
            $table->string('mobile_otp', 4)->nullable();
            $table->string('password')->nullable();
            $table->string('api_token', 80)->unique()
                ->nullable()
                ->default(null);
            $table->string('image',150)->nullable();       
            $table->timestamp('email_verified_at')->nullable();  
            $table->string('remember_token',100)->nullable(); 
            $table->boolean('new_user')->default(1);
            $table->boolean('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
