<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmpProductFreequentlyBroughtTogethersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_product_freequently_brought_togethers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tmp_product_1');
            $table->bigInteger('tmp_product_2');
            $table->bigInteger('tmp_product_3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_product_freequently_brought_togethers');
    }
}
