<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmpProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('variant', 50)->nullable();
            $table->string('dcin', 50);
            $table->string('ean', 50)->nullable();
            $table->string('upc', 50)->nullable();
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->string('other_brand', 50)->nullable();
            $table->unsignedBigInteger('category_id');
            $table->boolean('pathayapura_listing')->default(0);
            $table->timestamps();

            $table->index('brand_id');
            $table->index('category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_products');
    }
}
