<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->string('email')->nullable()->change();
            $table->string('first_name',50)->after('id')->nullable();
            $table->string('last_name',50)->after('first_name')->nullable();
            $table->string('mobile',30)->after('email')->nullable();
            $table->string('mobile_otp',4)->after('mobile')->nullable();
            $table->enum('gender',['m','f'])->after('mobile')->nullable();
            $table->date('dob')->after('gender')->nullable();
            $table->string('api_token', 80)->after('password')
                        ->unique()
                        ->nullable()
                        ->default(null);
            $table->softDeletes();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('name')->nullable();
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('mobile');
            $table->dropColumn('mobile_otp');
            $table->dropColumn('gender');
            $table->dropColumn('dob');
            $table->dropColumn('api_token');
            $table->dropSoftDeletes();   
        });
    }
}
