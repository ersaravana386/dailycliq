<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id')->nullable();
            $table->unsignedBigInteger('order_id')->nullable();
            $table->integer('unit')->nullable();
            $table->double('mrp')->nullable();
            $table->double('selling_price')->nullable();
            $table->integer('offer_type_id')->nullable();
            $table->float('offer_percentage')->nullable();
            $table->double('offer_amount')->nullable();
            $table->double('grand_total_without_offer')->nullable();
            $table->double('net_total')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_products');
    }
}
