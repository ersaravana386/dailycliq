<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('payment_ref_id')->nullable();
            $table->unsignedBigInteger('order_id')->nullable();
            $table->string('payment_type')->nullable();
            $table->string('bank',100)->nullable();
            $table->string('account_number',100)->nullable();
            $table->string('ifsc',100)->nullable();
            $table->string('card_holder_name',100)->nullable();
            $table->string('card_expiry',100)->nullable();
            $table->double('amount')->nullable();
            $table->string('gateway_ref_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_payments');
    }
}
