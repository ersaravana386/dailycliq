<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TmpProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tmp_products', function (Blueprint $table) {
            $table->string('name')->nullable()->after('id');
            $table->integer('step')->default(0)->after('completed_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tmp_products', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('step');
        });
    }
}
