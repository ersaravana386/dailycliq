<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFieldsLengthClasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('length_classes', function (Blueprint $table) {
            $table->boolean('active')->after('unit')->default(1);
            $table->unsignedInteger('sort_order')->after('active')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('length_classes', function (Blueprint $table) {
            $table->dropColumn('active');
            $table->dropColumn('sort_order');
        });
    }
}
