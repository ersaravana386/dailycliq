<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('weight_class_id')->nullable();
            $table->unsignedBigInteger('length_class_id')->nullable();
            $table->unsignedBigInteger('vendor_id')->nullable();
            $table->unsignedBigInteger('variant')->nullable();

            $table->string('name',150)->nullable();            
            $table->string('dcin',50)->nullable();
            $table->string('hsn',50)->nullable();
            $table->string('ean',50)->nullable();
            $table->string('upc',50)->nullable();            
            $table->string('other_brand',100)->nullable();           
            $table->boolean('pathayapura_listing')->default(0);
            $table->mediumText('warranty_details')->nullable();
            
            $table->float('gst')->nullable();
            $table->boolean('sess')->default(0);
            $table->float('price')->nullable();
            $table->float('mrp')->nullable();
            $table->integer('order_quantity')->nullable();
            $table->integer('stock_unit')->nullable();
            $table->boolean('cod')->default(0);
            $table->float('weight')->nullable();
            $table->float('length')->nullable();
            $table->float('width')->nullable();
            $table->float('height')->nullable();
            $table->mediumText('return_policy')->nullable();
            $table->integer('shipping_processing_time')->nullable();
            $table->boolean('free_delivery')->default(0);
            
            $table->float('star')->nullable();
            $table->boolean('assured')->default(0);
            $table->boolean('featured_product')->default(0);
            
            $table->boolean('completed_status')->default(1);
            $table->boolean('approved')->default(1);
            $table->integer('step')->default(0);
            $table->longText('description')->nullable();
            $table->mediumText('admin_comments')->nullable();
<<<<<<< HEAD
=======

            //Murugan Start
            $table->integer('store_99')->default(0);
            $table->integer('hot_deal')->default(0);
            $table->integer('coupon')->default(0);
            $table->integer('discount')->default(0);

            // Murugan End 
>>>>>>> 744b282750c17b2bbe717c7f4eeed34a2ae0051e
            $table->timestamps();
            $table->softDeletes();

            //indexes
            $table->index('brand_id');
            $table->index('category_id');
            $table->index('weight_class_id');
            $table->index('length_class_id');
            $table->index('vendor_id');
            $table->index('variant');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
