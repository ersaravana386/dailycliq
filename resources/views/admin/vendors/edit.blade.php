@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
    <a class="breadcrumb-item" href="{{route('showVendors')}}">Vendors</a>
    <span class="breadcrumb-item active">Edit</span>
  </nav>
</div><!-- br-pageheader -->

<div class="br-pagebody">
    <div class="br-section-wrapper">
      <h6 class="br-section-label">Edit Vendor</h6><br>
      <form id='vendorForm' method="POST" action="javascript:;" data-parsley-validate>
          @method('POST')
        <div class="form-layout form-layout-1">
          <div class="row mg-b-25">
            <div class="col-lg-6 ">
              <div class="form-group pd-5">
                <label class="form-control-label">Company name: <span class="tx-danger">*</span></label>
              <input class="form-control" type="text" value='{{$vendor->company_name}}' name="company_name" id="company_name"  placeholder="Enter company name">
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-6">
              <div class="form-group pd-5">
                <label class="form-control-label">Contact person: <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" value="{{$vendor->contact_person}}" name="contact_person" id="contact_person" placeholder="Enter contact person name">
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-6">
                <div class="form-group pd-5">
                  <label class="form-control-label">Address: <span class="tx-danger">*</span></label>
                  <textarea rows="3" class="form-control" name="address" id="address" placeholder="Enter address">{{$vendor->address}}
                  </textarea>
                </div>
              </div><!-- col-4 -->
            <div class="col-lg-6">
              <div class="form-group pd-5">
                <label class="form-control-label">Email: <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="email" id="email" value="{{$vendor->email}}" placeholder="Enter email address">
              </div>
            </div><!-- col-8 -->
            <div class="col-lg-6">
              <div class="form-group pd-5">
                <label class="form-control-label">Mobile: <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="mobile" id="mobile" value="{{$vendor->mobile}}" placeholder="Enter mobile">
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-6">
              <div class="form-group pd-5">
                <label class="form-control-label">Phone: <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="phone" id="phone" value="{{$vendor->phone}}" placeholder="Enter phone">
              </div>
            </div><!-- col-4 -->
            
              <div class="col-lg-6">
                <div class="form-group pd-5">
                  <label class="form-control-label">Trade license number: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="trade_license_number" value="{{$vendor->trade_license_number}}" id="trade_license_number" placeholder="Enter trade license number">
                </div>
              </div><!-- col-4 -->
              
              <div class="col-md-6">
                <div class="form-group pd-5">
                  <label class="form-control-label">Trade license document: <span class="tx-danger">*</span></label>
                  <img src="{{asset($vendor->trade_license_document)}}" class="img-fluid wd-100" alt="" id="trade_document_2">
                  <img id="trade_document_1" src="#" alt="your image" style="width:80px;"/>
                  <label for="trade_license_document" class="if-outline if-outline-info"><i class="icon ion-ios-upload-outline tx-24"></i>
                  <input type="file" name="trade_license_document" id="trade_license_document" class="inputfile">
                  <span id="trade_license_document_label">Change file</span>
                  </label>
                </div>
              </div><!-- col-4-->
              <div class="col-lg-6">
                <div class="form-group pd-5">
                  <label class="form-control-label">FSSAI Lic.No: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="fssai_number" value="{{$vendor->fssai_number}}" id="fssai_number" placeholder="Enter FSSAI number">
                </div>
              </div><!-- col-4 -->     
              <div class="col-md-6">
                <div class="form-group pd-5">
                  <label class="form-control-label">GST Document: <span class="tx-danger">*</span></label>
                  <img src="{{asset($vendor->gst_document)}}" class="img-fluid wd-100" alt="">
                  <label for="gst_document" class="if-outline if-outline-info"><i class="icon ion-ios-upload-outline tx-24"></i>
                  <input type="file" name="gst_document" id="gst_document" class="inputfile" >
                </div>
                  <label class="form-control-label">GST Number: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="gst_number" value="{{$vendor->gst_number}}" id="gst_number" placeholder="Enter GST number">
                </div>
              </div><!-- col-4 -->
              <div class="col-md-6">
                <div class="form-group pd-5">
                  <label class="form-control-label">GST Document: <span class="tx-danger">*</span></label>
                  <img src="{{asset($vendor->gst_document)}}" class="img-fluid wd-100" alt="" id="gst_document_2">
                  <img id="gst_document_1" src="#" alt="your image" style="width:80px;"/>
                  <label for="gst_document" class="if-outline if-outline-info"><i class="icon ion-ios-upload-outline tx-24"></i>
                  <input type="file" name="gst_document" id="gst_document" class="inputfile">
                  <span id="gst_document_label">Change file</span>
                  </label>
                </div>
              </div><!-- col-4-->
              <div class="col-md-6">
                <div class="form-group pd-5">
                  <label class="form-control-label">Vendor Image: </label>
                  <img src="{{asset($vendor->image)}}" class="img-fluid wd-100" alt="" id="vendor_document_2">
                  <img id="vendor_document_1" src="#" alt="your image" style="width:80px;"/>
                  <label for="image" class="if-outline if-outline-info"><i class="icon ion-ios-upload-outline tx-24"></i>
                  <input type="file" name="image" id="image" class="inputfile" >
                  <span id="image_label">Change file</span>
                  </label></div>
              </div><!-- col-4-->
          </div><!-- row -->

          <div class="form-layout-footer">
          <input type="hidden" name="code" value="{{$vendor->code}}">
            <input type="submit" class="btn btn-info mg-b-10 " value="Update">
            <a class="btn btn-secondary mg-b-10 " href="{{route('showVendors')}}">Cancel</a>
          </div><!-- form-layout-footer -->
              <div class="msgDiv"></div>
        </div><!-- form-layout -->
      </form>

     
    </div><!-- br-section-wrapper -->
  </div><!-- br-pagebody -->
@endsection
@section('scripts')
<script>
    //profile form
    $("#vendorForm").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        rules: {
            company_name: {
              required: true
            },
            contact_person: {
              required: true
            },
            address:{
              required: true
            },
            email: {
              required: true,
              email: true
            },
            mobile: {
              required: true,
              number:true,
            },
            phone: {
              required: true,
              number:true,
            },
            trade_license_number: {
              required: true
            }
        },
        messages: {
            company_name: {
              required: "Company name is required"
            },
            contact_person: {
              required: "Contact person name is required"
            },
            address:{
              required: "Address is required"
            },
            email: {
              required: "Email is required",
              email: "Email is not valid"
            },
            mobile: {
              required: "Mobile is required",
              number: "Invalid format"
            },
            phone: {
              required: "phone number is required",
              number: "Invalid format"
            },
            trade_license_number: {
              required: "Trade license number is required"
            },
        },
        submitHandler: function(form) {
            $('.msgDiv').html(``);
            var data = new FormData(form);
            $.ajax({
                type: "post",
                url: "{{route('updateVendor')}}",
                data: data,
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.status == 1) {
                        
                        $('.msgDiv').html(` <div class="alert alert-success">` + data.message + `</div>`);
                        var scrollPos =  $(".br-pagebody").offset().top;
                        $(window).scrollTop(scrollPos);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                            location.href = "{{route('showVendors')}}";
                        }, 2000);

                    } else {
                        var html = "";
                        $.each(data.errors, function(key, value) {
                            html += value + "<br/>";
                        });
                        $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                        var scrollPos =  $(".br-pagebody").offset().top;
                        $(window).scrollTop(scrollPos);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                        }, 4000);
                    }
                }
            });
            return false;
        }
    });
</script>
 <script>
    $('#trade_license_document').on('change', function() {
    var fileName = $(this).val();
    //replace the "Choose a file" label
    $('#trade_license_document_label').html(fileName);
    })
    $('#gst_document').on('change', function() {
    var fileName = $(this).val();
    //replace the "Choose a file" label
    $('#gst_document_label').html(fileName);
    })
    $('#image').on('change', function() {
    var fileName = $(this).val();
    //replace the "Choose a file" label
    $('#image_label').html(fileName);
    })

    //menu active
    $('#vendor_menu').find('a').addClass('active');
</script>
<script>
$(document).ready(function(){
  $('#gst_document_1').hide();
  $('#trade_document_1').hide();
  $('#vendor_document_1').hide();
});
$("#gst_document").change(function() {
  readURL(this);
});
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#gst_document_1').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
    $('#gst_document_1').show();
    $('#gst_document_2').hide();
  }
}
$("#trade_license_document").change(function() {
  readURL1(this);
});
function readURL1(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#trade_document_1').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
    $('#trade_document_1').show();
    $('#trade_document_2').hide();
  }
}
$("#image").change(function() {
  readURL2(this);
});
function readURL2(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#vendor_document_1').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
    $('#vendor_document_1').show();
    $('#vendor_document_2').hide();
  }
}
</script>
@endsection