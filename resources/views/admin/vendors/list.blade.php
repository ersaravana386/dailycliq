@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-10">
    <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
    <span class="breadcrumb-item active">Vendors</span>
  </nav>
</div><!-- br-pageheader -->
<div class="br-pagetitle">
  <i class="icon icon ion-ios-bookmarks-outline"></i>
  <div>
    <h4>Vendors</h4>
    <p class="mg-b-0">Vendor Listing</p>
  </div>
</div><!-- d-flex -->

<div class="br-pagebody">
  <div class="br-section-wrapper">

    <div id="datatable1_filter" class="dataTables_filter">
      <div class="row">
        <div class="col-lg-9">
          <form action="{{ route('showVendors') }}" method="get">
            <div class="input-group input-group-md mb-3 col-md-4">
              <input type="text" name="search" value="{{$search}}" placeholder="search" class="form-control" value="">
              <div class="input-group-append">
                <span class="input-group-btn">
                  <button class="btn btn-default input-group-close-icon" type="submit">
                    <i class="fa fa-search"></i>
                  </button>
                </span>
              </div>
            </div>
          </form>
        </div>
        <div class="col-lg-2">
          {{-- <div class="input-group input-group-md mb-3 col-md-4">
              <a href="{{route('showNewVendors')}}" class="btn btn-teal btn-with-icon" style="float-left" >
          <div class="ht-40 justify-content-between">
            <span class="icon wd-40"><i class="fa fa-plus"></i></span>
            <span class="pd-x-15">Add New Vendor</span>
          </div>
          </a>
        </div> --}}
      </div>
    </div>
  </div>

  <div class="col-lg-12 p-r-0 title-margin-right " id="msgShowDiv">
    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block sessionDiv">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
    @endif
  </div>
  <div class="bd bd-gray-300 rounded table-responsive">
    <table class="table mg-b-0">
      <thead>
        <tr>
          <th class="wd-5p">#</th>
          <th class="wd-20p">Company name</th>
          <th class="wd-20p">Contact person</th>
          <th class="wd-15p">E-mail</th>
          <th class="wd-10p">Mobile</th>
          <th class="wd-15p">Address</th>
          <th class="wd-20p">Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($vendors as $key=> $item)
        <tr>
          <th scope="row">{{$vendors->firstItem()+$key}}</th>
          <td>{{$item->company_name}}</td>
          <td>{{$item->contact_person}}</td>
          <td>{{$item->email}}</td>
          <td>{{$item->mobile}}</td>
          <td>{{$item->address}}</td>
          <td>
            <div class="btn-group" role="group" aria-label="Basic example">
              <a href="javascript:;" onclick="sendInvite({{$item->code}})" data-toggle="modal" data-target="#inviteModal" class=" btn btn-outline-teal" title="Send Invitation" id="sendInvite">
                <i class="menu-item-icon fa fa-paper-plane"></i>
              </a>
              <a href="{{route('showEditVendor',['code'=>$item->code])}}" class=" btn btn-outline-teal" title="edit">
                <i class="menu-item-icon fa fa-edit"></i>
              </a>
              <a href="#" class=" btn btn-outline-teal" title="view" data-toggle="modal" data-target="#viewModal{{$item->code}}">
                <i class="menu-item-icon fa fa-eye"></i>
              </a>
              @if($item->active == 1)
              <a href="javascript:;" onclick="blockToggle({{$item->code}},'{{ $item->company_name }}',{{$item->active}})" class=" btn btn-outline-teal" title="Block">
                <i class="menu-item-icon fa fa-lock"></i>
              </a>
              @else
              <a href="javascript:;" onclick="blockToggle({{$item->code}},'{{ $item->company_name }}',{{$item->active}})" class=" btn btn-outline-teal" title="Unblock">
                <i class="menu-item-icon fa fa-unlock"></i>
              </a>
              @endif
              <a href="javascript:;" onclick="deleteToggle({{$item->code}},'{{ $item->company_name }}')" class=" btn btn-outline-teal" title="Delete">
                <i class="menu-item-icon fa fa-trash"></i>
              </a>
              @if($item->approved == 0)
              <a class=" btn btn-outline-teal" title="approve" onclick="return confirm('Are you sure you want to approve this vendor?');" href="{{route('approveVendor',['vendor'=>$item->id])}}">
                <i class="menu-item-icon fa fa-check"></i>
              </a>
              @endif
            </div>
          </td>
        </tr>

        <!-- view modal -->
        <div class="modal fade effect-flip-horizontal " id="viewModal{{$item->code}}">
          <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content bd-0">
              <div class="modal-body pd-0">
                <div class="row flex-row-reverse no-gutters">
                  <div class="col-lg-6 rounded-left">
                    <div class="pd-40">
                      <h2 class="mg-b-20 tx-inverse lh-2 tx-uppercase"><br></h4>
                        <p class="tx-12 mg-b-10">
                          <span class="">Trade License Number:</span>
                          <span class="tx-uppercase tx-12 tx-bold d-block mg-b-10"> {{$item->trade_license_number}}</span>
                          @if($item->trade_license_document)
                          <a href="{{asset($item->trade_license_document)}}" title="Trade license document">
                            <span class="tx-uppercase tx-30 tx-bold d-block mg-b-10">
                              <i class="fa fa-file"></i>
                            </span>
                          </a>
                          @endif

                          <span class="">GST Number:</span>
                          <span class="tx-uppercase tx-12 tx-bold d-block mg-b-10">
                            {{$item->gst_number}}
                          </span>

                          @if($item->gst_document)
                          <a target="_blank" href="{{asset($item->gst_document)}}" title="GST Document">
                            <span class="tx-uppercase tx-30 tx-bold d-block mg-b-10">
                              <i class="fa fa-file"></i>
                            </span>
                          </a>
                          @endif

                          <span class="">PAN Number:</span>
                          <span class="tx-uppercase tx-12 tx-bold d-block mg-b-10">
                            {{$item->pan_number}}
                          </span>

                          @if($item->pancard)
                          <a target="_blank" href="{{asset($item->pancard)}}" title="PAN Document">
                            <span class="tx-uppercase tx-30 tx-bold d-block mg-b-10">
                              <i class="fa fa-file"></i>
                            </span>
                          </a>
                          @endif

                          @if($item->image)
                          <span>image:</span>
                          <span class="tx-uppercase tx-12 tx-bold d-block mg-b-20">
                            <a href="{{asset($item->image)}}" title="GST Document">
                              <img src="{{asset($item->image)}}" class="img-fluid wd-100" alt=""></span>
                          </a>
                          @endif

                        </p>
                        <a href="{{route('showEditVendor',['code'=>$item->code])}}" class=" btn " title="Edit">
                          <button type="button" class="btn btn-primary tx-11  pd-y-12 pd-x-25 tx-mont tx-medium">EDIT</button>
                        </a>
                        <button type="button" class="btn btn-secondary tx-11  pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>

                    </div>
                  </div><!-- col-6 -->
                  <div class="col-lg-6 rounded-left">
                    <div class="pd-40">
                      <h2 class="mg-b-20 tx-inverse lh-2 tx-uppercase">{{$item->company_name}}</h4>
                        <p class="tx-12 mg-b-20">
                          <span class="">Contact Person:</span>
                          <span class="tx-uppercase tx-12 tx-bold d-block mg-b-10"> {{$item->contact_person}}</span>
                          <span>email:</span>
                          <span class="tx-uppercase tx-12 tx-bold d-block mg-b-10"> {{$item->email}}</span>
                          <span>Mobile Number:</span>
                          <span class="tx-uppercase tx-12 tx-bold d-block mg-b-10"> {{$item->mobile}}</span>
                          <span>Phone Number:</span>
                          <span class="tx-uppercase tx-12 tx-bold d-block mg-b-10"> {{$item->phone}}</span>
                          <span>Contact Person:</span>
                          <span class="tx-uppercase tx-12 tx-bold d-block mg-b-10"> {{$item->contact_person}}</span>
                          <span>Our Address:</span>
                          <span class="tx-uppercase tx-12 tx-bold d-block mg-b-10"> {{$item->address}}</span>
                        </p>
                    </div>
                  </div><!-- col-6 -->

                </div><!-- row -->

              </div><!-- modal-body -->
              <div class="modal-footer">
              </div>
            </div><!-- modal-content -->
          </div><!-- modal-dialog -->
        </div><!-- modal -->

        @endforeach
      </tbody>
    </table>
    <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
      {{ $vendors->appends(['search' => $search])->links() }}
    </div>
  </div><!-- bd -->

</div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->

<!-- delete modal -->
<div id="modaldemo8" class="modal fade">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <form id="deleteForm">
      <div class="modal-content bd-0 tx-14">
        <div class="modal-header pd-y-20 pd-x-25">
          <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Delete</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body pd-25">
          <h4 class="lh-3 mg-b-20 tx-inverse">Are you sure you want to delete<br><label id="vendorSpan"></label>?</h4>
          <p class="mg-b-4 dltmsgDiv"></p>
        </div>
        <input type="hidden" name="vendorCodeDelete" id="vendorCodeDelete">
        <div class="modal-footer">
          <input type="submit" class="btn btn-primary tx-11  pd-y-12 pd-x-25 tx-mont tx-medium" value="Delete">
          <button type="button" class="btn btn-secondary tx-11  pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
        </div>
      </div>
    </form>
  </div><!-- modal-dialog -->
</div><!-- modal -->
<!-- block/unblock modal -->
<div id="blockPopup" class="modal fade">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <form id="blockForm">
      <div class="modal-content bd-0 tx-14">
        <div class="modal-header pd-y-20 pd-x-25">
          <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><label id="vendorSpanHeading"></label></h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body pd-25">
          <h4 class="lh-3 mg-b-20 tx-inverse">Are you sure you want to <br><label id="vendorSpanTerm"></label> <label id="vendorSpanName"></label>?</h4>
          <p class="mg-b-4 blockmsgDiv"></p>
        </div>
        <input type="hidden" name="vendorCodeBlock" id="vendorCodeBlock">
        <div class="modal-footer">
          <input type="submit" id="blockUnblockBtn" class="btn btn-primary tx-11  pd-y-12 pd-x-25 tx-mont tx-medium" value="Delete">
          <button type="button" class="btn btn-secondary tx-11  pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
        </div>
      </div>
    </form>
  </div><!-- modal-dialog -->
</div><!-- modal -->

<!-- invitation modal -->
<div id="inviteModal" class="modal fade effect-scale">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="col-lg-12 rounded-left">
      <div class="col-md-12 col-xl-12 mg-t-30 mg-xl-t-0">
        <div class="d-flex  ht-300 pos-relative align-items-center">
          <div class="sk-wave">
            <div class="sk-rect sk-rect1 bg-gray-800"></div>
            <div class="sk-rect sk-rect2 bg-gray-800"></div>
            <div class="sk-rect sk-rect3 bg-gray-800"></div>
            <div class="sk-rect sk-rect4 bg-gray-800"></div>
            <div class="sk-rect sk-rect5 bg-gray-800"></div>
          </div>
        </div><!-- d-flex -->
      </div><!-- col-4 -->

    </div><!-- row -->
  </div><!-- modal-dialog -->
</div><!-- modal -->


@endsection
@section('scripts')
<script>
  function deleteToggle(code, name) {
    $('#modaldemo8').modal('toggle');
    $('#vendorCodeDelete').val(code);
    $('#vendorSpan').html(name);
    $('#contentDiv').html(`<div class="col-sm-12" id="contentDiv"> <h5 class="mb-2 text-center">Are you sure you want to delete agent <span>${ name }</span>?</h5></div>`);
  }
</script>
<script>
  $("#deleteForm").validate({
    submitHandler: function(form) {
      var form = document.getElementById('deleteForm');
      var data = new FormData(form);
      $('.dltmsgDiv').html(``);
      $.ajax({
        type: "post",
        url: "{{route('deleteVendor')}}",
        data: data,
        dataType: "json",
        processData: false,
        contentType: false,
        cache: false,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(response) {
          if (response.status == 1) {
            $('.dltmsgDiv').html(`<p class="alert alert-success mg-b-4">` + response.message + `</p>`);
            setTimeout(function() {
              $('#deletePopup').modal('hide');
              $('.dltmsgDiv').html(``);
              window.location.reload();
            }, 1000);
          } else {

          }
        }
      });
      return false;
    }
  });
</script>
<script>
  function blockToggle(code, name, active) {
    if (active == 1) {
      var term = 'block';
      var btn = 'Block';
    } else {
      var term = 'unblock';
      var btn = 'Unblock';
    }
    $('#blockPopup').modal("toggle");
    $('#vendorCodeBlock').val(code);
    $('#vendorSpanHeading').html(term);
    $('#vendorSpanTerm').html(term);
    $('#vendorSpanName').html(name);
    $('#blockUnblockBtn').val(btn);
  }
</script>
<script>
  $("#blockForm").validate({
    submitHandler: function(form) {
      var form = document.getElementById('blockForm');
      var data = new FormData(form);
      $('.blockmsgDiv').html(``);
      $.ajax({
        type: "post",
        url: "{{route('blockVendor')}}",
        data: data,
        dataType: "json",
        processData: false,
        contentType: false,
        cache: false,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(response) {
          if (response.status == 1) {
            $('.blockmsgDiv').html(`<div class="alert alert-success">` + response.message + `</div>`);
            setTimeout(function() {
              window.location.reload();
            }, 1000);
          } else {

          }
        }
      });
      return false;
    }
  });
</script>
<script>
  function sendInvite(code) {

    $.ajax({
      type: "post",
      url: "{{route('sendInvite')}}",
      data: {
        code: code
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(response) {
        $('#inviteModal').modal('toggle');
        if (response.status == 1) {
          $('#msgShowDiv').html(` <div class="col-lg-12 p-r-0 title-margin-right " id="msgShowDiv"> <div class="alert alert-success alert-block sessionDiv" >  <button type="button" class="close" data-dismiss="alert">×</button> <strong>${ response.message }</strong></div></div>`);
          setTimeout(function() {
            $('#msgShowDiv').html('');
          }, 2000);
        } else {
          $('#msgShowDiv').html(` <div class="col-lg-12 p-r-0 title-margin-right " id="msgShowDiv"> <div class="alert alert-danger alert-block sessionDiv" >  <button type="button" class="close" data-dismiss="alert">×</button> <strong>${ response.errors }</strong></div></div>`);
          setTimeout(function() {
            $('#msgShowDiv').html('');
          }, 2000);
        }

      }
    });
    return false;
  }

  //menu active
  $('#vendor_menu').find('a').addClass('active');
</script>
@endsection