@extends('admin.layouts.app')
@section('content')
<div class="br-pagetitle">
        <i class="icon ion-ios-home-outline"></i>
        <div>
          <h4>Dashboard</h4>
         
        </div>
      </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  
  <?php /*?><?php $gh1=$data[0]->number;if($gh1!=''){$a=$data[0]->number;}else{$a=0;} $gh =$data[0]->number; if($gh!=''){$b=$data[1]->number;}else{$b=0;} ?><?php  $g1="[['Sta','Number'],['Block',$a],['Active',$b]]";?>

  <script type="text/javascript">
   var analytics = <?php echo $g1; ?>

   google.charts.load('current', {'packages':['corechart']});

   google.charts.setOnLoadCallback(drawChart);

   function drawChart()
   {
    var data = google.visualization.arrayToDataTable(analytics);
    var options = {
     title : 'Percentage of Active and Block Vendors Details'
    };
    var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
    chart.draw(data, options);
   }
  </script>
<?php */?>

<!-- <h1><?php  $g1; ?></h1> -->
<script type="text/javascript">
   var analytics1 = <?php echo $product_id; ?>

   google.charts.load('current', {'packages':['corechart']});

   google.charts.setOnLoadCallback(drawChart);

   function drawChart()
   {
    var data = google.visualization.arrayToDataTable(analytics1);
    var options = {
     title : 'Percentage of Product Sales Report Details'
    };
    var chart = new google.visualization.PieChart(document.getElementById('pie_chart'));
    chart.draw(data, options);
   }
  </script>


<script type="text/javascript">
      var visitor = <?php echo $order_payments; ?>;
      console.log(visitor);
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable(visitor);
        var options = {
          title: 'Total Sales Yearly Wise',
          curveType: 'function',
          legend: { position: 'bottom' }
        };
        var chart = new google.visualization.LineChart(document.getElementById('linechart'));
        chart.draw(data, options);
      }
    </script>







      <div class="br-pagebody">
        <div class="row row-sm">
          <div class="col-sm-6 col-xl-3">
            <div class="bg-info rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-earth tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                  <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Total User's</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{ $users_count }} </p>
                  <!-- <span class="tx-11 tx-roboto tx-white-8">24% higher yesterday</span> -->
                </div>
              </div>
              <div id="ch1" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
          <div class="col-sm-6 col-xl-3 mg-t-20 mg-sm-t-0">
            <div class="bg-purple rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-bag tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                  <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Today Sales Amount</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{ $total_paid }}</p>
                  <!-- <span class="tx-11 tx-roboto tx-white-8">$390,212 before tax</span> -->
                </div>
              </div>
              <div id="ch3" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
          <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
            <div class="bg-teal rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-monitor tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                  <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Total Live Products</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{ $total_product_live }}</p>
                  <!-- <span class="tx-11 tx-roboto tx-white-8">23% average duration</span> -->
                </div>
              </div>
              <div id="ch2" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
          <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
            <div class="bg-primary rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-clock tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                  <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Total Order's</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{ $total_order }}</p>
                  <!-- <span class="tx-11 tx-roboto tx-white-8">65.45% on average time</span> -->
                </div>
              </div>
              <div id="ch4" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
        </div><!-- row -->


        <br>
        <div class="row row-sm">
          <div class="col-sm-6 col-xl-3">
            <div class="bg-info rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-clock tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                  <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Shipping Order's</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{ $confirm_order }} </p>
                  <!-- <span class="tx-11 tx-roboto tx-white-8">24% higher yesterday</span> -->
                </div>
              </div>
              <div id="ch1" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
          <div class="col-sm-6 col-xl-3 mg-t-20 mg-sm-t-0">
            <div class="bg-purple rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-earth tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                  <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Pending Order's</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{ $pending_order }}</p>
                  <!-- <span class="tx-11 tx-roboto tx-white-8">$390,212 before tax</span> -->
                </div>
              </div>
              <div id="ch3" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
          <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
            <div class="bg-teal rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-bag tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                  <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Confirmed Order's</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{ $confirm_order }}</p>
                  <!-- <span class="tx-11 tx-roboto tx-white-8">23% average duration</span> -->
                </div>
              </div>
              <div id="ch2" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
          <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
            <div class="bg-primary rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-clock tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                  <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Cancel  Order's</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{ $cancel_order }}</p>
                  <!-- <span class="tx-11 tx-roboto tx-white-8">65.45% on average time</span> -->
                </div>
              </div>
              <div id="ch4" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
        </div><!-- row -->

        <div class="row row-sm mg-t-20">
        <div class="col-lg-6">
              
        <div class="card bd-0 shadow-base pd-25 mg-t-20">
            <div class="panel-body" align="center">
              <!-- <div id="piechart_3d" style="width:350px; height:250px;"> -->
              <div id="linechart" style="width:350px; height:250px;">

              </div>
           </div>
          </div>
        
        
        </div>
        <div class="col-lg-6">
        
        <div class="card shadow-base bd-0 pd-25 mg-t-20">
            <div class="panel-body" align="center">
              <div id="pie_chart" style="width:350px; height:250px;">

              </div>
           </div>
        </div>
        
        
        </div>
        </div>

        <div class="row row-sm mg-t-20">
        <div class="col-lg-6">
              
        <!-- <div class="card bd-0 shadow-base pd-25 mg-t-20">
            <div class="panel-body" align="center">
              <div id="linechart" style="width:350px; height:250px;">

              </div>
           </div>
          </div>
        
        
        </div> -->
        <div class="col-lg-6">
        
        
        
        
        </div>
        </div>

      </div><!-- br-pagebody -->
      @endsection
      @section('scripts')
      <script>
      //menu active
      $('#dashboard_menu').find('a').addClass('active');
      </script>  
      @endsection('')