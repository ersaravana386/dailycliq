<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {{-- <!-- Twitter -->
        <meta name="twitter:site" content="@themepixels">
        <meta name="twitter:creator" content="@themepixels">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="Bracket Plus">
        <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
        <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
        <!-- Facebook -->
        <meta property="og:url" content="http://themepixels.me/bracketplus">
        <meta property="og:title" content="Bracket Plus">
        <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">
        <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
        <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1200">
        <meta property="og:image:height" content="600">
        <!-- Meta -->
        <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
        <meta name="author" content="ThemePixels"> --}}
        <title>Daily CliQ</title>
        <!-- vendor css -->
        <link href="{{asset('app/lib/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
        <link href="{{asset('app/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
        <!-- Bracket CSS -->
        <link rel="stylesheet" href="{{asset('app/css/brackets2.css')}}">
    </head>
    <body>
        <div class="d-flex align-items-center justify-content-center ht-100v">
            <img src="{{asset('app/img/img22.jpg')}}" class="wd-100p ht-100p object-fit-cover" alt="">
            <div class="overlay-body bg-black-6 d-flex align-items-center justify-content-center">
                <form role="form" method="post" action="javascript:;" id="loginForm">
                    <div id="loginDiv" class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 rounded bd bd-white-2 bg-black-7">
                        <div class="signin-logo tx-center tx-28 tx-bold tx-white">
                            <span class="tx-normal"></span> Daily<span class="tx-info">CliQ</span>
                            <span class="tx-normal"></span>
                        </div>
                        <div class="tx-white-5 tx-center mg-b-60">Admin login</div>
                        <div class="form-group">
                            <input type="text" class="form-control fc-outline-dark" name="email" placeholder="Enter your username">
                        </div><!-- form-group -->
                        <div class="form-group">
                            <input type="password" class="form-control fc-outline-dark" name="password" placeholder="Enter your password">
                            <a href="javascript:;" onClick="forgotPassword()" class="tx-info tx-12 d-block mg-t-10">Forgot password?</a>
                        </div><!-- form-group -->
                        <button type="submit" class="btn btn-info btn-block">Sign In</button>
                        <div class="msgDiv mg-t-20"></div>
                    </div><!-- login-wrapper -->
                </form>
                <form role="form" method="post" action="javascript:;" id="forgotPassword">
                    <div id="forgotDiv"  style="display:none" class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 rounded bd bd-white-2 bg-black-7">
                        <div class="signin-logo tx-center tx-28 tx-bold tx-white">
                            <span class="tx-normal">[</span> Daily <span class="tx-info">Cliq</span>
                            <span class="tx-normal">]</span>
                        </div>
                        <div class="tx-white-5 tx-center mg-b-60">Forgot  Password</div>
                        <div class="form-group">
                            <input type="text" class="form-control fc-outline-dark" name="email3" id="email3" placeholder="Enter your email">
                        </div><!-- form-group -->
                        <button type="submit" class="btn btn-info btn-block" id="forgotButton">Submit</button>
                        <div class="msgDiv2 mg-t-20"></div>
                        <div class="mg-t-60 tx-center">
                            <a href="javascript:;" onClick="loginForm()" class="tx-info">Back to login</a>
                        </div>
                    </div><!-- login-wrapper -->
                </form>
            </div><!-- overlay-body -->
        </div><!-- d-flex -->
        <script src="{{asset('app/lib/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('app/lib/jquery-ui/ui/widgets/datepicker.js')}}"></script>
        <script src="{{asset('app/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('js/jquery.validate.js')}}"></script>
        <script>
                                function forgotPassword() {
                                    $('#loginDiv').hide();
                                    $("#forgotDiv").slideDown(500);
                                }
                                function loginForm() {
                                    $('#forgotDiv').hide();
                                    $("#loginDiv").slideDown(500);
                                }
        </script>
        <script>
            $("#loginForm").validate({
                normalizer: function (value) {
                    return $.trim(value);
                },
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    }
                },
                messages: {
                    email: {
                        required: 'Email is  required',
                        email: 'Email is invalid'
                    },
                    password: {
                        required: 'Password is required'
                    }
                },
                submitHandler: function (form) {
                    var form = document.getElementById('loginForm');
                    var data = new FormData(form);
                    $('.msgDiv').html(``);
                    $.ajax({
                        type: "POST",
                        url: "{{route('login')}}",
                        data: $(form).serialize(),
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (data) {
                            if (data.status == 1) {
                                window.location.href = "{{route('dashboard')}}";
                            } else {
                                var html = "";
                                $.each(data.errors, function (key, value) {
                                    html += value + "<br/>";
                                });
                                $('.msgDiv').html(` <div class="dispError">` + html + `</div> `);
                                setTimeout(function () {
                                    $('.msgDiv').html(``);
                                }, 6000);
                            }
                        }
                    });
                    return false;
                }
            });
        </script>
        <script>
            $("#forgotPassword").validate({
                normalizer: function (value) {
                    return $.trim(value);
                },
                rules: {
                    email3: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    email3: {
                        required: 'Email is  required',
                        email: 'Email is invalid'
                    }
                },
                submitHandler: function (form) {
                    $('#forgotButton').prop('disabled', 'disabled');
                    var form = document.getElementById('forgotPassword');
                    var data = new FormData(form);
                    $('.msgDiv').html(``);
                    $.ajax({
                        type: "POST",
                        url: "{{route('forgotPassword')}}",
                        data: $(form).serialize(),
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (data) {
                            if (data.status == 1) {
                                $('#email3').val('');
                                $('.msgDiv2').html(` <div class="dispSuccess">` + data.message + `</div> `);
                                setTimeout(function () {
                                    $('.msgDiv2').html(``);
                                }, 3000);
                            } else {
                                var html = "";
                                $.each(data.errors, function (key, value) {
                                    html += value + "<br/>";
                                });
                                $('.msgDiv2').html(` <div class="dispError">` + html + `</div> `);
                                setTimeout(function () {
                                    $('.msgDiv2').html(``);
                                }, 3000);
                            }
                            $('#forgotButton').prop('disabled', false);
                        }
                    });
                    return false;
                }
            });
        </script>
    </body>
</html>