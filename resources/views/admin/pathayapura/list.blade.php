@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-10">
        <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
        <span class="breadcrumb-item active">Pathayapura</span>
        <span class="breadcrumb-item active">Products</span>
    </nav>
</div><!-- br-pageheader -->
<div class="br-pagetitle">
    <i class="icon icon ion-ios-bookmarks-outline"></i>
    <div>
        <h4>Pathayapura</h4>
        <p class="mg-b-0">Pathayapura Listing</p>
    </div>
</div><!-- d-flex -->

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <div id="datatable1_filter" class="dataTables_filter">
            <div class="row">
                <div class="col-lg-9">
                    <form action="" method="get">
                        <div class="input-group input-group-md mb-3 col-md-4">
                            <input type="text" name="search" value="" placeholder="search" class="form-control" value="">
                            <div class="input-group-append">
                                <span class="input-group-btn">
                                    <button class="btn btn-default input-group-close-icon" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2">
                    <div class="input-group input-group-md mb-3 col-md-4">
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 p-r-0 title-margin-right " id="msgShowDiv">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block sessionDiv">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        @endif
    </div>
    <div class="bd bd-gray-300 rounded table-responsive" style="margin-top:10px;">
    @php
        $i=0;
    @endphp
        <table class="table mg-b-0">
            <thead>
                <tr>
                    <th class="wd-10p">#</th>
                    <th class="wd-15p">Product Name</th>
                    <th class="wd-15p">Brand</th>
                    <th class="wd-10p">Category</th>
                    <th class="wd-10p">Vendor</th>
                    <th class="wd-10p">Stock Unit</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i=0;
                @endphp
                @foreach($products as $key=>$value)
                <tr>
                    <td>{{++$i}}</td>
                    <td>{{$value->name}}</td>
                    <td>@if($value->brand){{$value->brand->name}}@endif</td>
                    <td>@if($value->category){{$value->category->name}}@endif</td>
                    <td>@if($value->vendor){{$value->vendor->company_name}}@endif</td>
                    <td>{{$value->stock_unit}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
              {{ $products->appends(['search' => $search])->links() }}
        </div>
  
    </div><!-- bd -->

</div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
@include('ui.ajaxloader')
@endsection
@section('scripts')
<script>
    //menu active
    $('#pathayapura_menu').find('a').addClass('active');
</script>
<script>
    $(document).ready(function () {
            $("#select_all").click(function () {
                $(".check_box").attr('checked', this.checked);
            });
    });
</script>
ndsection