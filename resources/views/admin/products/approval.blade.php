@extends('admin.layouts.app')
@section('content')
<div class="br-pagebody">
    <div class="container-fluid pt-2">
        <div class="row row-sm justify-content-center new">
            <div class="col-12 col-sm-10 col-md-12 col-lg-12">
                <div class="card bg-white border-0 shadow-sm py-5 px-4">
                    <div class="item">
                        @include('admin.layouts.progressbar')
                        <form class="inset row" action="javascript:;" id="mainForm">
                            <div class="col-lg-12">
                                <div class="form-group pd-5">
                                    <label class="form-control-label" for="approved">Status: <span class="tx-danger">*</span></label>
                                    <select name="approved" class="form-control" id="approved">
                                        <option value="">--Select Status--</option>
                                        <option @if($product->approved == 1) selected @endif value="1">Approved</option>
                                        <option @if($product->approved == 2) selected @endif value="2">Rejected</option>
                                        <option @if($product->approved == 3) selected @endif value="3">Re-review</option>
                                    </select>
                                </div>
                            </div><!-- col-6 -->
                            <div class="col-lg-12 ">
                                <div class="form-group pd-5">
                                    <label class="form-control-label" for="admin_comments">Admin Comments: <span class="tx-danger">*</span></label>
                                    <textarea class="form-control form-control-sm" name="admin_comments" id="admin_comments" placeholder="Enter comments" rows="5">{{$product->admin_comments}}</textarea>
                                </div>
                            </div><!-- col-6 -->

                            <div class="col-sm-12 text-left">
                                <input class="btn btn-primary btn-sm button-inner" name="save" type="submit" value="Approve" />
                                <a class="btn btn-secondary btn-sm" href="{{route('showNewCatelogAdditionalInfo',['product'=>$product->id])}}">Back</a>
                            </div>
                        </form>
                    </div><!-- card -->
                </div>
            </div><!-- row -->
        </div>
    </div><!-- br-pagebody -->
    @endsection
    @section('scripts')
    <script>
        $.validator.addMethod("lettersndashes", function(value, element) {
            return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
        }, "Field must contain only letters, numbers, or dashes.");
    </script>
    <script>
        $("#mainForm").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            rules: {
                approved: {
                    required: true,
                }
            },
            messages: {
                approved: {
                    required: "Status is required",
                }
            },
            submitHandler: function(form) {
                $('.msgDiv').html(``);
                var data = new FormData(form);
                $.ajax({
                    type: "post",
                    url: "{{route('savePendingProductListApproval',['product'=>$product->id])}}",
                    data: data,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                    cache: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        if (data.status == 1) {
                            window.location.href = "{{route('showAdminProductList')}}";
                            $('.msgDiv').html(` <div class="alert alert-success">` + data.message + `</div>`);

                        } else {
                            var html = "";
                            $.each(data.errors, function(key, value) {
                                html += value + "<br/>";
                            });
                            $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                            setTimeout(function() {
                                $('.msgDiv').html(``);
                            }, 4000);
                        }
                    }
                });
                return false;
            }
        });
    </script>
    @endsection