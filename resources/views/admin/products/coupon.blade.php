@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
      
    <span class="breadcrumb-item active">Discount</span>
  </nav>
</div><!-- br-pageheader -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<div class="br-pagebody">
<div class="container box">
   
   <div class="panel panel-default">
   
    <div class="panel-body">
     <div class="form-group">
      <input type="text" name="search" id="search" class="form-control" placeholder="Search VendorName , Product Name, Brand Name,Categories , Price , MRP " autocomplete="off" />
     </div>
     <div class="table-responsive">
      <h3 align="center">Total Data : <span id="total_records"></span></h3>
      <table class="table table-striped table-bordered">
       <thead>
        <tr>
        <th>Vendor Name</th>
         <th>Name</th>
         <th>Brand</th>
         <th>Category</th>
         <th>Price</th>
         <th>MRP</th>
         <th>Add Coupons</th>
         <th>Add Discount</th>
        
        </tr>
       </thead>
       <tbody>

       </tbody>
      </table>
     </div>
    </div> 
      
  </div><!-- br-pagebody -->
@endsection
@section('scripts')
<script>
$(document).ready(function(){

 fetch_customer_data();

 function fetch_customer_data(query = '')
 {
  $.ajax({
   url:"{{ route('live_search.action') }}",
   method:'GET',
   data:{query:query},
   dataType:'json',
   success:function(data)
   {
    $('tbody').html(data.table_data);
    $('#total_records').text(data.total_data);
   }
  })
 }

 $(document).on('keyup', '#search', function(){
  var query = $(this).val();
  fetch_customer_data(query);
 });
});
</script>
@endsection