@extends('admin.layouts.app')
@section('content')
<div class="br-pagebody">
    <div class="container-fluid pt-2">
        <div class="row row-sm justify-content-center new">
            <div class="col-12 col-sm-10 col-md-12 col-lg-12">
                <div class="card bg-white border-0 shadow-sm py-5 px-4">
                    <div class="item">
                       @include('admin.layouts.progressbar_variant')
                        <form method="POST" enctype="multipart/form-data" id="upload_image_form" action="javascript:void(0)">
                            <div class="row">
                                <div class="col-md-12 mb-2">
                                    <img id="image_preview_container" src="{{ asset('images/preview.png') }}" alt="preview image" style="max-height: 150px;">
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="file" name="image" placeholder="Choose image" id="image">
                                        <span class="text-danger">{{ $errors->first('title') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row mt-4 mb-0">
                                        <div class="col-sm-10">
                                            <input class="btn btn-primary btn-sm button-inner" name="save" type="submit" value="Save" />
                                            @if($product->step >= 4)
                                            <a class="btn btn-primary btn-sm button-inner" href="{{route('showNewAdminvariantAddinfo',['product'=>$variant->id,'variant'=>$product->id])}}">Continue</a>
                                            @endif
                                            <a class="btn btn-secondary btn-sm" href="{{route('showNewCatelogPackageDetails',['product'=>$product->id])}}">Back</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- card -->
                    <hr>
                    <div class="bd bd-gray-300 rounded table-responsive">
                        <table class="table mg-b-0">
                            <thead>
                                <tr>
                                    <th class="wd-5p">#</th>
                                    <th class="wd-15p">Image</th>
                                    <th class="wd-20p">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $i= 0;
                                $image_list = $product_images;
                                if($image_list->count() <= 0) {
                                  $image_list = $product->images; 
                                }
                                @endphp
                                @foreach ($image_list as $key=> $image)
                                <tr>
                                    <th scope="row">
                                        @if($product_images->count() > 0)
                                            {{++$i}}
                                        @else
                                            <input type="checkbox" name="selected_images[]" value="{{$image->id}}">
                                        @endif
                                    </th>
                                    <td>
                                        @if($image->image)
                                        <a target="_blank" href="{{asset($image->image)}}"><img src="{{asset($image->image)}}" style="width:80px;"></a>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            @if($product_images->count() > 1)
                                            <a onclick="return confirm('Are you sure you want to delete this image?');" href="{{route('deleteNewAdminvariantImages',['image'=>$image->id])}}" class=" btn btn-outline-teal" title="Delete category">
                                                <i class="menu-item-icon fa fa-trash"></i>
                                            </a>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                              
                            </tbody>
                        </table>
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                            @if($product_images->count() <= 0 && $product->images->count() > 0)
                                <button class="btn btn-primary btn-sm button-inner" onclick="saveParentImages();">Save Parent Images</button>
                            @endif    
                        </div>
                    </div>
                    <!-- bd -->
                </div>
            </div>
        </div><!-- row -->
    </div>
</div><!-- br-pagebody -->
@endsection
@section('scripts')
<script type="text/javascript">    
    $(document).ready(function(e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#image').change(function() {

            let reader = new FileReader();
            reader.onload = (e) => {
                $('#image_preview_container').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);

        });

        $('#upload_image_form').submit(function(e) {
            e.preventDefault();

            var formData = new FormData(this);

            $.ajax({
                type: 'POST',
                url: "{{ route('saveNewAdminvariantImages',['product'=>$variant->id,'variant'=>$product->id])}}",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: (data) => {
                    this.reset();
                    if (data.status == true) {
                        window.location.reload();
                    } else {
                        var html = "";
                        $.each(data.errors, function(key, value) {
                            html += value + "<br/>";
                        });
                        $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                        }, 4000);
                    }
                }
            });
        });
    });

    function saveParentImages(){
            var images = []
            $('input[name="selected_images[]"]:checked').each(function(key,value){     
                images.push(value.value);
            });

            $.ajax({
                type: 'POST',
                url: "{{ route('saveNewAdminvariantImageOld',['product'=>$variant->id , 'variant'=>$variant->variant])}}",
                data: {
                    images
                },
                dataType: 'json',
                success: (data) => {
                    if (data.status == true) {
                        window.location.reload();
                    } else {
                        var html = "";
                        $.each(data.errors, function(key, value) {
                            html += value + "<br/>";
                        });
                        $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                        }, 4000);
                    }
                }
            });
    }
</script>
@endsection