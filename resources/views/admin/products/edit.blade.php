@extends('admin.layouts.app')
@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
    <a class="breadcrumb-item" href="{{route('showProducts')}}">Products</a>
    <span class="breadcrumb-item active">Edit</span>
  </nav>
</div><!-- br-pageheader -->

<div class="br-pagebody">
    <div class="br-section-wrapper">
      <h6 class="br-section-label">Edit Product {{$product->name}}</h6><br>
      <form id='pageForm' method="POST" action="javascript:;" data-parsley-validate>
          @method('POST')
        <div class="form-layout form-layout-1">
          <div class="row mg-b-25">
            <div class="col-lg-6">
              <div class="form-group pd-5">
                <label class="form-control-label">Product ID: <span class="tx-danger">*</span></label>
                <input class="form-control form-control-sm" type="text" value='{{$product->product_id}}' name="product_id" id="product_id"  placeholder="Enter Product ID">
                <input type="hidden" name="id" value="{{$product->id}}">
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-6 ">
              <div class="form-group pd-5">
                <label class="form-control-label">Name: <span class="tx-danger">*</span></label>
                <input class="form-control form-control-sm" type="text" value='{{$product->name}}' name="name" id="name"  placeholder="Enter product name">
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-6 ">
              <div class="form-group pd-5">
                <label class="form-control-label">Category: <span class="tx-danger">*</span></label>
                <select name="category" id="category" class="form-control form-control-sm select2">
                  @foreach ($categories as $category)
                    <option value="{{$category->id}}" @if($product->category_id == $category->id) selected @endif >{{$category->name}}</option>
                  @endforeach
                </select>
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-6 ">
              <div class="form-group pd-5">
                <label class="form-control-label">Brand: <span class="tx-danger">*</span></label>
                <select name="brand" id="brand" class="form-control form-control-sm select2">
                  @foreach ($brands as $brand)
                    <option value="{{$brand->id}}" @if($product->brand_id == $brand->id) selected @endif >{{$brand->name}}</option>
                  @endforeach
                </select>
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-6 ">
              <div class="form-group pd-5">
                <label class="form-control-label">UPC: <span class="tx-danger">*</span></label>
                <input class="form-control form-control-sm" type="text" value='{{$product->upc}}' name="upc" id="upc"  placeholder="Enter UPC">
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-6 ">
              <div class="form-group pd-5">
                <label class="form-control-label">EAN: <span class="tx-danger">*</span></label>
                <input class="form-control form-control-sm" type="text" value='{{$product->ean}}' name="ean" id="ean"  placeholder="Enter EAN">
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-6 ">
              <div class="form-group pd-5">
                <label class="form-control-label">GST: <span class="tx-danger">*</span></label>
                <input class="form-control form-control-sm" type="text" value='{{$product->gst}}' name="gst" id="gst"  placeholder="Enter GST">
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-6 ">
              <div class="form-group pd-5">
                <label class="form-control-label">SESS: <span class="tx-danger">*</span></label>
                <input class="form-control form-control-sm" type="text" value='{{$product->sess}}' name="sess" id="sess"  placeholder="Enter SESS">
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-6">
              <div class="form-group pd-5">
                <label class="form-control-label">Warranty Details: <span class="tx-danger">*</span></label>
                <textarea class="form-control form-control-sm" name="warranty" id="warranty"  placeholder="Warranty">{{$product->warranty_details}}</textarea>
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-3">
              <div class="form-group pd-5">
                <label class="form-control-label">Weight: <span class="tx-danger">*</span></label>
                <input class="form-control form-control-sm" type="text" value='{{round($product->weight,2)}}' name="weight" id="weight"  placeholder="Enter Weight">
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-3">
              <div class="form-group pd-5">
                <label class="form-control-label">Weight Category: <span class="tx-danger">*</span></label>
                <select class="form-control form-control-sm select2" name="weight_category" id="weight_category">
                  @foreach ($weight_classes as $weight_class)
                    <option value="{{$weight_class->id}}" @if($product->weight_class_id == $weight_class->id) selected @endif >{{$weight_class->name}}</option>
                  @endforeach
                </select>
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-3">
              <div class="form-group pd-5">
                <label class="form-control-label">Length: <span class="tx-danger">*</span></label>
                <input class="form-control form-control-sm" type="text" value='{{round($product->length,2)}}' name="length" id="length"  placeholder="Enter Length">
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-3">
              <div class="form-group pd-5">
                <label class="form-control-label">Width: <span class="tx-danger">*</span></label>
                <input class="form-control form-control-sm" type="text" value='{{round($product->width,2)}}' name="width" id="width"  placeholder="Enter Width">
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-3">
              <div class="form-group pd-5">
                <label class="form-control-label">Height: <span class="tx-danger">*</span></label>
                <input class="form-control form-control-sm" type="text" value='{{round($product->height,2)}}' name="height" id="height"  placeholder="Enter Height">
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-3">
              <div class="form-group pd-5">
                <label class="form-control-label">Dimensions Unit: <span class="tx-danger">*</span></label>
                <select class="form-control form-control-sm select2" name="dimensions_unit" id="dimensions_unit">
                  @foreach ($length_classes as $length_class)
                    <option value="{{$length_class->id}}" @if($product->length_class_id == $length_class->id) selected @endif >{{$length_class->name}}</option>
                  @endforeach
                </select>
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-3">
              <div class="form-group pd-5">
                <label class="form-control-label">Assured Product: <span class="tx-danger">*</span></label>
                <select name="assured" id="assured" class="form-control form-control-sm">
                    <option value="{{'0'}}" @if($product->assured == 0) selected @endif >{{'No'}}</option>
                    <option value="{{'1'}}" @if($product->assured == 1) selected @endif >{{'Yes'}}</option>
                </select>
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-3">
              <div class="form-group pd-5">
                <label class="form-control-label">Featured Product: <span class="tx-danger">*</span></label>
                <select name="featured" id="featured" class="form-control form-control-sm">
                    <option value="{{'0'}}" @if($product->featured_product == 0) selected @endif >{{'No'}}</option>
                    <option value="{{'1'}}" @if($product->featured_product == 1) selected @endif >{{'Yes'}}</option>
                </select>
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-6 ">
              <div class="form-group pd-5">
                <label class="form-control-label">Description: <span class="tx-danger">*</span></label>
              <textarea class="form-control form-control-sm" name="description" id="description"  placeholder="Enter description">{{$product->description}}</textarea>
              </div>
            </div><!-- col-6 -->
          </div><!-- row -->
          <div class="form-layout-footer">
            <input type="submit" class="btn btn-info mg-b-10 " value="Submit">
            <a class="btn btn-secondary mg-b-10 " href="{{route('showProducts')}}">Cancel</a>
          </div><!-- form-layout-footer -->
          <div class="msgDiv"></div>
        </div><!-- form-layout -->
      </form>

    </div><!-- br-section-wrapper -->
  </div><!-- br-pagebody -->
@endsection
@section('scripts')
<script>
  $(document).ready(function() {
    $('.select2').select2();
  });
    //profile form
    $("#pageForm").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        rules: {
            name: {
              required: true
            },
            description:{
              required: true
            },
            category:{
              required: true
            },
            manufacturer:{
              required: true
            },
            brand:{
              required: true
            },
            upc:{
              required: true
            },
            ean:{
              required: true
            },
            dsin:{
              required: false
            },
            isbn:{
              required: true
            },
            mpn:{
              required: true
            },
            tax:{
              required: true
            },
            tax_Category:{
              required: true
            },
            weight:{
              required: true
            },
            weight_category:{
              required: true
            },
            length:{
              required: true
            },
            width:{
              required: true
            },
            height:{
             required: true
            },
            dimensions_unit:{
              required: true
            }
        },
        messages: {
            name: {
              required: "Manufacturer name is required"
            },
            description:{
              required: "Description is required"
            },
            category:{
              required: "Category is required"
            },
            manufacturer:{
              required: "Manufacturer is required"
            },
            brand:{
              required: "Brand is required"
            },
            upc:{
              required: "UPC is required"
            },
            ean:{
              required: "EAN is required"
            },
            dsin:{
              required: "DSIN is required"
            },
            isbn:{
              required: "ISBN is required"
            },
            mpn:{
              required: "MPN is required"
            },
            tax:{
              required: "TAX is required"
            },
            tax_Category:{
              required: "TAX Category is required"
            },
            weight:{
              required: "Weight is required"
            },
            weight_category:{
              required: "Weight category is required"
            },
            length:{
              required: "Length is required"
            },
            width:{
              required: "Width is required"
            },
            height:{
             required:  "Height is required"
            },
            dimensions_unit:{
              required: "Dimensions unit is required"
            }
        },
        submitHandler: function(form) {
            $('.msgDiv').html(``);
            var data = new FormData(form);
            $.ajax({
                type: "post",
                url: "{{route('editProduct',['id'=>$product->id])}}",
                data: data,
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.status == 1) {

                        $('.msgDiv').html(` <div class="alert alert-success">` + data.message + `</div>`);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                            location.href = "{{route('showProducts')}}";
                        }, 2000);

                    } else {
                        var html = "";
                        $.each(data.errors, function(key, value) {
                            html += value + "<br/>";
                        });
                        $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                        }, 4000);
                    }
                }
            });
            return false;
        }
    });
</script>
 <script>
    $('#image').on('change', function() {
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $('#image_label').html(fileName);
    })

    //menu active
    $('#manufacturers_menu').find('a').addClass('active');
</script>
@endsection
