@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
  
    <span class="breadcrumb-item active">New</span>
  </nav>
</div><!-- br-pageheader -->

@foreach($products as $items)
@endforeach
<div class="br-pagebody">
    <div class="br-section-wrapper">
      <h6 class="br-section-label">Coupons Register Details</h6><br>
      <form id='vendorForm' method="POST" action="javascript:;" data-parsley-validate>
          @method('POST')
        <div class="form-layout form-layout-1">
          <div class="row mg-b-25">
            <input type="hidden" name="product_id" value="{{$items->id}}">
            <input type="hidden" name="vendor_id" value="{{$items->vendor_id}}">
            <input type="hidden" name="category_id" value="{{$items->category_id}}">
            <input type="hidden" name="brand_id" value="{{$items->brand_id}}">
            <div class="col-lg-6 ">
              <div class="form-group pd-5">
                <label class="form-control-label">Vendor name: <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="company_name" id="vendor_name" value="{{$items->company_name}}" readonly>
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-6">
              <div class="form-group pd-5">
                <label class="form-control-label">Brand Name: <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="brand_name" id="brand_name" value="{{$items->brand_name}}" readonly>
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-6">
                <div class="form-group pd-5">
                  <label class="form-control-label">Category Name: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="category_name" id="address" value="{{$items->cate}}" readonly>
                </div>
              </div><!-- col-4 -->
            <div class="col-lg-6">
              <div class="form-group pd-5">
                <label class="form-control-label">Product Name : <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="product_name" id="email" value="{{$items->name}}" readonly> 
              </div>
            </div><!-- col-8 -->
            <div class="col-lg-6">
              <div class="form-group pd-5">
                <label class="form-control-label">Price: <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="price" id="mobile"  value="{{$items->price}}" readonly> 
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-6">
              <div class="form-group pd-5">
                <label class="form-control-label">MRP: <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="mrp" id="phone" value="{{$items->mrp}}" readonly>
              </div>
            </div><!-- col-4 -->

            <div class="col-lg-6">
              <div class="form-group pd-5">
                <label class="form-control-label">Offer Percentage: <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="offer_percentage" id="offer_percentage"  > 
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-6">
              <div class="form-group pd-5">
                <label class="form-control-label">Offer Price: <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="offer_price" id="offer_price" >
              </div>
            </div><!-- col-4 -->

            <div class="col-lg-6">
              <div class="form-group pd-5">
              <label class="form-control-label">Start Date : <span class="tx-danger">*</span></label>
                <input class="form-control" type="date" name="valid_from" id="valid_from"  > 
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-6">
              <div class="form-group pd-5">
              <label class="form-control-label">End Date: <span class="tx-danger"></span></label>
                <input class="form-control" type="date" name="valid_to" id="valid_to"  > 
                </div>
            </div><!-- col-4 -->

            
            <div class="col-lg-6">
              <div class="form-group pd-5">
              <label class="ckbox ckbox-success">
                    <input  name="offer" type="checkbox" value="1">
                    <span>Buy one and Get One Product  Offer's</span>
                    
                </label>
            </div>    

            </div><!-- col-4 -->
            <div class="col-lg-6">
              <div class="form-group pd-5">
              

               
                <div>
            </div><!-- col-4 -->
            
              
              
          </div><!-- row -->

          <div class="form-layout-footer">
            <button class="btn btn-info mg-b-10 ">Add</button>
          
          </div><!-- form-layout-footer -->
          <div class="msgDiv"></div>
        </div><!-- form-layout -->
      </form>

     
    </div><!-- br-section-wrapper -->
  </div><!-- br-pagebody -->
@endsection


@section('scripts')
<script>
    //profile form
    $("#vendorForm").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        rules: {
            offer_percentage: {
              required: true
            },
            offer_price: {
              required: true
            },
            valid_from:{
              required: true
            },
            valid_to:{
              required: true
            }
            
        },
        messages: {
            offer_percentage: {
              required: "Offer Percentage  is required"
            },
            offer_price: {
              required: "Offer Price  is required"
            },
            valid_from:{
              required: "Start Date  is required"
            },
            valid_to:{
              required: "End Date is required"
            }
            
        },
        submitHandler: function(form) {
            $('.msgDiv').html(``);
            var data = new FormData(form);
            $.ajax({
                type: "post",
                url: "{{route('adddiscount')}}",
                data: data,
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                  console.log(data);
                    if (data.status == 1) {
                        $('.msgDiv').html(`
                            <div class="alert alert-success">` + data.message + `</div>
                        `);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                            location.href = "{{route('coupon')}}";
                        }, 4000);

                    } else {
                        var html = "";
                        $.each(data.errors, function(key, value) {
                            html += value + "<br/>";
                        });
                        $('.msgDiv').html(`
                            <div class="alert alert-danger m-2">` + html + `</div>
                        `);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                        }, 4000);
                    }
                }
            });
            return false;
        }
    });
</script>
 
@endsection