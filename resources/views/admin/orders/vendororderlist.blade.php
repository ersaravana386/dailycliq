@extends('admin.layouts.app')
<style>
.buttons-html5{
    text-transform: uppercase;
    font-size: 11px;
    letter-spacing: 1px;
    font-weight: 500;
    padding: 12px 20px;
    background-color: #f4d078;
    border: 1px solid black;
    border-radius: 2px;
    margin-bottom:2px;
}
</style>
@section('content')
    <div class="br-pagebody">
        <div class="row no-gutters widget-1 shadow-base" style="margin-top:4rem !important">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="card shadow-base bd-0">
                    <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                        <h6 class="card-title tx-uppercase tx-12 mg-b-0">Filter Order</h6>
                    </div><!-- card-header -->
                @foreach($orders as $ord)
                    <form id="sort-form" method="get" action="{{route('SortOrder',['vendorid'=>$ord->vendor_id])}}"> 
                @endforeach
                        <div class="form-layout form-layout-1">
                            <div class="row mg-b-25">
                                <div class="col-lg-4">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="referancenumber" value="" placeholder="Referance Number">
                                        </div>
                                </div><!-- col-4 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="customername" value="" placeholder="Customer Name">
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <select class="form-control select222" style="width:100%;" name="orderstatus">
                                            <option>Order Status</option>
                                            <option  value="1">Completed</option>
                                            <option  value="2">Pending</option>
                                            <option  value="3">Refunded</option>
                                            <option  value="4">Failed</option>
                                            <option  value="5">Canceled</option>
                                        </select>
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <select class="form-control select222" style="width:100%;" name="paymenttype" >
                                            <option>Payment Type</option>
                                            <option  value="Credit Card">Credit Card</option>
                                            <option  value="Online Payment">Online Payment</option>
                                            <option  value="Debit Card">Debit Card</option>
                                            <option  value="Cash On  Delivery">Cash On  Delivery</option>
                                            <option  value="Others">Others</option>
                                        </select>
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-md-4 col-lg-2 mg-t-10 mg-lg-t-0">
                                    <div class="form-group mg-b-0">
                                    <input type="text" class="form-control form-control-datepicker tx-14" data-language="en" placeholder="Date of order from" name="orderfrom">
                                    </div><!-- form-group -->
                                </div><!-- col-2 -->
                                <div class="col-md-4 col-lg-2 mg-t-10 mg-lg-t-0">
                                    <div class="form-group mg-b-0">
                                    <input type="text" class="form-control form-control-datepicker tx-14" data-language="en" placeholder="Date of order to" name="orderto">
                                    </div><!-- form-group -->
                                </div><!-- col-2 -->
                            </div><!-- row -->
                            <div class="form-layout-footer">
                                <button class="btn buttons-html5" style="border: 1px solid black;">Search</button>
                            </div><!-- form-layout-footer -->
                        </div>
                    </form>
                </div><!-- card -->
            </div>
        </div>        
  
        <div class="pt-2">
            <div class="row row-sm justify-content-center new">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <!-- <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">Vendor Product Listing</h4> -->
                    <div class="card bg-white border-0 shadow-sm py-5 px-4">
                        <div class="" >
                            <div class="" >
                                <div class="pull-right col-md-4">
                                    <form class="form-inline my-2 my-lg-0 flex-nowrap w-100 w-md-75 ">
                                        <label class="ckbox mg-b-0" style="margin-bottom:0px;">
                                            <input type="checkbox" onchange="checkAll(this)" name="chk[]"><span></span>
                                        </label>
                                        <select class="form-control select2222" style="width:125px;">
                                                    <option value="1">Completed</option>
                                                    <option value="2">Pending</option>
                                                    <option value="3">Refunded</option>
                                                    <option value="4">Failed</option>
                                                    <option value="5">Canceled</option>
                                        </select>&nbsp;&nbsp;
                                        <button class="btn buttons-html5" style="border: 1px solid black;" type="submit">Submit</button>
                                    </form>
                                </div>
                                <div class="col-md-8"></div>
                            </div>
                            <div class="col-lg-12">
              <div class="msgDiv"></div>
            </div>
    </div>
    <div class="bd bd-gray-300 rounded table-responsive" >
        <table class="table mg-b-0">
            <thead>
                <tr>
                    <th></th>
                    <th>#</th>
                    <th>Reference No:</th>
                    <th>Payment Type</th>
                    <th>Unit</th>
                    <th>Ordered  At</th>
                    <th>Billed At</th>
                    <th>Shipping Address</th>
                    <th>Billing Address</th>
                    <th>Amount</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i=0;
                @endphp
                @foreach($orders as $key=>$value)
                <tr>
                    <td>
                        <label class="ckbox mg-b-0" style="margin-bottom:2px;">
                                <input type="checkbox" name="selected[]" value="$value->id" class="check_box"><span></span>
                        </label>
                    </td>
                    <td scope="row">{{++$i}}</td>
                    <td scope="row">{{$value->order_reference_number}}
                        <br>
                        <a target="_blank"><label class="badge badge-info customer_dialog" id="">View Customer</label></a>|
                            <div class="cus_dialog" title="Customer Details">
                                <table class="table table-hover"  style="width:100%">
                                    <tbody>
                                        <tr scope="row">
                                            <td style="color:white">Customer Name</td>
                                            <td style="color:white">@if($value->user){{$value->user->name}}@endif</td>
                                        </tr>
                                        <tr scope="row">
                                            <td style="color:white">Customer Email</td>
                                            <td style="color:white">@if($value->user){{$value->user->email}}@endif</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        <a href="{{route('viewProductsOrder',['orderid'=>$value->id])}}" target="_blank"><label class="badge badge-warning">View Product</label></a>                
                    </td>
                    <td scope="row">@if($value->payment){{$value->payment->payment_type}}@endif</td>
                    <td scope="row"></td>
                    <td scope="row">{{$value->order_at}}</td>
                    <td scope="row">{{$value->paid_at}}</td>
                    <td scope="row">{{$value->shipping_address_id}}</td>
                    <td scope="row">{{$value->billing_address_id}}</td>
                    <td scope="row">{{$value->total}}</td>
                    <td scope="row">
                                       @if($value->status=='5')
                                       <label class="badge badge-danger p-2">Canceled</label>
                                       @else
                                        <select class="form-control select22" style="width:120px;" data-id="{{$value->id}}">
                                            <option @if($value->status=='1'){{'selected'}}@endif value="1">Completed</option>
                                            <option @if($value->status=='2'){{'selected'}}@endif value="2">Pending</option>
                                            <option @if($value->status=='3'){{'selected'}}@endif value="3">Refunded</option>
                                            <option @if($value->status=='4'){{'selected'}}@endif value="4">Failed</option>
                                            <option @if($value->status=='5'){{'selected'}}@endif value="5">Canceled</option>
                                        </select>
                                        @endif
                    </td>   
                </tr>
                @endforeach
            </tbody>
        </table>
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                            {{ $orders->appends(['search' => $search])->links() }}
                            </div>
                        </div>
                    </div>
                    <!-- bd -->
                </div>
            </div>
        </div><!-- row -->
    </div>
@endsection
@section('scripts')
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/redmond/jquery-ui.css" rel="stylesheet" type="text/css"/>  
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script>
        $('.cus_dialog').hide();
        $('.customer_dialog').click(function(){
            $(".cus_dialog").dialog({
                dialogClass: "cus_dialog",
                modal: false, 
                height: 200, 
                width: 500,
                show:
                {
                    effect:"blind",
                    duration:100
                },
            });
            $(".cus_dialog .ui-widget-content").css("background-color", "#343a40");
            $(".cus_dialog .ui-dialog-titlebar").css("background", "#008b8b"); 
        });
</script>
<script>
    //menu active
    $('#order_menu').find('a').addClass('active');
</script>
<script>
$(document).ready(function(){
      $('.select22').change(function(){
          var id= $(this).data('id');
          var item =$(this);
          var selectedstatus = $('.select22 option:selected').val();
          if((id=='') || (selectedstatus=='')){
            $('.msgDiv').html(`<p class="alert alert-danger mg-b-4">Unable to process your request</p>`).focus();
            setTimeout(function() {
              $('.msgDiv').html(``);
              window.location.reload();
            }, 1000);
          }else{
              if(selectedstatus == '5'){
                if(confirm("are you sure? Action can't be undone")){
                    $.ajax({
                    url: "{{route('orderChangeStatus')}}",
                    data: {'id':id,'status':selectedstatus} ,
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'POST',
                    success: function(data) {
                        $('.msgDiv').html(data).focus();
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                            window.location.reload();
                            }, 2000);
                        }
                    });
                }else{
                    return false;
                }
              }
              else{
                $.ajax({
                url: "{{route('orderChangeStatus')}}",
                data: {'id':id,'status':selectedstatus} ,
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'POST',
                success: function(data) {
                    $('.msgDiv').html(data).focus();
                    setTimeout(function() {
                        $('.msgDiv').html(``);
                        window.location.reload();
                        }, 1000);
                    }
                });
              }
          }

      });
  });
</script>
<script>
    $('#select_all').change(function(){
        $('.check_box').attr('checked',this.checked);
    });
</script>
@endsection