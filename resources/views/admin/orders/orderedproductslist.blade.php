@extends('admin.layouts.app')
@section('content')
<div class="br-pagebody">
    <div class="container-fluid pt-2">
        <div class="row row-sm justify-content-center new">
            <div class="col-12 col-sm-10 col-md-12 col-lg-12">
                 <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">Order Product Listing</h4>
                <div class="card bg-white border-0 shadow-sm py-5 px-4">
                    <div class="bd bd-gray-300 rounded table-responsive">
                        <table class="table mg-b-0">
                            <thead>
                                <tr>
                                    <th class="wd-5p">#</th>
                                    <th class="wd-10p">NAME</th>
                                    <th class="wd-10p">DCIN</th>
                                    <th class="wd-10p">Image</th>
                                    <th class="wd-10p">PRICE</th>
                                    <th class="wd-10p">MRP</th>
                                    <th class="wd-10p">BRAND</th>
                                    <th class="wd-10p">CATEGORY</th>
                                    <th class="wd-10p">Comments</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $i= 0;
                            @endphp
                            @foreach ($products as $key=> $item)
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                <td scope="row">
                                    @if($item->productlist){{$item->productlist->name}} <br><label class="badge badge-success">Approved</label>@endif
                                </td>
                                <td scope="row">@if($item->productlist){{$item->productlist->dcin}}@endif</td>
                                <td>
                                    @if($item->productlist)
                                        @if($item->productlist->images->first())
                                            @if(file_exists($item->productlist->images->first()->image))
                                                <a target="_blank" href="{{asset($item->productlist->images->first()->image)}}"><img src="{{asset($item->productlist->images->first()->image)}}" style="width:80px;"></a>
                                            @endif
                                        @endif
                                    @endif
                                </td>
                                <td scope="row">@if($item->productlist){{$item->productlist->price}}@endif</td>
                                <td scope="row">@if($item->productlist){{$item->productlist->mrp}}@endif</td>
                                <td scope="row"> 
                                    @if($item->productlist)
                                        @if($item->productlist->brand)
                                            {{$item->productlist->brand->name}}
                                        @else
                                        {{$item->productlist->other_brand}}
                                        @endif
                                    @endif
                                </td>
                                <td scope="row">@if($item->productlist){{$item->productlist->category->name}}@endif</td>
                                <td scope="row">
                                    no comments
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                     
                        </div>
                    </div>
                </div>
                <!-- bd -->
            </div>
        </div>
    </div><!-- row -->
</div>
@include('ui.ajaxloader')
@endsection
@section('scripts')
<script>
    //menu active
    $('#order_menu').find('a').addClass('active');
</script>

@endsection