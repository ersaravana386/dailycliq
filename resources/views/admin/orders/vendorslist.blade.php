@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-10">
        <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
        <span class="breadcrumb-item active">Flash Sale</span>
    </nav>
</div><!-- br-pageheader -->
<div class="br-pagetitle">
    <i class="icon icon ion-ios-bookmarks-outline"></i>
    <div>
        <h4>Orders Report</h4>
        <p class="mg-b-0">Vendors Listing</p>
    </div>
</div><!-- d-flex -->

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <div id="datatable1_filter" class="dataTables_filter">
            <div class="row">
                <div class="col-lg-9">
                    <form action="" method="get">
                        <div class="input-group input-group-md mb-3 col-md-4">
                            <input type="text" name="search" value="" placeholder="search" class="form-control" value="">
                            <div class="input-group-append">
                                <span class="input-group-btn">
                                    <button class="btn btn-default input-group-close-icon" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2">
            </div>
        </div>
    </div>

    <div class="bd bd-gray-300 rounded table-responsive">
    <table class="table mg-b-0">
            <thead>
                <tr>
                    <th class="wd-10p">#</th>
                    <th class="wd-10p">vendor Name</th>
                    <th class="wd-10p">Completed Orders</th>
                    <th class="wd-10p">Pending Orders</th>
                    <th class="wd-10p">Failed Orders</th>
                    <th class="wd-10p">Refunded Orders</th>
                    <th class="wd-10p">cancelled Orders</th>
                    <th class="wd-5p">Actions</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i=0;
                @endphp
                @foreach($details as $detail)
                <tr>
                    <td scope="row">{{$i=$i+1}}</td>
                    <td scope="row">{{$detail->company_name}}</td>
                    <td scope="row">
                            {{$detail->completed_orders}}      
                    </td>
                    <td>{{$detail->pending_orders}}</td>
                    <td>{{$detail->failed_orders}}</td>
                    <td>{{$detail->refunded_orders}}</td>
                    <td>{{$detail->cancelled_orders}}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a class="btn btn-outline-teal" href="{{route('showOrdersOrder',['vendor'=>$detail->id])}}" title="View Orders"> 
                                <i class="menu-item-icon   fa fa-eye"></i>
                            </a>
                        <div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
              {{ $details->appends(['search' => $search])->links() }}
        </div>
  
    </div><!-- bd -->

</div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
@include('ui.ajaxloader')
@endsection
@section('scripts')
</script>
<script>
    //menu active
    $('#order_menu').find('a').addClass('active');
</script>
<script>
    $(document).ready(function () {
            $("#select_all").click(function () {
                $(".check_box").attr('checked', this.checked);
            });
    });
</script>
<script>
    $('#approve_status').change(function (e) {
        var val= $('#approve_status').val();
        var checked = [];
        $.each($("input[name='selected_vals[]']:checked"), function(){
            checked.push($(this).val());
        });
        if(checked.length!=0)            
            if(confirm("are you sure?")){
                $.ajax({
                    type:'post',
                    url :"{{route('multipleVendorApproveFlashSale')}}",
                    dateType:'json',
                    data:{checked:checked,val:val},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data){
                        if(data.status == true){ 
                            location.reload(); 
                        }
                    },error:function(data){
                        console.log(data);
                        alert('error,try again');
                    }
                });
            }else{
                    return false;
            }
        else{
            return false;
        }
    });
</script>
@endsection