@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-10">
        <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
        <span class="breadcrumb-item active">Users</span>
    </nav>
</div><!-- br-pageheader -->
<div class="br-pagetitle">
    <i class="icon icon ion-ios-bookmarks-outline"></i>
    <div>
        <h4>Users</h4>
        <p class="mg-b-0">Users Listing</p>
    </div>
</div><!-- d-flex -->

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <div id="datatable1_filter" class="dataTables_filter">
            <div class="row">
                <div class="col-lg-9">
                    <form action="" method="get">
                        <div class="input-group input-group-md mb-3 col-md-4">
                            <input type="text" name="search" value="" placeholder="search" class="form-control" value="">
                            <div class="input-group-append">
                                <span class="input-group-btn">
                                    <button class="btn btn-default input-group-close-icon" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2">
            </div>
        </div>
    </div>

    <div class="col-lg-12 p-r-0 title-margin-right " id="msgShowDiv">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 msgDiv"></div> 
            </div>
    </div>
    <div class="bd bd-gray-300 rounded table-responsive" style="">
        <table class="table mg-b-0">
            <thead>
                <tr>
                    <th class="wd-5p">#</th>
                    <th class="wd-10p">Name</th>
                    <th class="wd-10p">Email</th>
                    <th class="wd-10p">Gender</th>
                    <th class="wd-10p">DOB</th>
                    <th class="wd-10p">Contact</th>
                    <th class="wd-10p">Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i=0;
                @endphp
                @foreach($users as $key=>$value)
                <tr>
                    <td>{{++$i}}</td>
                    <td>{{$value->first_name}}&nbsp{{$value->last_name}}</td>
                    <td>{{$value->email}}</td>
                    <td>@if($value->gender == "m"){{'Male'}}
                        @else {{'Female'}}    
                        @endif
                    </td>
                    <td>{{$value->dob}}</td>
                    <td>{{$value->mobile}}</td>
                    <td>
                        @if($value->status == 1)
                            <select name="user_action" id="user_action" class="form-control select22" style="width:140px">
                                <option value="" selected disabled>{{'User Blocked'}}</option>
                                <option value="0" data-id="{{$value->id}}">{{'Unblock User'}}</option>
                            </select>
                        @else 
                            <select name="user_action" id="user_action" class="form-control select22" style="width:140px">
                                <option value="" selected disabled>{{'Select Action'}}</option>
                                <option value="1" data-id="{{$value->id}}">{{'Block User'}}</option>
                            </select>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
              {{ $users->appends(['search' => $search])->links() }}
        </div>
  
    </div><!-- bd -->

</div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
@include('ui.ajaxloader')
@endsection
@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    //menu active
    $('#user_menu').find('a').addClass('active');
</script>
<script>
 $(document).ready(function(){
    $('#user_action').change(function(e) {
        var val= $('#user_action').val();
        var id= $(this).find(':selected').data('id');
        if(id=='' || val == ''){
            $('.msgDiv').html(`<p class="alert alert-danger mg-b-4">Unable to process your request</p>`).focus();
            setTimeout(function() {
              $('.msgDiv').html(``);
              window.location.reload();
            }, 1000);
          }else{
            if(confirm("are you sure?")){
                $.ajax({
                    type:'post',
                    url :"{{route('changeStatusUserManagement')}}",
                    dateType:'json',
                    data:{'val':val,'id':id},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data){
                        if(data.status == true){
                            var message=data.message;
                            $('.msgDiv').html('<p class="alert alert-success mg-b-4">' + message + '</p>').focus();
                            setTimeout(function() {
                            $('.msgDiv').html(``);
                                window.location.reload();
                            }, 2000);
                        }else{
                            $('.msgDiv').html('<p class="alert alert-danger mg-b-4">Unable To Make Any Change</p>').focus();
                            setTimeout(function() {
                            $('.msgDiv').html(``);
                                window.location.reload();
                            }, 2000);
                        }
                    },error:function(data){
                        $('.msgDiv').html(`<p class="alert alert-danger mg-b-4">Unable to process your request</p>`).focus();
                            setTimeout(function() {
                            $('.msgDiv').html(``);
                                window.location.reload();
                            }, 2000);
                    }
                });
            }else{
                    return false;
            }
         }       
    });
 });
</script>
endsection