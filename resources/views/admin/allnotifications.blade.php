@extends('admin.layouts.app')
<style>
br{
    line-height:0.5em;
}
</style>
@section('content')
<div class="container">
    <div class="container" style="margin-top:100px;">
        <div class="page-header" style="border-bottom:1px solid #cfcfcf">
            <h5 style="font-family:Segoe UI">Notifications For You</h5>
            <a href="javascript:void(0)" style="font-family:calibri;float:right" id="mark_all_full">Mark All As Read</a>
        </div> 
        <div style="margin-top:20px;">
            <ul style="list-style-type: none; padding:0;">
            @foreach($notifications as $key=>$value)
                    <li style="border-bottom:1px solid;border-bottom-color:#c4c4c4">
                        <a href="{{route('viewAdminNotification',['id'=>$value->id])}}" class='media-list-link read' style="background-color:#dcdcde0">
                            <div class='media'>
                                <img src='https://via.placeholder.com/500' alt=''>
                                <div class='media-body'>
                                    <p class='noti-text'><strong>{{$value->process}}</strong></p><br>
                                    <p>{{$value->created_at->diffForHumans(null, true, true, 2)}}&nbsp{{'ago'}}</p>
                                    <span></span>
                                </div>
                            </div>
                        </a>
                    </li>
            @endforeach
            </ul>
        </div>      
    </div>
</div>
@include('ui.ajaxloader')
@endsection
@section('scripts')
<script>
$('#mark_all').click(function(){
      $.ajax({
        url:"{{route('adminMarkAllAsRead')}}",
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){
          if(data.status == 1){
            location.reload();
          }
        }
      });
    });
</script>
<script>
$('#mark_all_full').click(function(){
      $.ajax({
        url:"{{route('adminMarkAllAsRead')}}",
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){
          if(data.status == 1){
            location.reload();
          }
        }
      });
    });
</script>
@endsection