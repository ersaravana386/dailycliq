@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
    <a class="breadcrumb-item" href="{{route('showCategories')}}">Categories</a>
    <span class="breadcrumb-item active">New</span>
  </nav>
</div><!-- br-pageheader -->
@php
  $category_attribute_values = [];
  foreach ($category_attribute->attribute_values as $key => $value) {
    $category_attribute_values[] = $value->value;
  }   
  $category_attribute_values_string = implode(',',$category_attribute_values);
@endphp
<div class="br-pagebody">
    <div class="br-section-wrapper">
      <h6 class="br-section-label">Attributes</h6><br>
      <form id='pageForm' method="POST" action="javascript:;" data-parsley-validate>
          @method('POST')
        <div class="form-layout form-layout-1">
          <div class="row mg-b-25">            
            <div class="col-lg-6">
              <div class="form-group pd-5">
                <label class="form-control-label">Attribute Name: <span class="tx-danger">*</span></label>
                <input type="hidden" name="category" value="{{$category_attribute->category_id}}">
                <input type="hidden" name="attribute" value="{{$category_attribute->id}}">
                <input class="form-control form-control-sm" type="text" disabled name="attribute_name" value="{{$category_attribute->attribute}}">                
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-6">
              <div class="form-group pd-5">
                <label class="form-control-label">Searchable: <span class="tx-danger">*</span></label>
                <input name="searchable" type="checkbox" @if($category_attribute->searchable) checked @endif data-toggle="toggle" data-size="xs">                
              </div>
            </div><!-- col-6 -->
            <div class="col-lg-12 row" id="attribute_values_div">
              
            </div><!-- col-4 -->
          </div><!-- row -->
          <div class="form-layout-footer">
            <input type="submit" class="btn btn-info mg-b-10 " value="Update">
            <a class="btn btn-secondary mg-b-10 " href="{{route('listAttributes',['category'=>$category_attribute->category_id])}}">Cancel</a>
          </div><!-- form-layout-footer -->
          <div id="msgShowDiv"></div>
        </div><!-- form-layout -->
      </form>     
    </div><!-- br-section-wrapper -->
  </div><!-- br-pagebody -->
  <style>
    .attribute_value_label{
      background-color: #fbaa1a;
      margin: 0px 0px 10px 5px;
      padding: 5px 10px 5px 12px;
      border: solid thin lightgray;
      border-radius: 15px;
      line-height: 20px;
      font-size: 12px;
      min-width: 100px;
      text-align: center;
      cursor: pointer;
    }
    .attribute_value:checked + .attribute_value_label {
      font-weight:bold;
      font-size: 14px;
    }
  </style>
@endsection
@section('scripts')
<script>
  category_attribute_values = '{{$category_attribute_values_string}}'.split(',');
</script>
<script>
  let loader  = `
    <div class="text-center">
      <img style="width:60px;" src="{{asset('images/loader.gif')}}" alt="">
    </div>
  `;

  $('#pageForm').on('submit',function(){
    var data = $(this).serializeArray();
    if(data.length <5){
      return false;
    }
    $.ajax({
      type: "post",
      url: "{{route('updateCategoryAttribute')}}",
      data: $('#pageForm').serialize(),
      dataType: "json",
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(data) {
        if(data.status == 1){
          window.location.href = "{{route('listAttributes',['category'=>$category_attribute->category_id])}}"
        }else{
          var html = "";
          $.each(data.errors, function (key, value) {
              html += value + "<br/>";
          });
          setTimeout(function () {
              $('#ajaxLoader').modal('toggle');
              $('#msgShowDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
          }, 500);
          setTimeout(function () {
              $('#msgShowDiv').html(``);
          }, 4000);
        }
      }
    });
  });
</script>
 <script>
    //menu active
    $(document).ready(function(){
      setTimeout(function(){
        $('#attribute_values_div').html(loader);
        $.ajax({
          type: "get",
          url: "{{url('/admin/attributes/values').'/'.$attribute->id}}",
          data: $('#pageForm').serialize(),
          dataType: "json",
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(data) {
              options = "";
              if (data.status == 1) {
                $.each(data.attribute_values, function(key,value){
                  var checked = "";
                  if($.inArray(value.value,category_attribute_values) >= 0){
                    checked = "checked";
                  }
                  options +=  `<div class="attribute-options">
                      <input style="display:none;" type="checkbox" ${checked} class="attribute_value" name="attribute_value[]" value="${value.value}" id="attr${value.id}">
                      <label class="attribute_value_label" for="attr${value.id}">${value.value}</label>
                    </div>`;
                });            
              }
              $('#attribute_values_div').html(options);
          }
        });
        return false;
      },100);
    });
    $('#vendor_menu').find('a').addClass('active');
</script>
@endsection