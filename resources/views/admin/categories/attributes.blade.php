@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-10">
        <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
        <span class="breadcrumb-item active">Vendors</span>
    </nav>
</div><!-- br-pageheader -->
<div class="br-pagetitle">
    <i class="icon icon ion-ios-bookmarks-outline"></i>
    <div>
        <h4>Attributes</h4>
        <p class="mg-b-0">Attributes Listing</p>
    </div>
</div><!-- d-flex -->

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <div id="datatable1_filter" class="dataTables_filter">
            <div class="row">
                <div class="col-lg-9">
                </div>
                <div class="col-lg-2">
                    <div class="input-group input-group-md mb-3 col-md-4">
                        <a href="{{route('showNewAttribute',['category'=>$id])}}" class="btn btn-teal btn-with-icon" style="float-left" >
                            <div class="ht-40 justify-content-between">
                                <span class="icon wd-40"><i class="fa fa-plus"></i></span>
                                <span class="pd-x-15">Add New Attributes</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    <div class="col-lg-12 p-r-0 title-margin-right " id="msgShowDiv">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block sessionDiv">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        @endif
    </div>
    <div class="bd bd-gray-300 rounded table-responsive">
        <table class="table mg-b-0">
            <thead>
                <tr>
                    <th class="wd-20p">Attribute</th>
                    <th class="wd-20p">Searchable</th>
                    <th class="wd-20p">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($category->attributes as $attribute)
                <tr>
                    <td>{{$attribute->attribute}}</td>
                    <td>
                        <input data-id="{{$attribute->id}}" class="searchable" @if($attribute->searchable) checked @endif type="checkbox" data-toggle="toggle" data-size="xs"> 
                    </td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="{{route('showEditAttribute',['attribute'=>$attribute->id])}}" class="btn btn-sm btn-outline-teal" title="Edit category">
                                <i class="menu-item-icon fa fa-edit"></i>
                            </a>
                            <a href="javascript:;" data-id="{{$attribute->id}}" class="btn btn-sm btn-outline-teal btnDelete" title="Delete category">
                                <i class="menu-item-icon fa fa-trash"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div><!-- bd -->
</div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
@include('ui.ajaxloader')
@endsection
@section('scripts')
<script>
    //menu active
    $('#category_menu').find('a').addClass('active');
    $('.searchable').on('change',function(){
        var value = $(this).is(':checked');
        var id = $(this).data('id');
        $.ajax({
            type: "post",
            url: "{{route('updateSearchable')}}",   
            data: {
                id,
                value
            },
            dataType: "json",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                options = "";
                if (data.status == 1) {
                                
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $('.btnDelete').on('click',function(){
        var id = $(this).data('id');
        $.confirm({
            title: 'Delete!',
            content: 'Please confirm!',
            buttons: {
                confirm: function () {    
                    $.ajax({
                        type: "post",
                        url: "{{route('deleteAttribute')}}",   
                        data: {
                            id,
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {       
                                $.alert('Deleted!');                         
                                setTimeout(function(){
                                    window.location.reload();                                    
                                },1000);           
                            }
                        }
                    });    
                },
                cancel: function () {
                }
            }
        });
    });
</script>
@endsection