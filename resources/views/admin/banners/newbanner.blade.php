@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a class="breadcrumb-item" href="">Home</a>
    <a class="breadcrumb-item" href="   ">Banner</a>
    <span class="breadcrumb-item active">New</span>
  </nav>
</div><!-- br-pageheader -->
<div class="br-pagebody">
    <div class="br-section-wrapper">
      <h6 class="br-section-label">New Banner</h6><br>
      <div class="col-lg-12">
              <div class="msgDiv"></div>
      </div>
      <form id='pageForm' method="POST" action="javascript:;" enctype="multipart/form-data" data-parsley-validate>
          @method('POST')
        <div class="form-layout form-layout-1">
          <div class="row mg-b-25">
            <div class="col-lg-4">
              <div class="form-group pd-5">
                <label class="form-control-label">Banner type: <span class="tx-danger">*</span></label>
                    <select name="banner_type" id="banner_type" class="form-control form-control-sm select2">
                        <option selected disabled>Select Banner Type</option>
                        <option value="1">Home Banner</option>
                        <option value="2">Offer Banner</option>
                    </select>
              </div>
            </div><!-- col-4 -->
            <input type="text" value={{$product}} id="pro_id" name="pro_id" style="display:none">
            <div class="col-lg-4">
              <div class="form-group pd-5">
                <label class="form-control-label">Start Date: <span class="tx-danger">*</span></label>
              <input type="text" class="form-control" type="datetime-local" value='' name="startdate" id="startdate"  placeholder="Select Start Date">
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-4">
              <div class="form-group pd-5">
                <label class="form-control-label">End Date: <span class="tx-danger">*</span></label>
              <input type="text" class="form-control" type="datetime-local" value='' name="enddate" id="enddate"  placeholder="Select End Date">
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-4">
              <div class="form-group pd-5">
                <label class="form-control-label">Pathayapura Banner: <span class="tx-danger">*</span></label>
                    <select name="pathayapura_banner" id="pathayapura_banner" class="form-control form-control-sm select2">
                        <option selected disabled>Select Banner Type</option>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-4">
              <div class="form-group pd-5">
                <label class="form-control-label">Status: <span class="tx-danger">*</span></label>
                    <select name="acc_status" id="acc_status" class="form-control form-control-sm select2">
                        <option selected disabled>Select Banner Type</option>
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-4">
                <div class="form-group pd-5">
                  <label class="form-control-label">Image: </label>
                    <img src="" class="img-fluid wd-100" alt="" id="img">
                  <label for="image" class="if-outline if-outline-info"><i class="icon ion-ios-upload-outline tx-24"></i>
                  <input type="file" name="image" id="image" class="inputfile" >
                  <span id="img_label">Change file</span>
                  </label>
                </div>
            </div><!-- col-4-->
            <div class="col-lg-2">
              <div class="form-group pd-5">
                    <input type="submit" class="btn btn-info mg-b-10 mt-lg-4 mt-sm-0" value="Submit">
              </div>
            </div>
          </div><!-- row -->
        </div><!-- form-layout -->
      </form>     
</div>
    </div><!-- br-section-wrapper -->
  </div><!-- br-pagebody -->
@endsection
@section('scripts')
<link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js"></script>
    <script>
    //profile form
    $("#pageForm").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        rules: {
          startdate:{
            required: function(){
              if($('#banner_type').val() =="2"){
                return true;
              }
            }
            },
            enddate:{
            required: function(){
              if($('#banner_type').val() =="2"){
                return true;
              }
            }
            },
            banner_type:{
                required:true
            },
            image:{
                required:true
            },
            pathayapura_banner:{
                required:true
            },
            acc_status:{
              required:true
            }
        },
        submitHandler: function(form){
            $('.msgDiv').html(``);
            var id=$('#pro_val').val();
            if(id==''){
              $('.msgDiv').html(`<p class="alert alert-danger mg-b-4">Unable to process your request</p>`).focus();
              setTimeout(function() {
                $('.msgDiv').html(``);
                window.location.reload();
              }, 1000);
            }else{
            var data = new FormData(form);
            $.ajax({
                type: "post",
                url: "{{route('newProductBanner')}}",
                data:data,
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {

                    if (data.status == 1) {
                      $('.msgDiv').html(`<p class="alert alert-success mg-b-4">Successfully Added Banner.</p>`);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                            location.href = "";
                        }, 2000);
                    } else {
                        // var html = "";
                        // $.each(data.errors, function(key, value) {
                        //     html += value + "<br/>";
                        // });
                        $('.msgDiv').html(`<p class="alert alert-success mg-b-4">Unable to make any changes.</p>`);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                        }, 4000);
                    }
                },
                error:function(data) {
                  alert("");
                }
            });
          }
            return false;
        }
    });
</script>
<script>
    $(function(){
        // var today= new Date();
        // var day = today.getDate();
        // var month = today.getMonth();
        // var year = today.getFullYear();
        // var todayDate= year + '-' + month + '-' + day;
        // console.log(todayDate);
        $('#startdate').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:00',
            // autoclose: true,
            // todayBtn: true,
            // startDate : todayDate,
        });
        $('#enddate').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:00',
        });
    });
</script>
<script>
    $('#image').on('change', function() {
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $('#img_label').html(fileName);
    })
</script>
<script>
    $('#banner_type').change(function() {
       if($('#banner_type').val() == '1'){
        $("#startdate").attr('disabled','disabled');
        $('#startdate').val('');
        $("#enddate").attr('disabled','disabled');
        $('#enddate').val('');
       }
       else{
        $("#startdate").removeAttr('disabled');
        $("#enddate").removeAttr('disabled');
       }
    });
</script>
@endsection
