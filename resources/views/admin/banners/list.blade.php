@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-10">
        <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
        <span class="breadcrumb-item active">Banners</span>
    </nav>
</div><!-- br-pageheader -->
<div class="br-pagetitle">
    <i class="icon icon ion-ios-bookmarks-outline"></i>
    <div>
        <h4>Banners</h4>
        <p class="mg-b-0">Banners Listing</p>
    </div>
</div><!-- d-flex -->

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <div id="datatable1_filter" class="dataTables_filter">
            <div class="row">
                <div class="col-lg-9">
                    <form action="" method="get">
                        <div class="input-group input-group-md mb-3 col-md-4">
                            <input type="text" name="search" value="" placeholder="search" class="form-control" value="">
                            <div class="input-group-append">
                                <span class="input-group-btn">
                                    <button class="btn btn-default input-group-close-icon" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2">
            </div>
        </div>
    </div>
    @php 
        $i=0;
    @endphp   
    <div class="col-lg-12 p-r-0 title-margin-right " id="msgShowDiv">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block sessionDiv">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        @endif
    </div>
    <div class="bd bd-gray-300 rounded table-responsive">
        <table class="table mg-b-0">
            <thead>
                <tr>
                    <th class="wd-10p">#</th>
                    <th class="wd-20p">Product Name</th>
                    <th class="wd-15p">Category</th>
                    <th class="wd-15p">Vendor</th>
                    <th class="wd-15p">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                <tr>
                    <th scope="row">{{$i++}}</th>
                    <td scope="row">{{$product->name}}</td>
                    <td>@if($product->category){{$product->category->name}}@endif</td>
                    <td>@if($product->vendor){{$product->vendor->company_name}}@endif</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                        @if($product->banner)
                                <a href="{{route('editProductBanner',['id'=>$product->id])}}" class="btn btn-outline-teal" title="Edit Banner">
                                    <i class="menu-item-icon fa fa-edit"></i>
                                </a>
                        @else
                                <a href="{{route('addNewBanner',['id'=>$product->id])}}" class=" btn btn-outline-teal" onclick="" title="Add Banner">
                                    <i class="menu-item-icon fa fa-plus"></i>
                                </a>   
                        @endif   
                        </div>
                    </td>
                </tr>
               @endforeach
            </tbody>
        </table>
        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
            {{ $products->appends(['search' => $search])->links() }}
        </div>
    </div><!-- bd -->

</div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
<div id="blockPopup" class="modal fade">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form id="blockForm" action="javascript:;">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><label id="vendorSpanHeading"></label></h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pd-25">
                    <h4 class="lh-3 mg-b-20 tx-inverse">Are you sure you want to block<br><label id="categorySpanName"></label>?</h4>
                    <p class="mg-b-4 blockmsgDiv"></p>
                </div>
                <input type="hidden" name="id" id="categoryId">
                <div class="modal-footer">
                    <input type="submit" id="blockUnblockBtn" class="btn btn-primary tx-11  pd-y-12 pd-x-25 tx-mont tx-medium" value="Continue">
                    <button type="button" class="btn btn-secondary tx-11  pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </div><!-- modal-dialog -->
</div><!-- modal -->
@include('ui.ajaxloader')
@endsection
@section('scripts')
<script>
    //menu active
    $('#brands_menu').find('a').addClass('active');
</script>
<script type="text/javascript">

    function addFeatured(brand,name){
        if(confirm(`Do you really want to make this brand "${name}" as featured?`)){
            $.ajax({
                url: "{{route('brandAddFeatured')}}",
                data:{
                    brand
                },
                type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.status == true) {
                        window.location.reload();
                    } else {
                        var html = "";
                        $.each(data.errors, function(key, value) {
                            html += value + "<br/>";
                        });
                        setTimeout(function() {
                            $('#ajaxLoader').modal('toggle');
                            $('#msgShowDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                        }, 500);
                        setTimeout(function() {
                            $('#msgShowDiv').html(``);
                        }, 4000);
                    }

                }
            });        
        }
    }

    function deactivate(id, name) {
        showConfirmation(id, name);
    }

    function showConfirmation(id, name) {
        el = $('#blockPopup');
        $(el).find('#categorySpanName').html(name);
        $(el).find('#categoryId').val(id);
        $(el).modal('show');
    }

    function blockCategory(data) {
        $('#blockPopup').modal('toggle');
        $('#ajaxLoader').modal('toggle');
        $.ajax({
            url: "{{route('blockCategory')}}",
            data,
            type: 'post',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                if (data.status == true) {
                    window.location.reload();
                } else {
                    var html = "";
                    $.each(data.errors, function(key, value) {
                        html += value + "<br/>";
                    });
                    setTimeout(function() {
                        $('#ajaxLoader').modal('toggle');
                        $('#msgShowDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                    }, 500);
                    setTimeout(function() {
                        $('#msgShowDiv').html(``);
                    }, 4000);
                }

            }
        });
    }

    $('#blockForm').on('submit', function() {
        blockCategory($(this).serialize());
        return false;
    });
</script>
@endsection