@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-10">
        <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
        <span class="breadcrumb-item active">Product Review</span>
        <span class="breadcrumb-item active">Reviews</span>
    </nav>
</div><!-- br-pageheader -->
<div class="br-pagetitle">
    <i class="icon icon ion-ios-bookmarks-outline"></i>
    <div>
        <h4>Reviews</h4>
        <p class="mg-b-0">Review Listing</p>
    </div>
</div><!-- d-flex -->

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <div id="datatable1_filter" class="dataTables_filter">
            <div class="row">
                <div class="col-lg-9">
                    <form action="" method="get">
                        <div class="input-group input-group-md mb-3 col-md-4">
                            <input type="text" name="search" value="" placeholder="search" class="form-control" value="">
                            <div class="input-group-append">
                                <span class="input-group-btn">
                                    <button class="btn btn-default input-group-close-icon" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2">
                    <div class="input-group input-group-md mb-3 col-md-4">
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 p-r-0 title-margin-right " id="msgShowDiv">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block sessionDiv">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        @endif
    </div>
    <div class="bd bd-gray-300 rounded table-responsive">
        <table class="table mg-b-0">
            <thead>
                <tr>
                    <th class="wd-10p">#</th>
                    <th class="wd-10p">customer Name</th>
                    <th class="wd-10p">Product Name</th>
                    <th class="wd-10p">Heading</th>
                    <th class="wd-10p">Comment</th>
                    <th class="wd-10p">Stars</th>
                    <th class="wd-10p">Status</th>
                    <th class="wd-10p">Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i=0;
                @endphp
                @foreach($products as $key=>$value)
                <tr>
                    <td scope="row">{{$i=$i+1}}</td>
                    <td scope="row">@if($value->customer){{$value->customer->name}}@endif</td>
                    <td scope="row">@if($value->product){{$value->product->name}}@endif</td>
                    <td scope="row">{{$value->heading}}</td>
                    <td scope="row">{{$value->comment}}</td>
                    <td scope="row">{{$value->star}}</td>
                    <td scope="row">
                        @if($value->approve == 1)
                            <label class="badge badge-success">Approved</label>
                        @else
                            <label class="badge badge-danger">Rejected</label>
                        @endif
                    </td>
                    <td scope="row">
                        @if($value->approve == 1)
                            <div>
                                <a class="btn btn-outline-teal" href="{{route('reviewStatusProductReview',['review'=>$value->id,'status'=>0])}}" title="Reject Review">
                                    <i class="menu-item-icon fa fa-trash"></i>
                                </a>
                            <div>
                        @else
                            <div>
                                <a class="btn btn-outline-teal" href="{{route('reviewStatusProductReview',['review'=>$value->id,'status'=>1])}}" title="Approve Review">
                                    <i class="menu-item-icon fa fa-check"></i>
                                </a>
                            <div>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
              {{ $products->appends(['search' => $search])->links() }}
        </div>
  
    </div><!-- bd -->

</div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
@include('ui.ajaxloader')
@endsection
@section('scripts')
<script>
    //menu active
    $('#review_menu').find('a').addClass('active');
</script>
<script type="text/javascript">
    function deactivate(id, name) {
        showConfirmation(id, name);
    }

    function showConfirmation(id, name) {
        el = $('#blockPopup');
        $(el).find('#categorySpanName').html(name);
        $(el).find('#categoryId').val(id);
        $(el).modal('show');
    }

    function blockCategory(data) {
        $('#blockPopup').modal('toggle');
        $('#ajaxLoader').modal('toggle');
        $.ajax({
            url: "{{route('blockCategory')}}",
            data,
            type: 'post',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                if (data.status == true) {
                    window.location.reload();
                } else {
                    var html = "";
                    $.each(data.errors, function(key, value) {
                        html += value + "<br/>";
                    });
                    setTimeout(function() {
                        $('#ajaxLoader').modal('toggle');
                        $('#msgShowDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                    }, 500);
                    setTimeout(function() {
                        $('#msgShowDiv').html(``);
                    }, 4000);
                }

            }
        });
    }

    $('#blockForm').on('submit', function() {
        blockCategory($(this).serialize());
        return false;
    });
</script>
@endsection