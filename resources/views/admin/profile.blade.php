@extends('admin.layouts.app')
@section('content')
<div class="br-profile-page">

    <div class="card shadow-base bd-0 rounded-0 widget-4">
      <div class="card-header ht-75">
      </div><!-- card-header -->
      <div class="card-body">
        <div class="card-profile-img ht-75" style="height:50px;">
          <img src="{{asset(Auth::user()->image)}}" alt="">
        </div><!-- card-profile-img -->
        <h4 class="tx-normal tx-roboto tx-white mg-b-10">{{Auth::user()->name}}</h4>
        <p class="mg-b-1">{{Auth::user()->email}}</p>
        <p class="mg-b-20"> {{Auth::user()->mobile}}</p>
      </div><!-- card-body -->
    </div><!-- card -->

    <div class="br-pagebody">
      <div class="br-section-wrapper">
        <h6 class="br-section-label">Edit Profile</h6>
        <form id='profileForm' method="POST" action="javascript:;">
          <div class="form-layout form-layout-1">
            <div class="row mg-b-25">

              <div class="col-lg-6">
                <div class="form-group">
                  <label class="form-control-label">Name: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="name" value="{{Auth::user()->name}}" placeholder="Enter Name">
                </div>
              </div><!-- col-6-->
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="form-control-label">Email address: <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="email" value="{{Auth::user()->email}}" placeholder="Enter email address">
                </div>
              </div><!-- col-6-->
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="form-control-label">Mobile number: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="mobile" value="{{Auth::user()->mobile}}" placeholder="Enter Mobile number">
                  <input type="hidden" name="id" value="{{Auth::user()->id}}" >
                </div>
              </div><!-- col--6-->
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="form-control-label">Profile Image: <span class="tx-danger">*</span></label>
                  <input type="file" name="image" id="file-2" class="inputfile" data-multiple-caption="{count} files selected">
                  <label for="file-2" class="if-outline if-outline-info"><i class="icon ion-ios-upload-outline tx-24"></i>
                    <span>Choose a file</span>
                  </label>
                </div>
              </div><!-- col--6-->

            </div><!-- row -->
            <div class="form-layout-footer">
              <input type="submit" class="btn btn-info mg-b-10" value="Update" >
              <a class="btn btn-secondary mg-b-10 " href="{{route('dashboard')}}">Cancel</a>
              <div class="msgDiv"></div>
            </div><!-- form-layout-footer -->
          </div><!-- form-layout -->
        </form>
        <form id='passwordForm' method="POST" action="javascript:;">
            @method('POST')
        <h6 class="br-section-label">Change Password</h6>
       
        <div class="form-layout form-layout-1">
            <div class="row mg-b-25">
  
              
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Current password: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="password" name="current_password" id="current_password"  placeholder="Current password">
                </div>
              </div><!-- col-4-->
  
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">New password: <span class="tx-danger">*</span></label>
                <input class="form-control" type="password" name="new_password" id="new_password" placeholder="New password">
                </div>
              </div><!-- col-4-->
  
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Confirm password: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="password" name="confirm_password" id="confirm_password"  placeholder="Confirm password">
                  <input type="hidden" name="id" value="{{Auth::user()->id}}" >
                </div>
              </div><!-- col--4-->

            </div><!-- row -->
            <div class="form-layout-footer">
              <button class="btn btn-info mg-b-10 ">Update</button>
              <a class="btn btn-secondary mg-b-10 " href="{{route('dashboard')}}">Cancel</a>
              <div class="msgDiv2"></div>
            </div><!-- form-layout-footer -->
          </div><!-- form-layout -->
        </form>
      </div><!-- br-section-wrapper -->
    </div><!-- br-pagebody -->

  </div><!-- br-profile-page -->
  <!-- ########## END: MAIN PANEL ########## -->
@endsection
@section('scripts')
<script>
    //profile form
    $("#profileForm").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            mobile: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Name is required"
            },
            email: {
                required: "Email is required",
                email: "Email is not valid"
            },
            mobile: {
                required: "Mobile is required"
            }
        },
        submitHandler: function(form) {
            $('.msgDiv').html(``);
            var form = document.getElementById('profileForm');
              var data = new FormData(form);
            $.ajax({
                type: "post",
                url: "{{route('updateProfile')}}",
                data: data,
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.status == 1) {
                        $('.msgDiv').html(`
                            <div class="alert alert-success">` + data.message + `</div>
                        `);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                            location.reload();
                        }, 1000);

                    } else {
                        var html = "";
                        $.each(data.errors, function(key, value) {
                            html += value + "<br/>";
                        });
                        $('.msgDiv').html(`
                            <div class="alert alert-danger m-2">` + html + `</div>
                        `);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                        }, 4000);
                    }
                }
            });
            return false;
        }
    });

    $("#passwordForm").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        rules: {
            current_password: {
                required: true
            },
            new_password: {
                required: true
            },
            confirm_password: {
                required: true,
                equalTo: '#new_password'
            }
        },
        messages: {
            current_password: {
                required: "Current Password is required"
            },
            new_password: {
                required: "New Password is required"
            },
            confirm_password: {
                required: "Confirm Password is required",
                equalTo: 'Password and Confirm Password should be same'
            }
        },
        submitHandler: function(form) {
            $('.msgDiv2').html(``);
            $.ajax({
                type: "post",
                url: "{{route('updateProfilePassword')}}",
                data: $(form).serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.status == 1) {
                        $('.msgDiv2').html(`
                            <div class="alert alert-success">` + data.message + `</div>
                        `);
                        setTimeout(function() {
                            $('.msgDiv2').html(``);
                        }, 6000);
                        $('#current_password').val('');
                        $('#new_password').val('');
                        $('#confirm_password').val('');
                    } else {
                        var html = "";
                        $.each(data.errors, function(key, value) {
                            html += value + "<br/>";
                        });
                        $('.msgDiv2').html(`
                            <div class="alert alert-danger m-2">` + html + `</div>
                        `);
                        setTimeout(function() {
                            $('.msgDiv2').html(``);
                        }, 6000);
                    }
                }
            });
            return false;
        }
    });
</script>
@endsection