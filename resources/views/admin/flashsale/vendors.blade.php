@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-10">
        <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
        <span class="breadcrumb-item active">Flash Sale</span>
        <span class="breadcrumb-item active">Vendors</span>
    </nav>
</div><!-- br-pageheader -->
<div class="br-pagetitle">
    <i class="icon icon ion-ios-bookmarks-outline"></i>
    <div>
        <h4>Vendors</h4>
        <p class="mg-b-0">Vendors Listing</p>
    </div>
</div><!-- d-flex -->

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <div id="datatable1_filter" class="dataTables_filter">
            <div class="row">
                <div class="col-lg-9">
                    <form action="" method="get">
                        <div class="input-group input-group-md mb-3 col-md-4">
                            <input type="text" name="search" value="" placeholder="search" class="form-control" value="">
                            <div class="input-group-append">
                                <span class="input-group-btn">
                                    <button class="btn btn-default input-group-close-icon" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2">
                    <div class="input-group input-group-md mb-3 col-md-4">
                        <a href="{{route('newFlashSale')}}" class="btn btn-teal btn-with-icon" style="float-left" >
                        <!-- <div class="ht-40 justify-content-between">
                            <span class="icon wd-40"><i class="fa fa-plus"></i></span>
                            <span class="pd-x-15">Add New Flash Sale</span>
                        </div> -->
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
              <div class="msgDiv"></div>
    </div>
    <table class="form-inline my-2 my-lg-0 flex-nowrap w-100 w-md-75">
            <td></td><td></td><td></td>
            <td class="wd-5p">    
                    <label class="ckbox mg-b-0" style="margin-bottom:0px;">
                        <input type="checkbox" name="select_all" id="select_all" class=""><span></span>
                    </label>
            </td>
            <td>
                <select name="" id="approve_status" class="form-control select2222" style="width:160px;">
                    <option value="" selected disabled>Select Action</option>
                    <option value="1" id="approve">Approve All Products</option>
                    <option value="0" id="reject">Reject All Products</option>
                </select>
            </td>
    </table>
    <div class="bd bd-gray-300 rounded table-responsive" style="margin-top:10px;">
        <table class="table mg-b-0">
            <thead>
                <tr>
                    <th  class="wd-5p"></th>
                    <th class="wd-15p">#</th>
                    <th class="wd-15p">vendor Name</th>
                    <th class="wd-15p">product Count</th>
                    <th class="wd-15p">Actions</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i=0;
                @endphp
                @foreach($products as $detail)
                <tr>
                    <td>
                        <label class="ckbox mg-b-0" style="margin-bottom:0px;">
                            <input type="checkbox" name="selected_vals[]"  value='{{$detail->id}}' class="check_box"><span></span>
                        </label>
                    </td>
                    </label>
                    <td scope="row">{{$i=$i+1}}</td>
                    <td scope="row">{{$detail->company_name}}</td>
                    <td scope="row">{{$detail->product_count}}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a class="btn btn-outline-teal" href="{{route('productApproveFlashSale',['flashsaleid'=>$detail->flash_sale_id,'id'=>$detail->id])}}" title="View Products"> 
                                <i class="menu-item-icon fa fa-eye"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
              {{ $products->appends(['search' => $search])->links() }}
        </div>
  
    </div><!-- bd -->

</div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
@include('ui.ajaxloader')
@endsection
@section('scripts')
</script>
<script>
    //menu active
    $('#flash_menu').find('a').addClass('active');
</script>
<script>
    $(document).ready(function () {
            $("#select_all").click(function () {
                $(".check_box").attr('checked', this.checked);
            });
    });
</script>
<script>
    $('#approve_status').change(function (e) {
        var val= $('#approve_status').val();
        if(val == 0 ){
            var message= "Rejected All Products";
        }else{
            var message= "Approved All Products";
        }
        var checked = [];
        $.each($("input[name='selected_vals[]']:checked"), function(){
            checked.push($(this).val());
        });
        if( val == "" || checked.length == 0 ){
            $('.msgDiv').html(`<p class="alert alert-danger mg-b-4">Unable to process your request</p>`).focus();
            setTimeout(function() {
              $('.msgDiv').html(``);
              window.location.reload();
            }, 2000);
        }
        if(checked.length!=0)            
            if(confirm("are you sure?")){
                $.ajax({
                    type:'post',
                    url :"{{route('multipleVendorApproveFlashSale')}}",
                    dateType:'json',
                    data:{checked:checked,val:val},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data){
                        if(data.status == true){ 
                            $('.msgDiv').html('<p class="alert alert-success mg-b-4">' + message + '</p>').focus();
                            setTimeout(function() {
                            $('.msgDiv').html(``);
                                window.location.reload();
                            }, 2000);
                        }else{
                            $('.msgDiv').html('<p class="alert alert-danger mg-b-4">Unable To Make Any Change</p>').focus();
                            setTimeout(function() {
                            $('.msgDiv').html(``);
                                window.location.reload();
                            }, 2000);
                        }
                    },error:function(data){
                        console.log(data);
                        $('.msgDiv').html(`<p class="alert alert-danger mg-b-4">Unable to process your request</p>`).focus();
                            setTimeout(function() {
                            $('.msgDiv').html(``);
                                window.location.reload();
                            }, 2000);
                    }
                });
            }else{
                    return false;
            }
        else{
            return false;
        }
    });
</script>
<script type="text/javascript">
    function deactivate(id, name) {
        showConfirmation(id, name);
    }

    function showConfirmation(id, name) {
        el = $('#blockPopup');
        $(el).find('#categorySpanName').html(name);
        $(el).find('#categoryId').val(id);
        $(el).modal('show');
    }

    function blockCategory(data) {
        $('#blockPopup').modal('toggle');
        $('#ajaxLoader').modal('toggle');
        $.ajax({
            url: "{{route('blockCategory')}}",
            data,
            type: 'post',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                if (data.status == true) {
                    window.location.reload();
                } else {
                    var html = "";
                    $.each(data.errors, function(key, value) {
                        html += value + "<br/>";
                    });
                    setTimeout(function() {
                        $('#ajaxLoader').modal('toggle');
                        $('#msgShowDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                    }, 500);
                    setTimeout(function() {
                        $('#msgShowDiv').html(``);
                    }, 4000);
                }

            }
        });
    }

    $('#blockForm').on('submit', function() {
        blockCategory($(this).serialize());
        return false;
    });
</script>


@endsection