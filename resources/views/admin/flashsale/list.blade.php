@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-10">
        <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
        <span class="breadcrumb-item active">Flash Sale</span>
    </nav>
</div><!-- br-pageheader -->
<div class="br-pagetitle">
    <i class="icon icon ion-ios-bookmarks-outline"></i>
    <div>
        <h4>Flash Sales</h4>
        <p class="mg-b-0">Flash Sales Listing</p>
    </div>
</div><!-- d-flex -->

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <div id="datatable1_filter" class="dataTables_filter">
            <div class="row">
                <div class="col-lg-9">
                    <form action="" method="get">
                        <div class="input-group input-group-md mb-3 col-md-4">
                            <input type="text" name="search" value="" placeholder="search" class="form-control" value="">
                            <div class="input-group-append">
                                <span class="input-group-btn">
                                    <button class="btn btn-default input-group-close-icon" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2">
                    <div class="input-group input-group-md mb-3 col-md-4">
                        <a href="{{route('newFlashSale')}}" class="btn btn-teal btn-with-icon" style="float-left" >
                        <div class="ht-40 justify-content-between">
                            <span class="icon wd-40"><i class="fa fa-plus"></i></span>
                            <span class="pd-x-15">Add New Flash Sale</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 p-r-0 title-margin-right " id="msgShowDiv">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block sessionDiv">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        @endif
    </div>
    <div class="bd bd-gray-300 rounded table-responsive">
        <table class="table mg-b-0">
            <thead>
                <tr>
                    <th class="wd-5p">#</th>
                    <th class="wd-15p">Start Date</th>
                    <th class="wd-15p">Start Time</th>
                    <th class="wd-15p">End Date</th>
                    <th class="wd-15p">End Time</th>
                    <th class="wd-15p">Status</th>
                    <th class="wd-20p">Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i=0;
                @endphp
                @foreach($details as $key=>$value)
                <tr>
                    <td scope="row">{{$i=$i+1}}</td>
                    <td scope="row">{{date('d-m-Y',strtotime($value->starting_date))}}</td>
                    <td scope="row">{{date('H:i:A',strtotime($value->starting_date))}}</td>
                    <td scope="row">{{date('d-m-Y',strtotime($value->ending_date))}}</td>
                    <td scope="row">{{date('H:i:A',strtotime($value->ending_date))}}</td>
                    <td scope="row">
                        @if($value->status == 0)
                                <a href="{{route('statusFlashSale',['id'=>$value->id])}}" class="btn btn-sm btn-danger">Activate</a>
                        @else 
                                <a href="{{route('statusFlashSale',['id'=>$value->id])}}" class="btn btn-sm btn-success">Activated</a>
                        @endif
                    </td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a class="btn btn-outline-teal" href="{{route('vendorsFlashSale',['flashsale'=>$value->id])}}" title="View Vendors"> 
                                <i class="menu-item-icon   fa fa-eye"></i>
                            </a>
                            <a class="btn btn-outline-teal" href="{{route('deleteFlashSale',['flashsale'=>$value->id])}}" title="Delete">
                                <i class="menu-item-icon fa fa-trash"></i>
                            </a>
                        <div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
              {{ $details->appends(['search' => $search])->links() }}
        </div>
  
    </div><!-- bd -->

</div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
@include('ui.ajaxloader')
@endsection
@section('scripts')
<script>
    //menu active
    $('#flash_menu').find('a').addClass('active');
</script>
<script type="text/javascript">
    function deactivate(id, name) {
        showConfirmation(id, name);
    }

    function showConfirmation(id, name) {
        el = $('#blockPopup');
        $(el).find('#categorySpanName').html(name);
        $(el).find('#categoryId').val(id);
        $(el).modal('show');
    }

    function blockCategory(data) {
        $('#blockPopup').modal('toggle');
        $('#ajaxLoader').modal('toggle');
        $.ajax({
            url: "{{route('blockCategory')}}",
            data,
            type: 'post',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                if (data.status == true) {
                    window.location.reload();
                } else {
                    var html = "";
                    $.each(data.errors, function(key, value) {
                        html += value + "<br/>";
                    });
                    setTimeout(function() {
                        $('#ajaxLoader').modal('toggle');
                        $('#msgShowDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                    }, 500);
                    setTimeout(function() {
                        $('#msgShowDiv').html(``);
                    }, 4000);
                }

            }
        });
    }

    $('#blockForm').on('submit', function() {
        blockCategory($(this).serialize());
        return false;
    });
</script>
@endsection