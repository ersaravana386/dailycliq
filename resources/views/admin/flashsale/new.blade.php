@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a class="breadcrumb-item" href="">Home</a>
    <a class="breadcrumb-item" href="   ">Flash Sale</a>
    <span class="breadcrumb-item active">New</span>
  </nav>
</div><!-- br-pageheader -->

<div class="br-pagebody">
    <div class="br-section-wrapper">
      <h6 class="br-section-label">New Flash Sale</h6><br>
      <form id='pageForm' method="POST" action="javascript:;" data-parsley-validate>
          @method('POST')
        <div class="form-layout form-layout-1">
          <div class="row mg-b-25">
            <div class="col-lg-12">
              <div class="msgDiv"></div>
            </div>
            <div class="col-lg-4">
              <div class="form-group pd-5">
                <label class="form-control-label">Start Date: <span class="tx-danger">*</span></label>
              <input class="form-control fc-datepicker" type="datetime-local" value="" name="startdate" id="startdate"  placeholder="">
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-4">
              <div class="form-group pd-5">
                <label class="form-control-label">End Date: <span class="tx-danger">*</span></label>
              <input class="form-control" type="datetime-local" value='' name="enddate" id="enddate"  placeholder=" ">
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-4">
              <div class="form-group pd-5">
                    <input type="submit" class="btn btn-info mg-b-10 mt-lg-4 mt-sm-0" value="Submit">
              </div>
            </div>
          </div><!-- row -->
        </div><!-- form-layout -->
      </form>     
</div>
    </div><!-- br-section-wrapper -->
  </div><!-- br-pagebody -->
@endsection
@section('scripts')
    <script>
    //profile form
    
    $("#pageForm").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        rules: {
            startdate: {
              required: true
            },
            enddate:{
                required:true
            }
        },
        messages: {
            startdate: {
              required: "Start Date is required"
            },
            enddate: {
                required: "End Date is required"
            }
        },
        submitHandler: function(form) {
            $('.msgDiv').html(``);
            var data = new FormData(form);
            $.ajax({
                type: "post",
                url: "{{route('saveFlashSale')}}",
                data: data,
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {

                    if (data.status == 1) {
                        
                        $('.msgDiv').html(` <div class="alert alert-success">Flash Sale Added Sucessfully</div>`);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                            location.href = "{{route('showFlashSale')}}";
                        }, 2000);

                    }else {
                        var html = "";
                        $.each(data.errors, function(key, value) {
                            html += value + "<br/>";
                        });
                        $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                        }, 2000);
                        if(data.datemessage){
                          $('.msgDiv').html(`<div class="alert alert-danger m-2">` + data.datemessage + `</div>`);
                          setTimeout(function() {
                            $('.msgDiv').html(``);
                          }, 2000);
                        }
                    }
                },
                error:function(data) {
                        $('.msgDiv').html(`<p class="alert alert-danger mg-b-4">Unable to process your request</p>`).focus();
                            setTimeout(function() {
                            $('.msgDiv').html(``);
                              window.location.reload();
                        }, 2000);
                }
            });
            return false;
        }
    });
</script>
 <script>
    //menu active
    $('#flash_menu').find('a').addClass('active');
</script>
@endsection