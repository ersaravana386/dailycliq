@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
    <a class="breadcrumb-item" href="{{route('showBrands')}}">Brands</a>
    <span class="breadcrumb-item active">New</span>
  </nav>
</div><!-- br-pageheader -->

<div class="br-pagebody">
    <div class="br-section-wrapper">
    <h6 class="br-section-label">Edit Brand {{$brand->name}}</h6><br>
      <form id='pageForm' method="POST" action="javascript:;" data-parsley-validate>
          @method('POST')
        <div class="form-layout form-layout-1">
          <div class="row mg-b-25">
            
            <div class="col-lg-6 ">
              <div class="form-group pd-5">
                <label class="form-control-label">Name: <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" value='{{$brand->name}}' name="name" id="name"  placeholder="Enter brand name">
              </div>
            </div><!-- col-4 -->
            <div class="col-md-6">
                <div class="form-group pd-5">
                  <label class="form-control-label">Logo: </label>
                  @if($brand->logo)
                    <img src="{{asset($brand->logo)}}" class="img-fluid wd-100" alt="">
                  @endif
                  <label for="image" class="if-outline if-outline-info"><i class="icon ion-ios-upload-outline tx-24"></i>
                  <input type="file" name="image" id="image" class="inputfile" >
                  <span id="image_label">Upload file</span>
                  </label></div>
            </div><!-- col-4-->
          </div><!-- row -->

          <div class="form-layout-footer">
            <input type="submit" class="btn btn-info mg-b-10 " value="Submit">
            <a class="btn btn-secondary mg-b-10 " href="{{route('showBrands')}}">Cancel</a>
          </div><!-- form-layout-footer -->
          <div class="msgDiv"></div>
        </div><!-- form-layout -->
      </form>     

    </div><!-- br-section-wrapper -->
  </div><!-- br-pagebody -->
@endsection
@section('scripts')
<script>
    //profile form
    $("#pageForm").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        rules: {
            name: {
              required: true
            }
        },
        messages: {
            name: {
              required: "Brand name is required"
            }
        },
        submitHandler: function(form) {
            $('.msgDiv').html(``);
            var data = new FormData(form);
            $.ajax({
                type: "post",
                url: "{{route('updateBrand',['id'=>$brand->id])}}",
                data: data,
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.status == 1) {
                        
                        $('.msgDiv').html(` <div class="alert alert-success">` + data.message + `</div>`);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                            location.href = "{{route('showBrands')}}";
                        }, 2000);

                    } else {
                        var html = "";
                        $.each(data.errors, function(key, value) {
                            html += value + "<br/>";
                        });
                        $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                        }, 4000);
                    }
                }
            });
            return false;
        }
    });
</script>
 <script>
    $('#image').on('change', function() {
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $('#image_label').html(fileName);
    })

    //menu active
    $('#brands_menu').find('a').addClass('active');
</script>
@endsection