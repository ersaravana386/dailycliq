@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-10">
        <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
        <span class="breadcrumb-item active">Manufacturers</span>
    </nav>
</div><!-- br-pageheader -->
<div class="br-pagetitle">
    <i class="icon icon ion-ios-bookmarks-outline"></i>
    <div>
        <h4>Manufacturers</h4>
        <p class="mg-b-0">Manufacturers Listing</p>
    </div>
</div><!-- d-flex -->

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <div id="datatable1_filter" class="dataTables_filter">
            <div class="row">
                <div class="col-lg-9">
                    <form action="{{ route('showManufacturers') }}" method="get">
                        <div class="input-group input-group-md mb-3 col-md-4">
                            <input type="text" name="search" value="{{$search}}" placeholder="search" class="form-control" value="">
                            <div class="input-group-append">
                                <span class="input-group-btn">
                                    <button class="btn btn-default input-group-close-icon" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2">
                    <div class="input-group input-group-md mb-3 col-md-4">
                        <a href="{{route('showNewManufacturer')}}" class="btn btn-teal btn-with-icon" style="float-left" >
                        <div class="ht-40 justify-content-between">
                            <span class="icon wd-40"><i class="fa fa-plus"></i></span>
                            <span class="pd-x-15">Add New Manufacturer</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 p-r-0 title-margin-right " id="msgShowDiv">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block sessionDiv">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        @endif
    </div>
    <div class="bd bd-gray-300 rounded table-responsive">
        <table class="table mg-b-0">
            <thead>
                <tr>
                    <th class="wd-5p">#</th>
                    <th class="wd-20p">Name</th>
                    <th class="wd-15p">Image</th>
                    <th class="wd-20p">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($manufacturers as $key=> $manufacturer)
                <tr>
                    <th scope="row">{{$manufacturers->firstItem()+$key}}</th>
                    <td>{{$manufacturer->name}}</td>
                    <td>
                        @if($manufacturer->image)
                        <a target="_blank" href="{{asset($manufacturer->image)}}"><img src="{{asset($manufacturer->image)}}" style="width:60px;"></a>
                        @endif
                    </td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="{{route('editManufacturer',['id'=>$manufacturer->id])}}" class=" btn btn-outline-teal" title="Edit manufacturer">
                                <i class="menu-item-icon fa fa-edit"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
            {{ $manufacturers->appends(['search' => $search])->links() }}
        </div>
    </div><!-- bd -->

</div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
<div id="blockPopup" class="modal fade">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form id="blockForm" action="javascript:;">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><label id="vendorSpanHeading"></label></h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pd-25">
                    <h4 class="lh-3 mg-b-20 tx-inverse">Are you sure you want to block<br><label id="categorySpanName"></label>?</h4>
                    <p class="mg-b-4 blockmsgDiv"></p>
                </div>
                <input type="hidden" name="id" id="categoryId">
                <div class="modal-footer">
                    <input type="submit" id="blockUnblockBtn" class="btn btn-primary tx-11  pd-y-12 pd-x-25 tx-mont tx-medium" value="Continue">
                    <button type="button" class="btn btn-secondary tx-11  pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </div><!-- modal-dialog -->
</div><!-- modal -->
@include('ui.ajaxloader')
@endsection
@section('scripts')
<script>
    //menu active
    $('#manufacturers_menu').find('a').addClass('active');
</script>
<script type="text/javascript">
    function deactivate(id, name) {
        showConfirmation(id, name);
    }

    function showConfirmation(id, name) {
        el = $('#blockPopup');
        $(el).find('#categorySpanName').html(name);
        $(el).find('#categoryId').val(id);
        $(el).modal('show');
    }

    function blockCategory(data) {
        $('#blockPopup').modal('toggle');
        $('#ajaxLoader').modal('toggle');
        $.ajax({
            url: "{{route('blockCategory')}}",
            data,
            type: 'post',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                if (data.status == true) {
                    window.location.reload();
                } else {
                    var html = "";
                    $.each(data.errors, function(key, value) {
                        html += value + "<br/>";
                    });
                    setTimeout(function() {
                        $('#ajaxLoader').modal('toggle');
                        $('#msgShowDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                    }, 500);
                    setTimeout(function() {
                        $('#msgShowDiv').html(``);
                    }, 4000);
                }

            }
        });
    }

    $('#blockForm').on('submit', function() {
        blockCategory($(this).serialize());
        return false;
    });
</script>
@endsection