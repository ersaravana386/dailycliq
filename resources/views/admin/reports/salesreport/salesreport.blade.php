@extends('admin.layouts.app')
<link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
<style>
.buttons-html5{
    text-transform: uppercase;
    font-size: 11px;
    letter-spacing: 1px;
    font-weight: 500;
    padding: 12px 20px;
    background-color: #f4d078;
    border: 1px solid black;
    border-radius: 2px;
    margin-bottom:2px;
}
table {
    display: block;
    overflow-x: auto;
    white-space: nowrap;
}
</style>
@section('content')
    <div class="br-pagebody">
    <div class="row no-gutters widget-1 shadow-base" style="margin-top:6rem !important">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="card shadow-base bd-0">
                    <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                        <h6 class="card-title tx-uppercase tx-12 mg-b-0">Filter Order</h6>
                    </div><!-- card-header -->
                    @foreach($products as $key=> $item)
                        <form id="sort-form" method="get" action="{{route('salesReportReport',['vendorid'=>$item->order->vendor_id])}}"> 
                    @endforeach
                        <div class="form-layout form-layout-1">
                            <div class="row mg-b-25">
                                <div class="col-lg-4">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="referancenumber" value="{{$referancenumber ?? '' }}" placeholder="Referance Number">
                                        </div>
                                </div><!-- col-4 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="customername" value="{{$customername ?? '' }}" placeholder="Customer Name">
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <select class="form-control select222" style="width:100%;" name="orderstatus">
                                            <option>Order Status</option>
                                            <option  @if($orderstatus=='1'){{'selected'}} @endif value="1">Completed</option>
                                            <option  @if($orderstatus=='2'){{'selected'}} @endif value="2">Pending</option>
                                            <option  @if($orderstatus=='3'){{'selected'}} @endif value="3">Refunded</option>
                                            <option  @if($orderstatus=='4'){{'selected'}} @endif value="4">Failed</option>
                                            <option  @if($orderstatus=='5'){{'selected'}} @endif value="5">Canceled</option>
                                        </select>
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <select class="form-control select222" style="width:100%;" name="paymenttype" >
                                            <option>Payment Type</option>
                                            <option  @if($paymenttype=='Credit Card'){{'selected'}} @endif  value="Credit Card">Credit Card</option>
                                            <option  @if($paymenttype=='Online Payment'){{'selected'}} @endif value="Online Payment">Online Payment</option>
                                            <option  @if($paymenttype=='Debit Card'){{'selected'}} @endif value="Debit Card">Debit Card</option>
                                            <option  @if($paymenttype=='Cash On  Delivery'){{'selected'}} @endif value="Cash On  Delivery">Cash On  Delivery</option>
                                            <option  @if($paymenttype=='Others'){{'selected'}} @endif value="Others">Others</option>
                                        </select>
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <select class="form-control select10" style="width:100%;" name="categoryselected">
                                            <option>Category</option>
                                            @foreach($category as $key=>$list)
                                            <option   @if($categoryselected==$list->id){{'selected'}} @endif value="{{$list->id}}">{{$list->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div><!-- col-4 -->
                   
                                <div class="col-md-4 col-lg-2 mg-t-10 mg-lg-t-0">
                                    <div class="form-group mg-b-0">
                                    <input type="text" class="form-control form-control-datepicker tx-14" data-language="en" placeholder="Order from" value="{{$orderfrom ?? '' }}" name="orderfrom">
                                    </div><!-- form-group -->
                                </div><!-- col-2 -->
                                <div class="col-md-4 col-lg-2 mg-t-10 mg-lg-t-0">
                                    <div class="form-group mg-b-0">
                                    <input type="text" class="form-control form-control-datepicker tx-14" data-language="en" value="{{$orderto ?? '' }}" placeholder="Order to" name="orderto">
                                    </div><!-- form-group -->
                                </div><!-- col-2 -->
                            </div><!-- row -->
                            <div class="form-layout-footer">
                                <button class="btn buttons-html5" style="border: 1px solid black;">Search</button>
                            </div><!-- form-layout-footer -->
                        </div>
                    </form>
                </div><!-- card -->
            </div>
        </div>       
  
        <div class="pt-2">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 msgDiv">
                 </div> 
            </div>
            <div class="row row-sm justify-content-center new">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <!-- <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">Vendor Product Listing</h4> -->
                    <div class="card bg-white border-0 shadow-sm py-5 px-4">
                        <div class="">
                            <table class="table mg-b-0" id="example">
                                <thead>
                                    <tr>
                                        <th class="wd-5p"># 
                                        </th>
                                        <th class="wd-10p">Ref.No</th>
                                        <th class="wd-10p">Product</th>
                                        <th class="wd-10p">Unit</th>
                                        <th class="wd-10p">Customer</th>
                                        <th class="wd-10p">Payment Type</th>
                                        <th class="wd-10p">Amount</th>
                                        <th class="wd-10p">Category</th>
                                        <th class="wd-10p">Brand</th>
                                        <th class="wd-10p">Order At</th>
                                        <th class="wd-10p">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php
                                $i= 0;
                                $count =0;
                                @endphp
                                @foreach($products as $key=> $item)
                                <tr> 
                                    <td scope="row">{{++$i}}</td>
                                    <td scope="row">
                                        @if($item->order){{$item->order->order_reference_number}}@endif 
                                    </td>
                                    <td scope="row">
                                       @if($item->productlist){{$item->productlist->name}}@endif
                                    </td>
                                    <td scope="row">@if($item->unit){{$item->unit}}@endif</td>
                                    <td>
                                        @if($item->order->user){{$item->order->user->name}}@endif
                                    </td>
                                    <td>@if($item->order->payment){{$item->order->payment->payment_type}}@endif</td>
                                    <td>@if($item->net_total){{$item->net_total}}@endif</td>
                                    <td>@if($item->productlist && $item->productlist->category){{$item->productlist->category->name}}@endif</td>
                                    <td>@if($item->productlist && $item->productlist->brand){{$item->productlist->brand->name}}@endif</td>
                                    <td>@if($item->order->order_at){{date('d M Y h:i:A',strtotime($item->order->order_at))}}@endif</td>
                                    <td>@if($item->order->status =='1'){{'Completed'}}@endif
                                           @if($item->order->status =='2'){{'Pending'}}@endif
                                           @if($item->order->status =='3'){{'Refunded'}}@endif
                                           @if($item->order->status =='4'){{'Failed'}}@endif
                                           @if($item->order->status =='5'){{'Canceled'}}@endif
                                          
                                        
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                          
                            </div>
                        </div>
                    </div>
                    <!-- bd -->
                </div>
            </div>
        </div><!-- row -->
    </div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
<script>
  $('#catelogId').find('a').addClass('active');
  $("input[name='search']").autocomplete({
    source: function(request, response) {
      $.ajax({
        url: "{{route('searchProduct')}}",
        dataType: "json",
        data: {
          q: request.term
        },
        success: function(data) {
          response(data);
        }
      });
    },
    minLength: 3,
    select: function(event, ui) {
      // log( ui.item ?
      //   "Selected: " + ui.item.label :
      //   "Nothing selected, input was " + this.value);
    },
    open: function() {
      $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
    },
    close: function() {
      $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
    }
  });
   // Datepicker
   $('.fc-datepicker').datepicker({
          showOtherMonths: true,
          selectOtherMonths: true
        });

        $('#datepickerNoOfMonths').datepicker({
          showOtherMonths: true,
          selectOtherMonths: true,
          numberOfMonths: 2
        });
    // Check All CheckBoxes
    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }
</script>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        "paging": false,
        "ordering": false,
        "searching": false,
        "info": false
    } );
} );
</script>
@endsection
