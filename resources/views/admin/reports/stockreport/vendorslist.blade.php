@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-10">
        <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
        <span class="breadcrumb-item active">Reports</span>
        <span class="breadcrumb-item active">Stock Reports</span>
        <span class="breadcrumb-item active">Vendors</span>
    </nav>
</div><!-- br-pageheader -->
<div class="br-pagetitle">
    <i class="icon icon ion-ios-bookmarks-outline"></i>
    <div>
        <h4>Stock Report</h4>
        <p class="mg-b-0">Vendors Listing</p>
    </div>
</div><!-- d-flex -->

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <div id="datatable1_filter" class="dataTables_filter">
            <div class="row">
                <div class="col-lg-9">
                    <form action="" method="get">
                        <div class="input-group input-group-md mb-3 col-md-4">
                            <input type="text" name="search" value="" placeholder="search" class="form-control" value="">
                            <div class="input-group-append">
                                <span class="input-group-btn">
                                    <button class="btn btn-default input-group-close-icon" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2">
                    <div class="input-group input-group-md mb-3 col-md-4">
                        <a href="{{route('newFlashSale')}}" class="btn btn-teal btn-with-icon" style="float-left" >
                        <!-- <div class="ht-40 justify-content-between">
                            <span class="icon wd-40"><i class="fa fa-plus"></i></span>
                            <span class="pd-x-15">Add New Flash Sale</span>
                        </div> -->
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 p-r-0 title-margin-right " id="msgShowDiv">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block sessionDiv">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        @endif
    </div>
    <div class="bd bd-gray-300 rounded table-responsive">
        <table class="table mg-b-0">
            <thead>
                <tr>
                    <th class="wd-15p">#</th>
                    <th class="wd-15p">vendor Name</th>
                    <th class="wd-15p">product Count</th>
                    <th class="wd-15p">Actions</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i=0;
                @endphp
                @foreach($details as $detail)
                <tr>
                    <td scope="row">{{$i=$i+1}}</td>
                    <td scope="row">{{$detail->company_name}}</td>
                    <td scope="row">
                              {{$detail->count}}          
                    </td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a class="btn btn-outline-teal" href="{{route('stockReportReport',['vendorid'=>$detail->id])}}" title="View Products"> 
                                <i class="menu-item-icon fa fa-eye"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
              {{ $details->appends(['search' => $search])->links() }}
        </div>
  
    </div><!-- bd -->

</div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
@include('ui.ajaxloader')
@endsection
@section('scripts')
</script>
<script>
    //menu active
    $('#flash_menu').find('a').addClass('active');
</script>
<script>
    $(document).ready(function () {
            $("#select_all").click(function () {
                $(".check_box").attr('checked', this.checked);
            });
    });
</script>
<script>
    $('#approve_status').change(function (e) {
        var val= $('#approve_status').val();
        var checked = [];
        $.each($("input[name='selected_vals[]']:checked"), function(){
            checked.push($(this).val());
        });
        if(checked.length!=0)            
            if(confirm("are you sure?")){
                $.ajax({
                    type:'post',
                    url :"{{route('multipleVendorApproveFlashSale')}}",
                    dateType:'json',
                    data:{checked:checked,val:val},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data){
                        if(data.status == true){ 
                            location.reload(); 
                        }
                    },error:function(data){
                        console.log(data);
                        alert('error,try again');
                    }
                });
            }else{
                    return false;
            }
        else{
            return false;
        }
    });
</script>
<script type="text/javascript">
    function deactivate(id, name) {
        showConfirmation(id, name);
    }
    
    function showConfirmation(id, name) {
        el = $('#blockPopup');
        $(el).find('#categorySpanName').html(name);
        $(el).find('#categoryId').val(id);
        $(el).modal('show');
    }
</script>


@endsection