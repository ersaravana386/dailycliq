@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-10">
        <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
        <span class="breadcrumb-item active">Admin Management</span>
        <span class="breadcrumb-item active">Admins</span>
    </nav>
</div><!-- br-pageheader -->
<div class="br-pagetitle">
    <i class="icon icon ion-ios-bookmarks-outline"></i>
    <div>
        <h4>Admin Management</h4>
        <p class="mg-b-0">Admin Listing</p>
    </div>
</div><!-- d-flex -->

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <div id="datatable1_filter" class="dataTables_filter">
            <div class="row">
                <div class="col-lg-9">
                    <form action="" method="get">
                        <div class="input-group input-group-md mb-3 col-md-4">
                            <input type="text" name="search" value="" placeholder="search" class="form-control" value="">
                            <div class="input-group-append">
                                <span class="input-group-btn">
                                    <button class="btn btn-default input-group-close-icon" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2">
                    <div class="input-group input-group-md mb-3 col-md-4">
                        <a href="{{route('addNewAdminAdminManagement')}}" class="btn btn-teal btn-with-icon" style="float-left" >
                        <div class="ht-40 justify-content-between">
                            <span class="icon wd-40"><i class="fa fa-plus"></i></span>
                            <span class="pd-x-15">Add New Admin</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
              <div class="msgDiv"></div>
    </div>
    <div class="bd bd-gray-300 rounded table-responsive" style="margin-top:10px;">
    @php
        $i=0;
    @endphp
        <table class="table mg-b-0">
            <thead>
                <tr>
                    <th class="wd-5p">#</th>
                    <th class="wd-15p">Admin Name</th>
                    <th class="wd-15p">Email</th>
                    <th class="wd-15p">Mobile</th>
                    <th class="wd-10p">Actions</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i=0;
                @endphp
                @foreach($admins as $key=>$value)
                <tr>
                    <td>{{++$i}}</td>
                    <td>{{$value->name}}</td>
                    <td>{{$value->email}}</td>
                    <td>{{$value->mobile}}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a class="btn btn-outline-teal" href="{{route('editAdminAdminManagement',['id'=>$value->id])}}" title="Edit Admin"> 
                                <i class="menu-item-icon fa fa-edit"></i>
                            </a>
                        <div>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="#" class="btn btn-outline-teal" title="View Admin" data-toggle="modal" data-target="#viewModal{{$value->id}}" id="admin_view" name="admin_view" data-value="{{$value->id}}"> 
                                <i class="menu-item-icon fa fa-eye"></i>
                            </a>
                        <div>
                        @if($value->status == 1)
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <a href="" class="btn btn-outline-teal" title="Block Admin" data-id="{{$value->id}}" data-value="0" name="admin_status" id="admin_status">
                                    <i class="menu-item-icon fa fa-lock"></i>
                                </a>
                            <div>
                        @else
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <a href="" class="btn btn-outline-teal" title="Unblock Admin" data-id="{{$value->id}}" data-value="1" name="admin_status" id="admin_status"> 
                                    <i class="menu-item-icon fa fa-unlock"></i>
                                </a>
                            <div>
                        @endif
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="" class="btn btn-outline-teal" title="Delete Admin" id="admin_del" name="admin_del" data-value="{{$value->id}}"> 
                                <i class="menu-item-icon fa fa-trash"></i>
                            </a>
                        <div>
                    </td>
                </tr>

                   <!-- view modal -->
        <div class="modal fade effect-flip-horizontal " id="viewModal{{$value->id}}">
          <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content bd-0">
              <div class="modal-body pd-0">
                <div class="row flex-row-reverse no-gutters">
                  <div class="col-lg-6 rounded-left">
                    <div class="pd-40">
                      <h2 class="mg-b-20 tx-inverse lh-2 tx-uppercase"><br></h4>
                        <p class="tx-12 mg-b-10">
                          @if($value->image)
                          <span>image:</span>
                          <span class="tx-uppercase tx-12 tx-bold d-block mg-b-20">
                            <a href="{{asset($value->image)}}" title="Admin Image">
                              <img src="{{asset($value->image)}}" class="img-fluid wd-100" alt=""></span>
                          </a>
                          @endif
                        </p>
                        <a href="{{route('editAdminAdminManagement',['id'=>$value->id])}}" class=" btn " title="Edit">
                          <button type="button" class="btn btn-primary tx-11  pd-y-12 pd-x-25 tx-mont tx-medium">EDIT</button>
                        </a>
                        <button type="button" class="btn btn-secondary tx-11  pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>

                    </div>
                  </div><!-- col-6 -->
                  <div class="col-lg-6 rounded-left">
                    <div class="pd-40">
                      <h2 class="mg-b-20 tx-inverse lh-2 tx-uppercase">{{$value->name}}</h4>
                        <p class="tx-12 mg-b-20">
                            <span>email:</span>
                          <span class="tx-uppercase tx-12 tx-bold d-block mg-b-10"> {{$value->email}}</span>
                          <span class="">Mobile Number:</span>
                          <span class="tx-uppercase tx-12 tx-bold d-block mg-b-10"> {{$value->mobile}}</span>
                          <span>Status:</span>
                          @if($value->status == 1)
                            <span class="tx-uppercase tx-12 tx-bold d-block mg-b-10">{{'Active'}}</span>
                          @else
                            <span class="tx-uppercase tx-12 tx-bold d-block mg-b-10">{{'Blocked'}}</span>
                          @endif
                        </p>
                    </div>
                  </div><!-- col-6 -->

                </div><!-- row -->

              </div><!-- modal-body -->
              <div class="modal-footer">
              </div>
            </div><!-- modal-content -->
          </div><!-- modal-dialog -->
        </div><!-- modal -->

                @endforeach
            </tbody>
        </table>
        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
              {{$admins->appends(['search' => $search])->links() }}
        </div>
  
    </div><!-- bd -->

</div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
@include('ui.ajaxloader')
@endsection
@section('scripts')
<script>
    //menu active
    $('#admin_menu').find('a').addClass('active');
</script>

<script type="text/javascript">
    $(document).on('click','#admin_del',function(e) {
        e.preventDefault();
        var id=$(this).data('value');
        if( id == ""){
            $('.msgDiv').html(`<p class="alert alert-danger mg-b-4">Unable to process your request</p>`).focus();
            setTimeout(function() {
              $('.msgDiv').html(``);
              window.location.reload();
            }, 2000);
        }
        else{         
            if(confirm("are you sure?")){
                $.ajax({
                    type:'post',
                    url :"{{route('deleteAdminAdminManagement')}}",
                    dateType:'json',
                    data:{'id':id},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data){
                        if(data.status == true){ 
                            $('.msgDiv').html('<p class="alert alert-success mg-b-4">Admin deleted successfully</p>').focus();
                            setTimeout(function() {
                            $('.msgDiv').html(``);
                                window.location.reload();
                            }, 2000);
                        }else{
                            $('.msgDiv').html('<p class="alert alert-danger mg-b-4">Unable To Make Any Change</p>').focus();
                            setTimeout(function() {
                            $('.msgDiv').html(``);
                                // window.location.reload();
                            }, 2000);
                        }
                    },error:function(data){ 
                        console.log(data);
                        $('.msgDiv').html('<p class="alert alert-danger mg-b-4">Unable to process your request</p>').focus();
                            setTimeout(function() {
                            $('.msgDiv').html(``);
                                window.location.reload();
                            }, 2000);
                    }
                });
            }else{
                    return false;
            }
        }
    });
</script>

<script type="text/javascript">
    $(document).on('click','#admin_status',function(e) {
        e.preventDefault();
        var id=$(this).data('id');
        var val=$(this).data('value');
        if(val == 1){
            var message="Admin unblocked successfully"
        }else{
            var message="Admin blocked successfully"
        }
        if( id == ""){
            $('.msgDiv').html(`<p class="alert alert-danger mg-b-4">Unable to process your request</p>`).focus();
            setTimeout(function() {
              $('.msgDiv').html(``);
              window.location.reload();
            }, 2000);
        }
        else{         
            if(confirm("are you sure?")){
                $.ajax({
                    type:'post',
                    url :"{{route('blockAdminAdminManagement')}}",
                    dateType:'json',
                    data:{'id':id,'val':val},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data){
                        if(data.status == true){ 
                            $('.msgDiv').html('<p class="alert alert-success mg-b-4">' + message + '</p>').focus();
                            setTimeout(function() {
                            $('.msgDiv').html(``);
                                window.location.reload();
                            }, 2000);
                        }else{
                            $('.msgDiv').html('<p class="alert alert-danger mg-b-4">' + data.message + '</p>').focus();
                            setTimeout(function() {
                            $('.msgDiv').html(``);
                                // window.location.reload();
                            }, 2000);
                        }
                    },error:function(data){ 
                        console.log(data);
                        $('.msgDiv').html('<p class="alert alert-danger mg-b-4">Unable to process your request</p>').focus();
                            setTimeout(function() {
                            $('.msgDiv').html(``);
                                window.location.reload();
                            }, 2000);
                    }
                });
            }else{
                    return false;
            }
        }
    });
</script>
@endsection