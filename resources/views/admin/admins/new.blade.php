@extends('admin.layouts.app')
<style>
</style>
@section('content')
<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a class="breadcrumb-item" href="">Home</a>
    <a class="breadcrumb-item" href="   ">Admin Management</a>
    <span class="breadcrumb-item active">Add</span>
  </nav>
</div><!-- br-pageheader -->

<div class="br-pagebody">
    <div class="br-section-wrapper">
      <h6 class="br-section-label">Add Admin</h6><br>
      <div class="col-lg-12">
              <div class="msgDiv"></div>
      </div>
      <form id='pageForm' method="POST" action="javascript:;" enctype="multipart/form-data" data-parsley-validate>
          @method('POST')
        <div class="form-layout form-layout-1">
          <div class="row mg-b-25">
            <div class="col-lg-4">
              <div class="form-group pd-5">
                <label class="form-control-label">Name: <span class="tx-danger">*</span></label>
                <input type="text " class="form-control" value='' name="name" id="name" placeholder="Enter Name">
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-4">
              <div class="form-group pd-5" >
                <label class="form-control-label">Email: <span class="tx-danger">*</span></label>
                <input type="text " class="form-control" value='' name="email" id="email" placeholder="Enter Email">
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-4">
              <div class="form-group pd-5">
                <label class="form-control-label">Mobile: <span class="tx-danger">*</span></label>
                <input type="text " class="form-control" value='' name="mobile" id="mobile" placeholder="Eneter Mobile">
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-4">
              <div class="form-group pd-5">
                <label class="form-control-label">Status: <span class="tx-danger">*</span></label>
                    <select name="status" id="status" class="form-control form-control-sm select2">
                        <option value="" selected disabled>{{'Select Status'}}</option>
                        <option value="{{'1'}}">{{'Active'}}</option>
                        <option value="{{'0'}}">{{'Block'}}</option>
                    </select>
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-2">
              <div class="form-group pd-5">
                <label class="form-control-label">Password: <span class="tx-danger">*</span></label>
                <input type="text " class="form-control" value='' name="pass" id="pass" placeholder="Enter password">
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-2">
              <div class="form-group pd-5">
                <label class="form-control-label">Confirm Password: <span class="tx-danger">*</span></label>
                <input type="text " class="form-control" value='' name="cpass" id="cpass" placeholder="Enter password again">
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-4">
                <div class="form-group pd-5">
                  <label class="form-control-label">Image: </label>
                    <!-- <img src="" class="img-fluid wd-100" alt="" id="img"> -->
                  <label for="image" class="if-outline if-outline-info"><i class="icon ion-ios-upload-outline tx-24"></i>
                  <input type="file" name="image" id="image" class="inputfile" >
                  <span id="img_label">Change file</span>
                  </label>
                </div>
            </div><!-- col-4--> 
            <div class="col-lg-5">
              <div class="form-group pd-5">
                 <input type="submit" class="btn btn-info mg-b-10 mt-lg-4 mt-sm-0" value="&nbsp Save &nbsp"> 
              </div>
            </div>
          </div><!-- row -->
        </div><!-- form-layout -->
      </form>     
</div>
    </div><!-- br-section-wrapper -->
  </div><!-- br-pagebody -->
@endsection
@section('scripts')
<script>
    //menu active
    $('#admin_menu').find('a').addClass('active');
</script>
<script>
    //profile form
    $("#pageForm").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        rules: {
            name:{
                required: true
            },
            email:{
                required: true
            },
            mobile:{
                required:true
            },
            status:{
                required:true
            },
            pass:{
                required: function(){
                    if($('#cpass').val() != ""){
                        return true;
                    }
                }
            },
            cpass:{
                required: function(){
                    if($('#pass').val() != ""){
                        return true;
                    }
                }
            },
            admin_cpass:{
                equalTo: "#pass"
            }
        },
        submitHandler: function(form){
            var data = new FormData(form);
            $.ajax({
                type: "post",
                url: "{{route('newAdminAdminManagement')}}",
                data:data,
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.status == 1) {
                            $('.msgDiv').html(`<p class="alert alert-success mg-b-4">Successfully added admin</p>`);
                            setTimeout(function() {
                                $('.msgDiv').html(``);
                                location.href = "{{route('allAdminsAdminManagement')}}";
                            }, 2000);
                    } else {
                        if(data.message){
                            var html = "";
                            $.each(data.message, function(key, value) {
                                html += value + "<br/>";
                            });
                            $('.msgDiv').html('<p class="alert alert-danger mg-b-4">' + html + '</p>');
                            console.log(data.message);
                            setTimeout(function() {
                                $('.msgDiv').html(``);
                                window.location.reload();
                            }, 2000);
                        }else{
                            $('.msgDiv').html(`<p class="alert alert-danger mg-b-4">Unable to make any changes</p>`);
                            setTimeout(function() {
                                $('.msgDiv').html(``);
                                window.location.reload();
                            }, 2000);
                        }
                    }
                },
                error:function(data) {
                    $('.msgDiv').html('<p class="alert alert-danger mg-b-4">Unable to process your request</p>').focus();
                    setTimeout(function() {
                        $('.msgDiv').html(``);
                        window.location.reload();
                    }, 2000);
                }
            });
        }
    });
</script>
<script>
    $('#image').on('change', function() {
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $('#img_label').html(fileName);
    })
</script>
<script>
$('#image').change(function(){
    if($('#image').val() != ""){
        $('#img').css('display','none');
    }
});
</script>
@endsection
