@extends('admin.layouts.app')
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-10">
        <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
        <span class="breadcrumb-item active">Deal Of The Day</span>
        <span class="breadcrumb-item active">Vendors</span>
        <span class="breadcrumb-item active">Products</span>
    </nav>
</div><!-- br-pageheader -->
<div class="br-pagetitle">
    <i class="icon icon ion-ios-bookmarks-outline"></i>
    <div>
        <h4>Products</h4>
        <p class="mg-b-0">Products Listing</p>
    </div>
</div><!-- d-flex -->

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <div id="datatable1_filter" class="dataTables_filter">
            <div class="row">
                <div class="col-lg-9">
                    <form action="" method="get">
                        <div class="input-group input-group-md mb-3 col-md-4">
                            <input type="text" name="search" value="" placeholder="search" class="form-control" value="">
                            <div class="input-group-append">
                                <span class="input-group-btn">
                                    <button class="btn btn-default input-group-close-icon" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2">
                    <div class="input-group input-group-md mb-3 col-md-4">
                        <a href="{{route('newFlashSale')}}" class="btn btn-teal btn-with-icon" style="float-left" >
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
              <div class="msgDiv"></div>
    </div>
    <table class="form-inline my-2 my-lg-0 flex-nowrap w-100 w-md-75">
            <td></td><td></td><td></td>
            <td class="wd-5p">    
                    <label class="ckbox mg-b-0" style="margin-bottom:0px;">
                        <input type="checkbox" name="select_all" id="select_all" class=""><span></span>
                    </label>
            </td>
            <td>
                <select name="" id="approve_status" class="form-control select2222" style="width:160px;">
                    <option value="" selected disabled>Select Action</option>
                    <option value="1" id="approve">Approve All Products</option>
                    <option value="0" id="reject">Reject All Products</option>
                </select>
            </td>
    </table>
    <div class="bd bd-gray-300 rounded table-responsive" style="margin-top:10px;">
    @php
        $i=0;
    @endphp
        <table class="table mg-b-0">
            <thead>
                <tr>
                    <th class="wd-5p"></th>
                    <th class="wd-5p">#</th>
                    <th class="wd-10p">Vendor</th>
                    <th class="wd-10p">Category</th>
                    <th class="wd-10p">product</th>
                    <th class="wd-10p">Offer Amount</th>
                    <th class="wd-10p">Offer %</th>
                    <th class="wd-10p">Actions</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i=0;
                @endphp
                @foreach($details as $key=>$value)
                <tr>
                    <td>
                        <label class="ckbox mg-b-0" style="margin-bottom:0px;">
                            <input type="checkbox" name="selected_vals[]"  value='{{$value->product_id}}' class="check_box"><span></span>
                        </label>
                    </td>
                    <td>{{++$i}}</td>
                    <td>@if($value->vendor){{$value->vendor->company_name}}@endif</td>
                    <td>@if($value->category){{$value->category->name}}@endif</td>
                    <td>@if($value->product){{$value->product->name}}@endif</td>
                    <td>@if($value){{$value->offer_amount}}@endif</td>
                    <td>@if($value){{$value->offer_percentage}}@endif</td>
                    <td>
                            @if($value->approve == 1)
                                    <label class="badge badge-success">Approved</label>
                            @elseif($value->approve == 2)
                                    <label class="badge badge-danger">Rejected</label>
                            @else
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a class="btn btn-outline-teal" data-id="{{$value->product_id}}" data-value="{{$value->vendor_id}}" title="Approve Product" id="appr_dod"> 
                                        <i class="menu-item-icon fa fa-check"></i>
                                    </a>
                                <div>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a class="btn btn-outline-teal" data-id="{{$value->product_id}}" data-value="{{$value->vendor_id}}" title="Reject Products" id="rej_dod"> 
                                        <i class="menu-item-icon fa fa-trash"></i>
                                    </a>
                                <div>
                            @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
              {{ $details->appends(['search' => $search])->links() }}
        </div>
  
    </div><!-- bd -->

</div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
@include('ui.ajaxloader')
@endsection
@section('scripts')
<script>
    //menu active
    $('#dod_menu').find('a').addClass('active');
</script>
<script>
    $(document).ready(function () {
            $("#select_all").click(function () {
                $(".check_box").attr('checked', this.checked);
            });
    });
</script>
<script>
    $('#approve_status').change(function (e) {
        var val= $('#approve_status').val();
        if(val == 0 ){
            var message= "Products Rejected";
        }else{
            var message= "Products Approved";
        }
        var checked = [];
        $.each($("input[name='selected_vals[]']:checked"), function(){
            checked.push($(this).val());
        });
        if( val == "" || checked.length == 0 ){
            $('.msgDiv').html(`<p class="alert alert-danger mg-b-4">Unable to process your request</p>`).focus();
            setTimeout(function() {
              $('.msgDiv').html(``);
              window.location.reload();
            }, 2000);
        }
        if(checked.length!=0)            
            if(confirm("are you sure?")){
                $.ajax({
                    type:'post',
                    url :"{{route('multipleProductsDealOfTheDay')}}",
                    dateType:'json',
                    data:{checked:checked,val:val},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data){
                        if(data.status == true){ 
                            $('.msgDiv').html('<p class="alert alert-success mg-b-4">' + message + '</p>').focus();
                            setTimeout(function() {
                            $('.msgDiv').html(``);
                                window.location.reload();
                            }, 2000);
                        }else{
                            $('.msgDiv').html('<p class="alert alert-danger mg-b-4">Unable To Make Any Change</p>').focus();
                            setTimeout(function() {
                            $('.msgDiv').html(``);
                                window.location.reload();
                            }, 2000);
                        }
                    },error:function(data){
                        console.log(data);
                        $('.msgDiv').html(`<p class="alert alert-danger mg-b-4">Unable to process your request</p>`).focus();
                            setTimeout(function() {
                            $('.msgDiv').html(``);
                                window.location.reload();
                            }, 2000);
                    }
                });
            }else{
                    return false;
            }
        else{
            return false;
        }
    });
</script>
<script>
$(document).ready(function(){
      $('#appr_dod').on('click',function(){
          var id= $(this).data('id');
          var vendor_id= $(this).data('value');
          if(id==''){
            $('.msgDiv').html(`<p class="alert alert-danger mg-b-4">Unable to process your request</p>`).focus();
            setTimeout(function() {
              $('.msgDiv').html(``);
              window.location.reload();
            }, 1000);
          }else{
            $.ajax({
            url: "{{route('approveProductsDealOfTheDay')}}",
            data: {'id':id,'vendor_id':vendor_id},
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type:'POST',
            success: function(data) {
                if(data.status == true){
                    $('.msgDiv').html('<p class="alert alert-success mg-b-4">Product Approved</p>').focus();
                    setTimeout(function() {
                    $('.msgDiv').html(``);
                        window.location.reload();
                    }, 2000);
                }
                else{
                    $('.msgDiv').html('<p class="alert alert-danger mg-b-4">Unable To Make Any Change</p>').focus();
                    setTimeout(function() {
                    $('.msgDiv').html(``);
                        window.location.reload();
                    }, 2000);
                }
            },error:function(data){}
            });
         }
      });
  });
</script>
<script>
$(document).ready(function(){
      $('#rej_dod').on('click',function(){  
          var id= $(this).data('id');
          var vendor_id= $(this).data('value');
          if(id==''){
            $('.msgDiv').html(`<p class="alert alert-danger mg-b-4">Unable to process your request</p>`).focus();
            setTimeout(function() {
              $('.msgDiv').html(``);
              window.location.reload();
            }, 1000);
          }else{
            $.ajax({
            url: "{{route('rejectProductsDealOfTheDay')}}",
            data: {'id':id,'vendor_id':vendor_id},
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type:'POST',
            success: function(data) {
                if(data.status == true){
                    $('.msgDiv').html('<p class="alert alert-success mg-b-4">Product Rejected</p>').focus();
                    setTimeout(function() {
                    $('.msgDiv').html(``);
                        window.location.reload();
                    }, 2000);
                }
                else{
                    $('.msgDiv').html('<p class="alert alert-danger mg-b-4">Unable To Make Any Change</p>').focus();
                    setTimeout(function() {
                    $('.msgDiv').html(``);
                        window.location.reload();
                    }, 2000);
                }
            },error:function(data){}
            });
         }
      });
  });
</script>
@endsection