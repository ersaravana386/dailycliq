@extends('vendors.layouts.web')
<link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
<style>
.buttons-html5{
    text-transform: uppercase;
    font-size: 11px;
    letter-spacing: 1px;
    font-weight: 500;
    padding: 12px 20px;
    background-color: #f4d078;
    border: 1px solid black;
    border-radius: 2px;
    margin-bottom:2px;
}
</style>
@section('content')
    <div class="br-pagebody">
        <div class="row no-gutters widget-1 shadow-base" style="margin-top:6rem !important">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="card shadow-base bd-0">
                    <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                        <h6 class="card-title tx-uppercase tx-12 mg-b-0">Filter Order</h6>
                    </div><!-- card-header -->
                    <form id="sort-form" method="get" action="{{route('stockReport')}}"> 
                        <div class="form-layout form-layout-1">
                            <div class="row mg-b-25">
                      
                               <div class="col-md-4 col-lg-2 mg-t-10 mg-lg-t-0">
                                    <div class="form-group pd-5">
                                        <select class="form-control form-control-sm select22" name="process" id="process">
                                            <option @if($process=='1'){{'selected'}}@endif value='1'>In Stock</option>
                                            <option @if($process=='2'){{'selected'}}@endif value='2'>Out Stock</option>
                                            <option @if($process=='2'){{'selected'}}@endif value='2'>Purchase</option>
                                        </select>
                                    </div>
                                </div><!-- col-2 -->
                                <div class="col-md-4 col-lg-2 mg-t-10 mg-lg-t-0">
                                    <div class="form-group mg-b-0">
                                    <input type="text" class="form-control form-control-datepicker tx-14" data-language="en" placeholder="Stock from" value="{{$stockfrom ?? '' }}" name="stockfrom">
                                    </div><!-- form-group -->
                                </div><!-- col-2 -->
                                <div class="col-md-4 col-lg-2 mg-t-10 mg-lg-t-0">
                                    <div class="form-group mg-b-0">
                                    <input type="text" class="form-control form-control-datepicker tx-14" data-language="en" value="{{$stockto ?? '' }}" placeholder="Stock to" name="stockto">
                                    </div><!-- form-group -->
                                </div><!-- col-2 -->
                                <div class="col-md-4 col-lg-2 mg-t-10 mg-lg-t-0">
                                    <div class="form-group mg-b-0">
                                    <button class="btn btn-primary btn-sm button-inner my-2 my-sm-0">Search</button>
                                    </div><!-- form-group -->
                                </div><!-- col-2 -->
                            </div><!-- row -->
                            <div class="form-layout-footer">
                               
                            </div><!-- form-layout-footer -->
                        </div>
                    </form>
                </div><!-- card -->
            </div>
        </div>        
  
        <div class="pt-2">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 msgDiv">
                 </div> 
            </div>
            <div class="row row-sm justify-content-center new">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <!-- <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">Vendor Product Listing</h4> -->
                    <div class="card bg-white border-0 shadow-sm py-5 px-4">
                        <div class="bd bd-gray-300 rounded table-responsive">
                            <table class="table mg-b-0" id="example">
                                <thead>
                                    <tr>
                                        <th class="wd-5p">#</th>
                                        <th class="wd-15p">PRODUCT</th>
                                        <th class="wd-15p">PROCESS</th>
                                        <th class="wd-15p">UNIT</th>
                                        <th class="wd-15p">PROCESS TYPE</th>
                                        <th class="wd-15p">CREATED DATE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php
                                $i= 0;
                                @endphp
                                @if(count($stock)>0)
                                @foreach ($stock ?? '' as $key=> $item)
                                <tr>
                                    <td scope="row">{{++$i}}</td>
                                    <td scope="row">
                                        @if($item->product) {{$item->product->name}} @endif
                                    </td>
                                    <td scope="row">
                                        {{$item->process}}
                                    </td>
                                    <td scope="row">{{$item->unit}}</td>
                                    <td scope="row">
                                    @if($item->process_id ==1)
                                    {{'In Stock'}}
                                    @elseif($item->process_id ==2)
                                    {{'Out Stock'}}
                                    @elseif($item->process_id ==3)
                                    {{'Purchase'}}
                                    @endif
                                    </td>
                                    </td>
                                    <td scope="row">{{date('d M  Y',strtotime($item->created_at))}}</td>
                                </tr>
                                    @endforeach
                                    @else
                                <tr>
                                    <td scope="row" colspan=10><b><center><span style="color:#DC3545;">Sorry No Result Found</span></center><b></td>
                                </tr>
                                @endif
                                </tbody>
                            </table>
                            @if(count($stock)>0)
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                            {{ $stock->appends(['search' => $search ?? ''])->links() }}
                            </div>
                            @endif
                        </div>
                    </div>
                    <!-- bd -->
                </div>
            </div>
        </div><!-- row -->
    </div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
<script>
  $("input[name='search']").autocomplete({
    source: function(request, response) {
      $.ajax({
        url: "{{route('searchProduct')}}",
        dataType: "json",
        data: {
          q: request.term
        },
        success: function(data) {
          response(data);
        }
      });
    },
    minLength: 3,
    select: function(event, ui) {
      // log( ui.item ?
      //   "Selected: " + ui.item.label :
      //   "Nothing selected, input was " + this.value);
    },
    open: function() {
      $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
    },
    close: function() {
      $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
    }
  });
   // Datepicker
   $('.fc-datepicker').datepicker({
          showOtherMonths: true,
          selectOtherMonths: true
        });

        $('#datepickerNoOfMonths').datepicker({
          showOtherMonths: true,
          selectOtherMonths: true,
          numberOfMonths: 2
        });
    // Check All CheckBoxes
    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }
</script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        "paging": false,
        "ordering": false,
        "searching": false,
        "info": false
    } );
} );
</script>
@endsection
