 <!----------------------------------------------->
 <ul class="step-progressbar">
                     @if($product->id)
                      @if($product->step == 0)                        
                        <li class="step-progressbar__item step-progressbar__item--active"><a href="{{route('showNewCatelog')}}?product={{$product->id}}" class="@if($step == 1) text-danger @endif">Main Info</a></li>
                      @elseif($product->step > 0)
                        <li class="step-progressbar__item step-progressbar__item--complete"><a href="{{route('showNewCatelog')}}?product={{$product->id}}" class="@if($step == 1) text-danger @endif">Main Info</a></li>
                      @endif 

                      @if($product->step == 1)                          
                        <li class="step-progressbar__item step-progressbar__item--active"><a href="{{route('showNewPricingCatelog',['product'=>$product->id])}}" class="@if($step == 2) text-danger @endif">Pricing</a></li>
                      @elseif($product->step > 1)
                        <li class="step-progressbar__item step-progressbar__item--complete"><a href="{{route('showNewPricingCatelog',['product'=>$product->id])}}" class="@if($step == 2) text-danger @endif">Pricing</a></li>
                      @else
                        <li class="step-progressbar__item"><a href="javascript:;" class="@if($step == 2) text-danger @endif">Pricing</a></li>
                      @endif

                      @if($product->step == 2)                      
                        <li class="step-progressbar__item step-progressbar__item--active"><a href="{{route('showNewCatelogPackageDetails',['product'=>$product->id])}}" class="@if($step == 3) text-danger @endif">Package Info</a></li>
                      @elseif($product->step > 2)
                        <li class="step-progressbar__item step-progressbar__item--complete"><a href="{{route('showNewCatelogPackageDetails',['product'=>$product->id])}}" class="@if($step == 3) text-danger @endif">Package Info</a></li>
                      @else
                        <li class="step-progressbar__item"><a href="javascript:;" class="@if($step == 3) text-danger @endif">Package Info</a></li>
                      @endif

                      @if($product->step == 3)                      
                        <li class="step-progressbar__item step-progressbar__item--active"><a href="{{route('showNewCatelogImages',['product'=>$product->id])}}" class="@if($step == 4) text-danger @endif">Product Images</a></li>
                      @elseif($product->step > 3)
                        <li class="step-progressbar__item step-progressbar__item--complete"><a href="{{route('showNewCatelogImages',['product'=>$product->id])}}" class="@if($step == 4) text-danger @endif">Product Images</a></li>
                      @else
                        <li class="step-progressbar__item"><a href="javascript:;" class="@if($step == 4) text-danger @endif">Product Images</a></li>
                      @endif

                      @if($product->step == 4)                      
                        <li class="step-progressbar__item step-progressbar__item--active"><a href="{{route('showNewCatelogAdditionalInfo',['product'=>$product->id])}}" class="@if($step == 5) text-danger @endif">Additional Info</a></li>
                      @elseif($product->step > 3)
                        <li class="step-progressbar__item step-progressbar__item--complete"><a href="{{route('showNewCatelogAdditionalInfo',['product'=>$product->id])}}" class="@if($step == 5) text-danger @endif">Additional Info</a></li>
                      @else
                        <li class="step-progressbar__item"><a href="javascript:;" class="@if($step == 5) text-danger @endif">Additional Info</a></li>
                      @endif

                      @if($product->step == 5)                      
                        <li class="step-progressbar__item step-progressbar__item--active"><a href="{{route('showNewCatelogueFaq',['product'=>$product->id])}}" class="@if($step == 6) text-danger @endif">FAQ</a></li>
                      @elseif($product->step > 3)
                        <li class="step-progressbar__item step-progressbar__item--complete"><a href="{{route('showNewCatelogueFaq',['product'=>$product->id])}}" class="@if($step == 6) text-danger @endif">FAQ</a></li>
                      @else
                        <li class="step-progressbar__item"><a href="javascript:;" class="@if($step == 6) text-danger @endif">FAQ</a></li>
                      @endif

                     @else 
                      <li class="step-progressbar__item step-progressbar__item--active"><a href="javascript:;" class="@if($step == 1) text-danger @endif">Main Info</a></li>
                      <li class="step-progressbar__item"><a href="javascript:;">Pricing</a></li>
                      <li class="step-progressbar__item"><a href="javascript:;">Package Info</a></li>
                      <li class="step-progressbar__item"><a href="javascript:;">Product Images</a></li>   
                      <li class="step-progressbar__item"><a href="javascript:;">Additional Info</a></li>
                      <li class="step-progressbar__item"><a href="javascript:;">FAQ</a></li>
                     @endif
                  </ul>
            <!----------------------------------------------->