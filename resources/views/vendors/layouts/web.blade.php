<?php
    header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
    header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket Plus">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels"> --}}

    <title>Daily Cliq</title>

    <!-- vendor css -->
    <link href="{{asset('app/lib/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('app/lib/rickshaw/rickshaw.min.css')}}" rel="stylesheet">
    <link href="{{asset('app/lib/select2/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('app/lib/spinkit/css/spinkit.css')}}" rel="stylesheet">
    <link href="{{asset('app/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('app/lib/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet">
    <link href="{{asset('app/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{asset('app/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('app/css/icons.css')}}">
    <link rel="stylesheet" href="{{asset('app/css/bracket.css')}}">
    <script>
     let base_url = '{{route("loginVendor")}}';
    </script>

    <script src="{{asset('app/lib/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('app/lib/select2/js/select2.full.min.js')}}"></script>
    <script src="{{asset('app/lib/select2/js/select2.min.js')}}"></script>
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>

    <!-- ########## START: LEFT PANEL ########## -->
  <div class="br-logo"><a href="{{route('vendorDashboard')}}"> <img src="{{asset('app/img/logo-small.png')}}" class="mg-b-20 mg-lg-b-30 img-fluid" alt="daily cliq"></a></div>
    <div class="br-sideleft br-secondary sideleft-scrollbar">
      <label class="sidebar-label pd-x-10 mg-t-20 op-3">Navigation</label>
      <ul class="br-sideleft-menu">
        <li class="br-menu-item" id="dashboardLi">
        <a href="{{route('vendorDashboard')}}" class="br-menu-link @if(@$active[0]=='dashboard'){{'active'}} @endif">
            <i class="menu-item-icon icon ion-ios-home tx-24"></i>
            <span class="menu-item-label">Dashboard</span>
          </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item" id="catelogId">
          <a href="{{route('showCatelog')}}" class="br-menu-link @if(@$active[0]=='catelogue'){{'active'}} @endif">
            <i class="menu-item-icon icon ion-ios-bookmarks tx-24"></i>
            <span class="menu-item-label">Catalogue</span>
          </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item" id="inventoryId">
            <a href="{{route('showInventory')}}" class="br-menu-link @if(@$active[0]=='inventory'){{'active'}} @endif">
              <i class="menu-item-icon icon ion-ios-analytics tx-24"></i>
              <span class="menu-item-label">Inventory</span>
            </a><!-- br-menu-link -->
          </li><!-- br-menu-item -->
          <li class="br-menu-item">
            <a href="{{route('showOrder')}}" class="br-menu-link @if(@$active[0]=='orders'){{'active'}} @endif">
              <i class="menu-item-icon icon ion-ios-list tx-24"></i>
              <span class="menu-item-label">Orders</span>
            </a><!-- br-menu-link -->
          </li><!-- br-menu-item -->
          <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub  @if(@$active[0]=='advertising'){{'active'}} @endif @if((@$active[1]=='flashsale') ||(@$active[1]=='dealoftheday')){{'showsub'}} @endif">
              <i class="menu-item-icon icon ion-easel tx-24"></i>
              <span class="menu-item-label">Advertising</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
              <li class="sub-item active"><a href="{{route('flashSale')}}" class="sub-link  @if(@$active[1]=='flashsale'){{'active'}} @endif">Flash Sale</a></li>
              <li class="sub-item"><a href="{{route('showDealOftheDay')}}" class="sub-link @if(@$active[1]=='dealoftheday'){{'active'}} @endif">Deal Of The Day</a></li>
            </ul>
          </li><!-- br-menu-item -->
         
          <li class="br-menu-item">
            <a href="" class="br-menu-link @if(@$active[0]=='reports'){{'active'}} @endif">
              <i class="menu-item-icon icon ion-ios-copy tx-24"></i>
              <span class="menu-item-label">Reports</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
              <li class="sub-item"><a href="{{route('salesReport')}}" class="sub-link  @if(@$active[1]=='sales'){{'active'}} @endif">Sales Report</a></li>
              <li class="sub-item"><a href="{{route('stockReport')}}" class="sub-link  @if(@$active[1]=='stock'){{'active'}} @endif">Stock Report</a></li>
              <li class="sub-item"><a href="{{route('orderReport')}}" class="sub-link  @if(@$active[1]=='order'){{'active'}} @endif">Order Report</a></li>
            </ul>
          </li><!-- br-menu-item -->
          @php /*   <li class="br-menu-item">
            <a href="" class="br-menu-link">
              <i class="menu-item-icon icon ion-ios-speedometer tx-24"></i>
              <span class="menu-item-label">Performance</span>
            </a><!-- br-menu-link -->
          </li><!-- br-menu-item -->
          <li class="br-menu-item">
            <a href="" class="br-menu-link">
              <i class="menu-item-icon icon ion-android-textsms tx-24"></i>
              <span class="menu-item-label">Messages</span>
            </a><!-- br-menu-link -->
          </li><!-- br-menu-item -->
          <li class="br-menu-item">
            <a href="" class="br-menu-link">
              <i class="menu-item-icon ion-ios-cog tx-24"></i>
              <span class="menu-item-label">Account Details</span>
            </a><!-- br-menu-link -->
          </li><!-- br-menu-item --> 
          */ @endphp
      </ul><!-- br-sideleft-menu -->

    </div><!-- br-sideleft -->
    <!-- ########## END: LEFT PANEL ########## -->

    <!-- ########## START: HEAD PANEL ########## -->
    <div class="br-header">
      <div class="br-header-left">
        <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
        <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>

      </div><!-- br-header-left -->
      <div class="br-header-right">
        <nav class="nav">
        <div class="pd-x-25">
            <h2 id="brTime" class="tx-white tx-lato mg-b-5"></h2>
            <h6 id="brDate" class="tx-white tx-light op-3"></h6>
          </div>
          <div class="dropdown">
            <!-- <a href="#" class="nav-link pd-x-7 pos-relative" >
              <i class="icon ion-ios-email-outline tx-24"></i> -->
              <!-- start: if statement -->
              <!-- <span class="square-8 bg-danger pos-absolute t-15 r-0 rounded-circle"></span> -->
              <!-- end: if statement -->
            <!-- </a> -->

            <div class="dropdown-menu dropdown-menu-header">
              <<div class="dropdown-menu-label">
                <label>Messages</label>
                <a href="">+ Add New Message</a>
              </div>   <!-- d-flex -->

              <div class="media-list">
                <!-- loop starts here -->
                <a href="" class="media-list-link">
                  <div class="media">
                    <img src="https://via.placeholder.com/500" alt="">
                    <div class="media-body">
                      <div>
                        <p>Donna Seay</p>
                        <span>2 minutes ago</span>
                      </div><!-- d-flex -->
                      <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring.</p>
                    </div>
                  </div><!-- media -->
                </a>
                <!-- loop ends here -->
                <a href="" class="media-list-link read">
                  <div class="media">
                    <img src="https://via.placeholder.com/500" alt="">
                    <div class="media-body">
                      <div>
                        <p>Samantha Francis</p>
                        <span>3 hours ago</span>
                      </div><!-- d-flex -->
                      <p>My entire soul, like these sweet mornings of spring.</p>
                    </div>
                  </div><!-- media -->
                </a>
    
   
                <div class="dropdown-footer">
                  <a href=""><i class="fas fa-angle-down"></i> Show All Messages</a>
                </div>
              </div><!-- media-list -->
            </div><!-- dropdown-menu -->
          </div><!-- dropdown -->
          <div class="dropdown">
            <a href="#" class="nav-link pd-x-7 pos-relative" data-toggle="dropdown">
              <i class="icon ion-ios-bell-outline tx-24"></i>
              <!-- start: if statement -->

              <span class="square-8 bg-danger pos-absolute t-15 r-5 rounded-circle notification-mark"></span>
              <!-- end: if statement -->
            </a>
            <div class="dropdown-menu dropdown-menu-header">
              <div class="dropdown-menu-label">
                <label>Notifications</label>
                <a href="javascript:;" id="markall">Mark All as Read</a>
              </div><!-- d-flex -->
              <div class="media-list" id="notification">
                <!-- loop starts here -->

              </div><!-- media-list -->
            </div><!-- dropdown-menu -->
          </div><!-- dropdown -->
          <div class="dropdown">
            <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
              <span class="logged-name hidden-md-down">{{Auth::user()->name}}</span>
            <img src="{{asset(Auth::user()->image)}}" class="wd-32 rounded-circle" alt="">
              <span class="square-10 bg-success"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-header wd-250">
              <div class="tx-center">
                <a href=""><img src="{{asset(Auth::user()->image)}}" class="wd-80 rounded-circle" alt=""></a>
                <h6 class="logged-fullname">{{Auth::user()->name}}</h6>
                <p>{{Auth::user()->email}}</p>
              </div>

              <ul class="list-unstyled user-profile-nav">
              <li><a href="{{ route('showVendorProfile') }}"><i class="icon ion-ios-person"></i> Edit Profile</a></li>
              <li><a href="{{route('vendorLogout')}}"><i class="icon ion-power"></i> Sign Out</a></li>
              </ul>
            </div><!-- dropdown-menu -->
          </div><!-- dropdown -->
        </nav>
      </div><!-- br-header-right -->
    </div><!-- br-header -->
    <!-- ########## END: HEAD PANEL ########## -->

    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">

      @yield('content')

      <!--footer -->
      <footer class="br-footer">
        {{-- <div class="footer-left">
          <div class="mg-b-2">Copyright &copy; 2019. Iroid Technologies. All Rights Reserved.</div>
        </div>
        <div class="footer-right d-flex align-items-center">
          <span class="tx-uppercase mg-r-10">Share:</span>
          <a target="_blank" class="pd-x-5" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//themepixels.me/bracketplus/intro"><i class="fab fa-facebook tx-20"></i></a>
          <a target="_blank" class="pd-x-5" href="https://twitter.com/home?status=Bracket%20Plus,%20your%20best%20choice%20for%20premium%20quality%20admin%20template%20from%20Bootstrap.%20Get%20it%20now%20at%20http%3A//themepixels.me/bracketplus/intro"><i class="fab fa-twitter tx-20"></i></a>
        </div> --}}
      </footer>

    </div><!-- br-mainpanel -->
    <!-- ########## END: MAIN PANEL ########## -->

    
    <script src="{{asset('app/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('app/lib/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script src="{{asset('app/lib/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('app/lib/peity/jquery.peity.min.js')}}"></script>
    <script src="{{asset('app/lib/jquery.flot/jquery.flot.js')}}"></script>
    <script src="{{asset('app/lib/jquery.flot/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('app/lib/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
    <script src="{{asset('app/lib/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('app/lib/echarts/echarts.min.js')}}"></script>
    
    <!-- inner pages -->
    <script src="{{asset('app/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('app/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{asset('app/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('app/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
    
    <!-- end inner page -->
    <script src="{{asset('app/js/bracket.js')}}"></script>
    <script src="{{asset('app/js/ResizeSensor.js')}}"></script>
    <script src="{{asset('app/js/widgets.js')}}"></script>
    
    <!-- <script src="{{asset('app/js/dashboard.js')}}"></script>  -->
    <script src="{{asset('js/jquery.validate.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

    @yield('scripts')
        <!-- Start of  Zendesk Widget script -->
        <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=8c12e370-9057-4f11-8f3a-dacde0a11570"> </script>
    <!-- End of  Zendesk Widget script -->
    <script src="{{asset('app/lib/jquery-ui/ui/widgets/datepicker.js')}}"></script>
    <script>
$('.from-datepicker').datepicker({
    showOtherMonths: true,
    selectOtherMonths: true
  });

  $('.to-datepicker').datepicker({
    showOtherMonths: true,
    selectOtherMonths: true
  });
    </script>
    <script>
    $(document).ready(function(){
      $('.notification-mark').hide();
      $.ajax({
            url: "{{route('getVendorNotification')}}",
            type:'GET',
            success: function(data) {
                if(data!=''){
                  $('.notification-mark').show();
                  $('#notification').html(data);

                }
            }
      });
      $('#markall').on('click',function(){
        $.ajax({
            url: "{{route('markAllNotification')}}",
            type:'GET',
            success: function(data) {
              $('.notification-mark').hide();
                $('#notification').html('');
            }
      });
      });
    });

    </script>
  </body>
</html>
