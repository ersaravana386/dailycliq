 <!----------------------------------------------->
 <ul class="step-progressbar">
   @if($variant->id)
   @if($variant->step == 0)
   <li class="step-progressbar__item step-progressbar__item--active"><a href="{{route('showNewCatelognewvarient',['product'=>$variant->variant])}}?variant={{$variant->id}}" class="@if($step == 1) text-danger @endif">Main Info</a></li>
   @elseif($variant->step > 0)
   <li class="step-progressbar__item step-progressbar__item--complete"><a href="{{route('showNewCatelognewvarient',['product'=>$variant->variant])}}?variant={{$variant->id}}" class="@if($step == 1) text-danger @endif">Main Info</a></li>
   @endif

   @if($variant->step == 1)
   <li class="step-progressbar__item step-progressbar__item--active"><a href="{{route('showNewvarinatPricingCatelog',['product'=>$variant->variant,'variant'=>$variant->id])}}" class="@if($step == 2) text-danger @endif">Pricing</a></li>
   @elseif($variant->step > 1)
   <li class="step-progressbar__item step-progressbar__item--complete"><a href="{{route('showNewvarinatPricingCatelog',['product'=>$variant->variant,'variant'=>$variant->id])}}" class="@if($step == 2) text-danger @endif">Pricing</a></li>
   @else
   <li class="step-progressbar__item"><a href="javascript:;" class="@if($step == 2) text-danger @endif">Pricing</a></li>
   @endif

   @if($variant->step == 2)
   <li class="step-progressbar__item step-progressbar__item--active"><a href="{{route('showNewvariantCatelogPackageDetails',['product'=>$variant->variant,'variant'=>$variant->id])}}" class="@if($step == 3) text-danger @endif">Package Info</a></li>
   @elseif($variant->step > 2)
   <li class="step-progressbar__item step-progressbar__item--complete"><a href="{{route('showNewvariantCatelogPackageDetails',['product'=>$variant->variant,'variant'=>$variant->id])}}" class="@if($step == 3) text-danger @endif">Package Info</a></li>
   @else
   <li class="step-progressbar__item"><a href="javascript:;" class="@if($step == 3) text-danger @endif">Package Info</a></li>
   @endif

   @if($variant->step == 3)
   <li class="step-progressbar__item step-progressbar__item--active"><a href="{{route('showNewvariantCatelogImages',['product'=>$variant->variant, 'variant'=>$variant->id])}}" class="@if($step == 4) text-danger @endif">variant Images</a></li>
   @elseif($variant->step > 3)
   <li class="step-progressbar__item step-progressbar__item--complete"><a href="{{route('showNewvariantCatelogImages',['product'=>$variant->variant, 'variant'=>$variant->id])}}" class="@if($step == 4) text-danger @endif">variant Images</a></li>
   @else
   <li class="step-progressbar__item"><a href="javascript:;" class="@if($step == 4) text-danger @endif">variant Images</a></li>
   @endif

   @if($variant->step == 4)
   <li class="step-progressbar__item step-progressbar__item--active"><a href="{{route('showNewvariantCatelogAdditionalInfo',['product'=>$variant->variant, 'variant'=>$variant->id])}}" class="@if($step == 5) text-danger @endif">Additional Info</a></li>
   @elseif($variant->step > 4)
   <li class="step-progressbar__item step-progressbar__item--complete"><a href="{{route('showNewvariantCatelogAdditionalInfo',['product'=>$variant->variant, 'variant'=>$variant->id])}}" class="@if($step == 5) text-danger @endif">Additional Info</a></li>
   @else
   <li class="step-progressbar__item"><a href="javascript:;" class="@if($step == 5) text-danger @endif">Additional Info</a></li>
   @endif

   @if($variant->step == 5)
   <li class="step-progressbar__item step-progressbar__item--active"><a href="{{route('showNewvariantCatelogueFaq',['product'=>$variant->variant, 'variant'=>$variant->id])}}" class="@if($step == 6) text-danger @endif">FAQ</a></li>
   @elseif($variant->step > 5)
   <li class="step-progressbar__item step-progressbar__item--complete"><a href="{{route('showNewvariantCatelogueFaq',['product'=>$variant->variant, 'variant'=>$variant->id])}}" class="@if($step == 6) text-danger @endif">FAQ</a></li>
   @else
   <li class="step-progressbar__item"><a href="javascript:;" class="@if($step == 6) text-danger @endif">FAQ</a></li>
   @endif

   @else
   <li class="step-progressbar__item step-progressbar__item--active"><a href="javascript:;" class="@if($step == 1) text-danger @endif">Main Info</a></li>
   <li class="step-progressbar__item"><a href="javascript:;">Pricing</a></li>
   <li class="step-progressbar__item"><a href="javascript:;">Package Info</a></li>
   <li class="step-progressbar__item"><a href="javascript:;">variant Images</a></li>
   <li class="step-progressbar__item"><a href="javascript:;">Additional Info</a></li>
   <li class="step-progressbar__item"><a href="javascript:;">FAQ</a></li>
   @endif
 </ul>
 <!----------------------------------------------->