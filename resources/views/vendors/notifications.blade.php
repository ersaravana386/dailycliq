@extends('vendors.layouts.web')
@section('content')
<div class="br-pagebody">
    <div class="container-fluid pt-2">
        <div class="row row-sm justify-content-center new">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-8">
                <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">Notification List</h4> 
                <div class="card bg-white border-0 shadow-sm py-5 px-4">
                    <div class="col-lg-12 msgDiv" role="alert">
                    </div><!-- alert -->
                     <a href="{{route('markAllNotificationList')}}" class="pull-right">Mark All as Read</a>
                    <div class="bd bd-gray-300 rounded table-responsive">
                    <table class="table mg-b-0">
                            <thead>
                                <tr>
                                    <th class="wd-5p">#</th>
                                    <th class="wd-15p">Notification</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $i= 0;
                            @endphp
                            @foreach ($notification  as $key=> $item)
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                <td scope="row">
                                <a href="{{route('showNotification',['id'=>$item->id])}}" class='media-list-link read'><div class='media'><div class='media-body'>
                                   <strong>{{$item->process}}</strong></a>
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                        {{ $notification->appends(['search' => $search ?? ''])->links() }}  
                        </div>
                    </div>
                </div>
                <!-- bd -->
            </div>
        </div>
    </div><!-- row -->
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
<script>
$(document).ready(function(){
    $('.offer-button').click(function(){
        var form = $(this).closest("form");
        var offer =form.find('input[name="offer"]').val();
        var product =form.find('input[name="product"]').val();
        var serializedData = form.serialize();
        if(offer==''){
            $('.msgDiv').html(`<div class="alert alert-danger m-2">Offer Percentage is required</div>`);
                  setTimeout(function() {
                      $('.msgDiv').html(``);
                  }, 4000);
        }else if(product==''){
            $('.msgDiv').html(`<div class="alert alert-danger m-2">Product Details Should be required</div>`);
                  setTimeout(function() {
                      $('.msgDiv').html(``);
                  }, 4000);
        }else{
            $.ajax({
            type: "POST",
            url: "{{route('saveDealOftheDayProduct')}}",
            dataType: "json",
            data: {offer:offer,product:product},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
              if (data.status == 1) {
                  if(data.redirect){
                    window.location.href = data.redirect;
                  }
              } if (data.status == 0) {
                 
                  var html = "";
                  $.each(data.errors, function(key, value) {
                      html += value + "<br/>";
                  });
                  $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                  setTimeout(function() {
                      $('.msgDiv').html(``);
                  }, 4000);
             
              } 
            }
          });

        }
    });
});
</script>
@endsection
