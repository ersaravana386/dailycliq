<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Twitter -->
  <meta name="twitter:site" content="@themepixels">
  <meta name="twitter:creator" content="@themepixels">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="Daily CliQ">
  <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
  <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

  <!-- Facebook -->
  <meta property="og:url" content="http://themepixels.me/bracketplus">
  <meta property="og:title" content="Daily CliQ">
  <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

  <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
  <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
  <meta property="og:image:type" content="image/png">
  <meta property="og:image:width" content="1200">
  <meta property="og:image:height" content="600">

  <!-- Meta -->
  <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
  <meta name="author" content="ThemePixels">

  <title>Daily CliQ</title>

  <!-- vendor css -->
  <link href="{{asset('app/lib/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
  <link href="{{asset('app/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">

  <!-- Bracket CSS -->
  <link rel="stylesheet" href="{{asset('app/css/bracket.css')}}">
  <link rel="stylesheet" href="{{asset('app/lib/select2/css/select2.css')}}">
  <link rel="stylesheet" href="{{asset('app/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('app/css/fullform.css')}}">
  <link rel="stylesheet" href="{{asset('app/css/demo.css')}}">
  <script src="{{asset('js/modernizr.custom.js')}}"></script>
</head>

<body class="has-background-image login">
  <div class="has-shade login-right z-index-1"></div>

  <div class="container-fluid">
    <div class="row has-background-image login">
      <div class="fs-form-wrap" id="fs-form-wrap">
        <form class="fs-form fs-form-full" role="form" method="post" action="javascript:;" id="regform" autocomplete="off">
          <ol class="fs-fields">
            <li>
                <div class="form-group">
                    <label class="fs-field-label fs-anim-upper" for="company_name">Your legal company name?</label>
                    <input class="fs-anim-lower" id="company_name" name="company_name" type="text" placeholder="Ekart Commerce Pvt.Ltd"  required />
                </div>
                {{-- <div class="form-group">
                    <label class="fs-field-label fs-anim-upper" for="address">Address</label>
                    <textarea class="fs-anim-lower" id="address" name="address" type="text" placeholder="Eg: Ekart logistics" required></textarea>
                </div> --}}
                <div class="form-group">
                    <label class="fs-field-label fs-anim-upper" for="phone">Phone Number?</label>
                    <input class="fs-anim-lower" id="phone" name="phone" type="text" placeholder="0123-456789" required/>
                </div>
            </li>

            <li>
                <div class="form-group">
                    <label class="fs-field-label fs-anim-upper" for="store_name">Your store name?</label>
                    <input class="fs-anim-lower" id="store_name" name="store_name" type="text" placeholder="My Store" required />
                </div>
                <div class="form-group">
                    <label class="fs-field-label fs-anim-upper" for="location">Location?</label>
                    <input class="fs-anim-lower" id="location" name="location" type="text" placeholder="Kochi" required />
                </div>
            </li>

            <li>
              <div class="form-group">
                <label class="fs-field-label fs-anim-upper" for="q3" data-info="We won't send you spam, we promise...">Your selling categories?</label>
                  <select class="js-example-basic-multiple" name="category[]" id="category" multiple="multiple" rows='2' required>
                      <option value="1">Electronics</option>
                      <option value="2">Fashion</option>
                      <option value="3">Mobiles</option>
                    </select>
              </div>
            </li>

            <li>
                <div class="form-group">
                    <label class="fs-field-label fs-anim-upper" for="trade_license_number">Your trade license number?</label>
                    <input class="fs-anim-lower" id="trade_license_number" name="trade_license_number" type="text" placeholder="0123456789" required/>
                </div>
                <div class="form-group">
                    <label class="fs-field-label fs-anim-upper" for="q4">Your trade license document?</label>
                    <div class="custom-file-upload fs-anim-lower">
                        <input type="file" id="file" name="trade_license_document" />
                    </div>
                </div>
                <div class="form-group">
                        <label class="fs-field-label fs-anim-upper" for="phone">Your GST number?</label>
                        <input class="fs-anim-lower" id="gst_number" name="gst_number" type="text" placeholder="0123456789"/>
                    </div>
                    <div class="form-group">
                        <label class="fs-field-label fs-anim-upper" for="q4">Your GST document?</label>
                        <div class="custom-file-upload fs-anim-lower">
                            <input type="file" id="file" name="gst_document" />
                        </div>
                    </div>
            </li>

            <li>
                    <div class="form-group">
                            <label class="fs-field-label fs-anim-upper" for="name_as_in_bank">Your name in bank documents?</label>
                            <input class="fs-anim-lower" id="name_as_in_bank" name="name_as_in_bank" type="text" placeholder="Santi K Cazorla" required/>
                    </div>
                    <div class="form-group">
                        <label class="fs-field-label fs-anim-upper" for="account_type">Your bank account type</label>
                        <select class="js-example-basic-multiples" name="account_type" id="account_type" rows='2' required>
                            <option value="1">Savings</option>
                            <option value="2">Current</option>
                          </select>
                    </div>
                    <div class="form-group">
                            <label class="fs-field-label fs-anim-upper" for="account_number">Your bank account number?</label>
                            <input class="fs-anim-lower" id="account_number" name="account_number" type="text" placeholder="0123456789" required/>
                    </div>
                    <div class="form-group">
                            <label class="fs-field-label fs-anim-upper" for="ifsc_code">Your bank IFSC code?</label>
                            <input class="fs-anim-lower" id="ifsc_code" name="ifsc_code" type="text" placeholder="0123456789" required/>
                    </div>

                </li>
          </ol><!-- /fs-fields -->
          <div class="msgDiv mt-3"></div>
          <button class="fs-submit" type="submit">Launch your business</button>
        </form><!-- /fs-form -->
      </div>
    </div>
  </div>


  <!-- invitation modal -->
  <div id="inviteModal" class="modal fade effect-scale">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="col-lg-12 rounded-left">
        <div class="col-md-12 col-xl-12 mg-t-30 mg-xl-t-0">
          <div class="d-flex  ht-300 pos-relative align-items-center">
            <div class="sk-wave">
              <div class="sk-rect sk-rect1 bg-gray-800"></div>
              <div class="sk-rect sk-rect2 bg-gray-800"></div>
              <div class="sk-rect sk-rect3 bg-gray-800"></div>
              <div class="sk-rect sk-rect4 bg-gray-800"></div>
              <div class="sk-rect sk-rect5 bg-gray-800"></div>
            </div>
          </div><!-- d-flex -->
        </div><!-- col-4 -->

      </div><!-- row -->
    </div><!-- modal-dialog -->
  </div><!-- modal -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="{{asset('app/lib/select2/js/select2.full.min.js')}}"></script>
  <script src="{{asset('js/classie.js')}}"></script>
  <script src="{{asset('js/selectFx.js')}}"></script>
  <script src="{{asset('js/fullscreenForm.js')}}"></script>
  <script src="{{asset('app/js/file-upload.js')}}"></script>
  <script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
        placeholder: 'Please select ...',
        containerCssClass: 'fs-anim-lower',
        width: '100%',
        });
    });
  </script>

<script>
    $(document).ready(function() {
    $('.js-example-basic-multiples').select2({
        placeholder: 'Please select ...',
        containerCssClass: 'fs-anim-lower',
        width: '100%',
        minimumResultsForSearch: -1
    });
    });
</script>
  <script>
    (function () {
      var formWrap = document.getElementById('fs-form-wrap');
      [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function (el) {
        new SelectFx(el, {
          stickyPlaceholder: false,
          onChange: function (val) {
            document.querySelector('span.cs-placeholder').style.backgroundColor = val;
          }
        });
      });

      new FForm(formWrap, {
        onReview: function () {
          classie.add(document.body, 'overview'); // for demo purposes only
        }
      });
    })();
  </script>


  <script>
    $("#regform").submit(function(){
        var form = document.getElementById('regform');
        var data = new FormData(form);
        $.ajax({
                type: "post",
                url: "{{route('addRegistration')}}",
                data: data,
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                success: function(data) {
                    if (data.status == 1) {
                        $('.msgDiv').html(`<div class="alert alert-success">` + data.message + `</div>`);
                        setTimeout(function() {
                        $('.msgDiv').html(``);
                        location.href = "{{route('loginVendor')}}";
                    }, 2000);

                    } else {
                    var html = "";
                    $.each(data.errors, function(key, value) {
                        html += value + "<br/>";
                    });
                    $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                    setTimeout(function() {
                        $('.msgDiv').html(``);
                        }, 4000);
                    }
                }
        });
    });
  </script>
</body>

</html>
