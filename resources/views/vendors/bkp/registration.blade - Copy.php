<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Daily CliQ">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Daily CliQ">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Daily CliQ</title>

    <!-- vendor css -->
    <link href="{{asset('app/lib/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('app/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{asset('app/css/bracket.css')}}">
    <link rel="stylesheet" href="{{asset('app/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('app/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('app/css/fullform.css')}}">
    <script src="{{asset('js/modernizr.custom.js')}}"></script>
  </head>
  <body>
      
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <form id="myform" class="fs-form fs-form-full" autocomplete="off">
					<ol class="fs-fields">
						<li>
							<label class="fs-field-label fs-anim-upper" for="q1">What's your name?</label>
							<input class="fs-anim-lower" id="q1" name="q1" type="text" placeholder="Dean Moriarty" required/>
						</li>
						<li>
							<label class="fs-field-label fs-anim-upper" for="q2" data-info="We won't send you spam, we promise...">What's your email address?</label>
							<input class="fs-anim-lower" id="q2" name="q2" type="email" placeholder="dean@road.us" required/>
						</li>
						<li data-input-trigger>
							<label class="fs-field-label fs-anim-upper" for="q3" data-info="This will help us know what kind of service you need">What's your priority for your new website?</label>
							<div class="fs-radio-group fs-radio-custom clearfix fs-anim-lower">
								<span><input id="q3b" name="q3" type="radio" value="conversion"/><label for="q3b" class="radio-conversion">Sell things</label></span>
								<span><input id="q3c" name="q3" type="radio" value="social"/><label for="q3c" class="radio-social">Become famous</label></span>
								<span><input id="q3a" name="q3" type="radio" value="mobile"/><label for="q3a" class="radio-mobile">Mobile market</label></span>
							</div>
						</li>
						<li data-input-trigger>
							<label class="fs-field-label fs-anim-upper" data-info="We'll make sure to use it all over">Choose a color for your website.</label>
							<select class="cs-select cs-skin-boxes fs-anim-lower">
								<option value="" disabled selected>Pick a color</option>
								<option value="#588c75" data-class="color-588c75">#588c75</option>
								<option value="#b0c47f" data-class="color-b0c47f">#b0c47f</option>
								<option value="#f3e395" data-class="color-f3e395">#f3e395</option>
								<option value="#f3ae73" data-class="color-f3ae73">#f3ae73</option>
								<option value="#da645a" data-class="color-da645a">#da645a</option>
								<option value="#79a38f" data-class="color-79a38f">#79a38f</option>
								<option value="#c1d099" data-class="color-c1d099">#c1d099</option>
								<option value="#f5eaaa" data-class="color-f5eaaa">#f5eaaa</option>
								<option value="#f5be8f" data-class="color-f5be8f">#f5be8f</option>
								<option value="#e1837b" data-class="color-e1837b">#e1837b</option>
								<option value="#9bbaab" data-class="color-9bbaab">#9bbaab</option>
								<option value="#d1dcb2" data-class="color-d1dcb2">#d1dcb2</option>
								<option value="#f9eec0" data-class="color-f9eec0">#f9eec0</option>
								<option value="#f7cda9" data-class="color-f7cda9">#f7cda9</option>
								<option value="#e8a19b" data-class="color-e8a19b">#e8a19b</option>
								<option value="#bdd1c8" data-class="color-bdd1c8">#bdd1c8</option>
								<option value="#e1e7cd" data-class="color-e1e7cd">#e1e7cd</option>
								<option value="#faf4d4" data-class="color-faf4d4">#faf4d4</option>
								<option value="#fbdfc9" data-class="color-fbdfc9">#fbdfc9</option>
								<option value="#f1c1bd" data-class="color-f1c1bd">#f1c1bd</option>
							</select>
						</li>
						<li>
							<label class="fs-field-label fs-anim-upper" for="q4">Describe how you imagine your new website</label>
							<textarea class="fs-anim-lower" id="q4" name="q4" placeholder="Describe here"></textarea>
						</li>
						<li>
							<label class="fs-field-label fs-anim-upper" for="q5">What's your budget?</label>
							<input class="fs-mark fs-anim-lower" id="q5" name="q5" type="number" placeholder="1000" step="100" min="100"/>
						</li>
					</ol><!-- /fs-fields -->
					<button class="fs-submit" type="submit">Send answers</button>
				</form><!-- /fs-form -->
      </div>
    </div>
  </div>


  <!-- invitation modal -->
  <div id="inviteModal" class="modal fade effect-scale">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="col-lg-12 rounded-left">
            <div class="col-md-12 col-xl-12 mg-t-30 mg-xl-t-0">
                <div class="d-flex  ht-300 pos-relative align-items-center">
                  <div class="sk-wave">
                    <div class="sk-rect sk-rect1 bg-gray-800"></div>
                    <div class="sk-rect sk-rect2 bg-gray-800"></div>
                    <div class="sk-rect sk-rect3 bg-gray-800"></div>
                    <div class="sk-rect sk-rect4 bg-gray-800"></div>
                    <div class="sk-rect sk-rect5 bg-gray-800"></div>
                  </div>
                </div><!-- d-flex -->
              </div><!-- col-4 -->

        </div><!-- row -->
    </div><!-- modal-dialog -->
  </div><!-- modal -->

    <script src="{{asset('app/lib/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('app/lib/jquery-ui/ui/widgets/datepicker.js')}}"></script>
    <script src="{{asset('app/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('app/js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset('app/lib/parsleyjs/parsley.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate.js')}}"></script>
    <script src="{{asset('js/scrollreveal.js')}}"></script>
    <script src="{{asset('js/classie.js')}}"></script>
    <script src="{{asset('js/fullscreenForm.js')}}"></script>
    <script src="{{asset('js/selectFx.js')}}"></script>

    <script>
      <script>
			(function() {
				var formWrap = document.getElementById( 'fs-form-wrap' );

				[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
					new SelectFx( el, {
						stickyPlaceholder: false,
						onChange: function(val){
							document.querySelector('span.cs-placeholder').style.backgroundColor = val;
						}
					});
				} );

				new FForm( formWrap, {
					onReview : function() {
						classie.add( document.body, 'overview' ); // for demo purposes only
					}
				} );
			})();
		</script>
    </script>
    <script>
      $("#wizard2").on('change','#trade_license_document',function(){
            var fileName = $(this).val();
            //replace the "Choose a file" label
            $("#wizard2").find('#trade_license_document_label').html(fileName);
        });
        $("#wizard2").on('change','#gst_document',function(){
            var fileName = $(this).val();
            //replace the "Choose a file" label
            $("#wizard2").find('#gst_document_label').html(fileName);
        });
    </script>
    <script>
        $(document).ready(function(){
          'use strict';
          $('#wizard2').steps({
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
            onStepChanging: function (event, currentIndex, newIndex) {
              if(currentIndex < newIndex) {
                // Step 1 form validation
                if(currentIndex === 0) {
                    return true;
                //   var company_name = $('#company_name').parsley();
                //   var contact_person = $('#contact_person').parsley();
                //   var email = $('#email').parsley();
                //   if(company_name.isValid() && contact_person.isValid() && email.isValid()) {
                //     return true;
                //   } else {
                //     company_name.validate();
                //     contact_person.validate();
                //     email.validate()
                //   }
                }

                // Step 2 form validation
                if(currentIndex === 1) {
                    return true;
                //   var mobile = $('#mobile').parsley();
                //   var phone = $('#phone').parsley();
                //   var address = $('#address').parsley();

                //   if(mobile.isValid() && address.isValid() && phone.isValid() ) {
                //     return true;
                //   } else {
                //     mobile.validate();
                //     phone.validate();
                //     address.validate();
                //   }
                }
               // Always allow step back to the previous step even if the current step is not valid.
              } if(currentIndex === 2) {
                    return true;
              }if(currentIndex === 3) {
                    return true;
              }
              else { return true; }
            },
            onFinished: function (event, currentIndex) {
              var trade_license_number = $('#trade_license_number').parsley();
              var trade_license_document = $('#trade_license_document').parsley();
              var gst_number = $('#gst_number').parsley();
              var gst_document = $('#gst_document').parsley();
              if(trade_license_number.isValid() && trade_license_document.isValid() && gst_number.isValid()&& gst_document.isValid() ) {
                $("#wizard2").find("a[href='#finish']").addClass('disble-button');
                $("#wizard2").find("a[href='#finish']").addClass('loading-button');
                var form = document.getElementById('signUpForm');
                var data = new FormData(form);
                $.ajax({
                    type: "post",
                    url: "{{route('signUpVendor')}}",
                    data: data,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                    cache: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        if (data.status == 1) {
                            $('.msgDiv').html(`<div class="alert alert-success">` + data.message + `</div>`);
                            setTimeout(function() {
                                $('.msgDiv').html(``);
                                location.href = "{{route('loginVendor')}}";
                            }, 2000);

                        } else {
                          $("#wizard2").find("a[href='#finish']").removeClass('disble-button');
                            var html = "";
                            $.each(data.errors, function(key, value) {
                                html += value + "<br/>";
                            });
                            $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                            setTimeout(function() {
                                $('.msgDiv').html(``);
                            }, 4000);
                        }
                    }
                });
              } else {
                trade_license_number.validate();
                trade_license_document.validate();
                gst_number.validate();
                gst_document.validate();
              }
            }
          });
        });
      </script>
<script>
    $('#trade_license_document').on('change', function() {
      alert();
    var fileName = $(this).val();
    //replace the "Choose a file" label
    $('#trade_license_document_label').html(fileName);
    })
    $('#gst_document').on('change', function() {
    var fileName = $(this).val();
    //replace the "Choose a file" label
    $('#gst_document_label').html(fileName);
    })
    $('#image').on('change', function() {
    var fileName = $(this).val();
    //replace the "Choose a file" label
    $('#image_label').html(fileName);
    });

    // To style all selects
$('select').selectpicker();
</script>
  </body>
</html>

