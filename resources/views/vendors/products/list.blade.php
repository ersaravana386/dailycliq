@extends('vendors.layouts.web')
@section('content')

    <div class="br-pagebody">
      <div class="row no-gutters widget-1 shadow-base" style="margin-top:6rem !important">
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="card shadow-base bd-0">
                <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                <h6 class="card-title tx-uppercase tx-12 mg-b-0">List a new product</h6>
                </div><!-- card-header -->
                <div class="card-body d-xs-flex justify-content-between align-items-center" style="margin-bottom: 43px;">
                <h4 class="mg-b-0 tx-inverse">Search from Pathayapura catalogue</h4>
                </div><!-- card-body -->
                <div class="card-footer">
                    <form class="form-inline my-2 my-lg-0 flex-nowrap w-100 w-md-75 ">
                        <input class="form-control form-control-sm mr-2 has-border w-100" type="search" placeholder="Search" aria-label="Search" name="search" value="{{$search}}">
                        <button class="btn btn-primary btn-sm button-inner my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </div>
            </div><!-- card -->
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="card shadow-base bd-0">
                <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                <h6 class="card-title tx-uppercase tx-5 mg-b-0">
                    <a href="{{route('showVendorCategories')}}">
                    <button type="button" class="btn btn-link pl-0 mb-2 "> <h6 class="card-title tx-uppercase tx-12 mg-b-0 text-warning">Click here to create a new product listing</h6></button>
                    </a>
                </h6>
                </div><!-- card-header -->
                <div class="card-body d-xs-flex justify-content-between align-items-center"  style="margin-bottom: 109px;">
                <a href="{{route('showVendorProductList')}}">
                        <button type="button" class="btn btn-link pl-0"     >Pending products list</button>
                    </a>
                </div><!-- card-body -->
            </div><!-- card -->
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="card shadow-base bd-0">
                <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                <h6 class="card-title tx-uppercase tx-12 mg-b-0">
                    <a href="{{route('showVendorCategories')}}">
                    <button type="button" class="btn btn-link pl-0 mb-2">Inventory</button>
                    </a>
                </h6>
                </div><!-- card-header -->
                <div class="card-body d-xs-flex justify-content-between align-items-center">
                <div class="pd-x-25 pd-t-25 d-flex align-items-center">
                    <div class="product-inventory">
                        <p class="mg-b-5 tx-uppercase tx-10 tx-mont tx-semibold"><span>Active listings:</span> <span class="number">{{@$activelisting[0]->count}}</span></p>
                        <p class="tx-lato tx-white tx-normal"><span>Inactive listings:</span> <span class="number">{{@$pending[0]->count}}</span></p>
                        <p class="tx-12 tx-white-6 tx-roboto"><span>Out Of Stock:</span> <span class="number">{{@$stock[0]->count}}</span></p>
                        </div>
                        <div id="ch13" class="ht-160"></div>
                    </div><!-- pd-x-25 -->
                </div><!-- card-body -->
            </div><!-- card -->
        </div>
    </div>

<div class="br-pagebody">
    <div class="container-fluid pt-2">
        <div class="row row-sm justify-content-center new">
            <div class="col-12 col-sm-10 col-md-12 col-lg-12">
                <!-- <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">Vendor Product Listing</h4> -->
                <div class="card bg-white border-0 shadow-sm py-5 px-4">
                    <div class="bd bd-gray-300 rounded table-responsive">
                        <table class="table mg-b-0">
                            <thead>
                                <tr>
                                    <th class="wd-5p">#</th>
                                    <th class="wd-15p">NAME</th>
                                    <th class="wd-15p">DCIN</th>
                                    <th class="wd-15p">Image</th>
                                    <th class="wd-15p">PRICE</th>
                                    <th class="wd-15p">MRP</th>
                                    <th class="wd-15p">BRAND</th>
                                    <th class="wd-15p">CATEGORY</th>
                                    <th class="wd-15p">Comments</th>
                                    <th class="wd-20p">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $i= 0;
                            @endphp
                            @if(count($products)>0)
                            @foreach ($products as $key=> $item)
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                <td scope="row">
                                {{$item->name}} <br><label class="badge badge-success">Approved</label>
                                </td>
                                <td scope="row">{{$item->dcin}}</td>
                                <td>
                                    @if($item->images->first())
                                    @if(file_exists($item->images->first()->image))
                                    <a target="_blank" href="{{asset($item->images->first()->image)}}"><img src="{{asset($item->images->first()->image)}}" style="width:80px;"></a>
                                    @endif
                                    @endif
                                </td>
                                <td scope="row">{{$item->price}}</td>
                                <td scope="row">{{$item->mrp}}</td>
                                <td scope="row"> 
                                    @if($item->brand)
                                    {{$item->brand->name}}
                                    @else
                                    {{$item->other_brand}}
                                    @endif</td>
                                <td scope="row">{{$item->category->name}}</td>
                                <td scope="row">
                                    no comments
                                </td>
                                <td scope="row">
                                    <a class="btn btn-sm btn-success" onclick="return confirm('Are you sure you want to delete this product?');" href="{{route('deleteVendorApprovedProduct',['product'=>$item->id])}}" title="delete product">
                                            <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td scope="row" colspan=10><b><center><span style="color:#DC3545;">Sorry No Result Found</span></center><b></td>
                            </tr>
                            @endif
                            </tbody>
                        </table>
                        @if(count($products)>0)
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                        {{ $products->appends(['search' => $search])->links() }}
                        </div>
                        @endif  
                    </div>
                </div>
                <!-- bd -->
            </div>
        </div>
    </div><!-- row -->
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
<script>
  $('#catelogId').find('a').addClass('active');
  $("input[name='search']").autocomplete({
    source: function(request, response) {
      $.ajax({
        url: "{{route('searchProduct')}}",
        dataType: "json",
        data: {
          q: request.term
        },
        success: function(data) {
          response(data);
        }
      });
    },
    minLength: 3,
    select: function(event, ui) {
      // log( ui.item ?
      //   "Selected: " + ui.item.label :
      //   "Nothing selected, input was " + this.value);
    },
    open: function() {
      $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
    },
    close: function() {
      $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
    }
  });
</script>
@endsection
