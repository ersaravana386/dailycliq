@extends('vendors.layouts.web')
@section('content')
<div class="br-pagebody">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" integrity="sha256-rByPlHULObEjJ6XQxW/flG2r+22R5dKiAoef+aXWfik=" crossorigin="anonymous"/>
    <div class="container-fluid pt-2">
        <div class="row row-sm dashboard-summary list shadow-sm">
            <div class="card bg-white border-0 dashboard" id="card1">
                <div class="item">
                    <p>Add to Main categories</p>
                    <div class="pd-x-25 pd-t-25 d-flex align-items-center">
                        <ul class="list-group w-100 categorylist">
                            @foreach ($vendor_categories as $category)
                                <li class="list-group-item">{{$category->name}} 
                                    <span class="pull-right"><a href="{{route('showNewCatelog')."?category=".$category->id}}" class="btn btn-sm p-0" data-id="{{$category->id}}" data-listIndex="1"><i class="fa fa-plus"></i></a></span>
                                    @if($category->childrens->count()) 
                                        <span class="pull-right"><a href="javascript:;" class="btn btn-sm p-0 subListBtn" onclick="selectSubCateogry(this)" data-id="{{$category->id}}" data-listIndex="1"><i class="fa fa-list"></i></a></span>
                                    @endif    
                                </li>    
                            @endforeach                            
                        </ul>
                    </div><!-- pd-x-25 -->
                </div><!-- card -->
            </div>
        </div><!-- row -->
    </div>
</div><!-- br-pagebody -->
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
<script>
   var newProductUrl = "{{route('showNewCatelog')}}";
   function selectSubCateogry(self){
        $(self).closest('.categorylist').find('.list-group-item').removeClass('active');
        $(self).closest('.list-group-item').addClass('active');
        $(self).closest('.dashboard-summary').find($(self).closest('.card')).nextAll().remove();
        
        var id = $(self).data('id');
        var self = $(self); 
        $.ajax({
            type: "get",
            url: "{{route('getSubCategories')}}",
            data: {
                id
            },
            dataType: "json",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                if (data.status == 1) {
                    var listIndex = self.data('listindex');  
                    if(data.data.length > 0){
                        var loop = "";
                        var listIndex = self.closest('.dashboard-summary').find('.card').length + 1;
                        $.each(data.data,  function(key,value){
                            var subcatBtn = "";
                            if(value.subcategory_count){
                                subcatBtn = `<span><a href="javascript:;" class="btn btn-sm p-0 subListBtn" onclick="selectSubCateogry(this)" data-id="${value.id}" data-listIndex="${listIndex}"><i class="fa fa-list"></i></a></span>`
                            } 
                            loop += `
                                <li class="list-group-item">${value.name} 
                                    <span><a href="${newProductUrl+"?category="+value.id}" class="btn btn-sm p-0" data-id="${value.id}" data-listIndex="${listIndex}"><i class="fa fa-plus"></i></a></span>
                                    ${subcatBtn}
                                </li>   
                            `;
                        });
                        
                        var html = `
                            <div class="card bg-white border-0 dashboard" id="card${listIndex}">
                                <div class="item">
                                    <p>Subcategories under ${data.category}</p>
                                    <div class="pd-x-25 pd-t-25 d-flex align-items-center">
                                        <ul class="list-group w-100 categorylist">
                                            ${loop}                         
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        `;
                        $('.dashboard-summary').append(html);
                    }
                    
                } else {
                    var html = "";
                    $.each(data.errors, function(key, value) {
                        html += value + "<br/>";
                    });
                    $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                    setTimeout(function() {
                        $('.msgDiv').html(``);
                    }, 4000);
                }
            }
        });
    }
</script>
@endsection
