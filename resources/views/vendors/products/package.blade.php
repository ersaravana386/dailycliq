@extends('vendors.layouts.web')
@section('content')
<div class="br-pagebody">
  <div class="container-fluid pt-2">
    <div class="row row-sm justify-content-center new">
      <div class="col-12 col-sm-10 col-md-12 col-lg-12">
        <div class="card bg-white border-0 shadow-sm py-5 px-4">
          <div class="item">
            @include('vendors.layouts.progressbar')
            <form class="inset row" action="javascript:;" id="mainForm">
                <div class="col-lg-6 ">
                    <div class="form-group pd-5">
                      <label class="form-control-label" for="weight">Weight: <span class="tx-danger">*</span></label>
                      <input class="form-control form-control-sm" type="text" value='{{$product->weight}}' name="weight" id="weight"  placeholder="Enter Weight">
                    </div>
                  </div><!-- col-6 -->
                  <div class="col-lg-6 ">
                    <div class="form-group pd-5">
                      <label class="form-control-label" for="weight_unit">Weight Unit: <span class="tx-danger">*</span></label>
                      <select class="form-control form-control-sm select22" name="weight_unit" id="weight_unit">
                        @foreach ($weight_classes ?? '' as $weight_class)
                          <option @if($product->weight_class_id == $weight_class->id) checked @endif value="{{$weight_class->id}}">{{$weight_class->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div><!-- col-6 -->
                  <div class="col-lg-3">
                    <div class="form-group pd-5">
                      <label class="form-control-label" for="length">Length: <span class="tx-danger">*</span></label>
                      <input class="form-control form-control-sm" type="text" value='{{$product->length}}' name="length" id="length"  placeholder="Enter Length">
                    </div>
                  </div><!-- col-6 -->
                  <div class="col-lg-3">
                    <div class="form-group pd-5">
                      <label class="form-control-label" for="width">Width: <span class="tx-danger">*</span></label>
                      <input class="form-control form-control-sm" type="text" value='{{$product->width}}' name="width" id="width"  placeholder="Enter Width">
                    </div>
                  </div><!-- col-6 -->
                  <div class="col-lg-3">
                    <div class="form-group pd-5">
                      <label class="form-control-label" for="height">Height: <span class="tx-danger">*</span></label>
                      <input class="form-control form-control-sm" type="text" value='{{$product->height}}' name="height" id="height"  placeholder="Enter Height">
                    </div>
                  </div><!-- col-6 -->
                  <div class="col-lg-3">
                    <div class="form-group pd-5">
                      <label class="form-control-label" for="dimensions_unit">Dimensions Unit: <span class="tx-danger">*</span></label>
                      <select class="form-control form-control-sm select23" name="dimensions_unit" id="dimensions_unit">
                        @foreach ($weight_classes as $length_class)
                          <option  @if($product->length_class_id == $length_class->id) checked @endif  value="{{$length_class->id}}">{{$length_class->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div><!-- col-6 -->
                  <div class="col-lg-12">
                    <div class="form-group pd-5">
                      <label class="form-control-label" for="return_policy">Return & Refund Policy: <span class="tx-danger">*</span></label>
                      <textarea class="form-control form-control-sm"   name="return_policy" id="return_policy"  placeholder="Enter Return & Refund Policy" rows='5'>{{$product->return_policy}}</textarea>
                    </div>
                  </div><!-- col-6 -->
                  <div class="col-lg-12">
                    <div class="form-group pd-5">
                      <label class="form-control-label" for="warranty_details">Warranty Details: <span class="tx-danger">*</span></label>
                      <textarea class="form-control form-control-sm"   name="warranty_details" id="warranty_details"  placeholder="Enter Warranty Details" rows='5'>{{$product->warranty_details}}</textarea>
                    </div>
                  </div><!-- col-6 -->
                  <div class="col-lg-6 ">
                    <div class="form-group pd-5">
                      <label class="form-control-label" for="shipping_processing_time">Shipping processing time(Hours): <span class="tx-danger">*</span></label>
                      <select class="form-control form-control-sm select23"  name="shipping_processing_time" id="shipping_processing_time">
                        <option value="12" @if($product->shipping_processing_time == 12) selected @endif >12</optioin>
                        <option value="24" @if($product->shipping_processing_time == 24) selected @endif >24</optioin>
                        <option value="48" @if($product->shipping_processing_time == 48) selected @endif >48</optioin>
                      </select> 
                    </div>
                  </div><!-- col-6 -->
                  <div class="col-lg-6 ">
                    <div class="form-group pd-5">
                      <label class="form-control-label" for="free_delivery">Free delivery:</label>
                      <input @if($product->free_delivery) checked @endif type="checkbox" id="free_delivery" placeholder="Free delivery" name="free_delivery" data-toggle="toggle" data-size="sm" data-on="Yes" data-off="No">
                    </div>
                  </div><!-- col-6 -->
                <div class="col-sm-12 text-right">
                  <input class="btn btn-primary btn-sm button-inner" name="save" type="submit" value="Save"/>
                  <input class="btn btn-primary btn-sm button-inner" name="save" type="submit" value="Save & Continue" />
                  <a class="btn btn-secondary btn-sm" href="{{route('showNewPricingCatelog',['product'=>$product->id])}}">Back</a>
                </div>
            </form>
          </div><!-- card -->
        </div>
      </div>
    </div><!-- row -->
  </div>
</div><!-- br-pagebody -->
@endsection
@section('scripts')
<script>
    $('#catelogId').find('a').addClass('active');
    $('select[name="brand"]').on('change',function(){
      if($(this).val() == "other"){
        $('#brand_other_row').show();
      }else{
        $('#brand_other_row').hide();
        $('input[name="brand_other"]').val("");
      }
    });
    $.validator.addMethod("lettersndashes", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
    }, "Field must contain only letters, numbers, or dashes.");
</script>
<script>
      $(document).ready(function() {
         $('.select22').select2({
          placeholder: 'Please select ...',
          width: '100%',
          minimumResultsForSearch: -1,
        });
        $('.select23').select2({
          placeholder: 'Please select ...',
          width: '100%',
          minimumResultsForSearch: -1,
        });
      });
      $("#mainForm").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        rules: {
          weight:{
            required: true,
            number:true
          },
          weight_unit:{
              required:true,
              number:true
          },
          length:{
              required:function(element) {
                  return ($('#height').val()!='' || $('#width').val()!='');
               
              },
              number:true
          },
          width:{
            required:function(element) {
              return ($('#height').val()!='' || $('#length').val()!='');
              },
              number:true
          },
          height:{
            required:function(element) {
              return ($('#width').val()!='' || $('#length').val()!='');
              },
              number:true
          },
          dimensions_unit:{
            required:function(element) {
              
                if(($('#length').val()!='') || ($('#height').val()!='') || ($('#width').val()!='')){
                  return true;
                }
              },
              number:true
          },
          return_policy:{
            required:true,
          },
          shipping_processing_time:{
              required:true,
          },
        },
        messages: {
          weight:{
            required: "Weight is required",
            number: "Weight must be number"
          },
          weight_unit:{
              required: "Weight unit is required",
          },
          length:{
              required: "Length is required",
              number: "Length must be number"
          },
          width:{
              required: "Width is required",
              number:"Width must be number"
          },
          height:{
              required: "Height is required",
              number: "Height must be number"
          },
          dimensions_unit:{
              required:"Dimensions unit is required",
          },
          return_policy:{
            required:"Return & Refund Policy is required",
          },
          shipping_processing_time:{
            required:"Shipping processing time is required",
          },
        },
        submitHandler: function(form) {
          $('.msgDiv').html(``);
          var data = new FormData(form);
          $.ajax({
            type: "post",
            url: "{{route('saveNewCatelogPackageDetails',['product'=>$product->id])}}",
            data: data,
            dataType: "json",
            processData: false,
            contentType: false,
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
              if (data.status == 1) {
                  if(data.redirect){
                    window.location.href = data.redirect;
                  }else{
                     location.reload();
                  }
              } else {
                  var html = "";
                  $.each(data.errors, function(key, value) {
                      html += value + "<br/>";
                  });
                  $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                  setTimeout(function() {
                      $('.msgDiv').html(``);
                  }, 4000);
              }
            }
          });
          return false;
        }
    });
</script>
@endsection
