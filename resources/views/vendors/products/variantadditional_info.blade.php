@extends('vendors.layouts.web')
@section('content')
<div class="br-pagebody">
  <div class="container-fluid pt-2">
    <div class="row row-sm justify-content-center new">
      <div class="col-12 col-sm-10 col-md-12 col-lg-12">
        <div class="card bg-white border-0 shadow-sm py-5 px-4">
          <div class="item">
            @include('vendors.layouts.progressbar2')
            <form class="inset row" action="javascript:;" id="mainForm">
              <div class="col-lg-3">
                <div class="form-group pd-5">
                  <label class="form-control-label" for="name">Field Name: <span class="tx-danger">*</span></label>
                  <input class="form-control form-control-sm" type="text" value='' name="name" id="name" placeholder="Enter Name">
                </div>
              </div><!-- col-6 -->
              <div class="col-lg-6 ">
                <div class="form-group pd-5">
                  <label class="form-control-label" for="field_details">Details: <span class="tx-danger">*</span></label>
                  <textarea class="form-control form-control-sm" name="field_details" id="field_details" placeholder="Enter Field Details" rows="5">{{$product->value}}</textarea>
                </div>
              </div><!-- col-6 -->

              <div class="col-sm-12 text-left">
                <input class="btn btn-primary btn-sm button-inner" name="save" type="submit" value="Save" />
                <input class="btn btn-primary btn-sm button-inner" name="save" type="submit" value="Save & Continue" />
                <a class="btn btn-secondary btn-sm" href="{{route('showNewvariantCatelogImages',['product'=>$variant->variant,'variant'=>$variant->id])}}">Back</a>
              </div>
            </form>
          </div><!-- card -->
          <hr>
          <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table mg-b-0">
              <thead>
                <tr>
                  <th class="wd-5p">#</th>
                  <th class="wd-15p">Name</th>
                  <th class="wd-15p">Details</th>
                  <th class="wd-20p">Action</th>
                </tr>
              </thead>
              <tbody>
                @php
                $i= 0;
                @endphp
                @foreach ($additional_fields as $key=> $list)
                <tr>
                  <th scope="row">
                    @if($product->id == $variant->id)
                      {{++$i}}
                    @else
                      <input type="checkbox" name="selected_infos[]" value="{{$list->id}}">
                    @endif
                  </th>
                  <td>
                    {{$list->name}}
                  </td>
                  <td>
                    {{$list->value}}
                  </td>
                  <td>
                    <div class="btn-group" role="group" aria-label="Basic example">
                      @if($product->id == $variant->id)
                        <a onclick="return confirm('Are you sure you want to delete this additional information?');" href="{{route('deleteCatelogAdditionalInfoVariant',['additionalinfo'=>$list->id])}}" class=" btn btn-outline-teal" title="Delete Additional Information">
                          <i class="menu-item-icon fa fa-trash"></i>
                        </a>
                      @endif
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
              @if($product->id != $variant->id && count($additional_fields) > 0)
                <button class="btn btn-primary btn-sm button-inner" onclick="saveParentInfo();">Save Parent Details</button>
              @endif  
            </div>
          </div>
        </div>
      </div><!-- row -->
    </div>
  </div><!-- br-pagebody -->
  @endsection
  @section('scripts')
  <script>
    $.validator.addMethod("lettersndashes", function(value, element) {
      return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
    }, "Field must contain only letters, numbers, or dashes.");
  </script>
  <script>
    $("#mainForm").validate({
      normalizer: function(value) {
        return $.trim(value);
      },
      rules: {
          name:{
            required:function(element) {
              if(($('#field_details').val()!='')){
                return true;
              }
            },
          },
          field_details:{
              required:function(element) {
              if(($('#name').val()!='')){
                return true;
              }
            },
          }
        },
        messages: {
          name:{
            required: "Field Name is required",
          },
          field_details:{
              required: "Field Details is required",
          },
         
        },
      submitHandler: function(form) {
        $('.msgDiv').html(``);
        var data = new FormData(form);
        $.ajax({
          type: "post",
          url: "{{route('saveNewvariantCatelogAdditionalInfo',['product'=>$variant->variant,'variant'=>$variant->id])}}",
          data: data,
          dataType: "json",
          processData: false,
          contentType: false,
          cache: false,
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(data) {
            if (data.status == 1) {
              if (data.redirect) {
                window.location.href = data.redirect;
              }
              $('.msgDiv').html(` <div class="alert alert-success">` + data.message + `</div>`);
              setTimeout(function() {
                $('.msgDiv').html(``);
                location.href = "{{route('showCatelog')}}";
              }, 2000);
            }
            if (data.status == 0) {
              $('.msgDiv').html(` <div class="alert alert-success">` + data.message + `</div>`);
              setTimeout(function() {
                window.location.reload();
              }, 2000);
            } else {
              var html = "";
              $.each(data.errors, function(key, value) {
                html += value + "<br/>";
              });
              $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
              setTimeout(function() {
                $('.msgDiv').html(``);
              }, 4000);
            }
          }
        });
        return false;
      }
    });

    function saveParentInfo(){
            var infos = [];

            $('input[name="selected_infos[]"]:checked').each(function(key,value){     
              infos.push(value.value);
            });

            $.ajax({
                type: 'POST',
                url: "{{ route('saveNewvariantCatelogAdditionalInfoOld',['product'=>$variant->variant , 'variant'=>$variant->id])}}",
                data: {
                  infos
                },
                dataType: 'json',
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: (data) => {
                    if (data.status == true) {
                        window.location.reload();
                    } else {
                        var html = "";
                        $.each(data.errors, function(key, value) {
                            html += value + "<br/>";
                        });
                        $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                        }, 4000);
                    }
                }
            });
    }
  </script>
  @endsection