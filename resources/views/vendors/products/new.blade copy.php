@extends('vendors.layouts.web')
@section('content')
<div class="br-pagebody">
        <div class="container-fluid pt-2">
        <div class="row row-sm justify-content-center new">
          <div class="col-12 col-sm-10 col-md-12 col-lg-8">
            <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">List a new product</h4>
                        <div class="card bg-white border-0 shadow-sm py-5 px-4">
                          <div class="item">
                            <form class="inset">
                              <div class="form-group row">
                                <label for="product_name" class="col-sm-4 col-form-label">Product Name:</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                  <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Name your product">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="brand_name" class="col-sm-4 col-form-label">Product ID(DSIN):</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                  <input type="text" class="form-control" id="dsin" placeholder="DSIN" name="dsin">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="brand_name" class="col-sm-4 col-form-label">UPC:</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                  <input type="text" class="form-control" id="upc" placeholder="UPC" name="upc">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="brand_name" class="col-sm-4 col-form-label">EAN:</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                  <input type="text" class="form-control" id="ean" placeholder="EAN" name="ean">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="brand_name" class="col-sm-4 col-form-label">Brand Name:</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                  <select class="product-listing" name="brand_name" id="brand_name" rows='2' required>
                                    <option value="">select</option>
                                    @foreach($brands as $brand)
                                      <option value="{{$brand->id}}">{{$brand->name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="manufacturer" class="col-sm-4 col-form-label">Manufacturer:</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                  <select class="product-listing" name="manufacturer" id="manufacturer" rows='2' required>
                                    <option value="">select</option>
                                    @foreach($manufacturers as $manufacturer)
                                      <option value="{{$manufacturer->id}}">{{$manufacturer->name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="is_pathayapura_listing" class="col-sm-4 col-form-label">Pathayapura listing:</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                  <input type="checkbox" id="is_pathayapura_listing" placeholder="Pathayapura" name="pathayapura">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="material" class="col-sm-4 col-form-label">Material:</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                  <input type="text" class="form-control" id="material" placeholder="Material" name="material">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="manufacturer" class="col-sm-4 col-form-label">Warranty:</label>
                                <div class="col-sm-4 col-md-4 col-lg-4">
                                  <input type="number" class="form-control" id="warranty" placeholder="Warranty" name="warranty">
                                </div>
                                <div class="col-sm-2 col-md-2 col-lg-2">
                                  <select class="product-listing" name="warranty_period" id="warranty_period" rows='2' required>
                                    <option value="">Select</option>
                                    <option value="month">Months</option>
                                    <option value="year">Years</option>
                                  </select>
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="return_period" class="col-sm-4 col-form-label">Return Period(days):</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                  <input type="text" class="form-control" id="return_period" placeholder="Return Period" name="return_period">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="gst" class="col-sm-4 col-form-label">GST:</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                  <input type="text" class="form-control" id="gst" placeholder="GST" name="gst">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">Price of the product by the seller:</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                  <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text">INR</div>
                                    </div>
                                    <input type="text" class="form-control" id="price" placeholder="">
                                  </div>
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="recommended_price" class="col-sm-4 col-form-label"> Maximum Retail Price(inclusive VAT):</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                  <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text">INR</div>
                                    </div>
                                    <input type="text" class="form-control" id="recommended_price" placeholder="Max Retail Price">
                                  </div>
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="product_name" class="col-sm-4 col-form-label">Wholesale Price (inclusive VAT):</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                  <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text">INR</div>
                                    </div>
                                    <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Wholesale Price">
                                  </div>
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="condition" class="col-sm-4 col-form-label">Description:</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                  <textarea class="form-control" rows="4"></textarea>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="condition" class="col-sm-4 col-form-label">Weight Class:</label>
                                <div class="col-sm-4 col-md-4 col-lg-3 mb-4 mb-md-0">
                                  <select class="product-listing" name="category[]" id="weight" rows='2' required>
                                    <option value="">Select</option>
                                    <option value="1">Kg</option>
                                    <option value="1">Gm</option>
                                  </select>
                                </div>
                                <div class="col-sm-4 col-md-4 col-lg-3">
                                    <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Enter weight">
                                  </div>
                              </div>
                              <div class="form-group row">
                                <label for="condition" class="col-sm-4 col-form-label">Length Class:</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                  <select class="product-listing" name="category[]" id="length" rows='2' required>
                                    <option value="">Select</option>
                                    <option value="1">Meter</option>
                                    <option value="2">Centimeter</option>
                                    <option value="3">Millimeter</option>
                                  </select>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="condition" class="col-sm-4 col-form-label">Length:</label>
                                <div class="col-sm-2 mb-4 mb-md-0">
                                    <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Length">
                                </div>
                                <div class="col-sm-2 mb-4 mb-md-0">
                                    <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Width">
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Height">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="condition" class="col-sm-4 col-form-label">Color:</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                  <select class="product-listing" name="category[]" id="color" rows='2' multiple required>
                                    <option value="">Select</option>
                                    @foreach($colors as $color)
                                      <option value="{{$color->id}}">{{$color->name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="condition" class="col-sm-4 col-form-label">Size:</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                    <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Size">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="condition" class="col-sm-4 col-form-label">Condition:</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                  <select class="product-listing" name="category[]" id="condition" rows='2' required>
                                    <option value="">Select</option>
                                    <option value="1">New</option>
                                  </select>
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="condition" class="col-sm-4 col-form-label">Shipping - processing time:</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                    <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Shipping processing time">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="free_delivery" class="col-sm-4 col-form-label">Free delivery:</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                    <input type="checkbox" id="free_delivery" placeholder="Free delivery" name="free_delivery">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="condition" class="col-sm-4 col-form-label">FSSAI registration:</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                    <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="FSSAI registration">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="cod_available" class="col-sm-4 col-form-label">COD:</label>
                                <div class="col-sm-8 col-md-8 col-lg-6">
                                  <input type="checkbox" id="cod_available" placeholder="COD Available" name="cod">
                                </div>
                              </div>

                              <div class="form-group row mt-4 mb-0">
                                <div class="col-sm-10 text-right">
                                  <button class="btn btn-primary btn-sm button-inner" type="submit">
                                    Add Catalogue</button>
                                </div>
                              </div>
                            </form>
                            </div><!-- card -->
                          </div>
                        </div>
                      </div><!-- row -->
                    </div>
      </div><!-- br-pagebody -->

  <!-- add image modal -->
 <div class="modal fade effect-flip-horizontal " id="addImageModal">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content bd-0"> 
      <div class="modal-body pd-0">
      <form action="javascript:;" id="manufacturerForm">
        <div class="row flex-row-reverse no-gutters">
          <div class="col-lg-12 rounded-left">
            <div class="pd-40">
              <h2 class="mg-b-20 tx-inverse lh-2 tx-uppercase"><br></h2>
              <p class="tx-12 mg-b-10">
                <span>Manufacturer:</span>
                <span class="tx-uppercase tx-12 tx-bold d-block mg-b-20"></span>
                <input type="text" name="manufacturer" id="manufacturer" class="form-control">
              </p>
              <p class="tx-12 mg-b-10"> 
                <span>image:</span>
                <span class="tx-uppercase tx-12 tx-bold d-block mg-b-20"></span>
                <input type="file" name="image" id="image" class="form-control">
              </p>
              <button type="submit" class="btn btn-primary tx-11  pd-y-12 pd-x-25 tx-mont tx-medium">ADD</button>
              <button type="button" class="btn btn-secondary tx-11  pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
            </div>
            </div><!-- col-6 -->
          </div><!-- row -->
        </form>
        </div><!-- modal-body -->
        <div class="modal-footer">
      </div>
      </div><!-- modal-content -->
    </div><!-- modal-dialog -->
  </div><!-- modal -->
@endsection
@section('scripts')
<script>
    $('#catelogId').find('a').addClass('active');
</script>
<script>
      $(document).ready(function() {
         $('.product-listing').select2({
          placeholder: 'Please select ...',
          width: '100%',
          minimumResultsForSearch: -1
        });
      });
      function addNewManufacturer(){
        $('#addImageModal').modal('toggle');
      }
      $("#manufacturerForm").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        rules: {
            manufacturer: {
              required: true
            },
            image:{
              required: true
            }
        },
        messages: {
            manufacturer: {
              required: "Manufacturer name is required"
            },
            image:{
              required: "Image is required"
            }
        },
        submitHandler: function(form) {
          $('.msgDiv').html(``);
            var data = new FormData(form);
            $.ajax({
                type: "post",
                url: "{{route('newManufacturer')}}",
                data: data,
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.status == 1) {
                        $('.msgDiv').html(` <div class="alert alert-success">` + data.message + `</div>`);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                            location.href = "{{route('showManufacturers')}}";
                        }, 2000);

                    } else {
                        var html = "";
                        $.each(data.errors, function(key, value) {
                            html += value + "<br/>";
                        });
                        $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                        }, 4000);
                    }
                }
            });
            return false;
        }
    });
</script>
@endsection
