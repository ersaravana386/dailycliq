@extends('vendors.layouts.web')
@section('content')
<div class="br-pagebody">
  <div class="container-fluid pt-2">
    <div class="row row-sm justify-content-center new">
      <div class="col-12 col-sm-10 col-md-12 col-lg-12">
        <div class="card bg-white border-0 shadow-sm py-5 px-4">
          <div class="item">
            @include('vendors.layouts.progressbar2')
            <form class="inset" action="javascript:;" id="mainForm">              
              <div class="form-group row">
                <label for="gst" class="col-sm-4 col-form-label">GST(%):</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                <input type="number" class="form-control form-control-sm" id="gst" name="gst" placeholder="GST" value="{{$product->gst}}">
                    <label id="gst-error" class="error" for="gst" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="sess" class="col-sm-4 col-form-label">CESS:</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <input type="checkbox" name="sess" id="sess" @if($product->sess) checked @endif>
                  <br/>
                  <label id="sess-error" class="error" for="sess" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="price" class="col-sm-4 col-form-label">Seller Price:</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                <input type="number" class="form-control form-control-sm" id="price" name="price" placeholder="Seller price" value="{{$product->price}}">
                  <label id="price-error" class="error" for="price" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="mrp" class="col-sm-4 col-form-label">Maximum Retail Price(inclusive GST):</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <input type="number" class="form-control form-control-sm" id="mrp" name="mrp" placeholder="Maximum Retail Price" value="{{$product->mrp}}">
                  <label id="mrp-error" class="error" for="mrp" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="wholesale" class="col-sm-4 col-form-label">Wholesale Price (inclusive GST):</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <input type="number" class="form-control form-control-sm" id="wholesale" name="wholesale" placeholder="Wholesale Price" value="{{$product->wholesale}}">
                  <label id="wholesale-error" class="error" for="wholesale" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="wholesale" class="col-sm-4 col-form-label">MOQ(minimum order quantity):</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <input type="number" class="form-control form-control-sm" id="order_quantity" name="order_quantity" placeholder="Minimum Wholesale Unit" value="{{$product->order_quantity}}">
                  <label id="order_quantity-error" class="error" for="order_quantity" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="wholesale" class="col-sm-4 col-form-label">Commission(%):</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <input type="number" class="form-control form-control-sm" id="commission" name="commission" placeholder="Commission" value="{{$product->commission}}">
                  <label id="order_quantity-error" class="error" for="commission" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="cod" class="col-sm-4 col-form-label">COD Eligible:</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <input type="checkbox" name="cod" id="cod" @if($product->cod) checked @endif>
                  <br/>
                  <label id="cod-error" class="error" for="cod" style="display:none"></label>
                </div>
              </div>
              
              <div class="form-group row mt-4 mb-0">
                <div class="col-sm-10 text-right">
                  <input class="btn btn-primary btn-sm button-inner" name="save" type="submit" value="Save"/>
                  <input class="btn btn-primary btn-sm button-inner" name="save" type="submit" value="Save & Continue" />
                  <a class="btn btn-secondary btn-sm" href="{{route('showNewCatelognewvarient',['product'=>$product->id])}}?variant={{$variant->id}}">Back</a>
                </div>
              </div>
            </form>
          </div><!-- card -->
        </div>
      </div>
    </div><!-- row -->
  </div>
</div><!-- br-pagebody -->
@endsection
@section('scripts')
<script>
    $('#catelogId').find('a').addClass('active');
    $('select[name="brand"]').on('change',function(){
      if($(this).val() == "other"){
        $('#brand_other_row').show();
      }else{
        $('#brand_other_row').hide();
        $('input[name="brand_other"]').val("");
      }
    });
    $.validator.addMethod("lettersndashes", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
    }, "Field must contain only letters, numbers, or dashes.");
</script>
<script>
      $(document).ready(function() {
         $('.select22').select2({
          placeholder: 'Please select ...',
          width: '100%',
          minimumResultsForSearch: -1,
        });
        $('.select23').select2({
          placeholder: 'Please select ...',
          width: '100%',
          minimumResultsForSearch: -1,
        });
      });
      $("#mainForm").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        rules: {
            // gst:{
            //     required: true,
            //     number:true
            // },
            price:{
                required:true,
                number:true
            },
            mrp:{
                required:true,
                number:true
            },
            wholesale:{
                required:true,
                number:true
            },
            order_quantity:{
                required:true,
                number:true
            },
            commission:{
                required:true,
                number:true
            }
        },
        messages: {
            // gst:{
            //     required: "GST percentage is required",
            //     number: "GST is required"
            // },
            price:{
                required:"Price is required",
                number:"Price should be a number"
            },
            mrp:{
                required:"MRP is required",
                number:"MRP should be a number"
            },
            wholesale:{
                required:"Wholesale price is required",
                number:"Wholesale price should be a number"
            },
            order_quantity:{
                required:"minimum order quantity unit is required",
                number: "minimum order quantity unit should be a number"
            },
            commission:{
                required:"Commission is required",
                number: "Commission should be a number"
            }
        },
        submitHandler: function(form) {
          $('.msgDiv').html(``);
          var data = new FormData(form);
          $.ajax({
            type: "post",
            url: "{{route('saveNewvariantCatelogPricing',['product'=>$product->id,'variant'=>$variant->id])}}",
            data: data,
            dataType: "json",
            processData: false,
            contentType: false,
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
              if (data.status == 1) {
                  if(data.redirect){
                    window.location.href = data.redirect;
                  }else{
                    location.reload();
                  }
              } else {
                  var html = "";
                  $.each(data.errors, function(key, value) {
                      html += value + "<br/>";
                  });
                  $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                  setTimeout(function() {
                      $('.msgDiv').html(``);
                  }, 4000);
              }
            }
          });
          return false;
        }
    });
</script>
@endsection
