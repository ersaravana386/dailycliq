@extends('vendors.layouts.web')
@section('content')
<div class="br-pagebody">
  <div class="container-fluid pt-2">
    <div class="row row-sm justify-content-center new">
      <div class="col-12 col-sm-10 col-md-12 col-lg-12">
        <div class="card bg-white border-0 shadow-sm py-5 px-4">
          <div class="item">
            @include('vendors.layouts.progressbar2')
            <form class="inset" action="javascript:;" id="mainForm">
              <div class="form-group row">
                <label for="dcin" class="col-sm-4 col-form-label">Product Code (DCIN):</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <input type="hidden" name="category" value="{{$category->id}}">
                  <input type="hidden" name="product_id" value="{{$product->id}}">
                  <input type="hidden" name="variant_id" value="{{$variant->id}}">
                                   
                  <input readonly type="text" value="DCIN-{{time().rand(100,999)}}" class="form-control form-control-sm" id="dcin" name="dcin" placeholder="Product Code (DCIN)">
                  
                  <label id="dcin-error" class="error" for="dcin" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="dcin" class="col-sm-4 col-form-label">Product Name:</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <input type="text" value="{{$product->name}}" class="form-control form-control-sm" id="name" name="name" placeholder="Product Name">
                  <label id="name-error" class="error" for="name" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="ean" class="col-sm-4 col-form-label">EAN:</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <input type="text" class="form-control form-control-sm" id="ean" name="ean" placeholder="EAN" value="{{$product->ean}}">
                  <label id="ean-error" class="error" for="ean" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="ean" class="col-sm-4 col-form-label">HSN:</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <input type="text" class="form-control form-control-sm" id="hsn" name="hsn" placeholder="HSN" value="{{$product->hsn}}">
                  <label id="hsn-error" class="error" for="hsn" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="upc" class="col-sm-4 col-form-label">UPC:</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <input type="text" class="form-control form-control-sm" id="upc" name="upc" placeholder="UPC" value="{{$product->upc}}"> 
                  <label id="upc-error" class="error" for="upc" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="brand" class="col-sm-4 col-form-label">Brand:</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <select class="form-control select23" name="brand" id="brand">
                  <option></option>
                    <option value="other"  @if($product->other_brand!='') selected @endif >Other</option>
                  </select>
                  <label id="brand-error" class="error" for="brand" style="display:none"></label>
                </div>
              </div>
              <div id="brand_other_row" class="form-group row"   @if($product->other_brand=='') style="display:none;" @endif>
                <label for="brand_other" class="col-sm-4 col-form-label">Brand Name:</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <input type="text" class="form-control form-control-sm" id="brand_other" name="brand_other" value="{{$product->other_brand}}" placeholder="Brand Name">
                  <label id="brand_other-error" class="error" for="brand_other" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="upc" class="col-sm-4 col-form-label">Description:</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <textarea class="form-control form-control-sm" id="description" name="description" placeholder="Description" rows="5">{{$product->description}}</textarea>
                  <label id="description-error" class="error" for="upc" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="pathayapura" class="col-sm-4 col-form-label">Pathayapura Listing:</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <input type="checkbox" name="pathayapura_listing" id="pathayapura" @if($product->pathayapura_listing==1) checked @endif >
                  <br/>
                  <label id="pathayapura_listing-error" class="error" for="pathayapura_listing" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row town-list" @if($product->pathayapura_listing==0) style="display:none;" @endif> 
                <label for="brand" class="col-sm-4 col-form-label" >Town:</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <select class="form-control select22" name="town" id="town">
                  <option></option>
                      @foreach($town as $value)
                        @php
                          $selected="";
                              if($product->town == $value['id']){
                                $selected = "selected";
                              }
                        @endphp
                        <option {{$selected}} value="{{$value['id']}}">{{$value['name']}}</option>
                      @endforeach
                  </select>
                  <label id="town-error" class="error" for="town" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="assured" class="col-sm-4 col-form-label">Assured:</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <input type="checkbox" name="assured" id="assured" @if($product->assured==1) checked @endif >
                  <br/>
                  <label id="assured-error" class="error" for="assured" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="featured_product" class="col-sm-4 col-form-label">Featured Product:</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <input type="checkbox" name="featured_product" id="featured_product" @if($product->featured_product==1) checked @endif >
                  <br/>
                  <label id="featured_product-error" class="error" for="featured_product" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="featured_product" class="col-sm-4 col-form-label">Gift Ideals:</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <select class="form-control select2" data-placeholder="Please select.." name="gifts[]" multiple>
                  <option></option>
                    @foreach($gift as $value)
                        @php
                        if(isset($productgift) && sizeof($productgift) > 0){
                          $selected="";
                          foreach($productgift as $key=> $values){
                            if($values['tmp_gift_id'] == $value['id']){
                              $selected = "selected";
                            }
                          }
                        }
                        @endphp
                        <option {{$selected}} value="{{$value['id']}}">{{$value['name']}}</option>
                      @endforeach
                </select>
                  <label id="featured_product-error" class="error" for="featured_product" style="display:none"></label>
                </div>
              </div>
              @foreach($properties as $key => $attr_property)
                <div class="form-group row">
                  <label for="{{$key}}" class="col-sm-4 col-form-label text-capitalize">{{$key}}:</label>
                  <div class="col-sm-8 col-md-8 col-lg-6">
                    <select class="form-control select22" name="attr[{{$key}}]" id="{{$key}}">
                      <option></option>
                      @foreach($attr_property['values'] as $value)
                        @php 
                          $selected = "";
                          if(isset($attributes) && sizeof($attributes) > 0){
                            if(isset($attributes[$key]) && $attributes[$key] == $value){
                              $selected = "selected";
                            }
                          }
                        @endphp
                        <option {{$selected}} value="{{$value}}">{{$value}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              @endforeach
              <div class="form-group row">
                <label for="freequently_bought_together" class="col-sm-4 col-form-label">Freequenty Bought Together:(Up to 3 options only)</label>
                <div class="col-sm-8 col-md-8 col-lg-6">
                  <select class="form-control select2 select24" name="freequently_bought_together[]" id="freequently_bought_together" onchange="maxAllowedMultiselect(this, 2)" multiple>
                    @foreach($productlist as $value)
                        @php
                        if(isset($freequently_bought_together) && sizeof($freequently_bought_together) > 0){
                          $selected="";
                          foreach($freequently_bought_together as $key=> $values){
                            if($values['tmp_product_2'] == $value['id']){
                              $selected = "selected";
                            }else  if($values['tmp_product_3'] == $value['id']){
                              $selected = "selected";
                            }
                          }
                        }
                        @endphp
                        <option {{$selected}} value="{{$value['id']}}">{{$value['name']}}</option>
                      @endforeach
                  </select>
                  <label id="freequently_bought_together-error" class="error" for="freequently_bought_together" style="display:none"></label>
                </div>
              </div>
              <div class="form-group row mt-4 mb-0">
                <div class="col-sm-10 text-right">
                  <input class="btn btn-primary btn-sm button-inner" name="save" type="submit" value="Save"/>
                  <input class="btn btn-primary btn-sm button-inner" name="save" type="submit" value="Save & Continue" />
                  <a class="btn btn-secondary btn-sm" href="{{ route('showVendorProductList') }}">Back</a>
                </div>
              </div>
            </form>
          </div><!-- card -->
        </div>
      </div>
    </div><!-- row -->
  </div>
</div><!-- br-pagebody -->
@php 
    $brandid = isset($product->brand->id)?$product->brand->id:"";                    
@endphp
@endsection
@section('scripts')
<script>
    var brandid = '{{$brandid}}';
    $('#catelogId').find('a').addClass('active');
    $('select[name="brand"]').on('change',function(){
      if($(this).val() == "other"){
        $('#brand_other_row').show();
      }else{
        $('#brand_other_row').hide();
        $('input[name="brand_other"]').val("");
      }
    });
    $.validator.addMethod("lettersndashes", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
    }, "Field must contain only letters, numbers, or dashes.");
</script>
<script>
      $(document).ready(function() {
         $('.select22').select2({
          placeholder: 'Please select ...',
          width: '100%',
          minimumResultsForSearch: -1,
        });
                //check whether pathayapura is checked
        $('#pathayapura').click(function(){
            if($(this).prop("checked") == true){
                $('.town-list').show();
            }
            else if($(this).prop("checked") == false){
              $('.town-list').hide();
            }
        });
        //end checking
        $('.select23').select2({
          placeholder: 'Please select ...',
          width: '100%',
          minimumInputLength: 1, 
          ajax: {
            url: '{{route("searchBrand")}}',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            processResults: function (data) {
              // Transforms the top-level key of the response object from 'items' to 'results'
              return {
                results: data.items
              };
            }
          }
        }).on('change', function() {
            $(this).valid();
        });        

        if(brandid){
          // Fetch the preselected item, and add to the control
          var select23 = $('.select23');
          $.ajax({
            url: '{{route("searchBrandById")}}',
            dataType: 'json',
            data:{
              'id':brandid
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          }).then(function (data) {
              // create the option and append to Select2
              if(data.status){
                var option = new Option(data.item.text, data.item.id, true, true);
                select23.append(option).trigger('change');

                // manually trigger the `select2:select` event
                select23.trigger({
                    type: 'select2:select',
                    params: {
                        data: data.item
                    }
                });
              }
          });
        }

      });
      $("#mainForm").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        rules: {
          name: {
            required: true
          },
          dcin: {
            required: true
          },
          ean:{
            lettersndashes:true
          },
          upc:{
            lettersndashes:true
          },
          description:{
            required: true
          },
          brand:{
            required: true
          },
          brand_other:{
            required: function(element){
              return $("#brand").val()=="other";
            }
          },
          town:{
            required: function(element){
              var flag =false;
              if($('#pathayapura').is(":checked")){
                flag =true;
              }
              return flag;
            }
          },
        },
        messages: {
          name: {
            required: "Product Name is required"
          },
          dcin: {
            required: "Product code is required"
          },
          ean:{
            lettersndashes: "EAN should contain letters numbers and dashes"
          },
          upc:{
            lettersndashes: "UPC should contain letters numbers and dashes"
          },
          description:{
            required: "Product description is required"
          },
          brand:{
            required: "Brand is required"
          },
          brand_other:{
            required: "Brand name is required"
          },
          town:{
            required: "Town name is required"
          }
        },
        submitHandler: function(form) {
          $('.msgDiv').html(``);
          var data = new FormData(form);
          $.ajax({
            type: "post",
            url: "{{route('saveNewvariantCatelog')}}",
            data: data,
            dataType: "json",
            processData: false,
            contentType: false,
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
              if (data.status == 1) {
                  if(data.redirect){
                    window.location.href = data.redirect;
                  }else{
                    window.location.href = "{{route('showVendorProductList')}}";
                  }
              } else {
                  var html = "";
                  $.each(data.errors, function(key, value) {
                      html += value + "<br/>";
                  });
                  $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                  setTimeout(function() {
                      $('.msgDiv').html(``);
                  }, 4000);
              }
            }
          });
          return false;
        }
    });
</script>
<script>
function maxAllowedMultiselect(obj, maxAllowedCount) {
            var selectedOptions = jQuery('#'+obj.id+" option[value!=\'\']:selected");
            if (selectedOptions.length >= maxAllowedCount) {
                if (selectedOptions.length > maxAllowedCount) {
                    selectedOptions.each(function(i) {
                        if (i >= maxAllowedCount) {
                            jQuery(this).prop("selected",false);
                        }
                    });
                }
                jQuery('#'+obj.id+' option[value!=\'\']').not(':selected').prop("disabled",true);
            } else {
                jQuery('#'+obj.id+' option[value!=\'\']').prop("disabled",false);
            }
        }
</script>
@endsection
