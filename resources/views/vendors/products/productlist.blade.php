@extends('vendors.layouts.web')
@section('content')
<div class="br-pagebody">
    <div class="container-fluid pt-2">
        <div class="row row-sm justify-content-center new">
            <div class="col-12 col-sm-10 col-md-12 col-lg-12">
                <!-- <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">Vendor Product Listing</h4> -->
                <div class="card bg-white border-0 shadow-sm py-5 px-4">
                    <div class="bd bd-gray-300 rounded table-responsive">
                        <table class="table mg-b-0">
                            <thead>
                                <tr>
                                    <th class="wd-5p">#</th>
                                    <th class="wd-15p">NAME</th>
                                    <th class="wd-15p">DCIN</th>
                                    <th class="wd-15p">Image</th>
                                    <th class="wd-15p">PRICE</th>
                                    <th class="wd-15p">MRP</th>
                                    <th class="wd-15p">BRAND</th>
                                    <th class="wd-15p">CATEGORY</th>
                                    <th class="wd-15p">COMMENTS</th>
                                    <th class="wd-20p">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $i= 0;
                                @endphp
                                @if(count($products)>0)
                                @foreach ($products as $key=> $item)
                                <tr>
                                    <td scope="row">{{++$i}}</td>
                                    <td scope="row">
                                        {{$item->name}} <br />
                                        @if(!$item->approved)
                                        @if($item->completed_status == 0)
                                        <label class="badge badge-warning">Incomplete</label>
                                        @else
                                        <label class="badge badge-success">Waiting for approval</label>
                                        @endif
                                        @endif

                                        @if($item->approved == 2)
                                        <label class="badge badge-danger">Rejected</label>
                                        @elseif($item->approved == 3)
                                        <label class="badge badge-warning">Re-review</label>
                                        @endif

                                    </td>
                                    <td scope="row">{{$item->dcin}}</td>
                                    <td>
                                        @if($item->images->first())
                                        @if(file_exists($item->images->first()->image))
                                        <a target="_blank" href="{{asset($item->images->first()->image)}}"><img src="{{asset($item->images->first()->image)}}" style="width:80px;"></a>
                                        @endif
                                        @endif
                                    </td>
                                    <td scope="row">{{$item->price}}</td>
                                    <td scope="row">{{$item->mrp}}</td>
                                    <td scope="row">
                                        @if($item->brand)
                                        {{$item->brand->name}}
                                        @else
                                        {{$item->other_brand}}
                                        @endif
                                    </td>
                                    <td scope="row">{{$item->category->name}}</td>
                                    <td scope="row">
                                        {{$item->admin_comments}}
                                    </td>
                                    <td scope="row">
                                        <span>
                                            <a class="btn btn-sm btn-success" href="{{route('showNewCatelog')}}?product={{$item->id}}" title="edit">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </span>
                                        @if($item->approved != 2 && $item->approved != 3 && $item->completed_status == 1 && $item->id == $item->variant)
                                            <span>
                                                <a class="btn btn-sm btn-success" href="{{route('showNewCatelognewvarient',['product'=>$item->id])}}" title="add variant">
                                                    <i class="fa fa-copy"></i>
                                                </a>
                                            </span>
                                        @endif
                                        <span>
                                            <a class="btn btn-sm btn-success" onclick="return confirm('Are you sure you want to delete this product?');"  href="{{route('deleteCatelog',['product'=>$item->id])}}" title="delete product">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </span>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td scope="row" colspan=10><b><center><span style="color:#DC3545;">Sorry No Result Found</span></center><b></td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        @if(count($products)>0)
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                            {{ $products->appends(['search' => $search])->links() }}
                        </div>
                        @endif
                    </div>
                </div>
                <!-- bd -->
            </div>
        </div>
    </div><!-- row -->
</div>
</div><!-- br-pagebody -->
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function(e) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
</script>
@endsection