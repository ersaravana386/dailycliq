<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Daily CliQ">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Daily CliQ">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels"> --}}

    <title>Daily CliQ</title>

    <!-- vendor css -->
    <link href="{{asset('app/lib/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('app/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{asset('app/css/bracket.css')}}">
    <link rel="stylesheet" href="{{asset('app/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('app/lib/select2/css/select2.css')}}">
</head>

<body>
    <div id="overlay"></div>
    <div id="loader-wrapper" class="loading-div d-flex flex-column justify-content-center align-items-center" style="display:none!important">
        <p>Loading...</p>
        <div class="lds-ring">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>



    <div class="row no-gutters flex-row-reverse h-100-vh">
        <div class="col-md-12 bg-gray-200 d-flex align-items-center justify-content-center has-background-image login z-index-1">
            <div class="has-shade login-right"></div>
            <form class="registration position-relative" role="form" method="post" action="javascript:;" id="signUpForm">
                {{-- <div class="close-button">
                        <a href="{{route('vendorLogout')}}"><p class="mb-0">close</p></a>
        </div> --}}
        <div id="wizard2" class="pd-20">
            <h3>Company Information</h3>
            <section>
                <div class="form-group wd-xs-400">
                    <label class="form-control-label">Legal Company Name: <span class="tx-danger">*</span></label>
                    <input class="form-control" name="company_name" id="company_name" placeholder="Enter company name" type="text" required>
                </div><!-- form-group -->
                <div class="form-group wd-xs-400">
                    <label class="form-control-label">Phone Number: </label>
                    <input class="form-control" type="text" name="phone" id="phone" placeholder="Enter phone" data-parsley-error-message="Invalid phone number" data-parsley-type="number" data-parsley-minlength="10" data-parsley-maxlength="12">
                </div><!-- form-group -->
                <div class="form-group wd-xs-400">
                    <label class="fs-field-label" for="account_type">State</label>
                    <select class="js-example-basic-multiple" name="state" id="state" required data-parsley-errors-container="#checkbox-errors-1">
                        <option value="">Choose state</option>
                        @foreach ($states as $state )
                        <option value="{{$state->id}}">{{$state->name}}</option>
                        @endforeach
                    </select>
                    <div id="checkbox-errors-1"></div>

                </div>
                <div class="form-group wd-xs-400">
                    <label class="fs-field-label" for="account_type">District</label>
                    <select class="js-example-basic-multiple" name="district" id="district" required data-parsley-errors-container="#checkbox-errors-2">
                    </select>
                    <div id="checkbox-errors-2"></div>
                </div>

                <div class="form-group wd-xs-400">
                    <label class="form-control-label">Location: <span class="tx-danger">*</span></label>
                    <input class="form-control" id="location" name="location" type="text" placeholder="Kochi" required />
                </div>

                <div class="form-group wd-xs-400">
                    <label class="fs-field-label" for="business_type">Business Type</label>
                    <select class="js-example-basic-multiple" name="business_type" id="" required data-parsley-errors-container="#checkbox-errors-1">
                        @foreach($business_types as $business)
                        <option value="{{$business->id}}">{{$business->name}}</option>
                        @endforeach
                    </select>
                    <div id="checkbox-errors-1"></div>

                </div>
            </section>

            <h3>Store Information</h3>
            <section>
                <div class="form-group wd-xs-400">
                    <label class="form-control-label">Your Store Name: <span class="tx-danger">*</span></label>
                    <input class="form-control" id="store_name" name="store_name" type="text" placeholder="My Store" required />
                </div><!-- form-group -->
                <div class="selling_category">
                    <div class="form-group wd-xs-400">
                        <label class="fs-field-label" for="account_type">Your Selling Categories</label>
                        <select class="js-example-basic-multiple" name="category[]" id="category" multiple required data-parsley-errors-container="#checkbox-errors-3">
                            @foreach ($categories as $cats )
                            <option value="{{$cats->id}}">{{$cats->name}}</option>
                            @endforeach
                        </select>
                        <div id="checkbox-errors-3"></div>
                    </div>
                    <div class="form-group wd-xs-400">
                        <label class="form-control-label">Profile Picture: </label>
                        <div class="custom-file">
                            <input type="file" id="profile_picture" class="custom-file-input" name="profile_picture" data-parsley-fileextension='jpg,png,JPG' data-parsley-fileextension-message="Please choose image format">
                            <label class="custom-file-label" id="profile_picture_label">Choose a file</label>
                        </div>
                    </div>

                </div>
            </section>

            <h3>Business Details</h3>
            <section>
                <div class="form-group wd-xs-400">
                    <label class="form-control-label">Trade license document: <span class="tx-danger">*</span></label>
                    <div class="custom-file">
                        <input type="file" id="trade_license_document" class="custom-file-input" name="trade_license_document" required data-parsley-fileextension='jpg,png,JPG,docx,pdf,doc' data-parsley-fileextension-message="Incorrect file format">
                        <label class="custom-file-label" id="trade_license_document_label">Choose a file</label>
                    </div>
                </div>
                <div class="form-group wd-xs-400">
                    <label class="form-control-label">GST Number:</label>
                    <input class="form-control" type="text" name="gst_number" id="gst_number" placeholder="GST" data-parsley-gst data-parsley-gst-message="Invalid GST number" data-parsley-minlength="15" data-parsley-maxlength="15" style="text-transform:uppercase">
                </div>
                <div class="form-group wd-xs-400">
                    <label class="form-control-label">GST document:</label>
                    <div class="custom-file">
                        <input type="file" id="gst_document" class="custom-file-input" name="gst_document" data-parsley-fileextension='jpg,png,JPG,docx,pdf,doc' data-parsley-fileextension-message="Incorrect file format">
                        <label class="custom-file-label" id="gst_document_label">Choose a file</label>
                    </div>
                </div>
                <div class="form-group wd-xs-400">
                    <label class="form-control-label">Pan card: <span class="tx-danger">*</span></label>
                    <div class="custom-file">
                        <input type="file" id="pancard" class="custom-file-input" name="pancard" required data-parsley-fileextension='jpg,png,JPG,docx,pdf,doc' data-parsley-fileextension-message="Incorrect file format">
                        <label class="custom-file-label" id="pancard_label">Choose a file</label>
                    </div>
                </div>
                <div class="form-group wd-xs-400">
                    <label class="form-control-label">ID Proof: <span class="tx-danger">*</span></label>
                    <div class="custom-file">
                        <input type="file" id="id_proof" class="custom-file-input" name="id_proof" required data-parsley-fileextension='jpg,png,JPG,docx,pdf,doc' data-parsley-fileextension-message="Incorrect file format">
                        <label class="custom-file-label" id="id_proof_label">Choose a file</label>
                    </div>
                </div>
            </section>

            <h3>Bank Details</h3>
            <section>
                <div class="form-group wd-xs-400">
                    <label class="fs-field-label fs-anim-upper" for="name_as_in_bank">Your name in bank documents?</label>
                    <input class="form-control" id="name_as_in_bank" name="name_as_in_bank" type="text" placeholder="Santi K Cazorla" required />
                </div>
                <div class="form-group wd-xs-400">
                    <label class="fs-field-label" for="account_type">Your bank account type</label>
                    <select class="js-example-basic-multiple" name="account_type" id="account_type">
                        <option value="Savings">Savings</option>
                        <option value="Current">Current</option>
                    </select>
                </div>
                <div class="form-group wd-xs-400">
                    <label class="fs-field-label fs-anim-upper" for="account_number">Your bank account number?</label>
                    <input class="form-control" id="account_number" name="account_number" data-parsley-type="number" data-parsley-minlength="10" data-parsley-maxlength="16" data-parsley-error-message="Invalid account number" type="text" placeholder="0123456789" required />
                </div>
                <div class="form-group wd-xs-400">
                    <label class="fs-field-label fs-anim-upper" for="ifsc_code">Your bank IFSC code?</label>
                    <input class="form-control" id="ifsc_code" name="ifsc_code" type="text" placeholder="0123456789" data-parsley-type="alphanum" data-parsley-minlength="6" data-parsley-maxlength="11" required data-parsley-error-message="Invalid IFSC code" data-parsley-ifsc data-parsley-ifsc-message="Invalid IFSC code" style="text-transform:uppercase" />
                </div>
                <div class="form-group wd-xs-400">
                    <label class="form-control-label">Cancelled cheque: <span class="tx-danger">*</span></label>
                    <div class="custom-file">
                        <input type="file" id="cancel_cheque" class="custom-file-input" name="cancel_cheque" required data-parsley-fileextension='jpg,png,JPG,docx,pdf,doc' data-parsley-fileextension-message="Incorrect file format">
                        <label class="custom-file-label" id="cancel_cheque_label">Choose a file</label>
                    </div>
                </div>
            </section>

            <div class="msgDiv mt-3"></div>
        </div>

        </form>

    </div><!-- col -->



    </div><!-- row -->

    <!-- invitation modal -->
    <div id="inviteModal" class="modal fade effect-scale">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="col-lg-12 rounded-left">
                <div class="col-md-12 col-xl-12 mg-t-30 mg-xl-t-0">
                    <div class="d-flex ht-300 pos-relative align-items-center">
                        <div class="sk-wave">
                            <div class="sk-rect sk-rect1 bg-gray-800"></div>
                            <div class="sk-rect sk-rect2 bg-gray-800"></div>
                            <div class="sk-rect sk-rect3 bg-gray-800"></div>
                            <div class="sk-rect sk-rect4 bg-gray-800"></div>
                            <div class="sk-rect sk-rect5 bg-gray-800"></div>
                        </div>
                    </div><!-- d-flex -->
                </div><!-- col-4 -->

            </div><!-- row -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->


    {{-- modal susccess --}}


    <script src="{{asset('app/lib/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('app/lib/jquery-ui/ui/widgets/datepicker.js')}}"></script>
    <script src="{{asset('app/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('app/lib/peity/jquery.peity.min.js')}}"></script>
    <script src="{{asset('app/lib/highlightjs/highlight.pack.min.js')}}"></script>
    <script src="{{asset('app/lib/jquery-steps/build/jquery.steps.min.js')}}"></script>
    <script src="{{asset('app/lib/select2/js/select2.full.min.js')}}"></script>
    <script src="{{asset('app/lib/parsleyjs/parsley.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate.js')}}"></script>
    <script>
        function checksum(g) {
            let a = 65,
                b = 55,
                c = 36;
            return Array['from'](g).reduce((i, j, k, g) => {
                p = (p = (j.charCodeAt(0) < a ? parseInt(j) : j.charCodeAt(0) - b) * (k % 2 + 1)) > c ? 1 + (p - c) : p;
                return k < 14 ? i + p : j == ((c = (c - (i % c))) < 10 ? c : String.fromCharCode(c + b));
            }, 0);
        }

        function checkIfsc(str) {
            const regex = /^[A-Za-z]{4}\d{7}$/;
            let m;
            let response = false;
            if ((m = regex.exec(str)) !== null) {
                // The result can be accessed through the `m`-variable.
                m.forEach((match, groupIndex) => {
                    response = true;
                });
            }
            return response;
        }

        function checkPan(txt) {
            var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
            if (regpan.test(panVal)) {
                return true
            } else {
                return false
            }
        }

        window.Parsley.addValidator('gst', {
            validateString: function(value) {
                return checksum(value);
            },
            messages: {
                en: 'Invalid GST number'
            }
        });

        window.Parsley.addValidator('ifsc', {
            validateString: function(value) {
                return checkIfsc(value);
            },
            messages: {
                en: 'Invalid IFSC Code'
            }
        });

        window.Parsley.addValidator('pan', {
            validateString: function(value) {
                return checkPan(value);
            },
            messages: {
                en: 'Invalid PAN'
            }
        });



        $("#wizard2").on('change', '#profile_picture', function() {
            var fileName = $(this).val().replace("C:\\fakepath\\", "");
            $("#wizard2").find('#profile_picture_label').html(fileName);
        });
        $("#wizard2").on('change', '#id_proof', function() {
            var fileName = $(this).val().replace("C:\\fakepath\\", "");
            $("#wizard2").find('#id_proof_label').html(fileName);
        });
        $("#wizard2").on('change', '#pancard', function() {
            var fileName = $(this).val().replace("C:\\fakepath\\", "");
            $("#wizard2").find('#pancard_label').html(fileName);
        });
        $("#wizard2").on('change', '#trade_license_document', function() {
            var fileName = $(this).val().replace("C:\\fakepath\\", "");
            $("#wizard2").find('#trade_license_document_label').html(fileName);
        });
        $("#wizard2").on('change', '#gst_document', function() {
            var fileName = $(this).val().replace("C:\\fakepath\\", "");
            $("#wizard2").find('#gst_document_label').html(fileName);
        });
        $("#wizard2").on('change', '#cancel_cheque', function() {
            var fileName = $(this).val().replace("C:\\fakepath\\", "");
            $("#wizard2").find('#cancel_cheque_label').html(fileName);
        });
        $("#wizard2").on('change', '#state', function() {
            var state = $('#state').val();
            $.ajax({
                type: "POST",
                url: "{{route('getDistrict')}}",
                data: "state=" + state,
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(result) {
                    console.log(result);
                    if (result.status == 1) {
                        $("#wizard2").find('#district').find('option').remove();
                        $.each(result.district, function(key, value) {
                            var options = '<option value=' + value['id'] + '>' + value['name'] + '</option>';
                            $("#wizard2").find('#district').append(options);
                        });
                    } else {
                        $("#wizard2").find('#district').find('option').remove();
                        var options = 'No data ';
                        $("#wizard2").find('#district').append(options).selectpicker('refresh');
                    }
                }

            });
        });
    </script>
    <script>
        $(document).ready(function() {
            window.ParsleyValidator
                .addValidator('fileextension', function(value, requirement) {
                    var tagslistarr = requirement.split(',');
                    var fileExtension = value.split('.').pop();
                    var arr = [];
                    $.each(tagslistarr, function(i, val) {
                        arr.push(val);
                    });
                    if (jQuery.inArray(fileExtension, arr) != '-1') {
                        return true;
                    } else {
                        return false;
                    }
                }, 32)
                .addMessage('en', 'fileextension', 'File not support');
        });
        $(document).ready(function() {
            'use strict';
            $('#wizard2').steps({
                headerTag: 'h3',
                bodyTag: 'section',
                autoFocus: true,
                titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
                onStepChanging: function(event, currentIndex, newIndex) {
                    if (currentIndex < newIndex) {
                        // Step 1 form validation
                        if (currentIndex === 0) {
                            var company_name = $('#company_name').parsley();
                            var phone = $('#phone').parsley();
                            var district = $('#district').parsley();
                            var state = $('#state').parsley();
                            var location = $('#location').parsley();

                            if (company_name.isValid() && phone.isValid() && district.isValid() && state.isValid() && location.isValid()) {
                                return true;
                            } else {
                                company_name.validate();
                                phone.validate();
                                state.validate();
                                district.validate();
                                location.validate();
                            }
                        }
                        if (currentIndex === 1) {
                            var store_name = $('#store_name').parsley();
                            var category = $('#category').parsley();
                            var profile_picture = $('#profile_picture').parsley();
                            if (store_name.isValid() && category.isValid() && profile_picture.isValid()) {
                                return true;
                            } else {
                                store_name.validate();
                                category.validate();
                                profile_picture.validate();
                            }
                        }
                        // Step 2 form validation

                        if (currentIndex === 2) {
                            var trade_license_document = $('#trade_license_document').parsley();
                            var gst_number = $('#gst_number').parsley();
                            var gst_document = $('#gst_document').parsley();
                            var id_proof = $('#id_proof').parsley();
                            var pancard = $('#pancard').parsley();
                            if (trade_license_document.isValid() && id_proof.isValid() && pancard.isValid() && gst_document.isValid()) {
                                return true;
                            } else {
                                id_proof.validate();
                                pancard.validate()
                                trade_license_document.validate();
                                gst_number.validate();
                                gst_document.validate();
                            }
                        }

                        // Always allow step back to the previous step even if the current step is not valid.
                    } else {
                        return true;
                    }
                },
                onFinished: function(event, currentIndex) {

                    var name_as_in_bank = $('#name_as_in_bank').parsley();
                    var account_number = $('#account_number').parsley();
                    var ifsc_code = $('#ifsc_code').parsley();
                    var cancel_cheque = $('#cancel_cheque').parsley();
                    var account_type = $('#account_type').parsley();
                    if (name_as_in_bank.isValid() && account_number.isValid() && ifsc_code.isValid() && cancel_cheque.isValid() && account_type.isValid()) {
                        //  $('#loader-wrapper').show();
                        // $('#overlay').addClass('show');
                        $("#wizard2").find("a[href='#finish']").addClass('disble-button');
                        $("#wizard2").find("a[href='#finish']").addClass('loading-button');
                        var form = document.getElementById('signUpForm');
                        var data = new FormData(form);
                        $.ajax({
                            type: "post",
                            url: "{{route('addRegistration')}}",
                            data: data,
                            dataType: "json",
                            processData: false,
                            contentType: false,
                            cache: false,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(data) {
                                if (data.status == 1) {
                                    $('#loader-wrapper').hide();
                                    $('.msgDiv').html(`<div class="alert alert-success">` + data.message + `</div>`);
                                    setTimeout(function() {
                                        $('.msgDiv').html(`<div class="alert alert-success">` + data.message + `</div>`);
                                        location.href = "{{route('showLogin')}}";
                                    }, 10000);
                                } else {
                                    $("#wizard2").find("a[href='#finish']").removeClass('disble-button');
                                    var html = "";
                                    $.each(data.errors, function(key, value) {
                                        html += value + "<br/>";
                                    });
                                    $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                                    setTimeout(function() {
                                        $('.msgDiv').html(``);
                                    }, 4000);
                                }
                            }
                        });
                    } else {
                        name_as_in_bank.validate();
                        account_number.validate();
                        ifsc_code.validate();
                        cancel_cheque.validate();
                        account_type.validate();
                    }
                }
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2({
                placeholder: 'Please select ...',
                width: '100%',
                minimumResultsForSearch: -1,
                containerCssClass: 'selling_category',
                dropdownCssClass: 'no-search'
            });
        });
    </script>
</body>

</html>