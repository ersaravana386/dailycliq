@extends('vendors.layouts.web')
@section('content')
<div class="br-pagebody">
    <div class="container-fluid pt-2">
        <div class="row row-sm justify-content-center new">
            <div class="col-12 col-sm-10 col-md-12 col-lg-12">
                <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">Flash Sale Offer List</h4> 
                <div class="card bg-white border-0 shadow-sm py-5 px-4">
                    <div class="bd bd-gray-300 rounded table-responsive">
                        <table class="table mg-b-0">
                            <thead>
                                <tr>
                                    <th class="wd-5p">#</th>
                                    <th class="wd-15p">OFFER STATUS</th>
                                    <th class="wd-15p">OFFER START DATE</th>
                                    <th class="wd-15p">TIME</th>
                                    <th class="wd-15p">OFFER END DATE</th>
                                    <th class="wd-15p">TIME</th>
                                    <th class="wd-20p">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $i= 0;
                            @endphp
                            @if(count($flashsale)>0)
                            @foreach ($flashsale as $key=> $item)
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                <td scope="row">
                                   @php
                                   $today =date("Y-m-d h:m:s");
                                   if($today > $item->starting_date){
                                       if($today <= $item->ending_date){
                                           echo "Active";
                                       }else{
                                           echo "Upcomming";
                                       }
                                    }else if($today <= $item->starting_date){
                                        echo "Upcomming";
                                    }
                                   @endphp
                                </td>
                                <td scope="row">
                                  {{date('d M Y',strtotime($item->starting_date))}}
                                </td>
                                <td scope="row">
                                  {{date('h : m: A',strtotime($item->starting_date))}}
                                </td>
                                <td scope="row">
                                  {{date('d M Y',strtotime($item->ending_date))}}
                                </td>
                                <td scope="row">
                                  {{date('h : m: A',strtotime($item->ending_date))}}
                                </td>
                                <td scope="row">
                                    <a class="btn btn-sm btn-success" href="{{route('showFlashsale',['flashsale'=>$item->id])}}" title="add product to flash sale">
                                            <i class="icon ion-plus-circled" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td scope="row" colspan=10><b><center><span style="color:#DC3545;">Sorry No Result Found</span></center><b></td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        @if(count($flashsale)>0)
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                        {{ $flashsale->appends(['search' => $search])->links() }}
                        </div>
                        @endif
                    </div>
                </div>
                <!-- bd -->
            </div>
        </div>
    </div><!-- row -->
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
<script>
  //$('#catelogId').find('a').addClass('active');
  $("input[name='search']").autocomplete({
    source: function(request, response) {
      $.ajax({
        url: "{{route('searchProduct')}}",
        dataType: "json",
        data: {
          q: request.term
        },
        success: function(data) {
          response(data);
        }
      });
    },
    minLength: 3,
    select: function(event, ui) {
      // log( ui.item ?
      //   "Selected: " + ui.item.label :
      //   "Nothing selected, input was " + this.value);
    },
    open: function() {
      $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
    },
    close: function() {
      $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
    }
  });
</script>
@endsection
