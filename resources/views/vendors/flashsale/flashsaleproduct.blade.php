@extends('vendors.layouts.web')
@section('content')
<div class="br-pagebody">
  <div class="container-fluid pt-2">
    <div class="row row-sm justify-content-center new">
      <div class="col-12 col-sm-10 col-md-12 col-lg-12">
        <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">Flash Sale Offer Details</h4> 
        <div class="card bg-white border-0 shadow-sm py-5 px-4">
          <div class="item">
            <form class="inset row" action="javascript:;" id="mainForm">
                <div class="col-lg-3">
                    <div class="form-group pd-5">
                      <label class="form-control-label" for="unit"> Offer Start From</label>
                      @php
                       $datefrom = date('d M Y',strtotime($flashsale->starting_date));
                      @endphp
                      <input class="form-control form-control-sm" type="text" value='{{$datefrom}}' name="available" id="available"  placeholder="Available Unit" readonly>
                    </div>
                  </div><!-- col-6 -->
                  <div class="col-lg-3">
                    <div class="form-group pd-5">
                      <label class="form-control-label" for="unit"> Time</label>
                      @php
                       $timefrom = date('h : i : A',strtotime($flashsale->starting_date));
                      @endphp
                      <input class="form-control form-control-sm" type="text" value='{{$timefrom}}' name="available" id="available"  placeholder="Available Unit" readonly>
                    </div>
                  </div><!-- col-6 -->
                  <div class="col-lg-3">
                    <div class="form-group pd-5">
                      <label class="form-control-label" for="unit"> Offer End On</label>
                      @php
                       $dateto = date('d M Y',strtotime($flashsale->ending_date));
                      @endphp
                      <input class="form-control form-control-sm" type="text" value='{{$dateto}}' name="available" id="available"  placeholder="Available Unit" readonly>
                    </div>
                  </div><!-- col-6 -->  
                  <div class="col-lg-3">
                    <div class="form-group pd-5">
                      <label class="form-control-label" for="unit">Time</label>
                      @php
                      $timeto = date('h : i : A',strtotime($flashsale->ending_date));
                      @endphp
                      <input class="form-control form-control-sm" type="text" value='{{$timeto}}' name="available" id="available"  placeholder="Available Unit" readonly>
                    </div>
                  </div><!-- col-6 -->
            </form>
          </div><!-- card -->
        </div>
      </div>
    </div><!-- row -->
  </div>
</div><!-- br-pagebody -->
<div class="br-pagebody">
    <div class="container-fluid pt-2">
        <div class="row row-sm justify-content-center new">
            <div class="col-8 col-sm-10 col-md-8 col-lg-8">
                <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">Product List</h4> 
                <div class="card bg-white border-0 shadow-sm py-5 px-4">
                    <div class="col-lg-12 msgDiv" role="alert">
                    </div><!-- alert -->
                    <div class="bd bd-gray-300 rounded table-responsive">
                    <table class="table mg-b-0">
                            <thead>
                                <tr>
                                    <th class="wd-5p">#</th>
                                    <th class="wd-15p">PRODUCT</t>
                                    <th class="wd-15p">MRP</th>
                                    <th class="wd-15p">SELLING PRICE</th>
                                    <th class="wd-15p">OFFER(%)</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $i= 0;
                            @endphp
                            @if(count($products)>0)
                            @foreach ($products  as $key=> $item)
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                <td scope="row">
                                   @if($item->images->first())
                                    @if(file_exists($item->images->first()->image))
                                    <a target="_blank" href="{{asset($item->images->first()->image)}}"><img src="{{asset($item->images->first()->image)}}" class="wd-80 ht-80 mg-l-10 bd bd-gray-500 rounded-30" style="width:80px;"></a>
                                    @endif
                                    @endif <br> 
                                    {{$item->name}}
                                </td>
                                <td scope="row">
                                {{$item->mrp}}
                                </td>
                                <td scope="row">{{$item->price}}</td>
                                <td scope="row">
                                <form class="form-inline my-2 my-lg-0 flex-nowrap w-100 w-md-75 offer-form" action="jquery:;" method="post">
                                    <input class="form-control form-control-sm mr-2 has-border w-100" type="number" placeholder="Offer Eg:30" aria-label="Offer Eg:30" name="offer" value="">
                                    <input type="hidden" name="product" value="{{$item->id}}" class="offer_product_id">
                                    <button class="btn btn-primary btn-sm button-inner my-2 my-sm-0 offer-button" type="submit">Move</button>
                                </form>
                                </td>
                            </tr>
                            @endforeach
                            @else
                                <tr>
                                    <td scope="row" colspan=10><b><center><span style="color:#DC3545;">Sorry No Result Found</span></center><b></td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        @if(count($products)>0)
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                        {{ $products->appends(['search' => $search ?? ''])->links() }}  
                        </div>
                        @endif
                    </div>
                </div>
                <!-- bd -->
            </div>
            <div class="col-4 col-sm-10 col-md-4 col-lg-4">
                <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">Flash Sale  Added Product Listing</h4> 
                <div class="card bg-white border-0 shadow-sm py-5 px-4">
                    <div class="bd bd-gray-300 rounded table-responsive">
                    <table class="table mg-b-0">
                            <thead>
                                <tr>
                                    <th class="wd-5p">#</th>
                                    <th class="wd-15p">PRODUCT</th>
                                    <th class="wd-15p">OFFER</th>
                                    <th class="wd-15p">ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $i= 0;
                            @endphp
                            @if(count($flashsaleproducts)>0)
                            @foreach ($flashsaleproducts  as $key=> $item)
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                  <td>
                                    @if($item->images->first())
                                    @if(file_exists($item->images->first()->image))
                                    <a target="_blank" href="{{asset($item->images->first()->image)}}"><img src="{{asset($item->images->first()->image)}}" class="wd-80 ht-80 mg-l-10 bd bd-gray-500 rounded-30" style="width:80px;"></a>
                                    @endif
                                    @endif <br>
                                   <b> {{$item->product->name}}</b><br>
                                   @if($item->approve=='1')
                                   <label class="badge badge-success">Approved</label>
                                   @elseif($item->approve=='2')
                                   <label class="badge badge-danger">Rejected</label>
                                   @elseif($item->approve=='0')
                                   <label class="badge badge-warning">Pending</label>
                                   @endif
                                </td>
                                <td scope="row"> <b>{{$item->offer_amount}}({{$item->offer_percentage}}%)</b></td>
                                <td scope="row">
                                    <a class="btn btn-sm btn-success" onclick="return confirm('Are you sure you want to delete this product from flash sale ?');" href="{{route('deleteFlashsaleProduct',['flashsale'=>$flashsale->id,'product'=>$item->product_id  ])}}" title="delete flashsale product">
                                            <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td scope="row" colspan=10><b><center><span style="color:#DC3545;">Sorry No Result Found</span></center><b></td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        @if(count($flashsaleproducts)>0)
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                      
                        </div>
                        @endif
                    </div>
                </div>
                <!-- bd -->
            </div>
        </div>
    </div><!-- row -->
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
<script>
$(document).ready(function(){
    $('.offer-button').click(function(){
        var form = $(this).closest("form");
        var offer =form.find('input[name="offer"]').val();
        var product =form.find('input[name="product"]').val();
        var serializedData = form.serialize();
        if((offer=='')||(offer > 100)){
            $('.msgDiv').html(`<div class="alert alert-danger m-2">Offer Percentage is required uder 100(%)</div>`);
                  setTimeout(function() {
                      $('.msgDiv').html(``);
                  }, 4000);
        }else if(product==''){
            $('.msgDiv').html(`<div class="alert alert-danger m-2">Product Details Should be required</div>`);
                  setTimeout(function() {
                      $('.msgDiv').html(``);
                  }, 4000);
        }else{
            $.ajax({
            type: "POST",
            url: "{{route('saveFlashsaleProduct',['flashsale'=>$flashsale->id])}}",
            dataType: "json",
            data: {offer:offer,product:product},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
              if (data.status == 1) {
                  if(data.redirect){
                    window.location.href = data.redirect;
                  }
              } if (data.status == 0) {
                 
                  var html = "";
                  $.each(data.errors, function(key, value) {
                      html += value + "<br/>";
                  });
                  $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                  setTimeout(function() {
                      $('.msgDiv').html(``);
                  }, 4000);
             
              } 
            }
          });

        }
    });
});

       
       
    
</script>
@endsection
