@extends('vendors.layouts.web')
@section('content')
 
    <div class="br-pagebody">
        <div class="row no-gutters widget-1 shadow-base" style="margin-top:6rem !important">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="card shadow-base bd-0">
                    <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                        <h6 class="card-title tx-uppercase tx-12 mg-b-0">Filter Order</h6>
                    </div><!-- card-header -->
                    <form id="sort-form" method="get" action="{{route('SortOrder')}}"> 
                        <div class="form-layout form-layout-1">
                            <div class="row mg-b-25">
                                <div class="col-lg-4">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="referancenumber" value="{{$referancenumber ?? '' }}" placeholder="Referance Number">
                                        </div>
                                </div><!-- col-4 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="customername" value="{{$customername ?? '' }}" placeholder="Customer Name">
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <select class="form-control select222" style="width:100%;" name="orderstatus">
                                            <option>Order Status</option>
                                            <option  @if(@$orderstatus=='1'){{'selected'}} @endif value="1">Completed</option>
                                            <option  @if(@$orderstatus=='2'){{'selected'}} @endif value="2">Pending</option>
                                            <option  @if(@$orderstatus=='3'){{'selected'}} @endif value="3">Refunded</option>
                                            <option  @if(@$orderstatus=='4'){{'selected'}} @endif value="4">Failed</option>
                                            <option  @if(@$orderstatus=='5'){{'selected'}} @endif value="5">Canceled</option>
                                        </select>
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <select class="form-control select222" style="width:100%;" name="paymenttype" >
                                            <option  @if(@$paymenttype=='Credit Card'){{'selected'}} @endif  value="Credit Card">Credit Card</option>
                                            <option  @if(@$paymenttype=='Online Payment'){{'selected'}} @endif value="Online Payment">Online Payment</option>
                                            <option  @if(@$paymenttype=='Debit Card'){{'selected'}} @endif value="Debit Card">Debit Card</option>
                                            <option  @if(@$paymenttype=='Cash On  Delivery'){{'selected'}} @endif value="Cash On  Delivery">Cash On  Delivery</option>
                                            <option  @if(@$paymenttype=='Others'){{'selected'}} @endif value="Others">Others</option>
                                        </select>
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-md-4 col-lg-2 mg-t-10 mg-lg-t-0">
                                    <div class="form-group mg-b-0">
                                    <input type="text" class="form-control form-control-datepicker tx-14" data-language="en" placeholder="Date of order from" value="{{$orderfrom ?? '' }}" name="orderfrom">
                                    </div><!-- form-group -->
                                </div><!-- col-2 -->
                                <div class="col-md-4 col-lg-2 mg-t-10 mg-lg-t-0">
                                    <div class="form-group mg-b-0">
                                    <input type="text" class="form-control form-control-datepicker tx-14" data-language="en" value="{{$orderto ?? '' }}" placeholder="Date of order to" name="orderto">
                                    </div><!-- form-group -->
                                </div><!-- col-2 -->
                            </div><!-- row -->
                            <div class="form-layout-footer">
                                <button class="btn btn-primary btn-sm button-inner my-2 my-sm-0">Search</button>
                            </div><!-- form-layout-footer -->
                        </div>
                    </form>
                </div><!-- card -->
            </div>
        </div>        
  
        <div class="pt-2">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 msgDiv">
                 </div> 
            </div>
            <div class="row row-sm justify-content-center new">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <!-- <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">Vendor Product Listing</h4> -->
                    <div class="card bg-white border-0 shadow-sm py-5 px-4">
                        <div class="bd bd-gray-300 rounded table-responsive">
                            <div class="row">
                                <div class="pull-right col-md-4">
                                    <form class="form-inline my-2 my-lg-0 flex-nowrap w-100 w-md-75 ">
                                        <label class="ckbox mg-b-0" style="margin-bottom:0px;">
                                            <input type="checkbox" onchange="checkAll(this)" name="chk[]"><span></span>
                                        </label>
                                        <select class="form-control select2222" style="width:125px;">
                                                    <option value="1">Completed</option>
                                                    <option value="2">Pending</option>
                                                    <option value="3">Refunded</option>
                                                    <option value="4">Failed</option>
                                                    <option value="5">Canceled</option>
                                        </select>&nbsp;&nbsp;
                                        <button class="btn btn-primary btn-sm button-inner my-2 my-sm-0" type="submit">Submit</button>
                                    </form>
                                </div>
                                <div class="col-md-8"></div>
                            </div>
                            <table class="table mg-b-0">
                                <thead>
                                    <tr>
                                        <th class="wd-5p">
                                        </th>
                                        <th class="wd-5p"># 
                                        </th>
                                        <th class="wd-15p">Ref.No</th>
                                        <th class="wd-15p">Vendor</th>
                                        <th class="wd-15p">Customer</th>
                                        <th class="wd-15p">Payment Type</th>
                                        <th class="wd-15p">Amount</th>
                                        <th class="wd-15p">Order At</th>
                                        <th class="wd-15p">Billed At</th>
                                        <th class="wd-15p">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php
                                $i= 0;
                                $count =0;
                                @endphp
                                @if(count($orders)>0)
                                @foreach ($orders as $key=> $item)
                                <tr @if($item->status=='1') class="table-success" style="color:#115d04;" @endif @if($item->status=='2') class="table-danger" style="color:#921925;" @endif @if($item->status=='3') class="table-warning" style="color:#921925;" @endif @if($item->status=='4') class="table-primary" style="color:#921925;" @endif @if($item->status=='5') class="table-info" style="color:#921925;" @endif>
                                    <td scope="row"><label class="ckbox mg-b-0"><input type="checkbox" name="chech_list[]" value="{{$item->id}}"><span></span></label>
                                    </td>    
                                    <td scope="row">{{++$i}}</td>
                                    <td scope="row">
                                     <b>{{$item->order_reference_number}}</b> 
                                    <br><br><a href="{{route('showInvoice',['order'=>$item->id])}}"><label class="badge badge-danger">Invoice</label></a>|<a href="{{route('showCustomerDetails',['order'=>$item->id])}}" target="_blank"><label class="badge badge-info">View Customer</label></a>|<a href="{{route('showOrderedProduct',['order'=>$item->id])}}" target="_blank"><label class="badge badge-warning">View Product</label></a>
                                    </td>
                                    <td scope="row"><b>{{$item->vendor->company_name}}</b></td>
                                    <td>
                                        @if($item->images->first())
                                        @if(file_exists($item->images->first()->image))
                                        <a target="_blank" href="{{asset($item->images->first()->image)}}"><img src="{{asset($item->images->first()->image)}}" style="width:80px;"></a>
                                        @endif
                                        @endif
                                        <b>{{$item->user->name}}</b>
                                    </td>
                                    <td scope="row"><b>@if(@$item->payment->payment_type){{@$item->payment->payment_type}}@endif</b></td>
                                    <td scope="row"><b>{{$item->total}}</b></td>
                                    <td scope="row"><b>{{date('d M Y h:i:A',strtotime($item->order_at))}}</b></td>
                                    <td scope="row"><b>@if(($item->paid_at)!= '') {{date('d M Y h:i:A',strtotime($item->paid_at))}} @else {{'Not Billed/Paid'}} @endif
                                    </b></td>
                                    <td scope="row">
                                       @if($item->status=='5')
                                       <label class="badge badge-danger p-2">Canceled</label>
                                       @else
                                        <select class="form-control select22" style="width:125px;" data-id="{{$item->id}}">
                                            <option @if($item->status=='1'){{'selected'}}@endif value="1">Completed</option>
                                            <option @if($item->status=='2'){{'selected'}}@endif value="2">Pending</option>
                                            <option @if($item->status=='3'){{'selected'}}@endif value="3">Refunded</option>
                                            <option @if($item->status=='4'){{'selected'}}@endif value="4">Failed</option>
                                            <option @if($item->status=='5'){{'selected'}}@endif value="5">Canceled</option>
                                        </select>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td scope="row" colspan=10><b><center><span style="color:#DC3545;">Sorry No Result Found</span></center><b></td>
                                </tr>
                                @endif
                                </tbody>
                            </table>
                            @if(count($orders)>0)
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                            {{ $orders->appends(['search' => $search])->links() }}
                            </div>
                            @endif
                        </div>
                    </div>
                    <!-- bd -->
                </div>
            </div>
        </div><!-- row -->
    </div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
<script>
  $("input[name='search']").autocomplete({
    source: function(request, response) {
      $.ajax({
        url: "{{route('searchProduct')}}",
        dataType: "json",
        data: {
          q: request.term
        },
        success: function(data) {
          response(data);
        }
      });
    },
    minLength: 3,
    select: function(event, ui) {
      // log( ui.item ?
      //   "Selected: " + ui.item.label :
      //   "Nothing selected, input was " + this.value);
    },
    open: function() {
      $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
    },
    close: function() {
      $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
    }
  });
  $(document).ready(function(){
      $('.select22').on('change',function(){
          var id= $(this).data('id');
          var item =$(this);
          var selectedstatus = $(this). children("option:selected"). val();
          if(selectedstatus=='5'){
            if(confirm("are you sure?")){}
          }
          if((id=='') || (selectedstatus=='')){
            $('.msgDiv').html(`<p class="alert alert-danger mg-b-4">Unable to process your request</p>`).focus();
            setTimeout(function() {
              $('.msgDiv').html(``);
              window.location.reload();
            }, 1000);
          }else{
            $.ajax({
            url: "{{route('orderChangeStatus')}}",
            data: {'id':id,'status':selectedstatus} ,
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type:'POST',
            success: function(data) {
                $('.msgDiv').html(data).focus();
                setTimeout(function() {
                    $('.msgDiv').html(``);
                    window.location.reload();
                    }, 1000);
                  }
            });
          }

      });
    
  });
   // Datepicker
   $('.fc-datepicker').datepicker({
          showOtherMonths: true,
          selectOtherMonths: true
        });

        $('#datepickerNoOfMonths').datepicker({
          showOtherMonths: true,
          selectOtherMonths: true,
          numberOfMonths: 2
        });
    // Check All CheckBoxes
    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }
</script>
@endsection
