@extends('vendors.layouts.web')
@section('content')
<div class="br-pagebody">
    <div class="container-fluid pt-2">
        <div class="row row-sm justify-content-center new">
            <div class="col-12 col-sm-10 col-md-12 col-lg-12">
                 <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">User Details</h4>
                <div class="card bg-white border-0 shadow-sm py-5 px-4">
                    <div class="bd bd-gray-300 rounded table-responsive">
                        <table class="table mg-b-0">
                            <thead>
                                <tr>
                                    <th class="wd-5p">#</th>
                                    <th class="wd-15p">NAME</th>
                                    <th class="wd-15p">EMAIL</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $i= 0;
                            @endphp
                            @foreach ($users as $key=> $item)
                            @if($item->name)
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                <td scope="row">
                                {{$item->user->name}} <br><label class="badge badge-success">Active</label>
                                </td>
                                <td scope="row">{{$item->user->email}}</td>
                            </tr>
                            @else
                            <tr>
                                <td scope="row" colspan=3><b><center><span style="color:#DC3545;">Sorry User Details Not Avalialble</span></center><b></td>
                            </tr>
                          @endif
                           @endforeach
                            </tbody>
                        </table>
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                     
                        </div>
                    </div>
                </div>
                <!-- bd -->
            </div>
        </div>
    </div><!-- row -->
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
<script>
  $("input[name='search']").autocomplete({
    source: function(request, response) {
      $.ajax({
        url: "{{route('searchProduct')}}",
        dataType: "json",
        data: {
          q: request.term
        },
        success: function(data) {
          response(data);
        }
      });
    },
    minLength: 3,
    select: function(event, ui) {
      // log( ui.item ?
      //   "Selected: " + ui.item.label :
      //   "Nothing selected, input was " + this.value);
    },
    open: function() {
      $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
    },
    close: function() {
      $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
    }
  });
</script>
@endsection
