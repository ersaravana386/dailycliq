@extends('vendors.layouts.web')
@section('content')
<div class="br-pagebody">
    <div class="container-fluid pt-2">
        <div class="row row-sm justify-content-center new">
            <div class="col-12 col-sm-10 col-md-12 col-lg-12">
                 <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">Order Product Listing</h4>
                <div class="card bg-white border-0 shadow-sm py-5 px-4">
                    <div class="bd bd-gray-300 rounded table-responsive">
                        <table class="table mg-b-0">
                            <thead>
                                <tr>
                                    <th class="wd-5p">#</th>
                                    <th class="wd-15p">NAME</th>
                                    <th class="wd-15p">DCIN</th>
                                    <th class="wd-15p">Image</th>
                                    <th class="wd-15p">PRICE</th>
                                    <th class="wd-15p">MRP</th>
                                    <th class="wd-15p">BRAND</th>
                                    <th class="wd-15p">CATEGORY</th>
                                    <th class="wd-15p">Comments</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $i= 0;
                            @endphp
                            @if(count($products)>0)
                            @foreach ($products as $key=> $item)
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                <td scope="row">
                                {{$item->name}} <br><label class="badge badge-success">Approved</label>
                                </td>
                                <td scope="row">{{$item->dcin}}</td>
                                <td>
                                    @if($item->productlist->images->first())
                                    @if(file_exists($item->productlist->images->first()->image))
                                    <a target="_blank" href="{{asset($item->productlist->images->first()->image)}}"><img src="{{asset($item->productlist->images->first()->image)}}" style="width:80px;"></a>
                                    @endif
                                    @endif
                                </td>
                                <td scope="row">{{$item->productlist->price}}</td>
                                <td scope="row">{{$item->productlist->mrp}}</td>
                                <td scope="row"> 
                                    @if($item->productlist->brand)
                                    {{$item->productlist->brand->name}}
                                    @else
                                    {{$item->productlist->other_brand}}
                                    @endif</td>
                                <td scope="row">{{$item->productlist->category->name}}</td>
                                <td scope="row">
                                    no comments
                                </td>
                            </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td scope="row" colspan=10><b><center><span style="color:#DC3545;">Sorry No Result Found</span></center><b></td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        @if(count($products)>0)
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                     
                        </div>
                        @endif
                    </div>
                </div>
                <!-- bd -->
            </div>
        </div>
    </div><!-- row -->
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
<script>
  $("input[name='search']").autocomplete({
    source: function(request, response) {
      $.ajax({
        url: "{{route('searchProduct')}}",
        dataType: "json",
        data: {
          q: request.term
        },
        success: function(data) {
          response(data);
        }
      });
    },
    minLength: 3,
    select: function(event, ui) {
      // log( ui.item ?
      //   "Selected: " + ui.item.label :
      //   "Nothing selected, input was " + this.value);
    },
    open: function() {
      $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
    },
    close: function() {
      $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
    }
  });
</script>
@endsection
