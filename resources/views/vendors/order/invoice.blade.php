@extends('vendors.layouts.web')
@section('content')
<div class="br-pagebody pd-y-15 pd-l-20 pd-x-20 pd-sm-x-30" style="margin-top:90px;">
    <div class="row no-gutters card widget-1 shadow-base">
        <div class="panel panel-default" style="padding: 10px 10px 10px 10px;">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <h5 class="subtitle mb10">From</h5>
                        <img src="images/themeforest.png" class="img-responsive mb10" alt="">
                        <address>
                            <strong>DailyCliq</strong><br>
                            795 Folsom Ave, Suite 600<br>
                            San Francisco, CA 94107<br>
                            <abbr title="Phone">P:</abbr> (123) 456-7890
                        </address>
                        
                    </div><!-- col-sm-6 -->
                    <div class="col-sm-4">
                    <center><b><h5 class="title">INVOICE</h5></b></center>
                    </div>
                    
                    <div class="col-sm-4 text-right">
                        <h5 class="subtitle mb10">Invoice No.</h5>
                        <h4 class="text-primary">{{@$product[0]->order->invoice->invoice_number}}</h4>
                        
                        <h5 class="subtitle mb10">To</h5>
                        <address>
                            <strong>{{@$product[0]->order->deliveryaddress->name}}</strong><br>
                            @php
                            $address = explode(',',@$product[0]->order->deliveryaddress->address)
                            @endphp
                            @foreach($address as $item)
                            {{$item}}<br>
                            @endforeach
                            <strong>Mob:</strong>  {{@$product[0]->order->deliveryaddress->mobile_number}}<br>
                            <strong>State:</strong>  {{@$product[0]->order->deliveryaddress->state}}<br>
                            <strong>City:</strong>  {{@$product[0]->order->deliveryaddress->city}}<br>
                            <strong>Pincode:</strong>  {{@$product[0]->order->deliveryaddress->pin_code}}
                        </address>
                        
                        <p><strong>Invoice Date:</strong> {{date('F d, Y',strtotime(@$product[0]->order->invoice->created_at))}}</p>
                        
                        <p><strong>Due Date:</strong> {{date('F d, Y',strtotime('+30 days',strtotime($product[0]->order->invoice->created_at))) . PHP_EOL}}</p>
                        
                    </div>
                </div><!-- row -->
                
                <div class="table-responsive">
                <table class="table table-invoice">
                <thead>
                <tr>
                    <th>Item</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Total Price</th>
                </tr>
                </thead>
                <tbody>
                @php
                  $sub=0;
                  $gst=0;
                  $net=0;
                @endphp
                @foreach($product as $key=>$item)
                @php
                 $gst+=($item->selling_price * $item->productlist->gst)/100;
                 $sub+=$item->selling_price-$item->productlist->gst;
                 $net+=$item->selling_price;
                @endphp
                    <tr>
                        <td>
                            <div class="text-primary"><strong>{{$item->productlist->name}}</strong></div>
                            <small>{{$item->productlist->dcin}}</small>
                        </td>
                        <td>{{$item->unit}}</td>
                        <td><i class="fa fa-inr" aria-hidden="true"></i>{{number_format($item->selling_price)}}</td>
                        <td><i class="fa fa-inr" aria-hidden="true"></i>{{number_format($item->net_total)}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div><!-- table-responsive -->
            
                <table class="table table-total">
                    <tbody>
                        <tr>
                            <td><strong>Sub Total :</strong></td>
                            <td><i class="fa fa-inr" aria-hidden="true"></i>{{$sub}}</td>
                        </tr>
                        <tr>
                            <td><strong>GST :</strong></td>
                            <td><i class="fa fa-inr" aria-hidden="true"></i>{{$gst}}</td>
                        </tr>
                        <tr>
                            <td><strong>TOTAL :</strong></td>
                            <td><i class="fa fa-inr" aria-hidden="true"></i>{{$net}}</td>
                        </tr>
                    </tbody>
                </table>              
                <div class="mb40"></div>
                
                <div class="well">
                    Thank you for buying from us <strong>DailyCliq</strong>
                </div>
                
                
            </div><!-- panel-body -->
        </div>
    </div>
</div>
@endsection
@section('scripts')

@endsection
