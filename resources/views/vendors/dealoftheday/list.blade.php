@extends('vendors.layouts.web')
@section('content')
<div class="br-pagebody">
    <div class="container-fluid pt-2">
        <div class="row row-sm justify-content-center new">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-8">
                <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">Product List</h4> 
                <div class="card bg-white border-0 shadow-sm py-5 px-4">
                    <div class="col-lg-12 msgDiv" role="alert">
                    </div><!-- alert -->
                    <div class="bd bd-gray-300 rounded table-responsive">
                    <table class="table mg-b-0">
                            <thead>
                                <tr>
                                    <th class="wd-5p">#</th>
                                    <th class="wd-15p">PRODUCT</t>
                                    <th class="wd-15p">SELLING PRICE</th>
                                    <th class="wd-15p">Display Date</th>
                                    <th class="wd-15p">OFFER(%)</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $i= 0;
                            @endphp
                            @foreach ($products  as $key=> $item)
                            
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                <td scope="row">
                                   @if($item->images->first())
                                    @if(file_exists($item->images->first()->image))
                                    <a target="_blank" href="{{asset($item->images->first()->image)}}"><img src="{{asset($item->images->first()->image)}}" class="wd-80 ht-80 mg-l-10 bd bd-gray-500 rounded-30" style="width:80px;"></a>
                                    @endif
                                    @endif <br> 
                                    {{$item->name}}
                                </td>
                                <td scope="row">
                                {{$item->price}}
                                </td>
                                <td scope="row">
                                  
                                        <input type="text" class="form-control fc-datepicker offer-date" placeholder="Offer Date" name="offer_date">
                                   
                                </td>
                                <td scope="row">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                            <input class="form-control form-control-sm mr-2 has-border w-100" type="number" placeholder="Offer Eg:30" aria-label="Offer Eg:30" name="offer" value="">
                                            <input type="hidden" name="product" value="{{$item->id}}" class="offer_product_id">
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                            <button class="btn btn-primary btn-sm button-inner my-2 my-sm-0 offer-button" type="submit">Move</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            
                                @endforeach
                            </tbody>
                        </table>
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                        {{ $products->appends(['search' => $search ?? ''])->links() }}  
                        </div>
                    </div>
                </div>
                <!-- bd -->
            </div>
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-4">
                <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">Deal Of The Day Added Product Listing</h4> 
                <div class="card bg-white border-0 shadow-sm py-5 px-4">
                    <div class="bd bd-gray-300 rounded table-responsive">
                    <table class="table mg-b-0">
                            <thead>
                                <tr>
                                    <th class="wd-5p">#</th>
                                    <th class="wd-20p">PRODUCT</th>
                                    <th class="wd-10p">OFFER</th>
                                    <th class="wd-15p"></th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $i= 0;
                            @endphp
                            @foreach ($dealoftheday  as $key=> $item)
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                  <td>
                                    @if($item->images->first())
                                    @if(file_exists($item->images->first()->image))
                                    <a target="_blank" href="{{asset($item->images->first()->image)}}"><img src="{{asset($item->images->first()->image)}}" class="wd-80 ht-80 mg-l-10 bd bd-gray-500 rounded-30" style="width:80px;"></a>
                                    @endif
                                    @endif <br>
                                   <b> {{$item->product->name}}</b><br>
                                   <b> {{date('M d, Y',strtotime($item->start_date))}}</b><br>
                                   @if($item->approve=='1')
                                   <label class="badge badge-success">Approved</label>
                                   @elseif($item->approve=='2')
                                   <label class="badge badge-danger">Rejected</label>
                                   @elseif($item->approve=='0')
                                   <label class="badge badge-warning">Pending</label>
                                   @endif
                                </td>
                                <td scope="row"> <b>{{$item->offer_amount}}({{$item->offer_percentage}}%)</b></td>
                                <td scope="row">
                                    <a class="btn btn-sm btn-success" onclick="return confirm('Are you sure you want to delete this product from flash sale ?');" href="{{route('deleteDealOftheDayProduct',['dealoftheday'=>$item->id,'product'=>$item->product_id])}}" title="delete deal of the day product">
                                            <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                      
                        </div>
                    </div>
                </div>
                <!-- bd -->
            </div>
        </div>
    </div><!-- row -->
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
<script>
     // Datepicker
     $('.fc-datepicker').datepicker({
          showOtherMonths: true,
          selectOtherMonths: true,
          minDate: new Date()
        });
</script>
<script>
$(document).ready(function(){
    $('.offer-button').click(function(){
        var form = $(this).closest("tr");
        var offer =form.find('input[name="offer"]').val();
        var product =form.find('input[name="product"]').val();
        var offer_date =form.find('input[name="offer_date"]').val();
        if((offer=='') || (offer > 100 )){
            $('.msgDiv').html(`<div class="alert alert-danger m-2">Offer Percentage is required under 100(%)</div>`);
                  setTimeout(function() {
                      $('.msgDiv').html(``);
                  }, 4000);
        }else if(product==''){
            $('.msgDiv').html(`<div class="alert alert-danger m-2">Product Details Should be required</div>`);
                  setTimeout(function() {
                      $('.msgDiv').html(``);
                  }, 4000);
        }else if(offer_date==''){
            $('.msgDiv').html(`<div class="alert alert-danger m-2">Offer Date Should be required</div>`);
                  setTimeout(function() {
                      $('.msgDiv').html(``);
                  }, 4000);
        }else{
            $.ajax({
            type: "POST",
            url: "{{route('saveDealOftheDayProduct')}}",
            dataType: "json",
            data: {offer:offer,product:product,offer_date:offer_date},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
              if (data.status == 1) {
                  if(data.redirect){
                    window.location.href = data.redirect;
                  }
              } if (data.status == 0) {
                 
                  var html = "";
                  $.each(data.errors, function(key, value) {
                      html += value + "<br/>";
                  });
                  $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                  setTimeout(function() {
                      $('.msgDiv').html(``);
                  }, 4000);
             
              } 
            }
          });

        }
    });
});
</script>
@endsection
