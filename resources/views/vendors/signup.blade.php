<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket Plus">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels"> --}}

    <title>Daily CliQ</title>

    <!-- vendor css -->
    <link href="{{asset('app/lib/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('app/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{asset('app/css/bracket.css')}}">
    <link rel="stylesheet" href="{{asset('app/css/style.css')}}">
</head>

<body>
    <div class="row no-gutters flex-row-reverse h-100-vh">
        <div class="col-md-12 col-lg-6 bg-gray-200 d-flex align-items-center justify-content-center has-background-image login z-index-1">
            <div class="has-shade login-right"></div>
            <div class="login-pad px-0" role="form" method="post" action="javascript:;" id="otpForm" autocomplete="off">
                <div class="login-wrapper wd-350 text-white">
                    <h4 class="tx-inverse tx-center">Sign Up</h4>
                    <p class="tx-center mg-b-60">Create your seller account.</p>
                    <div class="form-group" id="otpMobile">
                        <input type="text" class="form-control" placeholder="Enter your mobile" name="mobile" value="91">
                    </div><!-- form-group -->
                    <div class="form-group d-none" id="otp">
                        <input type="text" class="form-control" placeholder="Enter OTP" name="otp">
                    </div><!-- form-group -->
                    <button class="btn btn-primary btn-sm button-inner my-2 my-sm-0 btn-block" id="submitOtp">Send OTP</button>
                    <button class="btn btn-primary btn-sm button-inner my-2 my-sm-0 btn-block d-none" id="verifyOtp">Verify OTP</button>
                    <div class="msgDiv mt-3"></div>
                    <div class="tx-center mt-3 d-none" id="resendOtp">Didn't get OTP ? <a href="javascript:;" class="tx-info" onclick="resendOtp()">Resend</a></div>
                    <div class="tx-center mt-3">Already a member? <a href="{{route('showLogin')}}" class="tx-info">Sign In</a></div>
                </div><!-- login-wrapper -->
            </div>
            <form class="login-pad px-0 d-none" role="form" method="post" action="javascript:;" id="signUpForm" autocomplete="off">
                <div class="login-wrapper wd-350 text-white">
                    <h4 class="tx-inverse tx-center">Sign Up</h4>
                    <p class="tx-center mg-b-60">Create your seller account.</p>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Enter your fullname" name="contact_person">
                    </div><!-- form-group -->
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Enter your mobile" name="mobile" readonly>
                    </div><!-- form-group -->
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Enter your email" name="email">
                    </div><!-- form-group -->
                    <div class="form-group">
                        <textarea class="form-control" placeholder="Enter your address" name="address"></textarea>
                    </div><!-- form-group -->
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Enter your PIN Code" name="pin">
                    </div><!-- form-group -->
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Enter your password" name="password" id="password" passwordCheck="passwordCheck">
                        <a href="javascript:;" onclick="passwordshow()">
                            <div class="ion-eye eye" id="eyeId"></div>
                        </a>
                    </div><!-- form-group -->
                    <div class="form-group tx-12">By clicking the Sign Up button you agreed to our privacy policy and terms of uses.
                        <a href="{{route('termsConditions')}}" class="tx-info" target="_blank"> Click here to view</a></div>
                    <button type="submit" class="btn btn-primary btn-sm button-inner my-2 my-sm-0 btn-block">Sign Up</button>
                    <div class="msgDiv mt-3"></div>
                    <div class="tx-center mt-3">Already a member? <a href="{{route('showLogin')}}" class="tx-info">Sign In</a></div>
                </div><!-- login-wrapper -->
            </form>
        </div><!-- col -->

        <div class="col-md-12 col-lg-6 bg-br-primary d-flex align-items-center justify-content-center ">
            <div class="has-shade login"></div>
            <div class="wd-100 wd-xl-450 text-white login-pad">
                <div class="logo pl-md-0 pb-0">
                    <img src="{{asset('app/img/logo.png')}}" class="img-fluid">
                </div>
                <h5 class="text-dark mt-4">Why Daily CliQ?</h5>
                <p class="text-dark line-height-normal">
                    E-Commerce today is a way of life and an extremely integral part of modern day shopping. DailyCliQ
                    presents in front of you, a unique online shopping experience with its huge variety of products from
                    numerous categories, making it a one stop shop for all daily needs. We are proud to announce that
                    DailyCliQ today is the only e-commerce platform which features a huge variety of indigenous products
                    from the God’s Own Country Kerala, and we are featuring sellers exclusively from Kerala and there by
                    truly standing by our tagline of the only Regional Shopping Portal in India. Join DailyCliQ today and be a
                    part of the beginning of a brand new era in the e-commerce industry.
                </p>
            </div><!-- wd-500 -->
        </div>
    </div><!-- row -->

    <script src="{{asset('app/lib/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('app/lib/jquery-ui/ui/widgets/datepicker.js')}}"></script>
    <script src="{{asset('app/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate.js')}}"></script>
    <script>
        function passwordshow() {
            var x = document.getElementById("password");
            if (x.type === "password") {
                x.type = "text";
                $('#eyeId').removeClass('ion-eye');
                $('#eyeId').addClass('ion-eye-disabled');
            } else {
                x.type = "password";
                $('#eyeId').removeClass('ion-eye-disabled');
                $('#eyeId').addClass('ion-eye')
            }
        }
    </script>
    {{-- <script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
    <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
    <script>
    jQuery.validator.addMethod("passwordCheck",
            function(value, element, param) {
                if (this.optional(element)) {
                    return true;
                } else if (!/[A-Z]/.test(value)) {
                    return false;
                } else if (!/[a-z]/.test(value)) {
                    return false;
                } else if (!/[0-9]/.test(value)) {
                    return false;
                }
                return true;
            },
            "error msg here");
    </script> --}}
    <script>
        $("#signUpForm").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            rules: {
                contact_person: {
                    required: true,
                },
                mobile: {
                    required: true,
                    number: true,
                    maxlength: 12,
                    minlength: 12
                },
                email: {
                    required: true,
                    email: true
                },
                address: {
                    required: true
                },
                pin: {
                    required: true
                },
                password: {
                    required: true,
                    minlength: 8
                }
            },
            messages: {
                contact_person: {
                    required: 'Your name is required'
                },
                mobile: {
                    required: 'Mobile number is required',
                    number: 'Invalid mobile number',
                    maxlength: 'Invalid mobile number',
                    minlength: 'Invalid mobile number'

                },
                email: {
                    required: 'Email is  required',
                    email: 'Email is invalid'
                },
                address: {
                    required: 'Address is  required'
                },
                pin: {
                    required: 'Pin Code is  required'
                },
                password: {
                    required: 'Password is required',
                    minlength: 'Require minimum 8 characters'
                }
            },
            submitHandler: function(form) {
                var form = document.getElementById('signUpForm');
                var data = new FormData(form);
                $('.msgDiv').html(``);
                $.ajax({
                    type: "POST",
                    url: "{{route('signUpVendor')}}",
                    data: $(form).serialize(),
                    dataType: "json",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        if (data.status == 1) {
                            window.location.href = "{{route('vendorDashboard')}}";
                        } else if (data.status == 2) {
                            var html = "";
                            $.each(data.errors, function(key, value) {
                                html += value + "<br/>";
                            });
                            $('.msgDiv').html(` <div class="dispError alert alert-danger">` + html + `</div> `);
                            setTimeout(function() {
                                $('.msgDiv').html(``);
                            }, 2000);;
                        } else {
                            var html = "";
                            $.each(data.errors, function(key, value) {
                                html += value + "<br/>";
                            });
                            $('.msgDiv').html(` <div class="dispError alert alert-danger">` + html + `</div> `);
                            setTimeout(function() {
                                $('.msgDiv').html(``);
                            }, 6000);
                        }
                    }
                });
                return false;
            }
        });
    </script>
    <script>
        function showOtpField() {
            $('#otpMobile').addClass('d-none');
            $('#submitOtp').addClass('d-none');
            $('#otp').removeClass('d-none');
            $('#resendOtp').removeClass('d-none');
            $('#verifyOtp').removeClass('d-none');
        };

        function resendOtp() {
            $('#otpMobile').removeClass('d-none');
            $('#submitOtp').removeClass('d-none');
            $('#otp').addClass('d-none');
            $('#resendOtp').addClass('d-none');
            $('#verifyOtp').addClass('d-none');
        };

        function hideOtpForm() {
            $('#otpForm').addClass('d-none');
            $('#signUpForm').removeClass('d-none');
        }

        $('#otpForm').find('#submitOtp').on('click', function() {
            var form = $('#otpForm');
            var mobile = $(form).find('input[name="mobile"]').val();
            if (mobile) {
                if (mobile.match(/^[a-z0-9]+$/i)) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('sendOtp')}}",
                        data: {
                            mobile
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == true) {
                                showOtpField();
                            }
                        }
                    });
                } else {
                    $('.msgDiv').html(` <div class="dispError alert alert-danger">Mobile number is invalid</div> `);
                    setTimeout(function() {
                        $('.msgDiv').html(``);
                    }, 6000);
                }
            } else {
                $('.msgDiv').html(` <div class="dispError alert alert-danger">Enter mobile number</div> `);
                setTimeout(function() {
                    $('.msgDiv').html(``);
                }, 6000);
            }
        });

        $('#otpForm').find('#verifyOtp').on('click', function() {
            var form = $('#otpForm');
            var mobile = $(form).find('input[name="mobile"]').val();
            var otp = $(form).find('input[name="otp"]').val();
            if (mobile && otp) {
                if (mobile.match(/^[a-z0-9]+$/i)) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('verifyOtp')}}",
                        data: {
                            mobile,
                            otp
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == true) {
                                showOtpField();
                                $('#signUpForm').find('input[name="mobile"]').val(mobile);
                                hideOtpForm();
                            } else {
                                $('.msgDiv').html(` <div class="dispError alert alert-danger">Invalid OTP</div> `);
                                setTimeout(function() {
                                    $('.msgDiv').html(``);
                                }, 6000);
                            }
                        }
                    });
                } else {
                    $('.msgDiv').html(` <div class="dispError alert alert-danger">Mobile number is invalid</div> `);
                    setTimeout(function() {
                        $('.msgDiv').html(``);
                    }, 6000);
                }
            } else {
                $('.msgDiv').html(` <div class="dispError alert alert-danger">Enter OTP</div> `);
                setTimeout(function() {
                    $('.msgDiv').html(``);
                }, 6000);
            }
        });
    </script>
    <script>
        function forgotPassword() {
            $('#loginDiv').hide();
            $("#forgotDiv").slideDown(500);
        }

        function loginForm() {
            $('#forgotDiv').hide();
            $("#loginDiv").slideDown(500);
        }
    </script>

</body>

</html>