@extends('vendors.layouts.web')
@section('content')
    <!-- ########## START: MAIN PANEL ########## -->

    <!-- ########## SALES REPORT ########## -->
      <div class="br-pagebody pd-y-15 pd-l-20 pd-x-20 pd-sm-x-30" style="margin-top:90px;">
        <div class="row no-gutters widget-1 shadow-base">
          <div class="col-sm-6 col-lg-3">
            <div class="card">
              <div class="card-header">
                <h6 class="card-title">Today's Sales</h6>
               
              </div><!-- card-header -->
              <div class="card-body">
                <span id="spark1">5,3,9,6,5,9,7,3,5,2</span>
                <span><i class="fa fa-inr" aria-hidden="true"></i>{{number_format($today[0]->total)}}</span>
              </div><!-- card-body -->
            </div><!-- card -->
          </div><!-- col-3 -->
          <div class="col-sm-6 col-lg-3 mg-t-1 mg-sm-t-0">
            <div class="card">
              <div class="card-header">
                <h6 class="card-title">This Week's Sales</h6>
                
              </div><!-- card-header -->
              <div class="card-body">
                <span id="spark2">2,8,7,8,2,6,5,3,5,2</span>
                <span class="tx-medium tx-inverse tx-32"><i class="fa fa-inr" aria-hidden="true"></i>{{number_format($week[0]->total)}}</span>
              </div><!-- card-body -->
            </div><!-- card -->
          </div><!-- col-3 -->
          <div class="col-sm-6 col-lg-3 mg-t-1 mg-lg-t-0">
            <div class="card">
              <div class="card-header">
                <h6 class="card-title">This Month's Sales</h6>
                
              </div><!-- card-header -->
              <div class="card-body">
                <span id="spark3">8,6,5,9,8,4,9,3,5,9</span>
                <span><i class="fa fa-rupee" aria-hidden="true"></i>{{number_format($month[0]->total)}}</span>
              </div><!-- card-body -->
            </div><!-- card -->
          </div><!-- col-3 -->
          <div class="col-sm-6 col-lg-3 mg-t-1 mg-lg-t-0">
            <div class="card">
              <div class="card-header">
                <h6 class="card-title">Overall Sales</h6>
               
              </div><!-- card-header -->
              <div class="card-body">
                <span id="spark4">8,3,9,6,3,7,1,3,8,5</span>
                <span><i class="fa fa-inr" aria-hidden="true"></i>{{number_format($sale[0]->total)}}</span>
              </div><!-- card-body -->
            </div><!-- card -->
          </div><!-- col-3 -->
        </div><!-- row -->
            <!-- ########## ORDER REPORT ########## -->
        <div class="row no-gutters widget-1 shadow-base" style="margin-top:20px;">
          <div class="col-sm-6 col-lg-3" style="background-color:green;">
            <div class="card btn-success">
              <div class="card-header">
                <h6 class="card-title">Completed Orders</h6>
               
              </div><!-- card-header -->
              <div class="card-body">
                <span>{{$completed[0]->count}}</span>
              </div><!-- card-body -->
            </div><!-- card -->
          </div><!-- col-3 -->
          <div class="col-sm-6 col-lg-3 mg-t-1 mg-sm-t-0">
            <div class="card">
              <div class="card-header">
                <h6 class="card-title">Pending Orders</h6>
               
              </div><!-- card-header -->
              <div class="card-body">
                <span class="tx-medium tx-inverse tx-32">{{$pending[0]->count}}</span>
              </div><!-- card-body -->
            </div><!-- card -->
          </div><!-- col-3 -->
          <div class="col-sm-6 col-lg-3 mg-t-1 mg-lg-t-0">
            <div class="card">
              <div class="card-header">
                <h6 class="card-title">Ready To Pickup</h6>
                
              </div><!-- card-header -->
              <div class="card-body">
                <span>{{$pending[0]->count}}</span>
              </div><!-- card-body -->
            </div><!-- card -->
          </div><!-- col-3 -->
          <div class="col-sm-6 col-lg-3 mg-t-1 mg-lg-t-0">
            <div class="card">
              <div class="card-header">
                <h6 class="card-title">Canceled/Refunded</h6>
               
              </div><!-- card-header -->
              <div class="card-body">
                <span>{{$cancelled[0]->count}}</span>
              </div><!-- card-body -->
            </div><!-- card -->
          </div><!-- col-3 -->
        </div><!-- row -->
        <!-- ########## PRODUCT REPORT ########## -->
        <div class="row no-gutters widget-1 shadow-base" style="margin-top:20px;">
          <div class="col-sm-6 col-lg-3" style="background-color:green;">
            <div class="card btn-success">
              <div class="card-header">
                <h6 class="card-title">Active Products</h6>
                
              </div><!-- card-header -->
              <div class="card-body">
                <span>{{$activelisting[0]->count}}</span>
              </div><!-- card-body -->
            </div><!-- card -->
          </div><!-- col-3 -->
          <div class="col-sm-6 col-lg-3 mg-t-1 mg-sm-t-0">
            <div class="card">
              <div class="card-header">
                <h6 class="card-title">Inactive Products</h6>
               
              </div><!-- card-header -->
              <div class="card-body">
                <span class="tx-medium tx-inverse tx-32">{{$pendinglisting[0]->count}}</span>
              </div><!-- card-body -->
            </div><!-- card -->
          </div><!-- col-3 -->
          <div class="col-sm-6 col-lg-3 mg-t-1 mg-lg-t-0">
            <div class="card">
              <div class="card-header">
                <h6 class="card-title">Limited Stock</h6>
                
              </div><!-- card-header -->
              <div class="card-body">
                <span>{{$limitedstock[0]->count}}</span>
              </div><!-- card-body -->
            </div><!-- card -->
          </div><!-- col-3 -->
          <div class="col-sm-6 col-lg-3 mg-t-1 mg-lg-t-0">
            <div class="card">
              <div class="card-header">
                <h6 class="card-title">Out of Stock</h6>
               
              </div><!-- card-header -->
              <div class="card-body">
                <span>{{$outofstock[0]->count}}</span>
              </div><!-- card-body -->
            </div><!-- card -->
          </div><!-- col-3 -->
        </div><!-- row -->


        <div class="row row-sm mg-t-20" style="margin-top:20px;">
          <div class="col-lg-6">
            <div class="card shadow-base bd-0">
              <div class="card-header bg-transparent pd-20">
                <h6 class="card-title tx-uppercase tx-12 mg-b-0">Most selling 5 products</h6>
              </div><!-- card-header -->
              <table class="table table-responsive mg-b-0 tx-12">
                <thead>
                  <tr class="tx-10">
                    <th class="pd-y-5">#</th>
                    <th></th>
                    <th class="pd-y-5">Product</th>
                    <th></th>
                    <th class="pd-y-5">Stock</th>
                    <th></th>
                    <th class="pd-y-5">Price</th>
                    <th></th>
                    <th class="pd-y-5">Commision</th>
                  </tr>
                </thead>
                <tbody>
                @php
                $i= 0;
                @endphp
                @foreach($most as $key=>$item)
                  <tr>
                     <td>{{++$i}}</td>
                     <td></td>
                    <td>
                      <a href="" class="tx-inverse tx-14 tx-medium d-block">{{$item->product[0]->productlist->name}}</a>
                      <span class="tx-11 d-block">DCIN:{{$item->product[0]->productlist->dcin}}</span>
                    </td>
                    <td></td>
                    <td class="tx-12">
                      {{$item->product[0]->productlist->stock_unit}}
                    </td>
                    <td></td>
                    <td class="tx-12">
                      {{number_format($item->product[0]->productlist->price)}}
                    </td>
                    <td></td>
                    <td class="tx-12">
                      {{number_format($item->product[0]->productlist->commission)}} %
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
             <!-- card-footer -->
            </div><!-- card -->
          </div><!-- col-6 -->
          <div class="col-lg-6 mg-t-20 mg-lg-t-0">
            <div class="card shadow-base bd-0">
              <div class="card-header pd-20 bg-transparent">
                <h6 class="card-title tx-uppercase tx-12 mg-b-0">Last 5 Orders</h6>
              </div><!-- card-header -->
              <table class="table table-responsive mg-b-0 tx-12">
                <thead>
                  <tr class="tx-10">
                    <th class="wd-10p pd-y-5">#</th>
                    <th></th>
                    <th class="wd-10p pd-y-5">Order Ref.No</th>
                    <th class="wd-10p pd-y-5"></th>
                    <th class="pd-y-5 tx-right">Total</th>
                    <th class="pd-x-10">Status</th>
                  </tr>
                </thead>
                <tbody>
                    @php
                        $i= 0;
                    @endphp
                    @foreach($most as $item)
                  <tr class="tx-10">
                    <td class="wd-10p pd-y-5">{{++$i}}</td>
                    <th></th>
                    <td>
                      <a href="#" class="tx-inverse tx-14 tx-medium d-block">{{$item->order_reference_number}}</a>
                    </td>
                    <td></td>
                    <td class="valign-middle tx-right">{{number_format($item->total)}}</td>
                    <td class="valign-middle"><span class="tx-success">
                    @if($item->status=='1'){{'Completed'}}@endif
                    @if($item->status=='2'){{'Pending'}}@endif
                    @if($item->status=='3'){{'Refunded'}}@endif
                    @if($item->status=='4'){{'Failed'}}@endif
                    @if($item->status=='5'){{'Canceled'}}@endif
                    </span></td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
             <!-- card-footer -->
            </div><!-- card -->
          </div><!-- col-6 -->
        </div>  
      </div><!-- br-pagebody -->
    <!-- ########## END: MAIN PANEL ########## -->

@endsection
@section('scripts')
<script>
    $('#dashboardLi').find('a').addClass('active');
</script>
@endsection
