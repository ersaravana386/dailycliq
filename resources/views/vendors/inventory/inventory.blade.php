@extends('vendors.layouts.web')
@section('content')
<div class="br-pagebody">
  <div class="container-fluid pt-2">
    <div class="row row-sm justify-content-center new">
      <div class="col-12 col-sm-10 col-md-12 col-lg-12">
        <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">Inventory Stock Update</h4> 
        <div class="card bg-white border-0 shadow-sm py-5 px-4">
          <div class="item">
            <form class="inset row" action="javascript:;" id="mainForm">
              <div class="col-lg-12 msgDiv" role="alert">
              
              </div><!-- alert -->
                <div class="col-lg-3">
                    <div class="form-group pd-5">
                      <label class="form-control-label" for="unit">Available Stock: <span class="tx-danger">*</span></label>
                      <input class="form-control form-control-sm" type="text" value='@if(is_null($products[0]->stock_unit)){{'0'}}@else{{$products[0]->stock_unit}} @endif' name="available" id="available"  placeholder="Available Unit" readonly>
                    </div>
                  </div><!-- col-6 -->
                <div class="col-lg-3">
                    <div class="form-group pd-5">
                      <label class="form-control-label" for="unit">Unit: <span class="tx-danger">*</span></label>
                      <input class="form-control form-control-sm" type="number" value='' name="unit" id="unit"  placeholder="Enter Unit">
                    </div>
                  </div><!-- col-6 -->
                  <div class="col-lg-3">
                    <div class="form-group pd-5">
                      <label class="form-control-label" for="process">Process: <span class="tx-danger">*</span></label>
                      <select class="form-control form-control-sm select22" name="process" id="process">
                          <option value='1'>In Stock</option>
                          <option value='2'>Out Stock</option>
                          <option value='2'>Purchase</option>
                      </select>
                    </div>
                  </div><!-- col-6 -->
             
                <div class="col-lg-3">
                  <div class="form-group pd-5">
                    <input class="btn btn-primary btn-sm mt-md-4 mb-sm-0" name="save" type="submit" value="Save"/>
                  </div>
                </div>
            </form>
          </div><!-- card -->
        </div>
      </div>
    </div><!-- row -->
  </div>
</div><!-- br-pagebody -->
<div class="br-pagebody">
    <div class="container-fluid pt-2">
        <div class="row row-sm justify-content-center new">
            <div class="col-12 col-sm-10 col-md-12 col-lg-12">
                <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">Inventory Listing</h4> 
                <div class="card bg-white border-0 shadow-sm py-5 px-4">
                    <div class="row">
                       <div class="col-md-8"></div>
                        <div class="pull-right col-md-4">
                            <form class="form-inline my-2 my-lg-0 flex-nowrap w-100 w-md-75 ">
                                <input class="form-control form-control-sm mr-2 has-border w-100" type="search" placeholder="Search" aria-label="Search" name="search" value="{{$search}}">
                                <button class="btn btn-primary btn-sm button-inner my-2 my-sm-0" type="submit">Search</button>
                            </form>
                        </div>
                    </div>
                    <div class="bd bd-gray-300 rounded table-responsive">
                    <table class="table mg-b-0">
                            <thead>
                                <tr>
                                    <th class="wd-5p">#</th>
                                    <th class="wd-15p">PROCESS</th>
                                    <th class="wd-15p">UNIT</th>
                                    <th class="wd-15p">PROCESS TYPE</th>
                                    <th class="wd-15p">CREATED DATE</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $i= 0;
                            @endphp
                            @if(count($inventory)>0)
                            @foreach ($inventory ?? '' as $key=> $item)
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                <td scope="row">
                                    {{$item->process}}
                                </td>
                                <td scope="row">{{$item->unit}}</td>
                                <td scope="row">
                                @if($item->process_id ==1)
                                  {{'In Stock'}}
                                @elseif($item->process_id ==2)
                                {{'Out Stock'}}
                                @elseif($item->process_id ==3)
                                {{'Purchase'}}
                                 @endif
                                </td>
                                </td>
                                <td scope="row">{{date('d M  Y',strtotime($item->created_at))}}</td>
                            </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td scope="row" colspan=10><b><center><span style="color:#DC3545;">Sorry No Result Found</span></center><b></td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        @if(count($inventory)>0)
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                        {{ $inventory ?? ''->appends(['search' => $search])->links() }}
                        </div>
                        @endif  
                    </div>
                </div>
                <!-- bd -->
            </div>
        </div>
    </div><!-- row -->
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
<script>
  $("input[name='search']").autocomplete({
    source: function(request, response) {
      $.ajax({
        url: "{{route('showProduct',['product'=>$products[0]->id])}}",
        dataType: "json",
        data: {
          q: request.term
        },
        success: function(data) {
          response(data);
        }
      });
    },
    minLength: 3,
    select: function(event, ui) {
      // log( ui.item ?
      //   "Selected: " + ui.item.label :
      //   "Nothing selected, input was " + this.value);
    },
    open: function() {
      $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
    },
    close: function() {
      $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
    }
  });
</script>
<script>
  
      $("#mainForm").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        rules: {
          unit:{
           required:true,
           number:true,
          },
          process:{
           required:true,
           number:true,
            },
        },
        messages: {
          unit:{
            required: "Unit is required",
          },
          process:{
              required: "Process is required",
          },
         
        },
        submitHandler: function(form) {
          $('.msgDiv').html(``);
          var data = new FormData(form);
          $.ajax({
            type: "post",
            url: "{{route('saveInventory',['product'=>$products[0]->id,'vendor'=>$products[0]->vendor_id])}}",
            data: data,
            dataType: "json",
            processData: false,
            contentType: false,
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
              if (data.status == 1) {
                  if(data.redirect){
                    window.location.href = data.redirect;
                  }
              } else  if (data.status == 0) {
                  var html = "";
                  $.each(data.errors, function(key, value) {
                      html += value + "<br/>";
                  });
                  $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                  setTimeout(function() {
                      $('.msgDiv').html(``);
                  }, 4000);
              }
            }
          });
          return false;
        }
    });
</script>
@endsection
