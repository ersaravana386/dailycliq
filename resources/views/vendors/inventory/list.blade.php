@extends('vendors.layouts.web')
@section('content')
<div class="br-pagebody">
    <div class="container-fluid pt-2">
        <div class="row row-sm justify-content-center new">
            <div class="col-12 col-sm-10 col-md-12 col-lg-12">
                <h4 class="tx-13 tx-uppercase tx-white tx-medium tx-spacing-1 mb-4">Inventory Listing</h4> 
                <div class="card bg-white border-0 shadow-sm py-5 px-4">
                    <div class="row">
                       <div class="col-md-8"></div>
                        <div class="pull-right col-md-4">
                            <form class="form-inline my-2 my-lg-0 flex-nowrap w-100 w-md-75 ">
                                <input class="form-control form-control-sm mr-2 has-border w-100" type="search" placeholder="Search" aria-label="Search" name="search" value="{{$search}}">
                                <button class="btn btn-primary btn-sm button-inner my-2 my-sm-0" type="submit">Search</button>
                            </form>
                        </div>
                    </div>
                    <div class="bd bd-gray-300 rounded table-responsive">
                        <table class="table mg-b-0">
                            <thead>
                                <tr>
                                    <th class="wd-5p">#</th>
                                    <th class="wd-15p">STOCK</th>
                                    <th class="wd-15p">NAME</th>
                                    <th class="wd-15p">DCIN</th>
                                    <th class="wd-15p">Image</th>
                                    <th class="wd-15p">PRICE</th>
                                    <th class="wd-15p">MRP</th>
                                    <th class="wd-15p">BRAND</th>
                                    <th class="wd-15p">CATEGORY</th>
                                    <th class="wd-15p">Comments</th>
                                    <th class="wd-20p">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $i= 0;
                            @endphp
                            @if(count($products)>0)
                            @foreach ($products as $key=> $item)
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                <td scope="row">
                                    <p class="badge badge-info mg-b-10">@if($item->stock_unit!=''){{$item->stock_unit}}@else{{'0'}}@endif</p>
                                </td>
                                <td scope="row">
                                {{$item->name}} <br><label class="badge badge-success">Approved</label>
                                </td>
                                <td scope="row">{{$item->dcin}}</td>
                                <td>
                                    @if($item->images->first())
                                    @if(file_exists($item->images->first()->image))
                                    <a target="_blank" href="{{asset($item->images->first()->image)}}"><img src="{{asset($item->images->first()->image)}}" style="width:80px;"></a>
                                    @endif
                                    @endif
                                </td>
                                <td scope="row">{{$item->price}}</td>
                                <td scope="row">{{$item->mrp}}</td>
                                <td scope="row"> 
                                    @if($item->brand)
                                    {{$item->brand->name}}
                                    @else
                                    {{$item->other_brand}}
                                    @endif</td>
                                <td scope="row">{{$item->category->name}}</td>
                                <td scope="row">
                                    no comments
                                </td>
                                <td scope="row">
                                    <a class="btn btn-sm btn-success" onclick="return confirm('Are you sure you want to delete this product?');" href="{{route('deleteVendorApprovedProduct',['product'=>$item->id])}}" title="delete product">
                                            <i class="fa fa-trash"></i>
                                    </a>
                                    <a class="btn btn-sm btn-success" href="{{route('showProduct',['product'=>$item->id])}}" title="manage stock">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td scope="row" colspan=10><b><center><span style="color:#DC3545;">Sorry No Result Found</span></center><b></td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        @if(count($products)>0)
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate">
                        {{ $products->appends(['search' => $search])->links() }}
                        </div>
                        @endif
                    </div>
                </div>
                <!-- bd -->
            </div>
        </div>
    </div><!-- row -->
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
<script>
  $("input[name='search']").autocomplete({
    source: function(request, response) {
      $.ajax({
        url: "{{route('searchProduct')}}",
        dataType: "json",
        data: {
          q: request.term
        },
        success: function(data) {
          response(data);
        }
      });
    },
    minLength: 3,
    select: function(event, ui) {
      // log( ui.item ?
      //   "Selected: " + ui.item.label :
      //   "Nothing selected, input was " + this.value);
    },
    open: function() {
      $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
    },
    close: function() {
      $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
    }
  });
</script>
<script>
    $('#inventoryId').find('a').addClass('active');
</script>
@endsection
