<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket Plus">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels"> --}}

    <title>Daily CliQ</title>

    <!-- vendor css -->
    <link href="{{asset('app/lib/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('app/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{asset('app/css/bracket.css')}}">
  </head>

  <body>
    <div class="row no-gutters flex-row-reverse ht-100v">
        <div class="col-md-6 bg-gray-200 d-flex align-items-center justify-content-center">

          <form role="form" method="post" action="javascript:;" id="ressetForm">
            <div class="login-wrapper wd-250 wd-xl-350 mg-y-30" >
              <h4 class="tx-inverse tx-center">Reset your password</h4>
              <p class="tx-center mg-b-40">Welcome back!</p>
              <div class="form-group">
                <input type="password" class="form-control" name="new_password" id="new_password" placeholder="Enter your new password">
              </div><!-- form-group -->
              <div class="form-group">
                <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Re-enter your password">
                <input type="hidden" value=<?php echo $email; ?> class="form-control" name="email" id="emai" >
            </div><!-- form-group -->
              <button type="submit" class="btn btn-info btn-block" id="resetButton">Submit</button>
              <div class="msgDiv mg-t-20"></div>
            </div><!-- login-wrapper -->
            
          </form>
        
        </div><!-- col -->
        <div class="col-md-6 bg-br-primary d-flex align-items-center justify-content-center">
          <div class="wd-250 wd-xl-450 mg-y-30">
            <div class="signin-logo tx-28 tx-bold tx-white"><span class="tx-normal">[</span> Daily <span class="tx-info">CliQ</span> <span class="tx-normal">]</span></div>
            <div class="tx-white mg-b-60">Online shopping site</div>

            <h5 class="tx-white">Why Daily CliQ?</h5>
            <p class="tx-white-6">When it comes to websites or apps, one of the first impression you consider is the design. It needs to be high quality enough otherwise you will lose potential users due to bad design.</p>
            <p class="tx-white-6 mg-b-60">When your website or app is attractive to use, your users will not simply be using it, they’ll look forward to using it. This means that you should fashion the look and feel of your interface for your users.</p>
          <a href="{{route('loginVendor')}}" class="btn btn-outline-light bd bd-white bd-2 tx-white pd-x-25 tx-uppercase tx-12 tx-spacing-2 tx-medium">Explore website</a>
          </div><!-- wd-500 -->
        </div>
    </div><!-- row -->

    <script src="{{asset('app/lib/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('app/lib/jquery-ui/ui/widgets/datepicker.js')}}"></script>
    <script src="{{asset('app/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate.js')}}"></script>
    <script>
        $("#ressetForm").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            rules: {
                new_password: {
                  required: true,
                  minlength: 6
                },
                confirm_password: {
                  required: true,
                  equalTo : "#new_password"
                }
            },
            messages: {
                new_password: {
                  required  : 'New password is  required',
                  minlength : 'Password should contain min. 6 characters'
                },
                confirm_password: {
                  required: 'Confirm password is required',
                  equalTo : 'Confirm password must be same as new password'
                }
            },
            submitHandler: function(form) {
                $('#resetButton').prop('disabled', 'disabled');
                var form = document.getElementById('ressetForm');
                var data = new FormData(form);
                $('.msgDiv').html(``);
                $.ajax({
                    type: "POST",
                    url: "{{route('vendorChangePassword')}}",
                    data: $(form).serialize(),
                    dataType: "json",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        if (data.status == 1) {
                            $('#new_password').val('');
                            $('#confirm_password').val('');
                            $('.msgDiv').html(` <div class="dispSuccess">` + data.message + `</div> `);
                          setTimeout(function() {
                            window.location.href = "{{route('loginVendor')}}";
                          }, 2000);
                          $('#resetButton').prop('disabled', false);
                        } else {
                            var html = "";
                            $.each(data.errors, function(key, value) {
                                html += value + "<br/>";
                            });
                            $('.msgDiv').html(` <div class="dispError">` + html + `</div> `);
                            setTimeout(function() {
                                $('.msgDiv').html(``);
                            }, 4000);
                            $('#resetButton').prop('disabled', false);
                        }
                    }
                });
                return false;
            }
        });
    </script>
    <script>
        function forgotPassword(){
            $('#loginDiv').hide();
            $('#forgotDiv').show();
        }
        function loginForm(){
            $('#forgotDiv').hide();
            $('#loginDiv').show();
        }
    </script>
  </body>
</html>
