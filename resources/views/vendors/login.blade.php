<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {{-- <!-- Twitter -->
        <meta name="twitter:site" content="@themepixels">
        <meta name="twitter:creator" content="@themepixels">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="Bracket Plus">
        <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
        <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

        <!-- Facebook -->
        <meta property="og:url" content="http://themepixels.me/bracketplus">
        <meta property="og:title" content="Bracket Plus">
        <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

        <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
        <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1200">
        <meta property="og:image:height" content="600">

        <!-- Meta -->
        <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
        <meta name="author" content="ThemePixels"> --}}

        <title>Daily CliQ</title>

        <!-- vendor css -->
        <link href="{{asset('app/lib/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
        <link href="{{asset('app/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">

        <!-- Bracket CSS -->
        <link rel="stylesheet" href="{{asset('app/css/bracket.css')}}">
        <link rel="stylesheet" href="{{asset('app/css/style.css')}}">
    </head>

    <body>
        <div class="row no-gutters flex-row-reverse h-100-vh">
            <div class="col-md-12 col-lg-6 bg-gray-200 d-flex align-items-center justify-content-center has-background-image login z-index-1">
                <div class="has-shade login-right"></div>
                <form class="login-pad px-0" role="form" method="post" action="javascript:;" id="loginForm">
                    <div class="login-wrapper text-white wd-350" id="loginDiv">
                        <h4 class="tx-inverse tx-center">Sign In</h4>
                        <p class="tx-center mg-b-60">Welcome to DailyCliQ Vendor Panel.</p>
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" placeholder="Enter your mail Id" required>
                        </div><!-- form-group -->
                        <div class="form-group mb-3">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Enter your password" required>
                            <a href="javascript:;" onclick="passwordshow()">
                                <div class="ion-eye eye" id="eyeId"></div>
                            </a>
                        </div><!-- form-group -->
                        <a href="javascript:;" onClick="forgotPassword()" class="tx-info tx-12 d-block mb-3">Forgot password?</a>
                        <button class="btn btn-primary btn-sm button-inner my-2 my-sm-0 btn-block" type="submit">
                            Sign In</button>
                        <div class="msgDiv mt-3"></div>
                        <div class="mt-3 tx-center"><a href="{{route('showSignUp')}}" class="tx-info">New Sellers Register Now.</a></div>
                    </div><!-- login-wrapper -->
                </form>

                <form role="form" method="post" action="javascript:;" id="forgotPassword">
                    <div class="login-wrapper text-white wd-350" id="forgotDiv" style="display:none">
                        <h4 class="tx-inverse tx-center">Forgot Password</h4>
                        <p class="tx-center mg-b-60">Please enter the registered email</p>
                        <div class="form-group">
                            <input type="text" class="form-control" name="email3" id="email3" placeholder="Enter your email" required>
                        </div><!-- form-group -->
                        <button type="submit" class="btn btn-block btn btn-primary btn-sm button-inner my-2 my-sm-0" id="forgotButton">Submit</button>
                        <div class="msgDiv2 mg-t-20"></div>
                        <div class="mt-3 tx-center">
                            <a href="javascript:;" onClick="loginForm()" class="tx-info mt-2">Back to login</a>
                        </div>
                    </div><!-- login-wrapper -->
                </form>

            </div><!-- col -->
            <div class="col-md-12 col-lg-6 bg-br-primary d-flex align-items-center justify-content-center ">
                <div class="has-shade login"></div>
                <div class="wd-100 wd-xl-450 text-white login-pad">
                    <div class="logo pl-md-0 pb-0">
                        <img src="{{asset('app/img/logo.png')}}" class="img-fluid">
                    </div>
                    <h5 class="text-dark mt-4">Why Daily CliQ?</h5>
                    <p class="text-dark line-height-normal">
                        E-Commerce today is a way of life and an extremely integral part of modern day shopping. DailyCliQ
                        presents in front of you, a unique online shopping experience with its huge variety of products from
                        numerous categories, making it a one stop shop for all daily needs. We are proud to announce that
                        DailyCliQ today is the only e-commerce platform which features a huge variety of indigenous products
                        from the God’s Own Country Kerala, and we are featuring sellers exclusively from Kerala and there by
                        truly standing by our tagline of the only Regional Shopping Portal in India. Join DailyCliQ today and be a
                        part of the beginning of a brand new era in the e-commerce industry.
                    </p>
                </div><!-- wd-500 -->
            </div>
        </div><!-- row -->

        <script src="{{asset('app/lib/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('app/lib/jquery-ui/ui/widgets/datepicker.js')}}"></script>
        <script src="{{asset('app/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('js/jquery.validate.js')}}"></script>
        <script>
                                function passwordshow() {
                                    var x = document.getElementById("password");
                                    if (x.type === "password") {
                                        x.type = "text";
                                        $('#eyeId').removeClass('ion-eye');
                                        $('#eyeId').addClass('ion-eye-disabled');
                                    } else {
                                        x.type = "password";
                                        $('#eyeId').removeClass('ion-eye-disabled');
                                        $('#eyeId').addClass('ion-eye')
                                    }
                                }
        </script>
        <script>
            $("#loginForm").validate({
                normalizer: function (value) {
                    return $.trim(value);
                },
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    }
                },
                messages: {
                    email: {
                        required: 'Email is  required',
                        email: 'Email is invalid'
                    },
                    password: {
                        required: 'Password is required'
                    }
                },
                submitHandler: function (form) {
                    var form = document.getElementById('loginForm');
                    var data = new FormData(form);
                    $('.msgDiv').html(``);
                    $.ajax({
                        type: "POST",
                        url: "{{route('loginVendor')}}",
                        data: $(form).serialize(),
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (data) {
                            if (data.status == 1) {
                                window.location.href = "{{route('vendorDashboard')}}";
                            } else if (data.status == 2) {
                                var html = "";
                                $.each(data.errors, function (key, value) {
                                    html += value + "<br/>";
                                });
                                $('.msgDiv').html(` <div class="dispError alert alert-danger">` + html + `</div> `);
                                setTimeout(function () {
                                    $('.msgDiv').html(``);
                                    window.location.href = "{{route('vendorLogout')}}"
                                }, 2000);
                                ;
                            } else {
                                var html = "";
                                $.each(data.errors, function (key, value) {
                                    html += value + "<br/>";
                                });
                                $('.msgDiv').html(` <div class="dispError alert alert-danger">` + html + `</div> `);
                                setTimeout(function () {
                                    $('.msgDiv').html(``);
                                }, 6000);
                            }
                        }
                    });
                    return false;
                }
            });
        </script>
        <script>
            function forgotPassword() {
                $('#loginDiv').hide();
                $("#forgotDiv").slideDown(500);
            }
            function loginForm() {
                $('#forgotDiv').hide();
                $("#loginDiv").slideDown(500);
            }
        </script>
        <script>
            $("#forgotPassword").validate({
                normalizer: function (value) {
                    return $.trim(value);
                },
                rules: {
                    email3: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    email3: {
                        required: 'Email is  required',
                        email: 'Email is invalid'
                    }
                },
                submitHandler: function (form) {
                    $('#forgotButton').prop('disabled', 'disabled');
                    var form = document.getElementById('forgotPassword');
                    var data = new FormData(form);
                    $('.msgDiv').html(``);
                    $.ajax({
                        type: "POST",
                        url: "{{route('vendorForgotPassword')}}",
                        data: $(form).serialize(),
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (data) {
                            if (data.status == 1) {
                                $('#email3').val('');
                                $('.msgDiv2').html(` <div class="dispSuccess">` + data.message + `</div> `);
                                setTimeout(function () {
                                    $('.msgDiv2').html(``);
                                }, 3000);
                            } else {
                                var html = "";
                                $.each(data.errors, function (key, value) {
                                    html += value + "<br/>";
                                });
                                $('.msgDiv2').html(` <div class="dispError">` + html + `</div> `);
                                setTimeout(function () {
                                    $('.msgDiv2').html(``);
                                }, 3000);
                            }
                            $('#forgotButton').prop('disabled', false);
                        }
                    });
                    return false;
                }
            });
        </script>
    </body>
</html>
