@extends('vendors.layouts.web')
@section('content')
<div class="br-pageheader">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
  <a class="breadcrumb-item" href="{{route('vendorDashboard')}}">Home</a>
    <span class="breadcrumb-item active">profile</span>
  </nav>
</div><!-- br-pageheader -->

<div class="br-pagebody">
    <div class="br-section-wrapper">
      <h6 class="br-section-label">Edit Profile</h6><br>
      <form id='vendorForm' method="POST" action="javascript:;" data-parsley-validate>
          @method('POST')
        <div class="form-layout form-layout-1">
          <div class="row mg-b-25">
            
            <div class="col-lg-6 ">
              <div class="form-group pd-5">
                <label class="form-control-label">Company name: <span class="tx-danger">*</span></label>
              <input class="form-control" type="text" value='{{$vendor->company_name}}' name="company_name" id="company_name"  placeholder="Enter company name">
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-6">
              <div class="form-group pd-5">
                <label class="form-control-label">Contact person: <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" value="{{$vendor->contact_person}}" name="contact_person" id="contact_person" placeholder="Enter contact person name">
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-6">
                <div class="form-group pd-5">
                  <label class="form-control-label">Address: <span class="tx-danger">*</span></label>
                  <textarea rows="3" class="form-control" name="address" id="address" placeholder="Enter address">{{$vendor->address}}
                  </textarea>
                </div>
              </div><!-- col-4 -->
            <div class="col-lg-6">
              <div class="form-group pd-5">
                <label class="form-control-label">Email: <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="email" id="email" value="{{$vendor->email}}" placeholder="Enter email address">
              </div>
            </div><!-- col-8 -->
            <div class="col-lg-6">
              <div class="form-group pd-5">
                <label class="form-control-label">Mobile: <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="mobile" id="mobile" value="{{$vendor->mobile}}" placeholder="Enter mobile">
              </div>
            </div><!-- col-4 -->
            <div class="col-lg-6">
              <div class="form-group pd-5">
                <label class="form-control-label">Phone: <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="phone" id="phone" value="{{$vendor->phone}}" placeholder="Enter phone">
              </div>
            </div><!-- col-4 -->
            
              <div class="col-lg-6">
                <div class="form-group pd-5">
                  <label class="form-control-label">Trade license number: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="trade_license_number" value="{{$vendor->trade_license_number}}" id="trade_license_number" placeholder="Enter trade license number">
                </div>
              </div><!-- col-4 -->
              
              <div class="col-md-6">
                <div class="form-group pd-5">
                  <label class="form-control-label">Trade license document: <span class="tx-danger">*</span></label>
                  <a href="{{asset($vendor->trade_license_document)}}">
                    <img src="{{asset($vendor->trade_license_document)}}" class="img-fluid wd-100" alt="">
                  </a>
                  <label for="trade_license_document" class="if-outline if-outline-info"><i class="icon ion-ios-upload-outline tx-24"></i>
                  <input type="file" name="trade_license_document" id="trade_license_document" class="inputfile" >
                  <span id="trade_license_document_label">Change file</span>
                  </label>
                </div>
              </div><!-- col-4-->
              <div class="col-lg-6">
                <div class="form-group pd-5">
                  <label class="form-control-label">GST Number: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="gst_number" value="{{$vendor->gst_number}}" id="gst_number" placeholder="Enter GST number">
                </div>
              </div><!-- col-4 -->
              <div class="col-md-6">
                <div class="form-group pd-5">
                  <label class="form-control-label">GST Document: <span class="tx-danger">*</span></label>
                  <a href="{{asset($vendor->gst_document)}}">
                    <img src="{{asset($vendor->gst_document)}}" class="img-fluid wd-100" alt="">
                  </a>
                  <label for="gst_document" class="if-outline if-outline-info"><i class="icon ion-ios-upload-outline tx-24"></i>
                  <input type="file" name="gst_document" id="gst_document" class="inputfile" >
                  <span id="gst_document_label">Change file</span>
                  </label>
                </div>
              </div><!-- col-4-->
              <div class="col-md-6">
                <div class="form-group pd-5">
                  <label class="form-control-label">Vendor Image: <span class="tx-danger">*</span></label>
                  <a href="{{asset($vendor->image)}}">
                    <img src="{{asset($vendor->image)}}" class="img-fluid wd-100" alt="">
                  </a>
                  <label for="image" class="if-outline if-outline-info"><i class="icon ion-ios-upload-outline tx-24"></i>
                  <input type="file" name="image" id="image" class="inputfile" >
                  <span id="image_label">Change file</span>
                  </label></div>
              </div><!-- col-4-->
          </div><!-- row -->

          <div class="form-layout-footer">
          <input type="hidden" name="code" value="{{$vendor->code}}">
            <input type="submit" class="btn btn-info mg-b-10 " value="Update">
            <a class="btn btn-secondary mg-b-10 " href="{{route('vendorDashboard')}}">Cancel</a>
          </div><!-- form-layout-footer -->
          <div class="msgDiv"></div>
        </div><!-- form-layout -->
      </form>

     
    </div><!-- br-section-wrapper -->
  </div><!-- br-pagebody -->
@endsection
@section('scripts')
<script>
    //profile form
    $("#vendorForm").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        rules: {
            company_name: {
              required: true
            },
            contact_person: {
              required: true
            },
            address:{
              required: true
            },
            email: {
              required: true,
              email: true
            },
            mobile: {
              required: true
            },
            trade_license_number: {
              required: true
            },
            gst_number: {
              required: true
            }
        },
        messages: {
            company_name: {
              required: "Company name is required"
            },
            contact_person: {
              required: "Contact person name is required"
            },
            address:{
              required: "Address is required"
            },
            email: {
              required: "Email is required",
              email: "Email is not valid"
            },
            mobile: {
              required: "Mobile is required"
            },
            trade_license_number: {
              required: "Trade license number is required"
            },
            gst_number: {
              required: "GST number is required"
            }
        },
        submitHandler: function(form) {
            $('.msgDiv').html(``);
            var data = new FormData(form);
            $.ajax({
                type: "post",
                url: "{{route('updateVendorProfile')}}",
                data: data,
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.status == 1) {
                        
                        $('.msgDiv').html(` <div class="alert alert-success">` + data.message + `</div>`);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                            location.href = "{{route('vendorDashboard')}}";
                        }, 2000);

                    } else {
                        var html = "";
                        $.each(data.errors, function(key, value) {
                            html += value + "<br/>";
                        });
                        $('.msgDiv').html(`<div class="alert alert-danger m-2">` + html + `</div>`);
                        setTimeout(function() {
                            $('.msgDiv').html(``);
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        }, 4000);
                    }
                }
            });
            return false;
        }
    });
</script>
<script>
    $('#trade_license_document').on('change', function() {
    var fileName = $(this).val();
    //replace the "Choose a file" label
    $('#trade_license_document_label').html(fileName);
    })
    $('#gst_document').on('change', function() {
    var fileName = $(this).val();
    //replace the "Choose a file" label
    $('#gst_document_label').html(fileName);
    })
    $('#image').on('change', function() {
    var fileName = $(this).val();
    //replace the "Choose a file" label
    $('#image_label').html(fileName);
    })
</script>
@endsection