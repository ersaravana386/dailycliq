<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket Plus">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels"> --}}

    <title>Daily CliQ</title>

    <!-- vendor css -->
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400,700&display=swap" rel="stylesheet">
    <link href="{{asset('app/lib/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('app/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{asset('app/css/bracket.css')}}">
    <link rel="stylesheet" href="{{asset('app/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('app/css/style.css')}}">
  </head>

  <body>
    <div class="row no-gutters flex-row-reverse h-100-vh has-background-image login">
            <div class="has-shade login-right z-index-1"></div>
        <div class="col-md-6 bg-gray-200 d-flex align-items-center justify-content-center z-index-1">
          <form role="form" method="post" action="javascript:;" id="loginForm">
            <div class="login-wrapper text-white wd-350 edit-profile" id="loginDiv">
              <h4 class="tx-inverse tx-center">Your profile</h4>
              <p class="tx-center mb-0">Review your details.</p>
              <button type="button" class="btn btn-link p-0 mt-2" id="edit-profile">Edit Profile</button>
              <div class="profile-container">
              <div class="form-group">
                  <label class="mb-1 d-inline-block">Fullname</label>
                <input type="text" class="form-control" name="name" placeholder="Enter your username" required>
              </div><!-- form-group -->
              <div class="form-group">
                    <label class="mb-1 d-inline-block">Email</label>
                  <input type="email" class="form-control" name="email" placeholder="Enter your email address" required>
                </div><!-- form-group -->
                <div class="form-group">
                        <label class="mb-1 d-inline-block">Mobile</label>
                      <input type="text" class="form-control" name="mobile" placeholder="Enter your phone number" required>
                    </div><!-- form-group -->
                    <div class="form-group">
                            <label class="mb-1 d-inline-block">Address</label>
                          <textarea type="textarea" class="form-control" rows="4" name="mobile" placeholder="Enter your phone number" required></textarea>
                        </div><!-- form-group -->
                        <button class="btn btn-reverse btn-arrow mt-1 float-right d-block">
                                <span>Submit<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 36.1 25.8" enable-background="new 0 0 36.1 25.8" xml:space="preserve"><g><line fill="none" stroke="#F49917" stroke-width="3" stroke-miterlimit="10" x1="0" y1="12.9" x2="34" y2="12.9"></line><polyline fill="none" stroke="#F49917" stroke-width="3" stroke-miterlimit="10" points="22.2,1.1 34,12.9 22.2,24.7   "></polyline></g></svg></span>
                              </button>
                    </div>
              <div class="msgDiv mg-t-20"></div>
            </div><!-- login-wrapper -->
          </form>

          <form role="form" method="post" action="javascript:;" id="forgotPassword">
            <div class="login-wrapper wd-250 wd-xl-350 mg-y-30 text-white" id="forgotDiv" style="display:none">
                    <h4 class="tx-inverse tx-center">Forgot Password</h4>
                    <p class="tx-center mg-b-60">Please enter the registered email</p>
                    <div class="form-group">
                      <input type="text" class="form-control" name="email3" id="email3" placeholder="Enter your email" required>
                    </div><!-- form-group -->
                    <button type="submit" class="btn btn-warning btn-block" id="forgotButton">Submit</button>
                    <div class="msgDiv2 mg-t-20"></div>
                    <div class="mg-t-60 tx-center">
                        <a href="javascript:;" onClick="loginForm()" class="tx-info">Back to login</a>
                    </div>
            </div><!-- login-wrapper -->
          </form>
        </div><!-- col -->

        <div class="col-md-6 bg-br-primary d-flex align-items-center flex-column justify-content-center z-index-1 landing-divider">
            <div class="form-container position-relative">
          <div class="wd-250 wd-xl-450 text-white mb-60">
          <h4 class="text-white mt-4 fira-sans font-weight-bold mb-0">Choose your</h4>
            <h2 class="text-white display-4 fira-sans font-weight-bold mb-2 mt-0">Store name</h2>
            <input class="form-control br-0" placeholder="Eg: My Store" type="text">
            <small class="text-white px-1 mt-2 d-inline-block" style="background:sienna">This is not reversible</small>
          </div><!-- wd-500 -->

          <div class="wd-250 wd-xl-450 text-white">
                <h4 class="text-white mt-4 fira-sans font-weight-bold mb-0">Choose your</h4>
                  <h2 class="text-white display-4 fira-sans font-weight-bold mb-2 mt-0">Location</h2>
                  <input class="form-control br-0" placeholder="Input box" type="text">
                </div><!-- wd-500 -->


                <select class="selectpicker" multiple>
  <option>Mustard</option>
  <option>Ketchup</option>
  <option>Relish</option>
</select>

                <button class="btn btn-reverse btn-arrow mt-5 float-right d-block">
                        <span>Submit<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 36.1 25.8" enable-background="new 0 0 36.1 25.8" xml:space="preserve"><g><line fill="none" stroke="#F49917" stroke-width="3" stroke-miterlimit="10" x1="0" y1="12.9" x2="34" y2="12.9"></line><polyline fill="none" stroke="#F49917" stroke-width="3" stroke-miterlimit="10" points="22.2,1.1 34,12.9 22.2,24.7   "></polyline></g></svg></span>
                      </button>
            </div>
            <div class="h-divider"></div>   
        </div>
    </div><!-- row -->

    <script src="{{asset('app/lib/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('app/js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset('app/lib/jquery-ui/ui/widgets/datepicker.js')}}"></script>
    <script src="{{asset('app/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate.js')}}"></script>
    <script src="{{asset('js/index.js')}}"></script>
   
    <script>
        $("#loginForm").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            rules: {
                email: {
                  required: true,
                  email: true
                },
                password: {
                  required: true
                }
            },
            messages: {
                email: {
                  required  : 'Email is  required',
                  email : 'Email is invalid'
                },
                password: {
                  required: 'Password is required'
                }
            },
            submitHandler: function(form) {
                var form = document.getElementById('loginForm');
                var data = new FormData(form);
                $('.msgDiv').html(``);
                $.ajax({
                    type: "POST",
                    url: "{{route('loginVendor')}}",
                    data: $(form).serialize(),
                    dataType: "json",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        if (data.status == 1) {
                            window.location.href = "{{route('vendorDashboard')}}";
                        }else if (data.status == 2) {
                            var html = "";
                            $.each(data.errors, function(key, value) {
                                html += value + "<br/>";
                            });
                            $('.msgDiv').html(` <div class="dispError alert alert-danger">` + html + `</div> `);
                            setTimeout(function() {
                                $('.msgDiv').html(``);
                                window.location.href = "{{route('vendorLogout')}}"
                            }, 2000);
                            ;
                        }  else {
                            var html = "";
                            $.each(data.errors, function(key, value) {
                                html += value + "<br/>";
                            });
                            $('.msgDiv').html(` <div class="dispError alert alert-danger">` + html + `</div> `);
                            setTimeout(function() {
                                $('.msgDiv').html(``);
                            }, 6000);
                        }
                    }
                });
                return false;
            }
        });
    </script>
    <script>
        function forgotPassword(){
            $('#loginDiv').hide();
            $("#forgotDiv").slideDown(500);
        }
        function loginForm(){
            $('#forgotDiv').hide();
            $("#loginDiv").slideDown(500);
        }
    </script>
    <script>
      $("#forgotPassword").validate({
          normalizer: function(value) {
              return $.trim(value);
          },
          rules: {
              email3: {
                  required: true,
                  email: true
              }
          },
          messages: {
              email3: {
                  required: 'Email is  required',
                  email: 'Email is invalid'
              }
          },
          submitHandler: function(form) {
              $('#forgotButton').prop('disabled', 'disabled');
              var form = document.getElementById('forgotPassword');
              var data = new FormData(form);
              $('.msgDiv').html(``);
              $.ajax({
                  type: "POST",
                  url: "{{route('vendorForgotPassword')}}",
                  data: $(form).serialize(),
                  dataType: "json",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  success: function(data) {
                      if (data.status == 1) {
                          $('#email3').val('');
                          $('.msgDiv2').html(` <div class="dispSuccess">` + data.message + `</div> `);
                          setTimeout(function() {
                              $('.msgDiv2').html(``);
                          }, 3000);
                      } else {
                          var html = "";
                          $.each(data.errors, function(key, value) {
                              html += value + "<br/>";
                          });
                          $('.msgDiv2').html(` <div class="dispError">` + html + `</div> `);
                          setTimeout(function() {
                              $('.msgDiv2').html(``);
                          }, 3000);
                      }
                      $('#forgotButton').prop('disabled', false);
                  }
              });
              return false;
          }
      });
  </script>
  </body>
</html>
