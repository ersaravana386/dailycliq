<!-- ajax loader -->
<div id="ajaxLoader" class="modal fade effect-scale">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="col-lg-12 rounded-left">
            <div class="col-md-12 col-xl-12 mg-t-30 mg-xl-t-0">
                <div class="d-flex  ht-300 pos-relative align-items-center">
                    <div class="sk-wave">
                        <div class="sk-rect sk-rect1 bg-gray-800"></div>
                        <div class="sk-rect sk-rect2 bg-gray-800"></div>
                        <div class="sk-rect sk-rect3 bg-gray-800"></div>
                        <div class="sk-rect sk-rect4 bg-gray-800"></div>
                        <div class="sk-rect sk-rect5 bg-gray-800"></div>
                    </div>
                </div><!-- d-flex -->
            </div><!-- col-4 -->
        </div><!-- row -->
    </div><!-- modal-dialog -->
</div><!-- modal -->