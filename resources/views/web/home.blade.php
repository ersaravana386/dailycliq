
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    {{-- <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket Plus">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels"> --}}

    <title>Daily CliQ</title>


    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{asset('app/css/bracket.css')}}">
    <link rel="stylesheet" href="{{asset('app/css/style.css')}}">
  </head>

  <body>
          <div class="container-fluid">
            <div class="home d-flex align-items-start align-items-md-center justify-content-center ht-100v">
              <div class="has-shade home"></div>
                <img src="{{asset('app/img/img24.jpeg')}}" class="wd-100p ht-100p img-fluid d-none d-lg-block" alt="">
            <div class="overlay-body d-flex align-items-center justify-content-center">
              <div class="d-md-flex align-items-center">
                <div>
                  <h5 class="tx-sm-32 text-dark text-center">Go online shopping with</h5>
                  <div class="logo-brand mb-3 mt-3">
                    <img src="app/img/logo-small.png" class="mg-b-20 mg-lg-b-30 img-fluid" alt="daily cliq">
                  </div>
                  <!-- <h2 class="tx-bold font-700 text-white">Daily <span class="tx-normal tx-info">CliQ</span></h2> -->
                  <h6 class="text-dark text-center">India's first regional online shopping</h6>
                  <div class="divider div-transparent div-dot"></div>

                  <div class="d-flex align-items-center jce mt-4">
                    <a href="javascript:;"  class="btn btn-oblong bd-2 pd-x-25 mr-2">
                      <button class="btn btn-primary btn-sm button-inner my-2 my-sm-0" type="submit">
                        Explore site</button>
                    </a>
                    <a href="{{route('showLogin')}}"  class="btn btn-oblong bd-2 pd-x-25">
                      <button class="btn btn-primary btn-sm button-inner my-2 my-sm-0" type="submit">Vendor login</button>
                    </a>
                  </div><!-- d-flex -->
                </div>
              </div><!-- col -->
          </div>
        </div>
      </div>

    <script src="{{asset('app/lib/jquery/jquery.js')}}"></script>
    <script src="{{asset('app/lib/popper.js/popper.js')}}"></script>
    <script src="{{asset('app/lib/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{asset('app/lib/parsleyjs/parsley.js')}}"></script>
    <script>
      $(function(){
        'use strict';

        function checkScroll() {
          if($(document).scrollTop() > 80) {
            $('#headPanel').addClass('scroll');
          } else {
            $('#headPanel').removeClass('scroll');
          }
        }

        // sticky header effect
        checkScroll();
        $(window).scroll(function() {
          checkScroll();
        });

        // animated smooth scroll on target from top menu
        $('#navbarMain .nav-link').on('click', function(e){

          $('#navbarMain').addClass('hidden-md-down');

          var target = $(this).attr('href');
          $('html, body').animate({
            scrollTop: $(''+target).offset().top
          }, 500);

          e.preventDefault();
        });


        // customer support form submission
        $('#helpRequestForm').on('submit', function(e){
          e.preventDefault();

          // Initiate Variables With Form Content
          var fullname = $('#fullname').val();
          var email = $('#email').val();
          var topic = $('#topic').val();
          var message = $('#message').val();
          var action = $(this).attr('action');

          $.ajax({
            type: 'POST',
            url: action,
            data: 'fullname=' + fullname + '&email=' + email + '&topic=' + topic + '&message=' + message,
            success : function(state){
              if (state == 'success'){
                $('#msgSuccess').removeClass('d-none');
              }
            }
          });
        });

        $('#showMenu').on('click', function(e){
          e.preventDefault();
          $('#navbarMain').toggleClass('hidden-md-down');
        });


      });
    </script>

    <!-- Hotjar Tracking Code for themepixels.me -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:821333,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>

  </body>
  </html>
