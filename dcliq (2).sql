-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 11, 2020 at 01:36 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dcliq`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '/profiles/default.png',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `mobile`, `email_verified_at`, `password`, `image`, `remember_token`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Saleesh', 'admin@gmail.com', '9747282318', NULL, '$2y$12$DUYvPXmfP9aqoAM0tbEH7ur2gkVw3BBltiyOMeC0BCO5/nEUN.eNS', '/profiles/default.png', NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `name`, `logo`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Size', NULL, 1, NULL, NULL),
(2, 'Material', NULL, 1, NULL, NULL),
(3, 'Length', NULL, 1, NULL, NULL),
(4, 'Fit', NULL, 1, NULL, NULL),
(5, 'Sleeve type', NULL, 1, NULL, NULL),
(6, 'Men\'s Shirt Collar Type', NULL, 1, NULL, NULL),
(7, 'Type', NULL, 1, NULL, NULL),
(8, 'Pattern', NULL, 1, NULL, NULL),
(9, 'Watch Movement', NULL, 1, NULL, NULL),
(10, 'Dial Shape', NULL, 1, NULL, NULL),
(11, 'Dial Color', NULL, 1, NULL, NULL),
(12, 'Strap material', NULL, 1, NULL, NULL),
(13, 'Features', NULL, 1, NULL, NULL),
(14, 'Themes', NULL, 1, NULL, NULL),
(15, 'Style', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_values`
--

CREATE TABLE `attribute_values` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_values`
--

INSERT INTO `attribute_values` (`id`, `attribute_id`, `value`) VALUES
(1, 1, 'Free size'),
(2, 1, 'Universal'),
(3, 1, '5XS'),
(4, 1, '4XS'),
(5, 1, '3XS'),
(6, 1, '2XS'),
(7, 1, 'XS'),
(8, 1, 'S'),
(9, 1, 'M'),
(10, 1, 'L'),
(11, 1, 'XL'),
(12, 1, 'XXL'),
(13, 1, '2XL'),
(14, 1, '3XL'),
(15, 1, '4XL'),
(16, 1, '5XL'),
(17, 1, '6XL'),
(18, 1, '7XL'),
(19, 1, '8XL'),
(20, 1, 'M Regular'),
(21, 1, 'L Regular'),
(22, 1, 'XL Regular'),
(23, 1, 'S Regular'),
(24, 1, 'XS Regular'),
(25, 1, 'XXL Regular'),
(26, 1, '18'),
(27, 1, '20'),
(28, 1, '22'),
(29, 1, '24'),
(30, 1, '26'),
(31, 1, '27'),
(32, 1, '28'),
(33, 1, '29'),
(34, 1, '30'),
(35, 1, '31'),
(36, 1, '32'),
(37, 1, '33'),
(38, 1, '34'),
(39, 1, '35'),
(40, 1, '36'),
(41, 1, '38'),
(42, 1, '39'),
(43, 1, '40'),
(44, 1, '42'),
(45, 1, '44'),
(46, 1, '46'),
(47, 1, '48'),
(48, 1, '50'),
(49, 1, '52'),
(50, 1, '54'),
(51, 1, '58'),
(52, 1, '60'),
(53, 1, '70'),
(54, 1, '75'),
(55, 1, '80'),
(56, 1, '85'),
(57, 1, '90'),
(58, 1, '95'),
(59, 1, '100'),
(60, 1, '105'),
(61, 1, '115'),
(62, 1, 'XS - 38'),
(63, 1, '85 cm'),
(64, 1, '80 cm'),
(65, 1, '90 cm'),
(66, 1, '95 cm'),
(67, 1, '100 cm'),
(68, 1, '110 cm'),
(69, 1, '70-75 cm'),
(70, 1, '80-85 cm'),
(71, 1, '90-95 cm'),
(72, 1, '95-100 cm'),
(73, 1, '100-105 cm'),
(74, 1, '105-110 cm'),
(75, 1, '110-115 cm'),
(76, 1, '0 -1 Month'),
(77, 1, '0 - 3 Months'),
(78, 1, '3 - 6 Months'),
(79, 1, '6 - 12 Months'),
(80, 1, '12 - 18 Months'),
(81, 1, '12 - 24 Months'),
(82, 1, '1 - 2 Years'),
(83, 1, '2 - 3 Years'),
(84, 1, '3 - 4 Years'),
(85, 1, '4 - 5 Years'),
(86, 1, '2 - 5 Years'),
(87, 1, '5 - 8 Years'),
(88, 1, '8 - 11 Year'),
(89, 1, '11 - 15 Years'),
(90, 1, '14 - 15 Years'),
(91, 1, '15 - 16 Years'),
(92, 1, '2'),
(93, 1, '4'),
(94, 1, '6'),
(95, 1, '8'),
(96, 1, '8A'),
(97, 1, '8B'),
(98, 1, '8C'),
(99, 1, '8D'),
(100, 1, '8E'),
(101, 1, '10A'),
(102, 1, '10B'),
(103, 1, '10C'),
(104, 1, '10D'),
(105, 1, '10E'),
(106, 1, '12A'),
(107, 1, '12B'),
(108, 1, '12C'),
(109, 1, '12D'),
(110, 1, '12E'),
(111, 1, '14A'),
(112, 1, '14B'),
(113, 1, '14C'),
(114, 1, '14D'),
(115, 1, '14E'),
(116, 1, '16A'),
(117, 1, '16B'),
(118, 1, '16C'),
(119, 1, '16D'),
(120, 1, '16E'),
(121, 1, '18A'),
(122, 1, '18B'),
(123, 1, '18C'),
(124, 1, '18D'),
(125, 1, '18E'),
(126, 1, '28A'),
(127, 1, '28B'),
(128, 1, '28C'),
(129, 1, '28D'),
(130, 1, '28DD'),
(131, 1, '28E'),
(132, 1, '28Z'),
(133, 1, '30A'),
(134, 1, '30B'),
(135, 1, '30C'),
(136, 1, '30D'),
(137, 1, '30DD'),
(138, 1, '30E'),
(139, 1, '30Z'),
(140, 1, '32A'),
(141, 1, '32B'),
(142, 1, '32C'),
(143, 1, '32D'),
(144, 1, '32DD'),
(145, 1, '32E'),
(146, 1, '32F'),
(147, 1, '32G'),
(148, 1, '32Z'),
(149, 1, '34A'),
(150, 1, '34B'),
(151, 1, '34C'),
(152, 1, '34D'),
(153, 1, '34DD'),
(154, 1, '34E'),
(155, 1, '34F'),
(156, 1, '34G'),
(157, 1, '34Z'),
(158, 1, '36A'),
(159, 1, '36B'),
(160, 1, '36C'),
(161, 1, '36D'),
(162, 1, '36DD'),
(163, 1, '36E'),
(164, 1, '36F'),
(165, 1, '36G'),
(166, 1, '36Z'),
(167, 1, '38A'),
(168, 1, '38B'),
(169, 1, '38C'),
(170, 1, '38D'),
(171, 1, '38DD'),
(172, 1, '38E'),
(173, 1, '38F'),
(174, 1, '38FF'),
(175, 1, '38G'),
(176, 1, '38Z'),
(177, 1, '40A'),
(178, 1, '40B'),
(179, 1, '40C'),
(180, 1, '40D'),
(181, 1, '40DD'),
(182, 1, '40E'),
(183, 1, '40F'),
(184, 1, '40FF'),
(185, 1, '40G'),
(186, 1, '40Z'),
(187, 1, '42A'),
(188, 1, '42B'),
(189, 1, '42C'),
(190, 1, '42D'),
(191, 1, '42DD'),
(192, 1, '42E'),
(193, 1, '42F'),
(194, 1, '42Z'),
(195, 1, '44A'),
(196, 1, '44B'),
(197, 1, '44C'),
(198, 1, '44D'),
(199, 1, '44DD'),
(200, 1, '44E'),
(201, 1, '44F'),
(202, 1, '44Z'),
(203, 1, '46B'),
(204, 1, '46C'),
(205, 1, '46D'),
(206, 1, '46DD'),
(207, 1, '46E'),
(208, 1, '46F'),
(209, 1, '46G'),
(210, 1, '46Z'),
(211, 1, '50A'),
(212, 1, '50B'),
(213, 1, '50C'),
(214, 1, '50D'),
(215, 1, '50DD'),
(216, 1, '50E'),
(217, 1, '50F'),
(218, 1, '50G'),
(219, 1, '50Z'),
(220, 1, '52A'),
(221, 1, '52B'),
(222, 1, '52C'),
(223, 1, '52D'),
(224, 1, '52DD'),
(225, 1, '52E'),
(226, 1, '52F'),
(227, 1, '52G'),
(228, 1, '52Z'),
(229, 1, '1 - 5 meters'),
(230, 1, '21X21'),
(231, 1, '11 X 11 INCHES'),
(232, 1, '10 X 10'),
(233, 1, '3'),
(234, 1, '4.5'),
(235, 1, '5'),
(236, 1, '5.5'),
(237, 1, '6.5'),
(238, 1, '7'),
(239, 1, '7.5'),
(240, 1, '8.5'),
(241, 1, '9'),
(242, 1, '9.5'),
(243, 1, '10'),
(244, 1, '10.5'),
(245, 1, '11'),
(246, 1, '11.5'),
(247, 1, '12'),
(248, 1, '12.5'),
(249, 1, '13'),
(250, 1, '3.5'),
(251, 2, 'Rayon'),
(252, 2, 'Leather'),
(253, 2, 'Cotton'),
(254, 2, 'Polyester'),
(255, 2, 'Mesh'),
(256, 2, 'Wool'),
(257, 2, 'Corduroy'),
(258, 2, 'Spandex'),
(259, 2, 'Satin'),
(260, 2, 'Linen'),
(261, 2, 'Denim'),
(262, 2, 'Rubber'),
(263, 2, 'Woolen'),
(264, 2, 'Synthetic'),
(265, 2, 'Nylon'),
(266, 2, 'Felt'),
(267, 2, 'Woolen Blending'),
(268, 2, 'Washed Cotton'),
(269, 2, 'Fleece'),
(270, 2, 'Knit'),
(271, 2, 'Silk'),
(272, 2, 'Straw'),
(273, 2, 'Plus Velvet'),
(274, 2, 'Canvas'),
(275, 2, 'Quick-drying Fabric'),
(276, 2, 'Lycra'),
(277, 2, 'Chiffon'),
(278, 2, 'Cotton/Elastane'),
(279, 2, 'Cotton/Lycra'),
(280, 2, 'Elastane'),
(281, 2, 'Georgette'),
(282, 2, 'Lace'),
(283, 2, 'Microfibre'),
(284, 2, 'Modal'),
(285, 2, 'Neoprene'),
(286, 2, 'Nylon/Spandex'),
(287, 2, 'Poly/Spandex'),
(288, 2, 'Polyamide'),
(289, 2, 'Polycotton'),
(290, 2, 'Viscose'),
(291, 2, 'Acrylic & Resin'),
(292, 2, 'Bamboo'),
(293, 2, 'Banarasi'),
(294, 2, 'Bemberg'),
(295, 2, 'Blended Fabric'),
(296, 2, 'Brocade'),
(297, 2, 'Chanderi'),
(298, 2, 'Cotton Lurex'),
(299, 2, 'Cotton Silk'),
(300, 2, 'Crepe'),
(301, 2, 'Faux Fur'),
(302, 2, 'Jamdhani'),
(303, 2, 'Jersey'),
(304, 2, 'Jute'),
(305, 2, 'Khadi'),
(306, 2, 'Kota'),
(307, 2, 'Lyocell'),
(308, 2, 'Mul'),
(309, 2, 'Net'),
(310, 2, 'Organic'),
(311, 2, 'Organza'),
(312, 2, 'Poplin'),
(313, 2, 'Raw Silk'),
(314, 2, 'Sequin'),
(315, 2, 'Fabric'),
(316, 2, 'ABS'),
(317, 2, 'Acrylic'),
(318, 2, 'Alloy'),
(319, 2, 'Aluminum'),
(320, 2, 'Bone'),
(321, 2, 'Brass'),
(322, 2, 'Bronze'),
(323, 2, 'Ceramic'),
(324, 2, 'Cobalt'),
(325, 2, 'Copper'),
(326, 2, 'Cotton Dori'),
(327, 2, 'Crystal'),
(328, 2, 'Enamel'),
(329, 2, 'Fibre'),
(330, 2, 'German Silver'),
(331, 2, 'Glass'),
(332, 2, 'Gold'),
(333, 2, 'Ivory'),
(334, 2, 'Lac'),
(335, 2, 'Metal'),
(336, 2, 'Mother of Pearl'),
(337, 2, 'Nickel'),
(338, 2, 'Oxidised Silver'),
(339, 2, 'Paddy'),
(340, 2, 'Paper'),
(341, 2, 'Plastic'),
(342, 2, 'Porcelain'),
(343, 2, 'Resin'),
(344, 2, 'Ribbon'),
(345, 2, 'Rose Gold'),
(346, 2, 'Shell'),
(347, 2, 'Silicone'),
(348, 2, 'Silk Dori'),
(349, 2, 'Silver'),
(350, 2, 'Stainless Steel'),
(351, 2, 'Steel'),
(352, 2, 'Sterling Silver'),
(353, 2, 'Stone'),
(354, 2, 'Terracotta'),
(355, 2, 'Titanium'),
(356, 2, 'Tungsten'),
(357, 2, 'White Gold'),
(358, 2, 'White Metal'),
(359, 2, 'Wood'),
(360, 2, 'Yellow Gold'),
(361, 2, 'Zinc'),
(362, 2, 'Aluminium'),
(363, 2, 'Artificial Leather'),
(364, 2, 'Carbon Fibre'),
(365, 2, 'Genuine Leather'),
(366, 2, 'Tyvek'),
(367, 2, 'Acetate'),
(368, 2, 'CR 39'),
(369, 2, 'Mineral Glass'),
(370, 2, 'Polycarbonate'),
(371, 2, 'TAC Resin'),
(372, 2, 'Others'),
(373, 2, 'Stainless steel'),
(374, 2, 'TR 90'),
(375, 2, 'Wooden'),
(376, 3, 'Long'),
(377, 3, 'Short'),
(378, 3, 'Regular'),
(379, 3, 'Mid Long'),
(380, 3, 'Full length'),
(381, 3, 'Below Knee'),
(382, 3, 'Midi/Calf Length'),
(383, 3, 'Mini/Short'),
(384, 3, 'Above Knee/Mid Thigh Length'),
(385, 3, 'Maxi/Full Length'),
(386, 3, 'Knee Length'),
(387, 4, 'Loose Fit'),
(388, 4, 'Regular Fit'),
(389, 4, 'Slim Fit'),
(390, 4, 'Skin Fit'),
(391, 5, '3/4 Sleeve'),
(392, 5, 'Cap Sleeve'),
(393, 5, 'Half Sleeve'),
(394, 5, 'Long Sleeve'),
(395, 5, 'Short Sleeve'),
(396, 5, 'Sleeveless'),
(397, 6, 'Classic'),
(398, 6, 'Cutaway '),
(399, 6, 'Bottom Down '),
(400, 6, 'Round Collar'),
(401, 7, '2GO Men Tights'),
(402, 7, 'Amor Ribbed Inner El.Brief Asstd & Wht '),
(403, 7, 'Ankle Length '),
(404, 7, 'Asymmetric Neck'),
(405, 7, 'Baggy Shorts'),
(406, 7, 'Bandhgala'),
(407, 7, 'Basic'),
(408, 7, 'Basic Brief'),
(409, 7, 'Basic Shorts'),
(410, 7, 'Beach Shorts'),
(411, 7, 'Bermuda Shorts'),
(412, 7, 'Bikini Brief'),
(413, 7, 'Board/Swim Shorts'),
(414, 7, 'Boat Neck'),
(415, 7, 'Body Shaper'),
(416, 7, 'Bold Brief'),
(417, 7, 'Bottom Full Tight Cpmpression'),
(418, 7, 'Boxer Brief'),
(419, 7, 'Artificial Leather'),
(420, 7, 'Silicone'),
(421, 7, 'Acrylic'),
(422, 7, 'Boxer Shorts'),
(423, 7, 'Boyshort'),
(424, 7, 'Brief'),
(425, 7, 'Brief Men'),
(426, 7, 'Brief Thong'),
(427, 7, 'Brief Underwear'),
(428, 7, 'Briefs'),
(429, 7, 'Briefs,Innerwear'),
(430, 7, 'Brif'),
(431, 7, 'Bullet Black Shirt Stud'),
(432, 7, 'Business Wear'),
(433, 7, 'Button Down'),
(434, 7, 'Calf Length'),
(435, 7, 'Cargo Shorts'),
(436, 7, 'Casual Trousers'),
(437, 7, 'Ckecks Inner Elastic Ultra Soften Modern Brief'),
(438, 7, 'Chino Shorts'),
(439, 7, 'Chinos'),
(440, 7, 'Classic Brief'),
(441, 7, 'Collared Neck'),
(442, 7, 'Compression Full Tight Pant'),
(443, 7, 'Compression Pants'),
(444, 7, 'Compression Shorts'),
(445, 7, 'Compression Tights'),
(446, 7, 'C-Open Brief'),
(447, 7, 'Corset'),
(448, 7, 'Cowl Neck'),
(449, 7, 'Cropped Trousers'),
(450, 7, 'Cufflink'),
(451, 7, 'Cufflink & Tie Pin Set'),
(452, 7, 'Cufflink Set'),
(453, 7, 'Cycling Shorts'),
(454, 7, 'Dastar'),
(455, 7, 'Denim Shorts'),
(456, 7, 'Designer'),
(457, 7, 'Dolphin Shorts'),
(458, 7, 'Double Breasted'),
(459, 7, 'Double Collared Neck'),
(460, 7, 'Ethnic Jacket and Kurtha Set'),
(461, 7, 'Ethnic Jacket and Pyjama Set'),
(462, 7, 'Ethnic Jacket, Kurtha and Churidhar Set'),
(463, 7, 'Ethnic Jacket, Kurtha and Dhoti Pant Set'),
(464, 7, 'Ethnic Jacket, Kurtha and Trouser Set'),
(465, 7, 'Evening'),
(466, 7, 'Fancy Scarf'),
(467, 7, 'Fline Brief'),
(468, 7, 'Footie Socks'),
(469, 7, 'Formals'),
(470, 7, 'Formal Pant'),
(471, 7, 'Formal Security Guard Trouser'),
(472, 7, 'Formal Trouser'),
(473, 7, 'Full Length'),
(474, 7, 'G String'),
(475, 7, 'Gym Shorts'),
(476, 7, 'Gym Supporter'),
(477, 7, 'Gym Vest'),
(478, 7, 'Gym West,Cycling,Running,Swimming'),
(479, 7, 'Halter Neck'),
(480, 7, 'Henley Neck'),
(481, 7, 'High Neck'),
(482, 7, 'Hip Brief'),
(483, 7, 'Hipster Brief'),
(484, 7, 'Hooded'),
(485, 7, 'Hooded Neck'),
(486, 7, 'IC15 Torero Premium Brief Trendy Contra'),
(487, 7, 'Jacket Fabric'),
(488, 7, 'Jockstrap'),
(489, 7, 'Jodhpuris'),
(490, 7, 'Joggers'),
(491, 7, 'Knee High'),
(492, 7, 'Kurtha and Churidhar Set'),
(493, 7, 'Kurtha and Dhoti Pant Set'),
(494, 7, 'Kurtha and Patiala Set'),
(495, 7, 'Kurtha and Pyjama Set'),
(496, 7, 'Kurtha and Trouser Set'),
(497, 7, 'Kurtha,Churidhar & Dupatta Set'),
(498, 7, 'Kurtha,Ethnic Jacket & Pyjama Set'),
(499, 7, 'Kurtha,Waistcoat & Pyjama Set'),
(500, 7, 'Long Brief'),
(501, 7, 'Long Trunk'),
(502, 7, 'Low Cut'),
(503, 7, 'Low Rise Brief'),
(504, 7, 'Luxury Brief'),
(505, 7, 'Maharaji'),
(506, 7, 'Mandarin'),
(507, 7, 'Mandarin Collar'),
(508, 7, 'Maternity Shapewear'),
(509, 7, 'Men Brief'),
(510, 7, 'Mens Innerwear and Sleepwear'),
(511, 7, 'Mercerised Vest'),
(512, 7, 'Mid Rise Brief'),
(513, 7, 'Mid Rise Briefs'),
(514, 7, 'Mid-Calf/Crew'),
(515, 7, 'Midway Brief'),
(516, 7, 'Mini Trunk'),
(517, 7, 'MINI TRUNK PRINT'),
(518, 7, 'Model Style'),
(519, 7, 'Modern Brief'),
(520, 7, 'MPDEL'),
(521, 7, 'Night Shorts'),
(522, 7, 'Over the Knee'),
(523, 7, 'Pagri'),
(524, 7, 'Pants'),
(525, 7, 'Parallel Trousers'),
(526, 7, 'Pathani Suit Set'),
(527, 7, 'Peds/Footie/No-Show'),
(528, 7, 'Peg Trouser'),
(529, 7, 'Peter Pan Collar'),
(530, 7, 'PLAIN BREIF'),
(531, 7, 'PLAIN MIMI TRUNK'),
(532, 7, 'Platinum Brief'),
(533, 7, 'Pleated'),
(534, 7, 'POCO BRIEF'),
(535, 7, 'Polo Neck'),
(536, 7, 'Pouch Underwear'),
(537, 7, 'Premium Brief'),
(538, 7, 'Printed Brief'),
(539, 7, 'Printed G string'),
(540, 7, 'Protective'),
(541, 7, 'Pyjama'),
(542, 7, 'RED'),
(543, 7, 'Regular Brief'),
(544, 7, 'Regular Shorts'),
(545, 7, 'Replay Brief'),
(546, 7, 'Round Neck'),
(547, 7, 'Running Shorts'),
(548, 7, 'RUPA Brief'),
(549, 7, 'Rupa Brief'),
(550, 7, 'Saree Shapewear'),
(551, 7, 'Scarf'),
(552, 7, 'Scoop Neck'),
(553, 7, 'Screw Back Shirt Stud'),
(554, 7, 'Seamless Stitch'),
(555, 7, 'Security Guard Trouser'),
(556, 7, 'Semi Long Plain Brief'),
(557, 7, 'Semi Seamless'),
(558, 7, 'SemiSeamless Brief'),
(559, 7, 'SEXY UNDERWEAR'),
(560, 7, 'Shaper Brief'),
(561, 7, 'Shawl Neck'),
(562, 7, 'Sherwani and Churidar Set'),
(563, 7, 'Shirt & Trouser Fabric'),
(564, 7, 'Shirt Fabric'),
(565, 7, 'Single Breasted'),
(566, 7, 'Sliding Pin Shirt Stud'),
(567, 7, 'SLUB BRIEF'),
(568, 7, 'Smart Brief'),
(569, 7, 'Smart cut'),
(570, 7, 'SMART CUT BRIEF'),
(571, 7, 'Sports Shorts'),
(572, 7, 'Sports Tight Half Shorts'),
(573, 7, 'SPORTSWEAR'),
(574, 7, 'Square Cut Brief'),
(575, 7, 'Square Neck'),
(576, 7, 'Stole'),
(577, 7, 'Stretch fabric ensuring comfort Supporter'),
(578, 7, 'string bikini'),
(579, 7, 'STRING BIKINI'),
(580, 7, 'STRIPE BRIEF'),
(581, 7, 'Stylish Brief'),
(582, 7, 'Suit'),
(583, 7, 'Suit Fabric'),
(584, 7, 'Supporter'),
(585, 7, 'Sweatbelts'),
(586, 7, 'Swim Shorts'),
(587, 7, 'Tailored Trousers'),
(588, 7, 'Tank'),
(589, 7, 'Tank Top Vest'),
(590, 7, 'Thigh'),
(591, 7, 'Thigh Shaper'),
(592, 7, 'Thong'),
(593, 7, 'Thong Brief'),
(594, 7, 'Thong Underwear'),
(595, 7, 'Thongs'),
(596, 7, 'THONGS'),
(597, 7, 'Tie & Cufflink'),
(598, 7, 'Tie Pin'),
(599, 7, 'Tie Pin Set'),
(600, 7, 'Tight'),
(601, 7, 'tight'),
(602, 7, 'TIGHT'),
(603, 7, 'TIGHTS'),
(604, 7, 'Tights'),
(605, 7, 'Top'),
(606, 7, 'Top - Pyjama Set'),
(607, 7, 'Training Tights'),
(608, 7, 'Trouser'),
(609, 7, 'Trouser Fabric'),
(610, 7, 'Trousers'),
(611, 7, 'Trunk'),
(612, 7, 'Tummy & Thigh Shaper'),
(613, 7, 'Tummy Shaper'),
(614, 7, 'Turtle Neck'),
(615, 7, 'Two Piece Suit'),
(616, 7, 'Urban Vest'),
(617, 7, 'V Neck'),
(618, 7, 'Velvet Trouser'),
(619, 7, 'Vest-outerwear'),
(620, 7, 'Vests'),
(621, 7, 'Viscose'),
(622, 7, 'Winter'),
(623, 7, 'X- Back'),
(624, 7, 'Y- Back'),
(625, 7, 'Bellies'),
(626, 7, 'Boat Shoes'),
(627, 7, 'Boots'),
(628, 7, 'Brogues'),
(629, 7, 'Canvas Shoes'),
(630, 7, 'Casuals'),
(631, 7, 'Clogs'),
(632, 7, 'Corporate Casuals'),
(633, 7, 'Dancing Shoes'),
(634, 7, 'Driving Shoes'),
(635, 7, 'Espadrilles'),
(636, 7, 'High Tops'),
(637, 7, 'Loafers'),
(638, 7, 'Mocassin'),
(639, 7, 'Outdoors'),
(640, 7, 'Party Wear'),
(641, 7, 'Slip On Sneakers'),
(642, 7, 'Sneakers'),
(643, 7, 'Deodorant Creams'),
(644, 7, 'Deodorant Roll-ons'),
(645, 7, 'Deodorant Sticks'),
(646, 7, 'Deodorant Gels'),
(647, 7, 'Deodorant Spray'),
(648, 7, 'Eau de Cologne'),
(649, 7, 'Eau de Parfum'),
(650, 7, 'Eau de Toilette'),
(651, 7, 'Eau Fraiche'),
(652, 7, 'Extrait De Parfum'),
(653, 7, 'Perfume'),
(654, 8, 'Abstract'),
(655, 8, 'Acid Washed'),
(656, 8, 'Aiyaary Collection, Solid'),
(657, 8, 'Animal Print, Graphic Print, Self Design'),
(658, 8, 'Animal Print, Self Design'),
(659, 8, 'Applique'),
(660, 8, 'Argyle'),
(661, 8, 'Aztec'),
(662, 8, 'Bandhani'),
(663, 8, 'Bandhni Print'),
(664, 8, 'Block Print'),
(665, 8, 'Broad Stripes'),
(666, 8, 'Camouflage'),
(667, 8, 'Camouflage, Printed'),
(668, 8, 'Checkered'),
(669, 8, 'Checkered, Solid'),
(670, 8, 'Chevron'),
(671, 8, 'Chevron/Zig Zag'),
(672, 8, 'Chikan Embroidery'),
(673, 8, 'Color block'),
(674, 8, 'Conversational'),
(675, 8, 'Digital Print'),
(676, 8, 'Dip Dye'),
(677, 8, 'Distressed'),
(678, 8, 'Dyed'),
(679, 8, 'Embellished'),
(680, 8, 'Embellished, Washed'),
(681, 8, 'Embroidered, Solid, Applique'),
(682, 8, 'Floral Print'),
(683, 8, 'Floral Print, Printed'),
(684, 8, 'Floral Print, Solid'),
(685, 8, 'Geometric Print'),
(686, 8, 'Graphic Print'),
(687, 8, 'Hand Painted'),
(688, 8, 'Heathered'),
(689, 8, 'Houndstooth'),
(690, 8, 'Ikat'),
(691, 8, 'Jacquard'),
(692, 8, 'Kalamkari'),
(693, 8, 'Lace'),
(694, 8, 'Laser Cut'),
(695, 8, 'Leheriya'),
(696, 8, 'Military Camouflage'),
(697, 8, 'Ombre'),
(698, 8, 'Ombre, Floral Print'),
(699, 8, 'Paisley'),
(700, 8, 'Patchwork'),
(701, 8, 'Perforations'),
(702, 8, 'Plain'),
(703, 8, 'Polka Print'),
(704, 8, 'Printed'),
(705, 8, 'Printed , Self Design'),
(706, 8, 'Printed, Animal Print'),
(707, 8, 'Printed, Floral Print'),
(708, 8, 'Printed, Graphic Print'),
(709, 8, 'Printed, Self Design'),
(710, 8, 'Printed, Solid'),
(711, 8, 'Printed, Washed'),
(712, 8, 'Resham Embroidery'),
(713, 8, 'Self Design'),
(714, 8, 'Self Design, Embroidered'),
(715, 8, 'Self Design, Solid'),
(716, 8, 'Self Design, Solid, Embroidered'),
(717, 8, 'Self Design, Striped, Solid'),
(718, 8, 'Self Design, Woven'),
(719, 8, 'Solid'),
(720, 8, 'Solid, Applique'),
(721, 8, 'Solid, Checkered'),
(722, 8, 'Solid, Colorblock'),
(723, 8, 'Solid, Embellished'),
(724, 8, 'Solid, Embroidered, Self Design'),
(725, 8, 'Solid, Printed'),
(726, 8, 'Solid, Self Design'),
(727, 8, 'Solid, Textured'),
(728, 8, 'Solid, Washed'),
(729, 8, 'Solid, Woven'),
(730, 8, 'Sports'),
(731, 8, 'Striped'),
(732, 8, 'Striped, Acid Washed'),
(733, 8, 'Stylised'),
(734, 8, 'Temple Border'),
(735, 8, 'Textured'),
(736, 8, 'Tie & Dye'),
(737, 8, 'Tribal'),
(738, 8, 'Tribal Print'),
(739, 8, 'Typography'),
(740, 8, 'Varsity'),
(741, 8, 'Washed'),
(742, 8, 'Washed, Embellished'),
(743, 8, 'Washed, Embroidered'),
(744, 8, 'Washed, Solid'),
(745, 8, 'Washed, Striped'),
(746, 8, 'Woven'),
(747, 8, 'Woven Design'),
(748, 8, 'Woven, Embroidered'),
(749, 8, 'Woven, Solid'),
(750, 9, 'Automatic Mechanical'),
(751, 9, 'Quartz'),
(752, 9, 'Digital'),
(753, 9, 'Analog'),
(754, 9, 'Analog-Digital'),
(755, 9, 'Smart Analog'),
(756, 9, 'Hybrid Smartwatch'),
(757, 10, 'Contemporary'),
(758, 10, 'Oval'),
(759, 10, 'Rectangle'),
(760, 10, 'Round'),
(761, 10, 'Tonneau'),
(762, 10, 'Square'),
(763, 11, 'Beige'),
(764, 11, 'Black'),
(765, 11, 'Blue'),
(766, 11, 'Brown'),
(767, 11, 'Gold'),
(768, 11, 'Green'),
(769, 12, 'Fabric'),
(770, 12, 'Genuine Leather'),
(771, 12, 'Metal'),
(772, 12, 'Plastic'),
(773, 12, 'Resin'),
(774, 12, 'Silicone'),
(775, 12, 'Stainless Steel'),
(776, 12, 'Synthetic'),
(777, 12, 'Synthetic Leather'),
(778, 13, 'Alarm Clock'),
(779, 13, 'Altimeter'),
(780, 13, 'Barometer'),
(781, 13, 'Calendar'),
(782, 13, 'Chronograph'),
(783, 13, 'Compass'),
(784, 13, 'Date Display'),
(785, 13, 'Luminous'),
(786, 13, 'Water Resistant'),
(787, 13, 'Gradient'),
(788, 13, 'Mirrored'),
(789, 13, 'Night Vision'),
(790, 13, 'Others'),
(791, 13, 'Polarized'),
(792, 13, 'Riding Glasses'),
(793, 13, 'UV Protection');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `business_types`
--

CREATE TABLE `business_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `business_types`
--

INSERT INTO `business_types` (`id`, `name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'None I am an individual', 1, NULL, NULL),
(2, 'State-owned business', 1, NULL, NULL),
(3, 'Publicity listed bussiness', 1, NULL, NULL),
(4, 'Privately-owned business', 1, NULL, NULL),
(5, 'Charity', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(510) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `top` int(11) NOT NULL DEFAULT 1,
  `sort_order` int(11) NOT NULL DEFAULT 1,
  `active` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `image`, `parent_id`, `top`, `sort_order`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Pathayappura', NULL, NULL, 0, 1, 1, 1, NULL, NULL),
(2, 'Mobiles & Accessories', NULL, NULL, 0, 1, 1, 1, NULL, NULL),
(3, 'Computers & Accessories', NULL, NULL, 0, 1, 1, 1, NULL, NULL),
(4, 'Electronics', NULL, NULL, 0, 1, 1, 1, NULL, NULL),
(5, 'Men\'s Fashion', NULL, NULL, 0, 1, 1, 1, NULL, NULL),
(6, 'Women\'s Fashion', NULL, NULL, 0, 1, 1, 1, NULL, NULL),
(7, 'Baby & Kids', NULL, NULL, 0, 1, 1, 1, NULL, NULL),
(8, 'Home & Kitchen', NULL, NULL, 0, 1, 1, 1, NULL, NULL),
(9, 'Beauty, Health & Diet', NULL, NULL, 0, 1, 1, 1, NULL, NULL),
(10, 'Sports & Fitness', NULL, NULL, 0, 1, 1, 1, NULL, NULL),
(11, 'Automobile', NULL, NULL, 0, 1, 1, 1, NULL, NULL),
(12, 'Industrial & Scientific Supplies', NULL, NULL, 0, 1, 1, 1, NULL, NULL),
(13, 'Food & Beverages', NULL, NULL, 0, 1, 1, 1, NULL, NULL),
(14, 'Books ', NULL, NULL, 0, 1, 1, 1, NULL, NULL),
(15, 'Movies, Music & Video Games', NULL, NULL, 0, 1, 1, 1, NULL, NULL),
(16, 'School Supplies & Stationery', NULL, NULL, 0, 1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_attributes`
--

CREATE TABLE `category_attributes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `attribute` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `searchable` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_attributes`
--

INSERT INTO `category_attributes` (`id`, `category_id`, `attribute`, `searchable`, `created_at`, `updated_at`) VALUES
(1, 1, 'Size', 1, '2020-03-01 08:54:17', '2020-03-01 08:54:17');

-- --------------------------------------------------------

--
-- Table structure for table `category_attribute_values`
--

CREATE TABLE `category_attribute_values` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_attribute_id` bigint(20) UNSIGNED NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_attribute_values`
--

INSERT INTO `category_attribute_values` (`id`, `category_attribute_id`, `value`, `created_at`, `updated_at`) VALUES
(1, 1, 'Free size', NULL, NULL),
(2, 1, 'Universal', NULL, NULL),
(3, 1, '5XS', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `prefecture_id` bigint(20) UNSIGNED NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `prefecture_id`, `city`, `latitude`, `longitude`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Aomori', '36.2048', '138.2529', NULL, NULL, NULL),
(2, 1, 'Hachinohe', '9.9312328', '76.26730409999999', NULL, NULL, NULL),
(3, 1, 'Hirosaki', '9.9312328', '76.26730409999999', NULL, NULL, NULL),
(4, 1, 'Towada', '9.9312328', '76.26730409999999', NULL, NULL, NULL),
(5, 1, 'Mutsu', '9.9312328', '76.26730409999999', NULL, NULL, NULL),
(6, 1, 'Goshogawara', '9.9312328', '76.26730409999999', NULL, NULL, NULL),
(7, 1, 'Misawa', '9.9312328', '76.26730409999999', NULL, NULL, NULL),
(8, 1, 'Kuroishi', '9.9312328', '76.26730409999999', NULL, NULL, NULL),
(9, 1, 'Tsugaru', '9.9312328', '76.26730409999999', NULL, NULL, NULL),
(10, 1, 'Hirakawa', '9.9312328', '76.26730409999999', NULL, '2020-02-21 15:40:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `color_classes`
--

CREATE TABLE `color_classes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `state_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `name`, `state_id`) VALUES
(1, 'Anantnag', 15),
(2, 'Bandipore', 15),
(3, 'Baramulla', 15),
(4, 'Budgam', 15),
(5, 'Doda', 15),
(6, 'Ganderbal', 15),
(7, 'Jammu', 15),
(8, 'Kargil', 15),
(9, 'Kathua', 15),
(10, 'Kishtwar', 15),
(11, 'Kulgam', 15),
(12, 'Kupwara', 15),
(13, 'Leh (Ladakh)', 15),
(14, 'Poonch', 15),
(15, 'Pulwama', 15),
(16, 'Rajouri', 15),
(17, 'Ramban', 15),
(18, 'Reasi', 15),
(19, 'Samba', 15),
(20, 'Shopian', 15),
(21, 'Srinagar', 15),
(22, 'Udhampur', 15),
(23, 'Bilaspur (Himachal Pradesh)', 14),
(24, 'Chamba', 14),
(25, 'Hamirpur (Himachal Pradesh)', 14),
(26, 'Kangra', 14),
(27, 'Kinnaur', 14),
(28, 'Kullu', 14),
(29, 'Lahul & Spiti', 14),
(30, 'Mandi', 14),
(31, 'Shimla', 14),
(32, 'Sirmaur', 14),
(33, 'Solan', 14),
(34, 'Una', 14),
(35, 'Amritsar', 28),
(36, 'Barnala', 28),
(37, 'Bathinda', 28),
(38, 'Faridkot', 28),
(39, 'Fatehgarh Sahib', 28),
(40, 'Firozpur', 28),
(41, 'Gurdaspur', 28),
(42, 'Hoshiarpur', 28),
(43, 'Jalandhar', 28),
(44, 'Kapurthala', 28),
(45, 'Ludhiana', 28),
(46, 'Mansa', 28),
(47, 'Moga', 28),
(48, 'Muktsar', 28),
(49, 'Patiala', 28),
(50, 'Rupnagar (Ropar)', 28),
(51, 'Sahibzada Ajit Singh Nagar (Mohali)', 28),
(52, 'Sangrur', 28),
(53, 'Shahid Bhagat Singh Nagar (Nawanshahr)', 28),
(54, 'Tarn Taran', 28),
(56, 'Almora', 34),
(57, 'Bageshwar', 34),
(58, 'Chamoli', 34),
(59, 'Champawat', 34),
(60, 'Dehradun', 34),
(61, 'Haridwar', 34),
(62, 'Nainital', 34),
(63, 'Pauri Garhwal', 34),
(64, 'Pithoragarh', 34),
(65, 'Rudraprayag', 34),
(66, 'Tehri Garhwal', 34),
(67, 'Udham Singh Nagar', 34),
(68, 'Uttarkashi', 34),
(69, 'Ambala', 13),
(70, 'Bhiwani', 13),
(71, 'Faridabad', 13),
(72, 'Fatehabad', 13),
(73, 'Gurgaon', 13),
(74, 'Hisar', 13),
(75, 'Jhajjar', 13),
(76, 'Jind', 13),
(77, 'Kaithal', 13),
(78, 'Karnal', 13),
(79, 'Kurukshetra', 13),
(80, 'Mahendragarh', 13),
(81, 'Mewat', 13),
(82, 'Palwal', 13),
(83, 'Panchkula', 13),
(84, 'Panipat', 13),
(85, 'Rewari', 13),
(86, 'Rohtak', 13),
(87, 'Sirsa', 13),
(88, 'Sonipat', 13),
(89, 'Yamuna Nagar', 13),
(90, 'Central Delhi', 10),
(91, 'East Delhi', 10),
(92, 'New Delhi', 10),
(93, 'North Delhi', 10),
(94, 'North East Delhi', 10),
(95, 'North West Delhi', 10),
(96, 'South Delhi', 10),
(97, 'South West Delhi', 10),
(98, 'West Delhi', 10),
(99, 'Ajmer', 29),
(100, 'Alwar', 29),
(101, 'Banswara', 29),
(102, 'Baran', 29),
(103, 'Barmer', 29),
(104, 'Bharatpur', 29),
(105, 'Bhilwara', 29),
(106, 'Bikaner', 29),
(107, 'Bundi', 29),
(108, 'Chittorgarh', 29),
(109, 'Churu', 29),
(110, 'Dausa', 29),
(111, 'Dholpur', 29),
(112, 'Dungarpur', 29),
(113, 'Ganganagar', 29),
(114, 'Hanumangarh', 29),
(115, 'Jaipur', 29),
(116, 'Jaisalmer', 29),
(117, 'Jalor', 29),
(118, 'Jhalawar', 29),
(119, 'Jhunjhunu', 29),
(120, 'Jodhpur', 29),
(121, 'Karauli', 29),
(122, 'Kota', 29),
(123, 'Nagaur', 29),
(124, 'Pali', 29),
(125, 'Pratapgarh (Rajasthan)', 29),
(126, 'Rajsamand', 29),
(127, 'Sawai Madhopur', 29),
(128, 'Sikar', 29),
(129, 'Sirohi', 29),
(130, 'Tonk', 29),
(131, 'Udaipur', 29),
(132, 'Agra', 33),
(133, 'Aligarh', 33),
(134, 'Allahabad', 33),
(135, 'Ambedkar Nagar', 33),
(136, 'Auraiya', 33),
(137, 'Azamgarh', 33),
(138, 'Bagpat', 33),
(139, 'Bahraich', 33),
(140, 'Ballia', 33),
(141, 'Balrampur', 33),
(142, 'Banda', 33),
(143, 'Barabanki', 33),
(144, 'Bareilly', 33),
(145, 'Basti', 33),
(146, 'Bijnor', 33),
(147, 'Budaun', 33),
(148, 'Bulandshahr', 33),
(149, 'Chandauli', 33),
(150, 'Chitrakoot', 33),
(151, 'Deoria', 33),
(152, 'Etah', 33),
(153, 'Etawah', 33),
(154, 'Faizabad', 33),
(155, 'Farrukhabad', 33),
(156, 'Fatehpur', 33),
(157, 'Firozabad', 33),
(158, 'Gautam Buddha Nagar', 33),
(159, 'Ghaziabad', 33),
(160, 'Ghazipur', 33),
(161, 'Gonda', 33),
(162, 'Gorakhpur', 33),
(163, 'Hamirpur', 33),
(164, 'Hardoi', 33),
(165, 'Hathras', 33),
(166, 'Jalaun', 33),
(167, 'Jaunpur', 33),
(168, 'Jhansi', 33),
(169, 'Jyotiba Phule Nagar', 33),
(170, 'Kannauj', 33),
(171, 'Kanpur Dehat', 33),
(172, 'Kanpur Nagar', 33),
(173, 'Kanshiram Nagar', 33),
(174, 'Kaushambi', 33),
(175, 'Kheri', 33),
(176, 'Kushinagar', 33),
(177, 'Lalitpur', 33),
(178, 'Lucknow', 33),
(179, 'Maharajganj', 33),
(180, 'Mahoba', 33),
(181, 'Mainpuri', 33),
(182, 'Mathura', 33),
(183, 'Mau', 33),
(184, 'Meerut', 33),
(185, 'Mirzapur', 33),
(186, 'Moradabad', 33),
(187, 'Muzaffarnagar', 33),
(188, 'Pilibhit', 33),
(189, 'Pratapgarh', 33),
(190, 'Rae Bareli', 33),
(191, 'Rampur', 33),
(192, 'Saharanpur', 33),
(193, 'Sant Kabir Nagar', 33),
(194, 'Sant Ravidas Nagar (Bhadohi)', 33),
(195, 'Shahjahanpur', 33),
(196, 'Shrawasti', 33),
(197, 'Siddharthnagar', 33),
(198, 'Sitapur', 33),
(199, 'Sonbhadra', 33),
(200, 'Sultanpur', 33),
(201, 'Unnao', 33),
(202, 'Varanasi', 33),
(203, 'Araria', 5),
(204, 'Arwal', 5),
(205, 'Aurangabad (Bihar)', 5),
(206, 'Banka', 5),
(207, 'Begusarai', 5),
(208, 'Bhagalpur', 5),
(209, 'Bhojpur', 5),
(210, 'Buxar', 5),
(211, 'Darbhanga', 5),
(212, 'East Champaran', 5),
(213, 'Gaya', 5),
(214, 'Gopalganj', 5),
(215, 'Jamui', 5),
(216, 'Jehanabad', 5),
(217, 'Kaimur (Bhabua)', 5),
(218, 'Katihar', 5),
(219, 'Khagaria', 5),
(220, 'Kishanganj', 5),
(221, 'Lakhisarai', 5),
(222, 'Madhepura', 5),
(223, 'Madhubani', 5),
(224, 'Munger', 5),
(225, 'Muzaffarpur', 5),
(226, 'Nalanda', 5),
(227, 'Nawada', 5),
(228, 'Patna', 5),
(229, 'Purnia', 5),
(230, 'Rohtas', 5),
(231, 'Saharsa', 5),
(232, 'Samastipur', 5),
(233, 'Saran', 5),
(234, 'Sheikhpura', 5),
(235, 'Sheohar', 5),
(236, 'Sitamarhi', 5),
(237, 'Siwan', 5),
(238, 'Supaul', 5),
(239, 'Vaishali', 5),
(240, 'West Champaran', 5),
(241, 'East Sikkim', 30),
(242, 'North Sikkim', 30),
(243, 'South Sikkim', 30),
(244, 'West Sikkim', 30),
(245, 'Anjaw', 3),
(246, 'Changlang', 3),
(247, 'Dibang Valley', 3),
(248, 'East Kameng', 3),
(249, 'East Siang', 3),
(250, 'Kurung Kumey', 3),
(251, 'Lohit', 3),
(252, 'Lower Dibang Valley', 3),
(253, 'Lower Subansiri', 3),
(254, 'Papum Pare', 3),
(255, 'Tawang', 3),
(256, 'Tirap', 3),
(257, 'Upper Siang', 3),
(258, 'Upper Subansiri', 3),
(259, 'West Kameng', 3),
(260, 'West Siang', 3),
(261, 'Dimapur', 25),
(262, 'Kiphire', 25),
(263, 'Kohima', 25),
(264, 'Longleng', 25),
(265, 'Mokokchung', 25),
(266, 'Mon', 25),
(267, 'Peren', 25),
(268, 'Phek', 25),
(269, 'Tuensang', 25),
(270, 'Wokha', 25),
(271, 'Zunheboto', 25),
(272, 'Bishnupur', 22),
(273, 'Chandel', 22),
(274, 'Churachandpur', 22),
(275, 'Imphal East', 22),
(276, 'Imphal West', 22),
(277, 'Senapati', 22),
(278, 'Tamenglong', 22),
(279, 'Thoubal', 22),
(280, 'Ukhrul', 22),
(281, 'Aizawl', 24),
(282, 'Champhai', 24),
(283, 'Kolasib', 24),
(284, 'Lawngtlai', 24),
(285, 'Lunglei', 24),
(286, 'Mamit', 24),
(287, 'Saiha', 24),
(288, 'Serchhip', 24),
(289, 'Dhalai', 32),
(290, 'North Tripura', 32),
(291, 'South Tripura', 32),
(292, 'West Tripura', 32),
(293, 'East Garo Hills', 23),
(294, 'East Khasi Hills', 23),
(295, 'Jaintia Hills', 23),
(296, 'Ri Bhoi', 23),
(297, 'South Garo Hills', 23),
(298, 'West Garo Hills', 23),
(299, 'West Khasi Hills', 23),
(300, 'Baksa', 4),
(301, 'Barpeta', 4),
(302, 'Bongaigaon', 4),
(303, 'Cachar', 4),
(304, 'Chirang', 4),
(305, 'Darrang', 4),
(306, 'Dhemaji', 4),
(307, 'Dhubri', 4),
(308, 'Dibrugarh', 4),
(309, 'Dima Hasao (North Cachar Hills)', 4),
(310, 'Goalpara', 4),
(311, 'Golaghat', 4),
(312, 'Hailakandi', 4),
(313, 'Jorhat', 4),
(314, 'Kamrup', 4),
(315, 'Kamrup Metropolitan', 4),
(316, 'Karbi Anglong', 4),
(317, 'Karimganj', 4),
(318, 'Kokrajhar', 4),
(319, 'Lakhimpur', 4),
(320, 'Morigaon', 4),
(321, 'Nagaon', 4),
(322, 'Nalbari', 4),
(323, 'Sivasagar', 4),
(324, 'Sonitpur', 4),
(325, 'Tinsukia', 4),
(326, 'Udalguri', 4),
(327, 'Bankura', 35),
(328, 'Bardhaman', 35),
(329, 'Birbhum', 35),
(330, 'Cooch Behar', 35),
(331, 'Dakshin Dinajpur (South Dinajpur)', 35),
(332, 'Darjiling', 35),
(333, 'Hooghly', 35),
(334, 'Howrah', 35),
(335, 'Jalpaiguri', 35),
(336, 'Kolkata', 35),
(337, 'Maldah', 35),
(338, 'Murshidabad', 35),
(339, 'Nadia', 35),
(340, 'North 24 Parganas', 35),
(341, 'Paschim Medinipur (West Midnapore)', 35),
(342, 'Purba Medinipur (East Midnapore)', 35),
(343, 'Puruliya', 35),
(344, 'South 24 Parganas', 35),
(345, 'Uttar Dinajpur (North Dinajpur)', 35),
(346, 'Bokaro', 16),
(347, 'Chatra', 16),
(348, 'Deoghar', 16),
(349, 'Dhanbad', 16),
(350, 'Dumka', 16),
(351, 'East Singhbhum', 16),
(352, 'Garhwa', 16),
(353, 'Giridih', 16),
(354, 'Godda', 16),
(355, 'Gumla', 16),
(356, 'Hazaribagh', 16),
(357, 'Jamtara', 16),
(358, 'Khunti', 16),
(359, 'Koderma', 16),
(360, 'Latehar', 16),
(361, 'Lohardaga', 16),
(362, 'Pakur', 16),
(363, 'Palamu', 16),
(364, 'Ramgarh', 16),
(365, 'Ranchi', 16),
(366, 'Sahibganj', 16),
(367, 'Seraikela-Kharsawan', 16),
(368, 'Simdega', 16),
(369, 'West Singhbhum', 16),
(370, 'Angul', 26),
(371, 'Balangir', 26),
(372, 'Baleswar', 26),
(373, 'Bargarh', 26),
(374, 'Bhadrak', 26),
(375, 'Boudh', 26),
(376, 'Cuttack', 26),
(377, 'Debagarh', 26),
(378, 'Dhenkanal', 26),
(379, 'Gajapati', 26),
(380, 'Ganjam', 26),
(381, 'Jagatsinghapur', 26),
(382, 'Jajapur', 26),
(383, 'Jharsuguda', 26),
(384, 'Kalahandi', 26),
(385, 'Kandhamal', 26),
(386, 'Kendrapara', 26),
(387, 'Kendujhar', 26),
(388, 'Khordha', 26),
(389, 'Koraput', 26),
(390, 'Malkangiri', 26),
(391, 'Mayurbhanj', 26),
(392, 'Nabarangapur', 26),
(393, 'Nayagarh', 26),
(394, 'Nuapada', 26),
(395, 'Puri', 26),
(396, 'Rayagada', 26),
(397, 'Sambalpur', 26),
(398, 'Subarnapur (Sonapur)', 26),
(399, 'Sundergarh', 26),
(400, 'Bastar', 7),
(401, 'Bijapur (Chhattisgarh)', 7),
(402, 'Bilaspur (Chhattisgarh)', 7),
(403, 'Dakshin Bastar Dantewada', 7),
(404, 'Dhamtari', 7),
(405, 'Durg', 7),
(406, 'Janjgir-Champa', 7),
(407, 'Jashpur', 7),
(408, 'Kabirdham (Kawardha)', 7),
(409, 'Korba', 7),
(410, 'Koriya', 7),
(411, 'Mahasamund', 7),
(412, 'Narayanpur', 7),
(413, 'Raigarh (Chhattisgarh)', 7),
(414, 'Raipur', 7),
(415, 'Rajnandgaon', 7),
(416, 'Surguja', 7),
(417, 'Uttar Bastar Kanker', 7),
(418, 'Alirajpur', 20),
(419, 'Anuppur', 20),
(420, 'Ashok Nagar', 20),
(421, 'Balaghat', 20),
(422, 'Barwani', 20),
(423, 'Betul', 20),
(424, 'Bhind', 20),
(425, 'Bhopal', 20),
(426, 'Burhanpur', 20),
(427, 'Chhatarpur', 20),
(428, 'Chhindwara', 20),
(429, 'Damoh', 20),
(430, 'Datia', 20),
(431, 'Dewas', 20),
(432, 'Dhar', 20),
(433, 'Dindori', 20),
(434, 'Guna', 20),
(435, 'Gwalior', 20),
(436, 'Harda', 20),
(437, 'Hoshangabad', 20),
(438, 'Indore', 20),
(439, 'Jabalpur', 20),
(440, 'Jhabua', 20),
(441, 'Katni', 20),
(442, 'Khandwa (East Nimar)', 20),
(443, 'Khargone (West Nimar)', 20),
(444, 'Mandla', 20),
(445, 'Mandsaur', 20),
(446, 'Morena', 20),
(447, 'Narsinghpur', 20),
(448, 'Neemuch', 20),
(449, 'Panna', 20),
(450, 'Raisen', 20),
(451, 'Rajgarh', 20),
(452, 'Ratlam', 20),
(453, 'Rewa', 20),
(454, 'Sagar', 20),
(455, 'Satna', 20),
(456, 'Sehore', 20),
(457, 'Seoni', 20),
(458, 'Shahdol', 20),
(459, 'Shajapur', 20),
(460, 'Sheopur', 20),
(461, 'Shivpuri', 20),
(462, 'Sidhi', 20),
(463, 'Singrauli', 20),
(464, 'Tikamgarh', 20),
(465, 'Ujjain', 20),
(466, 'Umaria', 20),
(467, 'Vidisha', 20),
(468, 'Ahmedabad', 12),
(469, 'Amreli', 12),
(470, 'Anand', 12),
(471, 'Banaskantha', 12),
(472, 'Bharuch', 12),
(473, 'Bhavnagar', 12),
(474, 'Dahod', 12),
(475, 'Gandhi Nagar', 12),
(476, 'Jamnagar', 12),
(477, 'Junagadh', 12),
(478, 'Kachchh', 12),
(479, 'Kheda', 12),
(480, 'Mahesana', 12),
(481, 'Narmada', 12),
(482, 'Navsari', 12),
(483, 'Panch Mahals', 12),
(484, 'Patan', 12),
(485, 'Porbandar', 12),
(486, 'Rajkot', 12),
(487, 'Sabarkantha', 12),
(488, 'Surat', 12),
(489, 'Surendra Nagar', 12),
(490, 'Tapi', 12),
(491, 'The Dangs', 12),
(492, 'Vadodara', 12),
(493, 'Valsad', 12),
(497, 'Ahmed Nagar', 21),
(498, 'Akola', 21),
(499, 'Amravati', 21),
(500, 'Aurangabad', 21),
(501, 'Beed', 21),
(502, 'Bhandara', 21),
(503, 'Buldhana', 21),
(504, 'Chandrapur', 21),
(505, 'Dhule', 21),
(506, 'Gadchiroli', 21),
(507, 'Gondia', 21),
(508, 'Hingoli', 21),
(509, 'Jalgaon', 21),
(510, 'Jalna', 21),
(511, 'Kolhapur', 21),
(512, 'Latur', 21),
(513, 'Mumbai', 21),
(514, 'Mumbai Suburban', 21),
(515, 'Nagpur', 21),
(516, 'Nanded', 21),
(517, 'Nandurbar', 21),
(518, 'Nashik', 21),
(519, 'Osmanabad', 21),
(520, 'Parbhani', 21),
(521, 'Pune', 21),
(522, 'Raigarh (Maharashtra)', 21),
(523, 'Ratnagiri', 21),
(524, 'Sangli', 21),
(525, 'Satara', 21),
(526, 'Sindhudurg', 21),
(527, 'Solapur', 21),
(528, 'Thane', 21),
(529, 'Wardha', 21),
(530, 'Washim', 21),
(531, 'Yavatmal', 21),
(532, 'Adilabad', 2),
(533, 'Anantapur', 2),
(534, 'Chittoor', 2),
(535, 'East Godavari', 2),
(536, 'Guntur', 2),
(537, 'Hyderabad', 2),
(538, 'Kadapa (Cuddapah)', 2),
(539, 'Karim Nagar', 2),
(540, 'Khammam', 2),
(541, 'Krishna', 2),
(542, 'Kurnool', 2),
(543, 'Mahbubnagar', 2),
(544, 'Medak', 2),
(545, 'Nalgonda', 2),
(546, 'Nellore', 2),
(547, 'Nizamabad', 2),
(548, 'Prakasam', 2),
(549, 'Rangareddy', 2),
(550, 'Srikakulam', 2),
(551, 'Visakhapatnam', 2),
(552, 'Vizianagaram', 2),
(553, 'Warangal', 2),
(554, 'West Godavari', 2),
(555, 'Bagalkot', 17),
(556, 'Bangalore', 17),
(557, 'Bangalore Rural', 17),
(558, 'Belgaum', 17),
(559, 'Bellary', 17),
(560, 'Bidar', 17),
(561, 'Bijapur (Karnataka)', 17),
(562, 'Chamrajnagar', 17),
(563, 'Chickmagalur', 17),
(564, 'Chikkaballapur', 17),
(565, 'Chitradurga', 17),
(566, 'Dakshina Kannada', 17),
(567, 'Davanagere', 17),
(568, 'Dharwad', 17),
(569, 'Gadag', 17),
(570, 'Gulbarga', 17),
(571, 'Hassan', 17),
(572, 'Haveri', 17),
(573, 'Kodagu', 17),
(574, 'Kolar', 17),
(575, 'Koppal', 17),
(576, 'Mandya', 17),
(577, 'Mysore', 17),
(578, 'Raichur', 17),
(579, 'Ramanagara', 17),
(580, 'Shimoga', 17),
(581, 'Tumkur', 17),
(582, 'Udupi', 17),
(583, 'Uttara Kannada', 17),
(584, 'Yadgir', 17),
(585, 'North Goa', 11),
(586, 'South Goa', 11),
(588, 'Alappuzha', 18),
(589, 'Ernakulam', 18),
(590, 'Idukki', 18),
(591, 'Kannur', 18),
(592, 'Kasaragod', 18),
(593, 'Kollam', 18),
(594, 'Kottayam', 18),
(595, 'Kozhikode', 18),
(596, 'Malappuram', 18),
(597, 'Palakkad', 18),
(598, 'Pathanamthitta', 18),
(599, 'Thiruvananthapuram', 18),
(600, 'Thrissur', 18),
(601, 'Wayanad', 18),
(602, 'Ariyalur', 31),
(603, 'Chennai', 31),
(604, 'Coimbatore', 31),
(605, 'Cuddalore', 31),
(606, 'Dharmapuri', 31),
(607, 'Dindigul', 31),
(608, 'Erode', 31),
(609, 'Kanchipuram', 31),
(610, 'Kanyakumari', 31),
(611, 'Karur', 31),
(612, 'Krishnagiri', 31),
(613, 'Madurai', 31),
(614, 'Nagapattinam', 31),
(615, 'Namakkal', 31),
(616, 'Nilgiris', 31),
(617, 'Perambalur', 31),
(618, 'Pudukkottai', 31),
(619, 'Ramanathapuram', 31),
(620, 'Salem', 31),
(621, 'Sivaganga', 31),
(622, 'Thanjavur', 31),
(623, 'Theni', 31),
(624, 'Thoothukudi (Tuticorin)', 31),
(625, 'Tiruchirappalli', 31),
(626, 'Tirunelveli', 31),
(627, 'Tiruppur', 31),
(628, 'Tiruvallur', 31),
(629, 'Tiruvannamalai', 31),
(630, 'Tiruvarur', 31),
(631, 'Vellore', 31),
(632, 'Viluppuram', 31),
(633, 'Virudhunagar', 31),
(640, 'South Andhamann', 36),
(643, 'Andra Pradesh', 36);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `length_classes`
--

CREATE TABLE `length_classes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` decimal(8,4) NOT NULL,
  `unit` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `sort_order` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `length_classes`
--

INSERT INTO `length_classes` (`id`, `name`, `value`, `unit`, `active`, `sort_order`, `created_at`, `updated_at`) VALUES
(1, 'Millimeter', '0.1000', 'mm', 1, 1, NULL, NULL),
(2, 'Centi meter', '1.0000', 'cm', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `manufactors`
--

CREATE TABLE `manufactors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers`
--

CREATE TABLE `manufacturers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_order` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `manufacturers`
--

INSERT INTO `manufacturers` (`id`, `name`, `image`, `sort_order`, `created_at`, `updated_at`) VALUES
(1, 'dfgdfg', 'images/manufacturers/1583899995.jpg', 1, '2020-03-10 22:43:15', '2020-03-10 22:43:15');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_11_07_121101_create_admins_table', 1),
(5, '2019_11_09_063158_create_vendors_table', 1),
(6, '2019_12_09_075119_create_categories_table', 1),
(7, '2019_12_13_050622_create_vendor_categories_table', 1),
(8, '2019_12_16_091432_create_weight_classes_table', 1),
(9, '2019_12_16_091814_create_length_classes_table', 1),
(10, '2019_12_16_092025_create_tax_classes_table', 1),
(11, '2019_12_16_144941_create_manufactors_table', 1),
(12, '2019_12_16_145012_create_brands_table', 1),
(13, '2019_12_16_145224_create_products_table', 1),
(14, '2019_12_16_145335_create_vendor_products_table', 1),
(15, '2019_12_20_110246_create_otps_table', 1),
(16, '2019_12_23_063235_create_business_types_table', 1),
(17, '2019_12_23_065910_add_business_type_to_vendors_table', 1),
(18, '2019_12_27_105551_add_address_to_vendors_table', 1),
(19, '2019_12_27_134412_add_approval_to_vendors_table', 1),
(20, '2019_12_30_100211_update_pan_number_vendors_table', 1),
(21, '2019_12_31_063253_create_manufacturers_table', 1),
(22, '2020_01_02_050207_update_description_tax_classes', 1),
(23, '2020_01_02_052521_update_fields_length_clases_table', 1),
(24, '2020_01_02_060521_update_fields_weight_classes', 1),
(25, '2020_01_02_061420_update_fields_to_products_table', 1),
(26, '2020_01_06_062122_create_product_images_table', 1),
(27, '2020_01_09_044055_create_color_classes_table', 1),
(28, '2020_01_09_112828_delete_field_products_table', 1),
(29, '2020_01_22_081229_create_attributes_table', 1),
(30, '2020_01_22_101925_create_attribute_values_table', 1),
(31, '2020_01_23_104343_create_category_attributes_table', 1),
(32, '2020_01_23_104516_create_category_attribute_values_table', 1),
(33, '2020_01_30_071643_create_tmp_products_table', 1),
(34, '2020_01_30_082122_create_tmp_product_attributes_table', 1),
(35, '2020_01_30_090946_create_tmp_product_images_table', 1),
(36, '2020_02_03_070658_update_to_tmp_products_table', 1),
(37, '2020_02_03_114719_update_to_length_classes_table', 1),
(38, '2020_02_05_053804_update_to_temp_products_table', 1),
(39, '2020_02_25_071828_create_tmp_additional_infos_table', 2),
(40, '2020_02_25_084835_create_tmp_porduct_additional_infos_table', 3),
(41, '2020_02_25_101546_create_tmp_porduct_faqs_table', 4),
(43, '2020_02_28_070111_tmp_products_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `otps`
--

CREATE TABLE `otps` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `otp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `otps`
--

INSERT INTO `otps` (`id`, `mobile`, `otp`, `created_at`, `updated_at`) VALUES
(1, '919946070864', '1791', '2020-02-23 23:19:16', '2020-02-23 23:19:16');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variant` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dcin` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ean` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upc` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_id` bigint(20) UNSIGNED DEFAULT NULL,
  `other_brand` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `pathayapura_listing` tinyint(1) NOT NULL DEFAULT 0,
  `gst` decimal(15,2) UNSIGNED DEFAULT NULL,
  `sess` tinyint(1) UNSIGNED DEFAULT 0,
  `price` decimal(15,2) UNSIGNED DEFAULT NULL,
  `mrp` decimal(15,2) UNSIGNED DEFAULT NULL,
  `wholesale` decimal(15,2) UNSIGNED DEFAULT NULL,
  `wholesale_unit` bigint(20) UNSIGNED DEFAULT NULL,
  `cod` tinyint(1) DEFAULT 0,
  `weight` double(15,2) DEFAULT NULL,
  `weight_class_id` bigint(20) UNSIGNED DEFAULT NULL,
  `length` double(15,2) DEFAULT NULL,
  `width` double(15,2) DEFAULT NULL,
  `height` double(15,2) DEFAULT NULL,
  `length_class_id` bigint(20) UNSIGNED DEFAULT NULL,
  `shipping_processing_time` int(11) DEFAULT NULL,
  `free_delivery` tinyint(1) NOT NULL DEFAULT 0,
  `vendor_id` bigint(20) UNSIGNED DEFAULT NULL,
  `completed_status` tinyint(1) NOT NULL DEFAULT 0,
  `approved` smallint(6) NOT NULL,
  `admin_comments` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `step` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `variant`, `dcin`, `ean`, `upc`, `brand_id`, `other_brand`, `category_id`, `pathayapura_listing`, `gst`, `sess`, `price`, `mrp`, `wholesale`, `wholesale_unit`, `cod`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `shipping_processing_time`, `free_delivery`, `vendor_id`, `completed_status`, `approved`, `admin_comments`, `step`, `created_at`, `updated_at`) VALUES
(25, 'OnePlus  6T', '57', 'DCIN-1583732041631', '52', '498', NULL, 'OnePlus', 2, 1, '3735181789654.00', 1, '35000.00', '32000.00', '31000.00', 12, 1, 150.00, 2, 150.00, 45.00, 45.00, 2, 2, 1, 1, 1, 0, 'tyutyu tyutyutyuty', 6, '2020-03-09 00:04:18', '2020-03-09 00:07:48'),
(38, 'OnePlus  7T Pro', '25', 'DCIN-1583904790630', '52', '498', NULL, 'OnePlus', 2, 1, '3735181789654.00', 1, '35000.00', '32000.00', '31000.00', 12, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 3, NULL, 2, '2020-03-11 00:03:19', '2020-03-11 00:03:24'),
(39, 'OnePlus  6T', '25', 'DCIN-1583905123327', '52', '498', NULL, 'OnePlus', 2, 1, '3735181789654.00', 1, '35000.00', '32000.00', '31000.00', 12, 1, 150.00, 1, 150.00, 45.00, 45.00, 1, 2, 1, 1, 1, 1, 'Test Comment', 7, '2020-03-11 00:08:47', '2020-03-11 02:25:20'),
(40, 'OnePlus  8', '39', 'DCIN-1583913598236', '52', '498', NULL, 'OnePlus', 2, 1, '3735181789654.00', 1, '35000.00', '32000.00', '31000.00', 12, 1, 150.00, 1, 150.00, 45.00, 45.00, 1, 2, 1, 1, 1, 1, 'sdfsddsf', 7, '2020-03-11 02:30:09', '2020-03-11 02:30:42'),
(41, 'OnePlus  5', '39', 'DCIN-1583913801669', '52', '498', NULL, 'OnePlus', 2, 1, '3735181789654.00', 1, '33000.00', '3000.00', '3000.00', 12, 1, 150.00, 1, 150.00, 45.00, 45.00, 1, 2, 1, 1, 0, 3, NULL, 5, '2020-03-11 02:33:35', '2020-03-11 02:34:23'),
(42, 'OnePlus  5', '41', 'DCIN-1583917604950', '52', '498', NULL, 'OnePlus', 2, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 3, NULL, 1, '2020-03-11 03:36:48', '2020-03-11 03:36:48'),
(43, 'OnePlus  6T', '25', 'DCIN-1583917893574', '52', '498', NULL, 'OnePlus', 2, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 3, NULL, 1, '2020-03-11 03:41:34', '2020-03-11 03:41:34'),
(44, 'OnePlus  5', '41', 'DCIN-1583919340316', '52', '498', NULL, 'OnePlus', 2, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 3, NULL, 1, '2020-03-11 04:05:50', '2020-03-11 04:05:50'),
(45, 'OnePlus  6T', '43', 'DCIN-1583920520334', '52', '498', NULL, 'OnePlus', 2, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 3, NULL, 1, '2020-03-11 04:25:22', '2020-03-11 04:25:22');

-- --------------------------------------------------------

--
-- Table structure for table `product_additional_infos`
--

CREATE TABLE `product_additional_infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_additional_infos`
--

INSERT INTO `product_additional_infos` (`id`, `product_id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(92, 22, 'Company bio', 'details', '2020-02-28 02:36:36', '2020-02-28 02:36:36'),
(93, 22, 'other', 'asdadasd', '2020-02-28 02:36:54', '2020-02-28 02:36:54'),
(94, 22, 'test', 'tessss', '2020-02-28 05:04:34', '2020-02-28 05:04:34'),
(95, 22, 'tests', 'sss', '2020-02-28 05:06:57', '2020-02-28 05:06:57'),
(96, 22, 'test', 'ewrwer', '2020-02-28 05:21:28', '2020-02-28 05:21:28'),
(97, 22, 'test', 'ewrwer', '2020-02-28 05:23:20', '2020-02-28 05:23:20'),
(98, 22, 'test', 'dsasdad', '2020-02-28 05:23:33', '2020-02-28 05:23:33'),
(99, 23, 'dfgdfg', 'asdasddas', '2020-03-05 23:08:36', '2020-03-05 23:08:36'),
(100, 23, 'dfgdg', 'sdfsdf', '2020-03-05 23:11:51', '2020-03-05 23:11:51'),
(101, 24, 'dfgdfg', 'dsfdsfsdfdsf', '2020-03-06 01:58:21', '2020-03-06 01:58:21'),
(102, 24, 'dfgdfg', 'dsfdsfdsf', '2020-03-08 23:52:55', '2020-03-08 23:52:55'),
(103, 24, 'dfgdfg', 'dsfdsfdsf', '2020-03-08 23:52:59', '2020-03-08 23:52:59'),
(104, 24, 'dfgdfg', 'dsfdsfdsf', '2020-03-08 23:53:02', '2020-03-08 23:53:02'),
(105, 24, 'dfgdfg', 'dsfdsfdsf', '2020-03-08 23:53:03', '2020-03-08 23:53:03'),
(106, 24, 'dfgdfg', 'dsfdsfdsf', '2020-03-08 23:53:03', '2020-03-08 23:53:03'),
(107, 25, 'dfgdfg', 'sdfsdfsdfdsf', '2020-03-09 00:07:42', '2020-03-09 00:07:42'),
(117, 36, 'dfgdfg', 'dfgdfgdfggf', '2020-03-10 00:56:03', '2020-03-10 00:56:03'),
(118, 36, 'dfgdfg', 'dfgdfgdfggf', '2020-03-10 00:56:50', '2020-03-10 00:56:50'),
(119, 36, 'test', 'test', '2020-03-10 01:00:32', '2020-03-10 01:00:32'),
(120, 37, 'dfgdfg', 'wdqwdqwdqwd', '2020-03-10 23:56:05', '2020-03-10 23:56:05'),
(121, 39, 'dfgdfg', 'sadasd asd asdasdasd', '2020-03-11 01:47:10', '2020-03-11 01:47:10'),
(122, 40, 'dfgdfg', 'sdfsdsdfsdf', '2020-03-11 02:30:34', '2020-03-11 02:30:34'),
(136, 41, 'sdfsdf', 'sdfsdfsfs sdfsdfdf', '2020-03-11 03:26:07', '2020-03-11 03:26:07');

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

CREATE TABLE `product_attributes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `attribute` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`id`, `product_id`, `attribute`, `value`, `created_at`, `updated_at`) VALUES
(1, 6, 'Size', 'Universal', '2020-03-01 22:56:00', '2020-03-01 22:56:00'),
(7, 22, 'Size', 'Universal', '2020-03-02 00:19:31', '2020-03-02 00:19:31');

-- --------------------------------------------------------

--
-- Table structure for table `product_faqs`
--

CREATE TABLE `product_faqs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_faqs`
--

INSERT INTO `product_faqs` (`id`, `product_id`, `question`, `answer`, `created_at`, `updated_at`) VALUES
(1, 6, 'Ques1', 'Answ1', '2020-02-28 02:37:08', '2020-02-28 02:37:08'),
(2, 6, 'Ques2', 'Answ2', '2020-02-28 02:37:28', '2020-02-28 02:37:28'),
(3, 6, 'qq', 'qq', '2020-02-28 05:25:56', '2020-02-28 05:25:56'),
(5, 6, 'ddd', 'asdasd', '2020-03-01 11:11:22', '2020-03-01 11:11:22'),
(6, 6, 'ewds', 'asdasdas', '2020-03-01 11:16:26', '2020-03-01 11:16:26'),
(7, 6, 'sasdasd', 'asdasd', '2020-03-01 11:17:11', '2020-03-01 11:17:11'),
(32, 22, 'Ques1', 'Answ1', '2020-02-28 02:37:08', '2020-02-28 02:37:08'),
(33, 22, 'Ques2', 'Answ2', '2020-02-28 02:37:28', '2020-02-28 02:37:28'),
(34, 22, 'qq', 'qq', '2020-02-28 05:25:56', '2020-02-28 05:25:56'),
(35, 22, 'ddd', 'asdasd', '2020-03-01 11:11:22', '2020-03-01 11:11:22'),
(36, 22, 'ewds', 'asdasdas', '2020-03-01 11:16:26', '2020-03-01 11:16:26'),
(37, 22, 'sasdasd', 'asdasd', '2020-03-01 11:17:11', '2020-03-01 11:17:11'),
(38, 23, 'Test Question?', 'Test Answer', '2020-03-05 23:06:25', '2020-03-05 23:06:25'),
(39, 23, 'Test Question?', 'sdfsdfsdfsdf', '2020-03-05 23:11:56', '2020-03-05 23:11:56'),
(40, 24, 'Test Question?', 'dsfsfds sdfdsf sdfds fsdfdsfdsfd sd', '2020-03-06 01:58:30', '2020-03-06 01:58:30'),
(41, 25, 'Test Question?', 'sdfs sdf sdfds', '2020-03-09 00:07:48', '2020-03-09 00:07:48'),
(42, 36, 'Test Question?', 'sdasdasdas', '2020-03-10 01:08:24', '2020-03-10 01:08:24'),
(43, 36, 'sdfsdfsdf', 'sdfsdfd sdfsdfsdfdsfdsfsdf', '2020-03-10 01:08:34', '2020-03-10 01:08:34'),
(44, 36, 'Test Question 123 ?', 'Test Question', '2020-03-10 01:08:51', '2020-03-10 01:08:51'),
(48, 39, 'Test Question?', 'sdfsdfsdfsdf dsfsdf', '2020-03-11 01:57:24', '2020-03-11 01:57:24'),
(50, 39, 'Test Question?', 'fasdff dsfs sdfsdfds sdfsdfs sdfsdf', '2020-03-11 02:03:21', '2020-03-11 02:03:21'),
(51, 39, 'Test Question?', 'asdasda asdadsasd asdasdasd', '2020-03-11 02:04:45', '2020-03-11 02:04:45'),
(52, 39, 'Test Question?', 'asdasda asdadsasd asdasdasd', '2020-03-11 02:04:48', '2020-03-11 02:04:48'),
(53, 39, 'Test Question?', 'asdasda asdadsasd asdasdasd', '2020-03-11 02:07:02', '2020-03-11 02:07:02'),
(55, 39, 'Sooraj?', 'Yes', '2020-03-11 02:10:34', '2020-03-11 02:10:34'),
(56, 40, 'Test Question?', 'sdfsdfdsfsdf', '2020-03-11 02:30:38', '2020-03-11 02:30:38');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image`, `created_at`, `updated_at`) VALUES
(12, 6, 'images/products/6/1582885393.png', '2020-02-28 04:53:13', '2020-02-28 04:53:13'),
(13, 6, 'images/products/6/1582885404.png', '2020-02-28 04:53:24', '2020-02-28 04:53:24'),
(14, 6, 'images/products/6/1582885429.png', '2020-02-28 04:53:49', '2020-02-28 04:53:49'),
(16, 6, 'images/products/6/1583079652.png', '2020-03-01 10:50:52', '2020-03-01 10:50:52'),
(17, 22, 'images/products/6/1582885393.png', '2020-02-28 04:53:13', '2020-02-28 04:53:13'),
(18, 22, 'images/products/6/1582885404.png', '2020-02-28 04:53:24', '2020-02-28 04:53:24'),
(19, 22, 'images/products/6/1582885429.png', '2020-02-28 04:53:49', '2020-02-28 04:53:49'),
(20, 22, 'images/products/6/1583079652.png', '2020-03-01 10:50:52', '2020-03-01 10:50:52'),
(21, 23, 'images/products/39/1583469340.jpg', '2020-03-05 23:05:40', '2020-03-05 23:05:40'),
(22, 23, 'images/products/39/1583469348.jpg', '2020-03-05 23:05:48', '2020-03-05 23:05:48'),
(23, 23, 'images/products/39/1583469354.jpg', '2020-03-05 23:05:54', '2020-03-05 23:05:54'),
(24, 24, 'images/products/40/1583479684.jpg', '2020-03-06 01:58:04', '2020-03-06 01:58:04'),
(25, 24, 'images/products/40/1583479690.jpg', '2020-03-06 01:58:10', '2020-03-06 01:58:10'),
(26, 24, 'images/products/40/1583479695.jpg', '2020-03-06 01:58:15', '2020-03-06 01:58:15'),
(27, 25, 'images/products/57/1583732242.jpg', '2020-03-09 00:07:22', '2020-03-09 00:07:22'),
(28, 25, 'images/products/57/1583732248.jpg', '2020-03-09 00:07:28', '2020-03-09 00:07:28'),
(29, 25, 'images/products/57/1583732253.jpg', '2020-03-09 00:07:33', '2020-03-09 00:07:33'),
(32, 36, 'images/products/36/1583812279.jpg', NULL, NULL),
(33, 36, 'images/products/36/1583812286.jpg', NULL, NULL),
(34, 37, 'images/products/37/1583904355.jpg', NULL, NULL),
(35, 37, 'images/products/37/1583904392.jpg', NULL, NULL),
(50, 39, 'images/products/57/1583732248.jpg', '2020-03-09 00:07:28', '2020-03-09 00:07:28'),
(51, 39, 'images/products/39/1583911006.jpg', '2020-03-11 01:46:46', '2020-03-11 01:46:46'),
(52, 39, 'images/products/39/1583911019.jpg', '2020-03-11 01:46:59', '2020-03-11 01:46:59'),
(53, 40, 'images/products/40/1583913622.jpg', '2020-03-11 02:30:22', '2020-03-11 02:30:22'),
(54, 40, 'images/products/40/1583913628.jpg', '2020-03-11 02:30:28', '2020-03-11 02:30:28'),
(55, 41, 'images/products/39/1583911006.jpg', '2020-03-11 01:46:46', '2020-03-11 01:46:46'),
(56, 41, 'images/products/39/1583911019.jpg', '2020-03-11 01:46:59', '2020-03-11 01:46:59');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`) VALUES
(2, 'Andhra Pradesh'),
(3, 'Arunachal Pradesh'),
(4, 'Assam'),
(5, 'Bihar'),
(7, 'Chhattisgarh'),
(10, 'Delhi'),
(11, 'Goa'),
(12, 'Gujarat'),
(13, 'Haryana'),
(14, 'Himachal Pradesh'),
(15, 'Jammu & Kashmir'),
(16, 'Jharkhand'),
(17, 'Karnataka'),
(18, 'Kerala'),
(20, 'Madhya Pradesh'),
(21, 'Maharashtra'),
(22, 'Manipur'),
(23, 'Meghalaya'),
(24, 'Mizoram'),
(25, 'Nagaland'),
(26, 'Odisha'),
(28, 'Punjab'),
(29, 'Rajasthan'),
(30, 'Sikkim'),
(31, 'Tamil Nadu'),
(32, 'Telangana'),
(33, 'Tripura'),
(34, 'Uttar Pradesh'),
(35, 'Uttarakhand'),
(36, 'West Bengal');

-- --------------------------------------------------------

--
-- Table structure for table `tax_classes`
--

CREATE TABLE `tax_classes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `sort_order` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_products`
--

CREATE TABLE `tmp_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variant` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dcin` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ean` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upc` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_id` bigint(20) UNSIGNED DEFAULT NULL,
  `other_brand` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `pathayapura_listing` tinyint(1) NOT NULL DEFAULT 0,
  `gst` decimal(15,2) UNSIGNED DEFAULT NULL,
  `sess` tinyint(1) UNSIGNED DEFAULT 0,
  `price` decimal(15,2) UNSIGNED DEFAULT NULL,
  `mrp` decimal(15,2) UNSIGNED DEFAULT NULL,
  `wholesale` decimal(15,2) UNSIGNED DEFAULT NULL,
  `order_quantity` bigint(20) UNSIGNED DEFAULT NULL,
  `cod` tinyint(1) DEFAULT 0,
  `commision` decimal(12,5) UNSIGNED DEFAULT NULL,
  `weight` double(15,2) DEFAULT NULL,
  `weight_class_id` bigint(20) UNSIGNED DEFAULT NULL,
  `length` double(15,2) DEFAULT NULL,
  `width` double(15,2) DEFAULT NULL,
  `height` double(15,2) DEFAULT NULL,
  `length_class_id` bigint(20) UNSIGNED DEFAULT NULL,
  `shipping_processing_time` int(11) DEFAULT NULL,
  `return_policy` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `free_delivery` tinyint(1) NOT NULL DEFAULT 0,
  `vendor_id` bigint(20) UNSIGNED DEFAULT NULL,
  `completed_status` tinyint(1) NOT NULL DEFAULT 0,
  `approved` smallint(6) NOT NULL DEFAULT 0,
  `admin_comments` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `step` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tmp_products`
--

INSERT INTO `tmp_products` (`id`, `name`, `variant`, `dcin`, `ean`, `upc`, `brand_id`, `other_brand`, `category_id`, `pathayapura_listing`, `gst`, `sess`, `price`, `mrp`, `wholesale`, `order_quantity`, `cod`, `commision`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `shipping_processing_time`, `return_policy`, `free_delivery`, `vendor_id`, `completed_status`, `approved`, `admin_comments`, `step`, `created_at`, `updated_at`) VALUES
(7, 'test', '7', 'DCIN-1583130158897', NULL, '123123', NULL, 'test', 1, 1, '123.00', 1, '12.00', '22.00', '22.00', 22, 1, NULL, 12.00, 1, 3.00, 33.00, 33.00, 1, 10, '', 1, 1, 1, 2, 'no comments', 6, '2020-03-02 00:55:01', '2020-03-11 05:50:08'),
(58, 'OnePlus  7T Pro', '58', 'DCIN-1583926856973', '52', '498', NULL, 'OnePlus', 2, 1, NULL, 0, '500.00', '450.00', '400.00', 50, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, NULL, 2, '2020-03-11 06:13:39', '2020-03-11 06:47:31');

-- --------------------------------------------------------

--
-- Table structure for table `tmp_product_additional_infos`
--

CREATE TABLE `tmp_product_additional_infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `temp_product_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tmp_product_additional_infos`
--

INSERT INTO `tmp_product_additional_infos` (`id`, `temp_product_id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 6, 'Company bio', 'details', '2020-02-28 02:36:36', '2020-02-28 02:36:36'),
(2, 6, 'other', 'asdadasd', '2020-02-28 02:36:54', '2020-02-28 02:36:54'),
(3, 6, 'test', 'tessss', '2020-02-28 05:04:34', '2020-02-28 05:04:34'),
(4, 6, 'tests', 'sss', '2020-02-28 05:06:57', '2020-02-28 05:06:57'),
(5, 6, 'test', 'ewrwer', '2020-02-28 05:21:28', '2020-02-28 05:21:28'),
(6, 6, 'test', 'ewrwer', '2020-02-28 05:23:20', '2020-02-28 05:23:20'),
(7, 6, 'test', 'dsasdad', '2020-02-28 05:23:33', '2020-02-28 05:23:33'),
(9, 7, 'test', 'asdasd', '2020-03-02 00:56:39', '2020-03-02 00:56:39'),
(14, 24, 'sadasdaqw', 'qweqwe', '2020-03-02 23:21:50', '2020-03-02 23:21:50'),
(32, 39, 'dfgdfg', 'asdasddas', '2020-03-05 23:08:36', '2020-03-05 23:08:36'),
(34, 39, 'dfgdg', 'sdfsdf', '2020-03-05 23:11:51', '2020-03-05 23:11:51'),
(35, 40, 'dfgdfg', 'dsfdsfsdfdsf', '2020-03-06 01:58:21', '2020-03-06 01:58:21'),
(36, 40, 'dfgdfg', 'dsfdsfdsf', '2020-03-08 23:52:55', '2020-03-08 23:52:55'),
(37, 40, 'dfgdfg', 'dsfdsfdsf', '2020-03-08 23:52:59', '2020-03-08 23:52:59'),
(38, 40, 'dfgdfg', 'dsfdsfdsf', '2020-03-08 23:53:02', '2020-03-08 23:53:02'),
(39, 40, 'dfgdfg', 'dsfdsfdsf', '2020-03-08 23:53:03', '2020-03-08 23:53:03'),
(40, 40, 'dfgdfg', 'dsfdsfdsf', '2020-03-08 23:53:03', '2020-03-08 23:53:03'),
(41, 57, 'dfgdfg', 'sdfsdfsdfdsf', '2020-03-09 00:07:42', '2020-03-09 00:07:42');

-- --------------------------------------------------------

--
-- Table structure for table `tmp_product_attributes`
--

CREATE TABLE `tmp_product_attributes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `temp_product_id` bigint(20) UNSIGNED NOT NULL,
  `attribute` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tmp_product_attributes`
--

INSERT INTO `tmp_product_attributes` (`id`, `temp_product_id`, `attribute`, `value`, `created_at`, `updated_at`) VALUES
(11, 6, 'Size', 'Universal', '2020-03-02 00:19:31', '2020-03-02 00:19:31'),
(12, 7, 'Size', 'Free size', '2020-03-02 00:55:01', '2020-03-02 00:55:01'),
(30, 24, 'Size', 'Universal', '2020-03-02 23:21:03', '2020-03-02 23:21:03'),
(31, 25, 'Size', 'Universal', '2020-03-02 23:22:11', '2020-03-02 23:22:11'),
(32, 26, 'Size', '5XS', '2020-03-02 23:22:20', '2020-03-02 23:22:20'),
(34, 7, 'Size', 'Free size', '2020-03-11 05:50:08', '2020-03-11 05:50:08');

-- --------------------------------------------------------

--
-- Table structure for table `tmp_product_faqs`
--

CREATE TABLE `tmp_product_faqs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `temp_product_id` bigint(20) UNSIGNED NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tmp_product_faqs`
--

INSERT INTO `tmp_product_faqs` (`id`, `temp_product_id`, `question`, `answer`, `created_at`, `updated_at`) VALUES
(1, 6, 'Ques1', 'Answ1', '2020-02-28 02:37:08', '2020-02-28 02:37:08'),
(2, 6, 'Ques2', 'Answ2', '2020-02-28 02:37:28', '2020-02-28 02:37:28'),
(3, 6, 'qq', 'qq', '2020-02-28 05:25:56', '2020-02-28 05:25:56'),
(5, 6, 'ddd', 'asdasd', '2020-03-01 11:11:22', '2020-03-01 11:11:22'),
(6, 6, 'ewds', 'asdasdas', '2020-03-01 11:16:26', '2020-03-01 11:16:26'),
(7, 6, 'sasdasd', 'asdasd', '2020-03-01 11:17:11', '2020-03-01 11:17:11'),
(8, 7, 'sdasd', 'asdasdasd', '2020-03-02 00:56:44', '2020-03-02 00:56:44'),
(13, 24, 'qweqwe', 'qweqwe', '2020-03-02 23:21:55', '2020-03-02 23:21:55'),
(15, 39, 'Test Question?', 'Test Answer', '2020-03-05 23:06:25', '2020-03-05 23:06:25'),
(16, 39, 'Test Question?', 'sdfsdfsdfsdf', '2020-03-05 23:11:56', '2020-03-05 23:11:56'),
(17, 40, 'Test Question?', 'dsfsfds sdfdsf sdfds fsdfdsfdsfd sd', '2020-03-06 01:58:30', '2020-03-06 01:58:30'),
(18, 57, 'Test Question?', 'sdfs sdf sdfds', '2020-03-09 00:07:48', '2020-03-09 00:07:48');

-- --------------------------------------------------------

--
-- Table structure for table `tmp_product_images`
--

CREATE TABLE `tmp_product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `temp_product_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tmp_product_images`
--

INSERT INTO `tmp_product_images` (`id`, `temp_product_id`, `image`, `created_at`, `updated_at`) VALUES
(12, 6, 'images/products/6/1582885393.png', '2020-02-28 04:53:13', '2020-02-28 04:53:13'),
(13, 6, 'images/products/6/1582885404.png', '2020-02-28 04:53:24', '2020-02-28 04:53:24'),
(14, 6, 'images/products/6/1582885429.png', '2020-02-28 04:53:49', '2020-02-28 04:53:49'),
(16, 6, 'images/products/6/1583079652.png', '2020-03-01 10:50:52', '2020-03-01 10:50:52'),
(17, 7, 'images/products/7/1583130341.png', '2020-03-02 00:55:41', '2020-03-02 00:55:41'),
(21, 24, 'images/products/24/1583211100.png', '2020-03-02 23:21:40', '2020-03-02 23:21:40'),
(36, 39, 'images/products/39/1583469340.jpg', '2020-03-05 23:05:40', '2020-03-05 23:05:40'),
(37, 39, 'images/products/39/1583469348.jpg', '2020-03-05 23:05:48', '2020-03-05 23:05:48'),
(38, 39, 'images/products/39/1583469354.jpg', '2020-03-05 23:05:54', '2020-03-05 23:05:54'),
(39, 40, 'images/products/40/1583479684.jpg', '2020-03-06 01:58:04', '2020-03-06 01:58:04'),
(40, 40, 'images/products/40/1583479690.jpg', '2020-03-06 01:58:10', '2020-03-06 01:58:10'),
(41, 40, 'images/products/40/1583479695.jpg', '2020-03-06 01:58:15', '2020-03-06 01:58:15'),
(42, 57, 'images/products/57/1583732242.jpg', '2020-03-09 00:07:22', '2020-03-09 00:07:22'),
(43, 57, 'images/products/57/1583732248.jpg', '2020-03-09 00:07:28', '2020-03-09 00:07:28'),
(44, 57, 'images/products/57/1583732253.jpg', '2020-03-09 00:07:33', '2020-03-09 00:07:33');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_name` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_type` bigint(20) UNSIGNED DEFAULT NULL,
  `contact_person` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pin` bigint(20) UNSIGNED NOT NULL,
  `location` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '/profiles/default.png',
  `pancard` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_proof` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trade_license_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trade_license_document` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pan_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `gst_document` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_as_in_bank` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ifsc_code` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancel_cheque` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reset_token` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_date` datetime DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `approved` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `code`, `company_name`, `store_name`, `business_type`, `contact_person`, `email`, `mobile`, `phone`, `address`, `pin`, `location`, `latitude`, `longitude`, `image`, `pancard`, `id_proof`, `trade_license_number`, `trade_license_document`, `gst_number`, `pan_number`, `gst_document`, `name_as_in_bank`, `account_type`, `account_number`, `ifsc_code`, `cancel_cheque`, `otp`, `email_verified_at`, `password`, `reset_token`, `reset_date`, `remember_token`, `status`, `active`, `approved`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1582520846983', 'Company name', 'My Store', 2, 'Saleesh', 'saleeshprakash@gmail.com', '919946070864', '9946070864', 'Pullarakatt(H)\r\nPO Methala \r\nKodungallur', 680669, NULL, NULL, NULL, 'images/vendors/profile/1582520846.png', 'images/vendors/pancard/1582520846.png', 'images/vendors/id_proof/1582520846.png', NULL, 'images/vendors/trade_license_document/1582520846.png', '07AAACI4798L1Z0', 'AAAPL1234C', 'images/vendors/gst_document/1582520846.png', 'Saleesh', 'Savings', '123456789111', 'IOBA0003452', 'images/vendors/cancel_cheque/1582520846.png', NULL, NULL, '$2y$10$DKWEjcjs8KKi/zFTkXvmUeSdWcX3M6hWR4bfNpdvA3Wiy14Rpecyi', '932515838998982058', NULL, NULL, 1, 1, 1, '2020-02-23 23:20:13', '2020-03-10 22:41:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_categories`
--

CREATE TABLE `vendor_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vendor_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendor_categories`
--

INSERT INTO `vendor_categories` (`id`, `vendor_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2020-02-23 23:37:26', '2020-02-23 23:37:26'),
(2, 1, 2, '2020-02-23 23:37:27', '2020-02-23 23:37:27'),
(3, 1, 3, '2020-02-23 23:37:27', '2020-02-23 23:37:27');

-- --------------------------------------------------------

--
-- Table structure for table `weight_classes`
--

CREATE TABLE `weight_classes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` decimal(15,8) NOT NULL,
  `unit` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `sort_order` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `weight_classes`
--

INSERT INTO `weight_classes` (`id`, `name`, `value`, `unit`, `active`, `sort_order`, `created_at`, `updated_at`) VALUES
(1, 'Kilogram', '1000.00000000', 'Kg', 1, 1, NULL, NULL),
(2, 'Gram', '1.00000000', 'Gm', 1, 1, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`),
  ADD UNIQUE KEY `admins_mobile_unique` (`mobile`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_values`
--
ALTER TABLE `attribute_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attribute_values_attribute_id_index` (`attribute_id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_types`
--
ALTER TABLE `business_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_attributes`
--
ALTER TABLE `category_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_attributes_category_id_index` (`category_id`);

--
-- Indexes for table `category_attribute_values`
--
ALTER TABLE `category_attribute_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_attribute_values_category_attribute_id_index` (`category_attribute_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities_prefecture_id_foreign` (`prefecture_id`);

--
-- Indexes for table `color_classes`
--
ALTER TABLE `color_classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign` (`state_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `length_classes`
--
ALTER TABLE `length_classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manufactors`
--
ALTER TABLE `manufactors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manufacturers`
--
ALTER TABLE `manufacturers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otps`
--
ALTER TABLE `otps`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `otps_mobile_unique` (`mobile`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmp_products_brand_id_index` (`brand_id`),
  ADD KEY `tmp_products_category_id_index` (`category_id`),
  ADD KEY `tmp_products_weight_class_id_index` (`weight_class_id`),
  ADD KEY `tmp_products_length_class_id_index` (`length_class_id`),
  ADD KEY `tmp_products_vendor_id_index` (`vendor_id`);

--
-- Indexes for table `product_additional_infos`
--
ALTER TABLE `product_additional_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmp_product_attributes_temp_product_id_index` (`product_id`);

--
-- Indexes for table `product_faqs`
--
ALTER TABLE `product_faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmp_product_images_temp_product_id_index` (`product_id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax_classes`
--
ALTER TABLE `tax_classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmp_products`
--
ALTER TABLE `tmp_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmp_products_brand_id_index` (`brand_id`),
  ADD KEY `tmp_products_category_id_index` (`category_id`),
  ADD KEY `tmp_products_weight_class_id_index` (`weight_class_id`),
  ADD KEY `tmp_products_length_class_id_index` (`length_class_id`),
  ADD KEY `tmp_products_vendor_id_index` (`vendor_id`);

--
-- Indexes for table `tmp_product_additional_infos`
--
ALTER TABLE `tmp_product_additional_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmp_product_attributes`
--
ALTER TABLE `tmp_product_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmp_product_attributes_temp_product_id_index` (`temp_product_id`);

--
-- Indexes for table `tmp_product_faqs`
--
ALTER TABLE `tmp_product_faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmp_product_images`
--
ALTER TABLE `tmp_product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmp_product_images_temp_product_id_index` (`temp_product_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vendors_business_type_foreign` (`business_type`);

--
-- Indexes for table `vendor_categories`
--
ALTER TABLE `vendor_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vendor_categories_vendor_id_foreign` (`vendor_id`),
  ADD KEY `vendor_categories_category_id_foreign` (`category_id`);

--
-- Indexes for table `weight_classes`
--
ALTER TABLE `weight_classes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `attribute_values`
--
ALTER TABLE `attribute_values`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=794;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `business_types`
--
ALTER TABLE `business_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `category_attributes`
--
ALTER TABLE `category_attributes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `category_attribute_values`
--
ALTER TABLE `category_attribute_values`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `color_classes`
--
ALTER TABLE `color_classes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=644;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `length_classes`
--
ALTER TABLE `length_classes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `manufactors`
--
ALTER TABLE `manufactors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `manufacturers`
--
ALTER TABLE `manufacturers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `otps`
--
ALTER TABLE `otps`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `product_additional_infos`
--
ALTER TABLE `product_additional_infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `product_attributes`
--
ALTER TABLE `product_attributes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `product_faqs`
--
ALTER TABLE `product_faqs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tax_classes`
--
ALTER TABLE `tax_classes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmp_products`
--
ALTER TABLE `tmp_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `tmp_product_additional_infos`
--
ALTER TABLE `tmp_product_additional_infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `tmp_product_attributes`
--
ALTER TABLE `tmp_product_attributes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `tmp_product_faqs`
--
ALTER TABLE `tmp_product_faqs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tmp_product_images`
--
ALTER TABLE `tmp_product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vendor_categories`
--
ALTER TABLE `vendor_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `weight_classes`
--
ALTER TABLE `weight_classes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
