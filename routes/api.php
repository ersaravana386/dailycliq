<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//login send otp
Route::post('sendotp','Api\LoginController@sendOtp');
 //get required datas after login
Route::get('dashboard','Api\DashboardController@getDashboardDetails');
//get products
Route::get('products','Api\ProductController@getProducts');
//get product details
Route::get('product/details','Api\ProductController@getProductDetails');
//get new arrivals
Route::get('newarrivals','Api\DashboardController@newArrivals');
//get product variants attributes
Route::post('product/variants/attributes','Api\ProductController@getProductVariantAttributes');

//get deals of the day
Route::get('dealsoftheday','Api\ProductController@getDealOfTheDay');
//get flashsales
Route::get('flashsales','Api\ProductController@getFlashSale');

//pathayapura product listing
Route::get('pathayapuraproductListing','Api\ProductController@pathayapuraproductListing')->name('productListing');
//pathayapura dashboard
Route::get('dashboardpathayapura','Api\DashboardController@dashboardpathayapura');
//get featured products
Route::get('featuredproducts','Api\DashboardController@featuredproducts');
//get best sellers
Route::get('bestsellers','Api\DashboardController@bestsellers');
//get new arrivals
Route::get('pathayapuranewarrivals','Api\DashboardController@pathayapuranewarrivals');
//get more items
Route::get('moreitems','Api\DashboardController@moreitems');
//login with social media
Route::post('loginwithsocialmedia','Api\LoginController@loginwithsocialmedia');

//review insertion 
Route::post('reviewInsert','Api\UserController@reviewInsert');

//get home banner
Route::get('getHomebanner','Api\DashboardController@getHomebanner');
//get offer banner
Route::get('getOfferbanner','Api\DashboardController@getOfferbanner');

//get guide
Route::get('getGuide','Api\GuideController@getGuide'); 

//get featured brands
Route::get('getFeaturedbrand','Api\BrandController@getFeaturedbrand');
//get other brands
Route::get('getOtherbrand','Api\BrandController@getOtherbrand');
//get all the brands
Route::get('getallbrands','Api\BrandController@getAllbrands');
//get budget bazar products
Route::get('getbudgetbazar','Api\ProductController@getBudgetbazar');

//get vendor details by id
Route::get('getvendorbyid/{id}','Api\VendorController@getvendorbyid')->name('getvendorbyid');
// get brand
Route::get('getbrand/{id}','Api\BrandController@getbrand')->name('getbrand');
//get shop by town
Route::get('shopbytown','Api\ProductController@shopBytown');
// get it free
Route::get('getitfree','Api\DashboardController@getitfree');
// email us
Route::post('emailus','Api\UserController@emailus');
//logout
Route::get('logout','Api\UserController@logout');
// get products by ean upc
Route::get('getproductsbyeanupc/{id}','Api\VendorController@getProductsbyeanupc')->name('getproductsbyeanupc');
// points details
Route::get('getpointdetails','Api\DashboardController@getPointdetails');
//point mall
Route::get('pointmall/{id}','Api\DashboardController@pointMall')->name('pointmall');
// get categories and subcategories
Route::get('getcategories','Api\ProductController@getCategories');

//login verify otp
Route::post('verifyotp','Api\LoginController@verifyOtp');
Route::group(['middleware' => 'auth:api'], function() {
    //set gender of the user if not set
    Route::post('setgender','Api\UserController@setGender');
    //save profile
    Route::post('profile','Api\UserController@saveProfile');
    //get profile
    Route::get('profile','Api\UserController@getProfile');  
    //add to wish list
    Route::post('wishlist','Api\ProductController@addRemoveWishList');  
    //get wish list
    Route::get('wishlist','Api\ProductController@getWishList');  
    //get other offers
    Route::get('otheroffers','Api\ProductController@otherOffers');  
    //add to cart
    Route::post('cart','Api\CartController@Cart');
    //get cart details
    Route::get('cart','Api\CartController@getCart');
    //delete cart
    Route::get('deletecart','Api\CartController@deleteCart');
    // update cart quantity
    Route::get('cartquantity/{quantity}/{id}','Api\CartController@updateCartquantity')->name('cartquantity');

    //address insertion
    Route::post('deliveryAddressinsertion','Api\UserController@deliveryAddressinsertion');
    //get address
    Route::get('getAddress','Api\UserController@getAddress');
    //edit address
    Route::get('editAddress/{id}','Api\UserController@editAddress')->name('editAddress');
    //update address
    Route::post('updateAddress/{id}','Api\Usercontroller@updateAddress')->name('updateAddress');
    //delete address
    Route::get('deleteAddress/{id}','Api\UserController@deleteAddress')->name('deleteAddress');

    //history
    Route::get('history','Api\UserController@history');

    //checkout page
    Route::get('checkout','Api\OrderController@checkout');

    //new buyer zone
    Route::get('newbuyerzone','Api\CouponController@newBuyerZone');

    // get order histories
    Route::get('getorder','Api\OrderController@getorder');

     //get main profile page
     Route::get('mainprofile','Api\UserController@getMainProfile');  
});